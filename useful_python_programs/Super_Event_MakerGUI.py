#By Wendell08, Writes super event code

import tkinter as tk
from tkinter import filedialog, Text
import shutil
import os

get_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

print("TEMPORARY DEBUG SHIT")
print(get_path)

super_event_gui_path = os.path.join(get_path, r"common\scripted_guis\TNO_big_event.txt")
super_event_gui_file = os.path.join(get_path, r"interface\TNO_SG_BigEvent.gui")
super_event_gfx_file = os.path.join(get_path, r"interface\TNO_SG_BigEvent.gfx")
super_event_loc_file = os.path.join(get_path, r"localisation\TNO_BigEvents_l_english.yml")

class App(tk.Frame):
	def __init__(self, master=None):
		super().__init__(master)
		self.select_image_file = ""
		self.select_music_file = ""
		self.master = master
		self.pack()
		self.create_widgets()

	def create_widgets(self):
		self.run_script = tk.Button(self)
		self.run_script["text"] = "Run Program"
		self.run_script["command"] = self.run_app
		self.run_script.pack(side="top")

		self.entry1text = tk.Label(self)
		self.entry1text["text"] = "Super Event ID\n(german_civil_war)"
		self.entry1text["wraplength"] = 170
		self.entry1text.pack(side="top")

		self.entry1 = tk.Entry(self)
		self.entry1.pack(side="top")

		self.select_image_button = tk.Button(self)
		self.select_image_button["text"] = "Select Super Event Image\n(.dds or .tga)"
		self.select_image_button["command"] = self.select_image_path
		self.select_image_button.pack(side="top")

		self.image_image = tk.Label(self)
		self.image_image["text"] = f"\nCurrent Image File:\n{self.select_image_file}\n"
		self.image_image["wraplength"] = 250
		self.image_image.pack(side="top")

		self.select_audio_button = tk.Button(self)
		self.select_audio_button["text"] = "Select Super Event Music(.wav)"
		self.select_audio_button["command"] = self.select_music_path
		self.select_audio_button.pack(side="top")

		self.music_label = tk.Label(self)
		self.music_label["text"] = f"\nCurrent Image File:\n{self.select_music_file}\n"
		self.music_label["wraplength"] = 250
		self.music_label.pack(side="top")

		self.entry2text = tk.Label(self)
		self.entry2text["text"] = "Super Event Quote"
		self.entry2text["wraplength"] = 170
		self.entry2text.pack(side="top")

		self.entry2 = tk.Entry(self)
		self.entry2.pack(side="top")

		self.entry3text = tk.Label(self)
		self.entry3text["text"] = "Super Event Quote Author"
		self.entry3text["wraplength"] = 170
		self.entry3text.pack(side="top")

		self.entry3 = tk.Entry(self)
		self.entry3.pack(side="top")

		self.entry4text = tk.Label(self)
		self.entry4text["text"] = "Super Event Option"
		self.entry4text["wraplength"] = 170
		self.entry4text.pack(side="top")

		self.entry4 = tk.Entry(self)
		self.entry4.pack(side="top")

	def select_image_path(self):
		self.select_image_file = filedialog.askopenfilename(initialdir=get_path+r"\gfx\superevent_pictures")
		self.image_image["text"] = f"\nCurrent Image File:\n{self.select_image_file}\n"

	def select_music_path(self):
		self.select_music_file = filedialog.askopenfilename(initialdir=get_path+r"\sound\menu")
		self.music_label["text"] = f"\nCurrent Music File:\n{self.select_music_file}\n"

	def run_app(self):
		shared_focus = 0
		super_event_name = self.entry1.get()
		super_event_quote = self.entry2.get()
		super_event_author = self.entry3.get()
		super_event_option = self.entry4.get()

		_, super_event_image = os.path.split(self.select_image_file)
		_, super_event_music =  os.path.split(self.select_music_file)
		
		try:
			shutil.copy(self.select_image_file, get_path+r"\gfx\superevent_pictures")
		except:
			print("Image file already in Folder")
			pass
		try:
			shutil.copy(self.select_music_file, get_path+r"\sound\menu")
		except:
			print("Music file already in Folder")
			pass

		#print(super_event_image, super_event_music)

		with open(super_event_gui_path, "r") as inp:
			line_list = inp.readlines()
			for index, line_item in enumerate(line_list):
				if "SUPER_EVENT_MAKER_GUI" in line_item:
					line_list[index] = '''	TNO_SG_big_event_'''+super_event_name+''' = {
		context_type = player_context
		window_name = "TNO_SG_big_event_'''+super_event_name+'''"

		visible = {
			NOT = { has_global_flag = TNO_my_world_is_on_fire_how_about_yours }
			has_country_flag = big_event_'''+super_event_name+'''
		}
	}

	TNO_SG_big_event_frame_'''+super_event_name+''' = {
		context_type = player_context
		window_name = "TNO_SG_big_event_frame_'''+super_event_name+'''"

		visible = {
			NOT = { has_global_flag = TNO_my_world_is_on_fire_how_about_yours }
			has_country_flag = big_event_'''+super_event_name+'''
		}

		effects = {
			Option_click = {
				clr_country_flag = big_event_'''+super_event_name+'''
			}
		}
	}

	##USE SUPER_EVENT_MAKER_GUI IN USEFUL PYTHON PROGRAMS TO MAKE NEW SUPER EVENTS##
'''
			with open(super_event_gui_path, "w") as out:
				for line_to_write in line_list:
					out.write(line_to_write)

		with open(super_event_gfx_file) as inp:
			line_list = inp.readlines()
			for index, line_item in enumerate(line_list):
				if "SUPER_EVENT_MAKER_GUI" in line_item:
					line_list[index] = '''	spriteType = {
		name = "GFX_superevent_'''+super_event_name+'''"
		textureFile = "gfx//superevent_pictures//'''+super_event_image+'''"
	}

	##USE SUPER_EVENT_MAKER_GUI IN USEFUL PYTHON PROGRAMS TO MAKE NEW SUPER EVENTS##
'''
			with open(super_event_gfx_file, "w") as out:
				for line_to_write in line_list:
					out.write(line_to_write)

		with open(super_event_gui_file) as inp:
			line_list = inp.readlines()
			for index, line_item in enumerate(line_list):
				if "SUPER_EVENT_MAKER_GUI" in line_item:
					line_list[index] = '''	containerWindowType = {
		name = "TNO_SG_big_event_'''+super_event_name+'''"
		size = { width = 580 height = 128 }
		position = { x=0 y=-210 }
		Orientation = center
		Origo = center
		clipping = no
		show_sound = '''+super_event_music[:-4]+''' 
				
		background = {
			name = "Background"
			quadTextureSprite ="GFX_tiled_window_1b_no_border"
		}
		instantTextBoxType = {
			name = "Title"
			position = { x = 0 y = 18 }
			font = "aldrich_24_outline"
			borderSize = {x = 0 y = 0}
			text = "BE_'''+super_event_name.upper()+'''_T"	
			maxWidth = 580
			maxHeight = 24
			format = centre
		}
		iconType = {
			name ="image"
			spriteType = "GFX_superevent_'''+super_event_name+'''"
			position = { x= -286 y = -18 }
			Orientation = center
			alwaystransparent = yes
		}
		
	}
	
	containerWindowType = {
		name = "TNO_SG_big_event_frame_'''+super_event_name+'''"
		size = { width = 588 height = 465 }
		position = { x=0 y=0 }
		Orientation = center
		Origo = center
		clipping = no
				
		background = {
			name = "Background"
			quadTextureSprite ="GFX_tiled_window_bigevent_border"
		}
		buttonType = {
			name = "Option"
			text = "BE_'''+super_event_name.upper()+'''_A"
			shortcut = "ESCAPE"
			position = { x = -110 y = 183 }
			quadTextureSprite ="GFX_button_221x34"
			buttonFont = "Bomb_24"
			Orientation = center
		}
		iconType = {
			name ="underlay"
			spriteType = "GFX_superevent_text_underlay"
			position = { x= 30 y = -200 }
			Orientation = lower_left
			alwaystransparent = yes
		}
		instantTextBoxType = {
			name = "text"
			position = {  x= 120 y = -195 }
			font = "Bomb_24"
			text = "BE_'''+super_event_name.upper()+'''_D"	
			maxWidth = 455
			maxHeight = 173
			fixedsize = yes
			format = right
			Orientation = lower_left
		}
	}

	##USE SUPER_EVENT_MAKER_GUI IN USEFUL PYTHON PROGRAMS TO MAKE NEW SUPER EVENTS##
'''
			with open(super_event_gui_file, "w") as out:
				for line_to_write in line_list:
					out.write(line_to_write)

		with open(super_event_loc_file, "r") as inp:
			line_list = inp.readlines()
			for index, line_item in enumerate(line_list):
				if "SUPER_EVENT_MAKER_GUI" in line_item:
					line_list[index] = '''BE_'''+super_event_name.upper()+'''_T:0 "'''+super_event_name.upper().replace("_", " ")+'''"
BE_'''+super_event_name.upper()+'''_D:0 "'''+super_event_quote+'''\\n- '''+super_event_author+'''"
BE_'''+super_event_name.upper()+'''_A:0 "'''+super_event_option+'''"

##USE SUPER_EVENT_MAKER_GUI IN USEFUL PYTHON PROGRAMS TO MAKE NEW SUPER EVENTS##'''
			with open(super_event_loc_file, "w") as out:
				for line_to_write in line_list:
					out.write(line_to_write)

		with open(get_path+r"\sound\tno_sound.asset", "r") as inp:
			music_file_found = False
			line_list = inp.readlines()
			for index, line_item in enumerate(line_list):
				if super_event_music in line_item:
					print("Music file already defined in tno_sound.asset")
					music_file_found = True
					break
				if "SUPER_EVENT_MAKER_GUI" in line_item:
					line_list[index] = '''sound = {
	name = "'''+super_event_music[:-4]+'''"
	file = "menu/'''+super_event_music+'''"
	always_load = no
}

##USE SUPER_EVENT_MAKER_GUI IN USEFUL PYTHON PROGRAMS TO MAKE NEW SUPER EVENTS##'''
			if music_file_found == False:
				with open(get_path+r"\sound\tno_sound.asset", "w") as out:
					for line_to_write in line_list:
						out.write(line_to_write)

		with open(get_path+r"\sound\gui\tno_gui_sound_effects.asset", "r") as inp:
			music_file_found = False
			line_list = inp.readlines()
			for index, line_item in enumerate(line_list):
				if super_event_music in line_item:
					print("Music file already defined in gui/tno_gui_sound_effects.asset")
					music_file_found = True
					break
				if "SUPER_EVENT_MAKER_GUI" in line_item:
					line_list[index] = '''soundeffect = {
	name = "'''+super_event_music[:-4]+'''"
	sounds = {
		sound = '''+super_event_music[:-4]+'''
	}
	volume = 0.75
}

##USE SUPER_EVENT_MAKER_GUI IN USEFUL PYTHON PROGRAMS TO MAKE NEW SUPER EVENTS##'''
			if music_file_found == False:
				with open(get_path+r"\sound\gui\tno_gui_sound_effects.asset", "w") as out:
					for line_to_write in line_list:
						out.write(line_to_write)

		with open(get_path+r"\sound\tno.asset", "r") as inp:
			music_file_found = False
			line_list = inp.readlines()
			for index, line_item in enumerate(line_list):
				if super_event_music in line_item:
					print("Music file already defined in tno.asset")
					music_file_found = True
					break
				if "SUPER_EVENT_MAKER_GUI" in line_item:
					line_list[index] = "\t\t"+super_event_music[:-4]+"\n\t\t##USE SUPER_EVENT_MAKER_GUI IN USEFUL PYTHON PROGRAMS TO MAKE NEW SUPER EVENTS##\n"
			if music_file_found == False:
				with open(get_path+r"\sound\tno.asset", "w") as out:
					for line_to_write in line_list:
						out.write(line_to_write)

		print("Finished superevent script execution")



root = tk.Tk()
root.geometry("260x510")
root.title("Super Event Maker")
app = App(master=root)
app.mainloop()
