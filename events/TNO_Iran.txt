﻿add_namespace = PER
add_namespace = percw

country_event = { # The Shah Is Assassinated
	id = PER.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event PER.1" }
	title = PER.1.t
	desc = PER.1.d

	is_triggered_only = yes
	fire_only_once = yes

	option = { 
		name = PER.1.a
		PER = {
			kill_country_leader = yes
			create_country_leader = {
				name = "National Assembly"
				desc = "POLITICS_IRAN_NATIONAL_ASSEMBLY_DESC"
				picture = "Portrait_Iran_National_Assembly.dds"
				expire = "1999.1.23"
				ideology = authoritarian_democrat_subtype
				traits = {
					
				}
			}
		}
		every_country = {
			news_event = {
				id = WORLD.650
				days = 1
			}
		}
		PER = {
			country_event = { 
				id = PER.2 
				days = 13	
			}
		}
	}
}

country_event = { # The Civil War Begins
	id = PER.2
	immediate = { log = "[GetDateText]: [Root.GetName]: event PER.2" }
	title = PER.2.t
	desc = PER.2.d

	is_triggered_only = yes
	fire_only_once = yes

	option = {
		name = PER.2.a
		hidden_effect = {
		PER = {
			retire_country_leader = yes
			set_politics = {	
				ruling_party = despotism
				last_election = "1933.3.5"
				election_frequency = 48
				elections_allowed = no
			}
			set_popularities = {
				communist = 0
				ultranational_socialism = 0
				socialist = 0
				social_democrat = 0
				social_liberal = 0
				market_liberal = 0
				social_conservative = 0
				authoritarian_democrat = 0
				despotism = 80
				fascism = 0
				national_socialism = 20
				burgundian_system = 0
			}
			create_country_leader = {
				name = "Farah Pahlavi"
				desc = "POLITICS_FARAH_PAHLAVI_DESC"
				picture = "Portrait_Iran_Farah_Pahlavi.dds"
				expire = "1999.1.23"
				ideology = despotism_subtype
				traits = {
					regent
				}
			}
			remove_ideas = PER_German_Persian_Oil_Company
			remove_ideas = PER_Mosaddeghs_Legacy
			add_ideas = PER_The_Iranian_Civil_War
			
			remove_ideas = PER_Asadollah_Alam_hog
			remove_ideas = PER_Teymur_Bakhtiar_sec
			remove_ideas = PER_Abbas_Aram_for
			remove_ideas = PER_Jahangir_Amuzegar_eco
			add_ideas = PER_Amir_Abbas_Hoveyda_hog
			add_ideas = PER_Mohsen_Pezeshkpour_for
			add_ideas = PER_Jamshid_Amouzegar_eco
			add_ideas = PER_Nematollah_Nassiri_sec
			
			load_oob = PER_Civil_War
			
			remove_state_core = 976
			remove_state_core = 413
			remove_state_core = 977
			remove_state_core = 972
			remove_state_core = 971
			remove_state_core = 412
			remove_state_core = 414
			remove_state_core = 978
			remove_state_core = 410
			remove_state_core = 1360
			remove_state_core = 419
			remove_state_core = 973
			remove_state_core = 974
			remove_state_core = 411
			remove_state_core = 979
			remove_state_core = 975
			remove_state_core = 416
			remove_state_core = 421
			
			add_offsite_building = { type = arms_factory level = 1 }
			add_offsite_building = { type = arms_factory level = 1 }
			
			load_focus_tree = PER_civil_war
			
			every_unit_leader = {
				limit = {
					is_needing_promotion_iran = yes
				}
				promote_leader = yes
			}
			
			#every_unit_leader = {
			#	limit = {
			#		is_IRL_unit_leader = yes
			#	}
			#	set_nationality = IRL
			#}
			
			every_unit_leader = {
				limit = {
					is_IRI_unit_leader = yes
				}
				set_nationality = IRI
			}
			
			every_unit_leader = {
				limit = {
					is_IRC_unit_leader = yes
				}
				set_nationality = IRC
			}
		}
		IRL = {
			transfer_state = 976
			add_state_core = 976
			transfer_state = 413
			add_state_core = 413
			transfer_state = 977
			add_state_core = 977
			transfer_state = 972
			add_state_core = 972
			transfer_state = 971
			add_state_core = 971
			transfer_state = 412
			add_state_core = 412
			
			inherit_technology = PREV
			add_ideas = PER_The_Iranian_Civil_War
			
			add_offsite_building = { type = arms_factory level = 1 }
			add_offsite_building = { type = arms_factory level = 1 }
			add_offsite_building = { type = arms_factory level = 1 }
			
			load_oob = IRL_Civil_War
			
			load_focus_tree = IRL_civil_war
			
			declare_war_on = {
				target = PER
				type = annex_everything
			}
		}
		IRC = {
			transfer_state = 414
			add_state_core = 414
			transfer_state = 978
			add_state_core = 978
			transfer_state = 410
			add_state_core = 410
			
			add_offsite_building = { type = arms_factory level = 1 }
			add_offsite_building = { type = arms_factory level = 1 }
			
			inherit_technology = PREV
			add_ideas = PER_The_Iranian_Civil_War
			
			load_oob = IRC_Civil_War
			
			load_focus_tree = IRC_civil_war
			
			declare_war_on = {
				target = PER
				type = annex_everything
			}
		}
		IRB = {
			transfer_state = 1360
			add_state_core = 1360
			
			inherit_technology = PREV
			add_ideas = PER_The_Iranian_Civil_War
			
			add_offsite_building = { type = arms_factory level = 1 }
			add_offsite_building = { type = arms_factory level = 1 }
			
			load_oob = IRB_Civil_War
			
			declare_war_on = {
				target = PER
				type = annex_everything
			}
		}
		IRA = {
			set_cosmetic_tag = IRA_COALITION
			transfer_state = 419
			add_state_core = 419
			transfer_state = 973
			add_state_core = 973
			transfer_state = 974
			add_state_core = 974
			
			inherit_technology = PREV
			add_ideas = PER_The_Iranian_Civil_War
			
			add_offsite_building = { type = arms_factory level = 1 }
			add_offsite_building = { type = arms_factory level = 1 }
			
			load_oob = IRA_Civil_War
			
			load_focus_tree = IRA_civil_war
			
			declare_war_on = {
				target = PER
				type = annex_everything
			}
		}
		IRI = {
			transfer_state = 411
			add_state_core = 411
			transfer_state = 979
			add_state_core = 979
			transfer_state = 975
			add_state_core = 975
			transfer_state = 416
			add_state_core = 416
			transfer_state = 421
			add_state_core = 421
			
			add_offsite_building = { type = arms_factory level = 1 }
			add_offsite_building = { type = arms_factory level = 1 }
			add_offsite_building = { type = arms_factory level = 1 }
			add_offsite_building = { type = arms_factory level = 1 }
			
			inherit_technology = PREV
			add_ideas = PER_The_Iranian_Civil_War
			
			load_oob = IRI_Civil_War
			
			create_faction = Revolutionary_Iranian_Liberation_Front
			add_to_faction = IRL
			add_to_faction = IRC
			add_to_faction = IRB
			add_to_faction = IRA
			declare_war_on = {
				target = PER
				type = annex_everything
			}
			load_focus_tree = IRI_civil_war
		}
		every_country = {
			news_event = {
				id = WORLD.651
				days = 1
			}
		}
		IRI = {
			country_event = { 
				id = PER.3
				days = 90
			}
		}
		set_global_flag = Iranian_Civil_War
		USA = {	country_event = { id = USA_IR.1 } }
		GER = { country_event = { id = reich_shared.19 } }
		}
	}
}

country_event = { #The Coalition Collapses
	id = PER.3
	immediate = { log = "[GetDateText]: [Root.GetName]: event PER.3" }
	title = PER.3.t
	desc = PER.3.d

	is_triggered_only = yes
	fire_only_once = yes

	option = { 
		name = PER.3.a
		IRI = {
			dismantle_faction = yes
		}
		if = {
			limit = {
				country_exists = IRA
			}
			IRA = {
				declare_war_on = {
					target = IRI
					type = annex_everything
				}
				declare_war_on = {
					target = IRL
					type = annex_everything
				}
				declare_war_on = {
					target = IRC
					type = annex_everything
				}
			}
		}
		if = {
			limit = {
				country_exists = IRI
			}
			IRI = {
				declare_war_on = {
					target = IRL
					type = annex_everything
				}
				declare_war_on = {
					target = IRC
					type = annex_everything
				}
			}
		}
		if = {
			limit = {
				country_exists = IRL
			}
			IRL = {
				declare_war_on = {
					target = IRC
					type = annex_everything
				}
			}
		}
		set_global_flag = TNO_IRA_collapsed_coalition
		every_country = {
			news_event = {
				id = WORLD.652
				days = 1
			}
		}
	}
}

### Germany-Iran Interaction

add_namespace = PER_GER

country_event = { ###Germany Reassures the Shaah
	id = PER_GER.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event PER_GER.1"}
	title = PER_GER.1.t
	desc = PER_GER.1.d

	is_triggered_only = yes
	fire_only_once = yes

	option = {
		name = GER_GER.1.a
	}
}

##Civilwar related events##
country_event = { #FarahVictory
	id = percw.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event percw.1"}
	title = percw.1.t
	desc = percw.1.d
	picture = GFX_report_event_egypt_party_farouk
	hidden = yes
	fire_only_once = yes
	trigger = {
		NOT = { has_country_flag = TNO_my_world_is_on_fire_how_about_yours }
		if = {
			PER = {
				has_global_flag = Iranian_Civil_War
				NOT = {
					has_war_with = IRL
					has_war_with = IRC
					has_war_with = IRB
					has_war_with = IRA
					has_war_with = IRI
				}
			}
		}
	}
	immediate = {
		news_event = {
			id = WORLD.18009
			days = 1
		}
	}
}
country_event = { #DemRepVictory
	id = percw.2
	immediate = {log = "[GetDateText]: [Root.GetName]: event percw.2"}
	title = percw.2.t
	desc = percw.2.d
	picture = GFX_report_event_egypt_party_farouk
	hidden = yes
	fire_only_once = yes
	trigger = {
		NOT = { has_country_flag = TNO_my_world_is_on_fire_how_about_yours }
		if = {
			IRL = {
				has_global_flag = Iranian_Civil_War
				NOT = {
					has_war_with = PER
					has_war_with = IRC
					has_war_with = IRB
					has_war_with = IRA
					has_war_with = IRI
				}
			}
		}
	}
	immediate = {
		news_event = {
			id = WORLD.18010
			days = 1
		}
	}
}
country_event = { #Bakhtiar Victory
	id = percw.3
	immediate = {log = "[GetDateText]: [Root.GetName]: event percw.3"}
	title = percw.3.t
	desc = percw.3.d
	picture = GFX_report_event_egypt_party_farouk
	hidden = yes
	fire_only_once = yes
	trigger = {
		NOT = { has_country_flag = TNO_my_world_is_on_fire_how_about_yours }
		if = {
			IRC = {
				has_global_flag = Iranian_Civil_War
				NOT = {
					has_war_with = PER
					has_war_with = IRL
					has_war_with = IRB
					has_war_with = IRA
					has_war_with = IRI
				}
			}
		}
	}
	immediate = {
		news_event = {
			id = WORLD.18011
			days = 1
		}
	}
}
country_event = { #Balluch Victory
	id = percw.4
	immediate = {log = "[GetDateText]: [Root.GetName]: event percw.4"}
	title = percw.4.t
	desc = percw.4.d
	picture = GFX_report_event_egypt_party_farouk
	hidden = yes
	fire_only_once = yes
	trigger = {
		NOT = { has_country_flag = TNO_my_world_is_on_fire_how_about_yours }
		if = {
			IRB = {
				has_global_flag = Iranian_Civil_War
				NOT = {
					has_war_with = PER
					has_war_with = IRL
					has_war_with = IRC
					has_war_with = IRA
					has_war_with = IRI
				}
			}
		}
	}
	immediate = {
		news_event = {
			id = WORLD.18012
			days = 1
		}
	}
}
country_event = { #Communist Victory
	id = percw.5
	immediate = {log = "[GetDateText]: [Root.GetName]: event percw.5"}
	title = percw.5.t
	desc = percw.5.d
	picture = GFX_report_event_egypt_party_farouk
	hidden = yes
	fire_only_once = yes
	trigger = {
		NOT = { has_country_flag = TNO_my_world_is_on_fire_how_about_yours }
		if = {
			IRA = {
				has_global_flag = Iranian_Civil_War
				NOT = {
					has_war_with = PER
					has_war_with = IRL
					has_war_with = IRC
					has_war_with = IRB
					has_war_with = IRI
				}
			}
		}
	}
	immediate = {
		news_event = {
			id = WORLD.18013
			days = 1
		}
	}
}
country_event = { #Khomeini Victory
	id = percw.6
	immediate = {log = "[GetDateText]: [Root.GetName]: event percw.6"}
	title = percw.6.t
	desc = percw.6.d
	picture = GFX_report_event_egypt_party_farouk
	hidden = yes
	fire_only_once = yes
	trigger = {
		NOT = { has_country_flag = TNO_my_world_is_on_fire_how_about_yours }
		if = {
			IRI = {
				has_global_flag = Iranian_Civil_War
				NOT = {
					has_war_with = PER
					has_war_with = IRL
					has_war_with = IRC
					has_war_with = IRB
					has_war_with = IRA
				}
			}
		}
	}
	immediate = {
		news_event = {
			id = WORLD.18014
			days = 1
		}
	}
}