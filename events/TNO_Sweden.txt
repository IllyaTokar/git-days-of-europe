﻿add_namespace = SWE

### Goering Invasion Stuff ###
country_event = { #The Viking Program
	id = SWE.200
	immediate = {log = "[GetDateText]: [Root.GetName]: event SWE.200"}
    title = SWE.200.t
    desc = SWE.200.d
    picture = GFX_report_event_german_riot

    is_triggered_only = yes

    option = {
        name = SWE.200.a
		GER = {
			add_ideas = SWE_odin_program
			hidden_effect = {
				country_event = { id = goering.1099 days = 3 }
			}
		}
    }
    option = {
        name = SWE.200.b
		ai_chance = { factor = 0 }
    }
}

country_event = { #British Volunteers Arrive!
	id = SWE.201
	immediate = {log = "[GetDateText]: [Root.GetName]: event SWE.201"}
    title = SWE.201.t
    desc = SWE.201.d
    picture = GFX_report_event_black_legion

    is_triggered_only = yes

    option = {
        name = SWE.201.a
    }
}