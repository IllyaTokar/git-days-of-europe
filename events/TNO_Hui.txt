﻿add_namespace = hui

country_event = { #rape and pillage
	id = hui.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event hui.1"}
 	title = hui.1.t
 	desc = hui.1.d
 	picture = GFX_report_event_china_russia
 	
	is_triggered_only = yes
 	fire_only_once = yes
 	
 	option = {
 		name = hui.1.a
	}
 }