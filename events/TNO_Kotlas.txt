﻿add_namespace = MIR

###Warlord event pack

country_event = {
	id = MIR.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.1"}
	title = MIR.1.t
	desc = MIR.1.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = { #Confess
		name = MIR.1.a
		set_country_flag = PRISONERS_DILEMMA_SILENT
		complete_national_focus = MIR_against_the_opposition
	}
	option = { #Silent
		name = MIR.1.b
		set_country_flag = PRISONERS_DILEMMA_CONFESS
		complete_national_focus = MIR_for_ourselves
	}
}

country_event = { #Prisoner's Dilemma explanation
	id = MIR.2
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.2"}
	title = MIR.2.t
	desc = MIR.2.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.2.a
		add_political_power = 10
	}
}

country_event = { #Our Situation
	id = MIR.3
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.3"}
	title = MIR.3.t
	desc = MIR.3.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.3.a
		add_war_support = 0.05
	}
}

country_event = { #Vorshilov's Requests
	id = MIR.4
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.4"}
	title = MIR.4.t
	desc = MIR.4.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.4.a
		add_manpower = 300
	}
}

country_event = { #Ready to reunify
	id = MIR.5
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.5"}
	title = MIR.5.t
	desc = MIR.5.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.5.a
		WRS = {
			add_state_core = 870
			add_state_core = 860
			add_state_core = 861
		}
		WRS_increase_tukha_influence_med = yes
		WRS_decrease_factionalism_high = yes

	}
}

country_event = { #Observing Ukhta
	id = MIR.6
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.6"}
	title = MIR.6.t
	desc = MIR.6.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.6.a
		WRS_decrease_zhukov_influence_low = yes
	}
}

country_event = { #Dig through records
	id = MIR.7
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.7"}
	title = MIR.7.t
	desc = MIR.7.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.7.a
		add_political_power = 25
		WRS_decrease_zhukov_influence_med = yes
	}
}

country_event = { #Mission to Archangelsk
	id = MIR.8
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.8"}
	title = MIR.8.t
	desc = MIR.8.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.8.a
		WRS_increase_tukha_influence_high = yes
	}
}

country_event = { #Plesetsk asks for reinforcements
	id = MIR.9
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.9"}
	title = MIR.9.t
	desc = MIR.9.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.9.a
		MIR = {
			country_event = {
				id = MIR.10
				days = 1
			}
		}
	}
	option = {
		name = MIR.9.b
		MIR = {
			country_event = {
				id = MIR.11
				days = 1
			}
		}
	}
}

country_event = { #WRS says yes
	id = MIR.10
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.10"}
	title = MIR.10.t
	desc = MIR.10.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.10.a
		add_manpower = 1500
		WRS_increase_tukha_influence_low = yes
	}
}

country_event = { #WRS says no
	id = MIR.11
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.11"}
	title = MIR.11.t
	desc = MIR.11.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.11.a
		WRS_decrease_factionalism_med = yes
	}
}

country_event = { #Lose The Orders
	id = MIR.12
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.12"}
	title = MIR.12.t
	desc = MIR.12.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.12.a
	}
}

country_event = { #Tukhachevsky's Domain
	id = MIR.13
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.13"}
	title = MIR.13.t
	desc = MIR.13.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.13.a
	}
}

country_event = { #Assemble The General Staff
	id = MIR.14
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.14"}
	title = MIR.14.t
	desc = MIR.14.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = { #Focus Onega
		name = MIR.14.a
		861 = {
			add_building_construction = {
				type = bunker
				level = 3
				instant_build = yes
				province = 6075
			}
			add_building_construction = {
				type = bunker
				level = 3
				instant_build = yes
				province = 9187
			}
		}
	}
	option = { #Focus Komi
		name = MIR.14.b
		860 = {
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 9123
			}
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 9124
			}
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 6100
			}
		}
		870 = {
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 7330
			}
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 3155
			}
		}
	}
	option = { #Focus Vologda
		name = MIR.14.c
		861 = {
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 72
			}
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 6166
			}
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 11074
			}
		}
		860 = {
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 6199
			}
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 3166
			}
			add_building_construction = {
				type = bunker
				level = 1
				instant_build = yes
				province = 6100
			}
		}
	}
}

country_event = {
	id = MIR.15
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.15"}
	title = MIR.15.t
	desc = MIR.15.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = { #Yes
		name = MIR.15.a
		MIR = {
			country_event = {
				id = MIR.16
				days = 1
			}
		}
	}
}

country_event = {
	id = MIR.16
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.16"}
	title = MIR.16.t
	desc = MIR.16.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.16.a
	}
}

country_event = { #Resurgence Program
	id = MIR.17
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.17"}
	title = MIR.17.t
	desc = MIR.17.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.17.a
		add_war_support = 0.025
		add_stability = 0.1
	}
}

country_event = {
	id = MIR.18
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.18"}
	title = MIR.18.t
	desc = MIR.18.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.18.a
	}
}

country_event = {
	id = MIR.19
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.19"}
	title = MIR.19.t
	desc = MIR.19.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.19.a
	}
}

country_event = { #Load prisoner's dilemma
	id = MIR.20
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.20"}
	title = MIR.20.t
	desc = MIR.20.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.20.a
		#load_focus_tree = TNO_prisoners_dilemma
	}
}

country_event = {
	id = MIR.21
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.21"}
	title = MIR.21.t
	desc = MIR.21.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.21.a
	}
}

country_event = {
	id = MIR.22
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.22"}
	title = MIR.22.t
	desc = MIR.22.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.22.a
	}
}

country_event = {
	id = MIR.23
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.23"}
	title = MIR.23.t
	desc = MIR.23.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.23.a
	}
}

country_event = {
	id = MIR.24
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.24"}
	title = MIR.24.t
	desc = MIR.24.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.24.a
	}
}

country_event = {
	id = MIR.25
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.25"}
	title = MIR.25.t
	desc = MIR.25.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.25.a
	}
}

country_event = {
	id = MIR.26
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.26"}
	title = MIR.26.t
	desc = MIR.26.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.26.a
	}
}

country_event = {
	id = MIR.27
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.27"}
	title = MIR.27.t
	desc = MIR.27.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.27.a
	}
}

country_event = {
	id = MIR.28
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.28"}
	title = MIR.28.t
	desc = MIR.28.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.28.a
	}
}

country_event = {
	id = MIR.29
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.29"}
	title = MIR.29.t
	desc = MIR.29.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.29.a
	}
}

country_event = {
	id = MIR.30
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.30"}
	title = MIR.30.t
	desc = MIR.30.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.30.a
	}
}

country_event = {
	id = MIR.31
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.31"}
	title = MIR.31.t
	desc = MIR.31.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.31.a
	}
}

country_event = {
	id = MIR.32
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.32"}
	title = MIR.32.t
	desc = MIR.32.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.32.a
	}
}

country_event = {
	id = MIR.33
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.33"}
	title = MIR.33.t
	desc = MIR.33.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.33.a
	}
}

country_event = {
	id = MIR.34
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.34"}
	title = MIR.34.t
	desc = MIR.34.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.34.a
	}
}

country_event = {
	id = MIR.35
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.35"}
	title = MIR.35.t
	desc = MIR.35.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.35.a
		TAR = {
			transfer_state = 249
			transfer_state = 256
			transfer_state = 854
		}
		BKR = {
			transfer_state = 651
		}
	}
}

country_event = {
	id = MIR.36
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.36"}
	title = MIR.36.t
	desc = MIR.36.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.36.a
	}
}

country_event = {
	id = MIR.37
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.37"}
	title = MIR.37.t
	desc = MIR.37.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.37.a
	}
}

country_event = {
	id = MIR.38
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.38"}
	title = MIR.38.t
	desc = MIR.38.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.38.a
	}
}

country_event = {
	id = MIR.39
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.39"}
	title = MIR.39.t
	desc = MIR.39.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.39.a
	}
}

country_event = {
	id = MIR.40
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.40"}
	title = MIR.40.t
	desc = MIR.40.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.40.a
	}
}

country_event = {
	id = MIR.41
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.41"}
	title = MIR.41.t
	desc = MIR.41.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.41.a
	}
}

country_event = {
	id = MIR.42
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.42"}
	title = MIR.42.t
	desc = MIR.42.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.42.a
	}
}

country_event = {
	id = MIR.43
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.43"}
	title = MIR.43.t
	desc = MIR.43.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.43.a
	}
}

country_event = {
	id = MIR.44
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.44"}
	title = MIR.44.t
	desc = MIR.44.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.44.a
	}
}

country_event = {
	id = MIR.45
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.45"}
	title = MIR.45.t
	desc = MIR.45.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.45.a
	}
}

country_event = {
	id = MIR.46
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.46"}
	title = MIR.46.t
	desc = MIR.46.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.46.a
	}
}

country_event = {
	id = MIR.47
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.47"}
	title = MIR.47.t
	desc = MIR.47.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.47.a
	}
}

country_event = {
	id = MIR.48
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.48"}
	title = MIR.48.t
	desc = MIR.48.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.48.a
	}
}

country_event = {
	id = MIR.49
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.49"}
	title = MIR.49.t
	desc = MIR.49.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.49.a
	}
}

country_event = {
	id = MIR.50
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.50"}
	title = MIR.50.t
	desc = MIR.50.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.50.a
	}
}

country_event = { #Tukhavhevsky - Tukhachevsky made official successor
	id = MIR.100
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.100"}
	title = MIR.100.t
	desc = MIR.100.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.100.a
		WRS = {
			create_country_leader = {
				name = "Mikhail Tukhachevsky"
				desc = "POLITICS_MIKHAIL_TUKHACHEVSKY_DESC"
				picture = "Portrait_WRS_Mikhail_Tukhachevsky.dds"
				expire = "1999.1.1"
				ideology = communist_subtype
				traits = { 
				the_red_bonaparte
				}
			}
		}
		WRS = {
			remove_all_ministers = yes
			add_ideas = {
				WRS_Ieronim_Uborevich_hog
				WRS_Dmitry_Ustinov_eco
				WRS_Alexey_Yepishev_for
				WRS_Yan_Gamarnik_sec
			}
		}
		MIR = {
			every_unit_leader = {
				set_nationality = WRS
			}
		}
		WRS = {
			annex_country = {
				target = MIR
				transfer_troops = yes
			}
		}
		WRS = {
			set_country_flag = tukha_in_power
		}
		WRS = {
			change_tag_from = MIR
		}
	}
}
country_event = { #Tukhachevsky - Zhukov made official successor
	id = MIR.101
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.101"}
	title = MIR.101.t
	desc = MIR.101.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.101.a
		country_event = {
			id = MIR.102
			days = 1
		}
	}
}
country_event = { #Challenge Zhukov's Ascension?
	id = MIR.102
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.102"}
	title = MIR.102.t
	desc = MIR.102.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.102.a
		ai_chance = {
			factor = 20
			modifier = {
				check_variable = { global.WRSFactionalism < 20 }
				factor = 0.25
			}
			modifier = {
				check_variable = { global.WRSFactionalism > 30 }
				factor = 1
			}
			modifier = {
				check_variable = { global.WRSFactionalism > 50 }
				factor = 5
			}
			modifier = {
				check_variable = { global.WRSFactionalism > 70 }
				factor = 10
			}
			modifier = {
				check_variable = { global.WRSFactionalism > 90 }
				factor = 15
			}
		}
		WRS = {
			country_event = {
				id = MIR.103
			}
		}
	}
	option = {
		name = MIR.102.b
		ai_chance = {
			factor = 60
		}
		WRS = {
			country_event = {
				id = MIR.104
			}
		}
	}
}
country_event = { #Zhukov - Tukhachevsky challenges
	id = MIR.103
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.103"}
	title = MIR.103.t
	desc = MIR.103.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes
	hidden = yes
	
	option = {
		name = MIR.103.a
		MIR = {
			declare_war_on = {
				target = WRS
				type = annex_everything
			}
		}
	}
}
country_event = { #Zhukov - Tukhachevsky folds
	id = MIR.104
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.104"}
	title = MIR.104.t
	desc = MIR.104.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes
	hidden = yes

	option = {
		name = MIR.104.a
		MIR = {
			every_unit_leader = {
				set_nationality = WRS
			}
		}
		WRS = {
			annex_country = {
				target = MIR
				transfer_troops = yes
			}
			change_tag_from = MIR
		}
	}
}
country_event = { #Altunin made official successor
	id = MIR.105
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.105"}
	title = MIR.105.t
	desc = MIR.105.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.105.a
		MIR = {
			country_event = {
				id = MIR.106
				days = 1
			}
		}
	}
}
country_event = { #Challenge Altunin?
	id = MIR.106
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.106"}
	title = MIR.106.t
	desc = MIR.106.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.106.a
		ai_chance = {
			factor = 15
			modifier = {
				check_variable = { global.WRSFactionalism < 20 }
				factor = 0.25
			}
			modifier = {
				check_variable = { global.WRSFactionalism > 30 }
				factor = 1
			}
			modifier = {
				check_variable = { global.WRSFactionalism > 50 }
				factor = 5
			}
			modifier = {
				check_variable = { global.WRSFactionalism > 70 }
				factor = 10
			}
			modifier = {
				check_variable = { global.WRSFactionalism > 90 }
				factor = 15
			}
		}
		WRS = {
			country_event = {
				id = MIR.107
			}
		}
	}
	option = {
		name = MIR.106.b
		trigger = {
			NOT = {
				has_global_flag = WRS_Player_Is_Cheating_Bastard
			}
		}
		ai_chance = {
			factor = 70
		}
		WRS = {
			country_event = {
				id = MIR.108
			}
		}
	}
}
country_event = { #Altunin - Tukhachevsky challenges
	id = MIR.107
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.107"}
	title = MIR.107.t
	desc = MIR.107.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes
	option = {
		name = MIR.107.a
		MIR = {
			declare_war_on = {
				target = WRS
				type = annex_everything
			}
		}
		MIR = {
			declare_war_on = {
				target = UKH
				type = annex_everything
			}
		}
	}
}
country_event = { #Altunin - Tukhachevsky folds
	id = MIR.108
	immediate = {log = "[GetDateText]: [Root.GetName]: event MIR.108"}
	title = MIR.108.t
	desc = MIR.108.d
	picture = GFX_report_event_lithuania_army

	is_triggered_only = yes

	option = {
		name = MIR.108.a
		MIR = {
			every_unit_leader = {
				set_nationality = WRS
			}
		}
		WRS = {
			annex_country = {
				target = MIR
				transfer_troops = yes
			}
			change_tag_from = MIR
		}
	}
}
