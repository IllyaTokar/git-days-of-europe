﻿add_namespace = VOL

###Warlord event pack

country_event = { #Intro event
	id = VOL.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event VOL.1"}
	title = VOL.1.t
	desc = VOL.1.desc
	picture = GFX_report_event_generic_mountain
	
	is_triggered_only = yes
	fire_only_once = yes
	
	option = { #
		name = VOL.1.a
		ai_chance = { factor = 1 }
		
	}
}