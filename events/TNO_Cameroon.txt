﻿############################
## Cameroon Events
############################

add_namespace = CAO
#Dunno what this is
 country_event = { #annexing land yeet
	id = CAO.20
	immediate = {log = "[GetDateText]: [Root.GetName]: event CAO.20"}
 	title = CAO.20.t
 	desc = CAO.20.desc
 	picture = GFX_report_event_ostsonne
 	
	is_triggered_only = yes
 	fire_only_once = yes
 	
 	option = {
 		name = CAO.20.a
		CAO = {
			transfer_state = 1203
			transfer_state = 1202
			transfer_state = 1201
			transfer_state = 1196
			transfer_state = 826
			transfer_state = 1197
			transfer_state = 1198
			transfer_state = 1200
			transfer_state = 1207
			transfer_state = 1205
			transfer_state = 1206
			transfer_state = 1199
			transfer_state = 1204
			transfer_state = 1189
			transfer_state = 558
		}
 	}
}

### Intro event

 country_event = { #intro lore
	id = CAO.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event CAO.1"}
 	title = CAO.1.t
 	desc = CAO.1.desc
 	picture = GFX_report_event_african_rebels

	is_triggered_only = yes
 	fire_only_once = yes
	
 	option = {
 		name = CAO.1.a
 	
	country_event = {
			id = CAO.2
			hours = 1
		}
	}
}

 country_event = { #intro lore part 2
	id = CAO.2
	immediate = {log = "[GetDateText]: [Root.GetName]: event CAO.2"}
 	title = CAO.2.t
 	desc = CAO.2.desc
 	picture = GFX_report_event_african_rebels
	is_triggered_only = yes
 	option = {
 		name = CAO.2.a
		set_country_flag = CAO_ECON
		load_focus_tree = Cameroon_econ
 	}

}

 country_event = { #Oil event
	id = CAO.3
	immediate = {log = "[GetDateText]: [Root.GetName]: event CAO.3"}
 	title = CAO.3.t
 	desc = CAO.3.desc
 	picture = GFX_report_event_african_rebels
	is_triggered_only = yes
 	option = {
 		name = CAO.3.a
		random_list = {
				90 = {
					country_event = CAO.4
				}
				10 = {
					country_event = CAO.5
				}
			}
 	}

}

country_event = { #No oil :(
	id = CAO.4
	immediate = {log = "[GetDateText]: [Root.GetName]: event CAO.4"}
 	title = CAO.4.t
 	desc = CAO.4.desc
 	picture = GFX_report_event_african_rebels
	is_triggered_only = yes
 	option = {
 		name = CAO.4.a
 	}
}	
	country_event = { #THICC OIL
	id = CAO.5
	immediate = {log = "[GetDateText]: [Root.GetName]: event CAO.5"}
 	title = CAO.5.t
 	desc = CAO.5.desc
 	picture = GFX_report_event_african_rebels
	is_triggered_only = yes
 	option = {
 		name = CAO.5.a
		add_resource = {
    type = oil
    amount = 33
    state = 793
		}
 	}
}

### TEST LOAD MIL TREE
	country_event = { #THICC OIL
	id = CAO.6
	immediate = {log = "[GetDateText]: [Root.GetName]: event CAO.6"}
 	title = CAO.6.t
 	desc = CAO.6.desc
 	picture = GFX_report_event_african_rebels
	is_triggered_only = yes
 	option = {
 		name = CAO.6.a
		set_country_flag = milreform
		load_focus_tree = Cameroon_milreform
		}
 	}
