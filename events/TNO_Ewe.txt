﻿############################
## Ewe Events
############################

add_namespace = EWE

 country_event = { #annexing land yeet
	id = EWE.20
	immediate = {log = "[GetDateText]: [Root.GetName]: event EWE.20"}
 	title = EWE.20.t
 	desc = EWE.20.desc
 	picture = GFX_report_event_ostsonne
 	
	is_triggered_only = yes
 	fire_only_once = yes
 	
 	option = {
 		name = EWE.20.a
		EWE = {
			transfer_state = 833
			transfer_state = 274
		}
 	}
}