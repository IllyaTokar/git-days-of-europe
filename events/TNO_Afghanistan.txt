﻿######################
##AFGHANISTAN EVENTS##
##  MADE BY MUPPER  ##
######################

add_namespace = afghanistan

country_event = {
	id = afghanistan.1 #new constitution!
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.1"}
 	title = afghanistan.1.t
 	desc = afghanistan.1.desc
 	picture = GFX_report_event_afghanistan_people

	is_triggered_only = yes

 	option = {
 		name = afghanistan.1.a
		set_politics = {
			ruling_party = social_democrat
			last_election = "1960.8.29"
			election_frequency = 48
			elections_allowed = yes
		}
		set_popularities = {
			communist = 0
			socialist = 0
			social_democrat = 44
			social_liberal = 24
			market_liberal = 0
			social_conservative = 32
			authoritarian_democrat = 0
			despotism = 0
			fascism = 0
			national_socialism = 0
			ultranational_socialism = 0
			burgundian_system = 0
		}
		add_ideas = AFG_Mohammad_Zahir_Shah
		random_list = {
			44 = {
				country_event = {
					id = afghanistan.2
					days = 250
				}
			}
			24 = {
				country_event = {
					id = afghanistan.3
					days = 250
				}
			}
			32 = {
				country_event = {
					id = afghanistan.4
					days = 250
				}
			}
		}
 	}
}

country_event = {
	id = afghanistan.2 #socdems elected
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.2"}
 	title = afghanistan.2.t
 	desc = afghanistan.2.desc
 	picture = GFX_report_event_afghanistan_people

	is_triggered_only = yes

 	option = {
 		name = afghanistan.2.a
		set_popularities = {
			communist = 0
			socialist = 0
			social_democrat = 44
			social_liberal = 24
			market_liberal = 0
			social_conservative = 32
			authoritarian_democrat = 0
			despotism = 0
			fascism = 0
			national_socialism = 0
			ultranational_socialism = 0
			burgundian_system = 0
		}
		if = {
			limit = { date > 1964.8.29 }
			set_politics = {
				ruling_party = social_democrat
				last_election = "1964.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		else_if = {
			limit = { date > 1968.8.29 }
			set_politics = {
				ruling_party = social_democrat
				last_election = "1968.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		else_if = {
			limit = { date > 1972.8.29 }
			set_politics = {
				ruling_party = social_democrat
				last_election = "1972.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		else = {
			set_politics = {
				ruling_party = social_democrat
				last_election = "1960.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		random_list = {
			34 = {
				country_event = {
					id = afghanistan.2
					days = 1461
				}
			}
			24 = {
				country_event = {
					id = afghanistan.3
					days = 1461
				}
			}
			22 = {
				country_event = {
					id = afghanistan.4
					days = 1461
				}
			}
			20 = {
				country_event = {
					id = afghanistan.5
					days = 1000
				}
			}
		}
 	}
}

country_event = {
	id = afghanistan.3 #libdems elected
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.3"}
 	title = afghanistan.3.t
 	desc = afghanistan.3.desc
 	picture = GFX_report_event_afghanistan_people

	is_triggered_only = yes

 	option = {
 		name = afghanistan.3.a
		set_popularities = {
			communist = 0
			socialist = 0
			social_democrat = 32
			social_liberal = 44
			market_liberal = 0
			social_conservative = 24
			authoritarian_democrat = 0
			despotism = 0
			fascism = 0
			national_socialism = 0
			ultranational_socialism = 0
			burgundian_system = 0
		}
		if = {
			limit = { date > 1964.8.29 }
			set_politics = {
				ruling_party = social_liberal
				last_election = "1964.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		else_if = {
			limit = { date > 1968.8.29 }
			set_politics = {
				ruling_party = social_liberal
				last_election = "1968.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		else_if = {
			limit = { date > 1972.8.29 }
			set_politics = {
				ruling_party = social_liberal
				last_election = "1972.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		else = {
			set_politics = {
				ruling_party = social_liberal
				last_election = "1960.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		random_list = {
			32 = {
				country_event = {
					id = afghanistan.2
					days = 1461
				}
			}
			44 = {
				country_event = {
					id = afghanistan.3
					days = 1461
				}
			}
			24 = {
				country_event = {
					id = afghanistan.4
					days = 1461
				}
			}
		}
 	}
}

country_event = {
	id = afghanistan.4 #condems elected
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.4"}
 	title = afghanistan.4.t
 	desc = afghanistan.4.desc
 	picture = GFX_report_event_afghanistan_people

	is_triggered_only = yes

 	option = {
 		name = afghanistan.4.a
		set_popularities = {
			communist = 0
			socialist = 0
			social_democrat = 32
			social_liberal = 24
			market_liberal = 0
			social_conservative = 44
			authoritarian_democrat = 0
			despotism = 0
			fascism = 0
			national_socialism = 0
			ultranational_socialism = 0
			burgundian_system = 0
		}
		if = {
			limit = { date > 1964.8.29 }
			set_politics = {
				ruling_party = social_conservative
				last_election = "1964.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		else_if = {
			limit = { date > 1968.8.29 }
			set_politics = {
				ruling_party = social_conservative
				last_election = "1968.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		else_if = {
			limit = { date > 1972.8.29 }
			set_politics = {
				ruling_party = social_conservative
				last_election = "1972.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		else = {
			set_politics = {
				ruling_party = social_conservative
				last_election = "1960.8.29"
				election_frequency = 48
				elections_allowed = yes
			}
		}
		random_list = {
			32 = {
				country_event = {
					id = afghanistan.2
					days = 1461
				}
			}
			24 = {
				country_event = {
					id = afghanistan.3
					days = 1461
				}
			}
			44 = {
				country_event = {
					id = afghanistan.4
					days = 1461
				}
			}
		}
 	}
}

country_event = {
	id = afghanistan.5 #daoud khan creates the republic
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.5"}
 	title = afghanistan.5.t
 	desc = afghanistan.5.desc
 	picture = GFX_report_event_afghanistan_people

	is_triggered_only = yes

 	option = {
 		name = afghanistan.5.a
		set_politics = {
			ruling_party = authoritarian_democrat
			last_election = "1960.8.29"
			election_frequency = 48
			elections_allowed = no
		}
		set_popularities = {
			communist = 14
			socialist = 0
			social_democrat = 0
			social_liberal = 18
			market_liberal = 0
			social_conservative = 12
			authoritarian_democrat = 56
			despotism = 0
			fascism = 0
			national_socialism = 0
			ultranational_socialism = 0
			burgundian_system = 0
		}
		remove_ideas = AFG_Mohammad_Zahir_Shah
		create_country_leader = {
			name = "Mohammad Daoud Khan"
			desc = "POLITICS_MOHAMMAD_DAOUD_KHAN_DESC"
			picture = "Portrait_Afghanistan_Mohammad_Daoud_Khan.dds"
			expire = "1999.1.1"
			ideology = authoritarian_democrat_subtype
			traits = {
			}
		}
		set_party_name = {
			ideology = authoritarian_democrat
			long_name = AFG_pdp_party_long
			name = AFG_pdp_party
		}
		drop_cosmetic_tag = yes
		set_cosmetic_tag = AFG_republic
 	}
}

country_event = {
	id = afghanistan.8 #terrorists in balochistan
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.8"}
	title = afghanistan.8.t
	desc = afghanistan.8.desc
	picture = GFX_report_event_generic_terrorist_attack

	is_triggered_only = yes
	fire_only_once = yes

	option = { #ok
		name = afghanistan.8.a
		444 = {
			set_demilitarized_zone = yes
		}
		hidden_effect = {
			if = {
				limit = {
					RAJ = {
						owns_state = 749
					}
				}
				RAJ = {
					country_event = {
						id = afghanistan.9
						days = 16
					}
				}
			}
			else_if = {
				limit = {
					PAK = {
						owns_state = 749
					}
				}
				PAK = {
					country_event = {
						id = afghanistan.9
						days = 16
					}
				}
			}
			else = {
				749 = {
					set_demilitarized_zone = yes
				}
			}
		}
	}
}

country_event = {
	id = afghanistan.9 #battle of gwadar
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.9"}
	title = afghanistan.9.t
	desc = afghanistan.9.desc
	picture = GFX_report_event_generic_terrorist_attack

	is_triggered_only = yes
	fire_only_once = yes

	option = { #ok
		name = afghanistan.9.a
		749 = {
			set_demilitarized_zone = yes
		}
	}
}

country_event = {
	id = afghanistan.10 #Japan - Afghanistan Requests Re-Entry Into the Sphere
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.10"}
	title = afghanistan.10.t
	desc = afghanistan.10.desc
	picture = GFX_report_event_afghanistan_people_2

	is_triggered_only = yes
	fire_only_once = yes

	option = { #ok
		name = afghanistan.10.a
		ai_chance = {
			factor = 50
		}
		AFG = {
			country_event = {
				id = afghanistan.11
				hours = 7
			}
		}
	}
	option = { #no
		name = afghanistan.10.b
		AFG = {
			country_event = {
				id = afghanistan.12
				hours = 7
			}
		}
	}
}

country_event = {
	id = afghanistan.11 #Japan Accepts!
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.11"}
	title = afghanistan.11.t
	desc = afghanistan.11.desc
	picture = GFX_report_event_afghanistan_people_2

	is_triggered_only = yes
	fire_only_once = yes

	option = { #ok
		name = afghanistan.11.a
#		remove_idea = Sphere_Observer
		add_ideas = Sphere_Economic_Dependent
		JAP = { add_to_faction = AFG }
		hidden_effect = {
			every_country = {
				news_event = {
					id = WORLD.490
				}
			}
		}
	}
}

country_event = {
	id = afghanistan.12 #Japan Declines
	immediate = {log = "[GetDateText]: [Root.GetName]: event afghanistan.12"}
	title = afghanistan.12.t
	desc = afghanistan.12.desc
	picture = GFX_report_event_afghanistan_people_2

	is_triggered_only = yes
	fire_only_once = yes

	option = { #ok
		name = afghanistan.12.a
	}
}