add_namespace = OFN_COG
add_namespace = OFN_COG_flavor


country_event = { 
	id = OFN_COG.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.1"}
	title = OFN_COG.1.t
	desc = OFN_COG.1.desc
	picture = GFX_report_event_central_africa_abrams_1

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.1.a
		add_political_power = 10
	}
}

country_event = { 
	id = OFN_COG.2
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.2"}
	title = OFN_COG.2.t
	desc = OFN_COG.2.desc
	picture = GFX_report_event_cog_schramme_1

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.2.a
		295 = {
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		if = {
			limit = {
			has_country_flag = OFN_COG_Work_With_Germans 
			}
			add_to_variable = { money_reserves = 8 }
			custom_effect_tooltip = OFN_COG_Money_8_tt
		}
		else = {
			add_to_variable = { money_reserves = 4 }
			custom_effect_tooltip = OFN_COG_Money_4_tt
		}
	}	
}

country_event = { 
	id = OFN_COG.4
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.4"}
	title = OFN_COG.4.t
	desc = OFN_COG.4.desc
	picture = GFX_report_event_central_africa_kasa_vubu_1

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.4.a
		add_to_variable = { OFN_MANDATE_rebel_stability = 0.02 }
		clamp_variable = {
			var = OFN_MANDATE_rebel_stability
			max = 1
			min = 0
		}
		custom_effect_tooltip = OFN_COG_rebel_stab_increase_tt
	}
}

country_event = { 
	id = OFN_COG.5
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.5"}
	title = OFN_COG.5.t
	desc = OFN_COG.5.desc
	picture = GFX_report_event_central_africa_mba_1

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.5.a
		add_to_variable = { OFN_MANDATE_rebel_stability = 0.1 }
		clamp_variable = {
			var = OFN_MANDATE_rebel_stability
			max = 1
			min = 0
		}
		custom_effect_tooltip = OFN_COG_rebel_stab_increase_tt
		subtract_from_variable = { money_reserves = 0.5 }
		custom_effect_tooltip = OFN_COG_Money_Negative_0_5_tt
		539 = {
		add_building_construction = {
			type = infrastructure
			level = 2
			instant_build = yes
		}
	}
	}
}

country_event = { 
	id = OFN_COG.6
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.6"}
	title = OFN_COG.6.t
	desc = OFN_COG.6.desc
	picture = GFX_report_event_cog_mobutu_army

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.6.a
		hidden_effect = {
			country_event = {
				id = OFN_COG.61
				days = 3
			}
		}
	}
}

country_event = { 
	id = OFN_COG.61
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.61"}
	title = OFN_COG.61.t
	desc = OFN_COG.61.desc
	picture = GFX_report_event_cog_mobutu_army

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.61.a
		add_to_variable = { OFN_MANDATE_rebel_stability = 0.05 }
		clamp_variable = {
			var = OFN_MANDATE_rebel_stability
			max = 1
			min = 0
		}
		custom_effect_tooltip = OFN_COG_rebel_stab_increase_tt
		remove_ideas = COG_OFN_Manace_of_Cameroon_2
	}
}

country_event = { 
	id = OFN_COG.7
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.7"}
	title = OFN_COG.7.t
	desc = OFN_COG.7.desc
	picture = GFX_report_event_central_africa_abrams_2

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.7.a
		add_to_variable = { OFN_MANDATE_rebel_stability = 0.05 }
		clamp_variable = {
			var = OFN_MANDATE_rebel_stability
			max = 1
			min = 0
		}
		custom_effect_tooltip = OFN_COG_rebel_stab_increase_tt
	}

	option = {
		name = OFN_COG.7.b
		add_to_variable = { money_reserves = 8 }
		custom_effect_tooltip = OFN_COG_Money_8_tt
	}
}

country_event = { 
	id = OFN_COG.3
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.3"}
	title = OFN_COG.3.t
	desc = OFN_COG.3.desc
	picture = GFX_report_event_cog_schramme_1

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.3.a
		add_ideas = COG_OFN_Employed_Mercs
		subtract_from_variable = { money_reserves = 1 }
		custom_effect_tooltip = OFN_COG_Money_Negative_1_tt	
	}
}

country_event = { 
	id = OFN_COG.10
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.10"}
	title = OFN_COG.10.t
	desc = OFN_COG.10.desc
	picture = GFX_report_event_central_africa_attack_1

	is_triggered_only = yes

	option = {
		name = OFN_COG.10.a
		random_owned_state = {
			damage_building = {
				type = infrastructure
				damage = 0.3
			}
		}
		subtract_from_variable = { OFN_MANDATE_rebel_stability = 0.05 }
		clamp_variable = {
			var = OFN_MANDATE_rebel_stability
			max = 1
			min = 0
		}
		custom_effect_tooltip = OFN_COG_rebel_stab_decrease_tt
	}
}

country_event = { 
	id = OFN_COG.11
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.11"}
	title = OFN_COG.11.t
	desc = OFN_COG.11.desc
	picture = GFX_report_event_central_africa_attack_2

	is_triggered_only = yes

	option = {
		name = OFN_COG.11.a
		random_owned_state = {
			damage_building = {
				type = arms_factory
				damage = 0.3
			}
		}
		subtract_from_variable = { OFN_MANDATE_rebel_stability = 0.05 }
		clamp_variable = {
			var = OFN_MANDATE_rebel_stability
			max = 1
			min = 0
		}
		custom_effect_tooltip = OFN_COG_rebel_stab_decrease_tt
	}
}

country_event = { 
	id = OFN_COG.12
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.12"}
	title = OFN_COG.12.t
	desc = OFN_COG.12.desc
	picture = GFX_report_event_cog_native_1

	is_triggered_only = yes

	option = {
		name = OFN_COG.12.a
		set_country_flag = OFN_COG_rebel_flareup
	}
}

country_event = { 
	id = OFN_COG.13
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.13"}
	title = OFN_COG.13.t
	desc = OFN_COG.13.desc
	picture = GFX_report_event_cog_merc_9

	is_triggered_only = yes

	option = {
		name = OFN_COG.13.a
		set_country_flag = OFN_COG_German_Mercs
	}
}

country_event = { 
	id = OFN_COG.14
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.14"}
	title = OFN_COG.14.t
	desc = OFN_COG.14.desc
	picture = GFX_report_event_cog_native_3

	is_triggered_only = yes

	option = {
		name = OFN_COG.14.a
		set_country_flag = OFN_COG_Lumumba_Stronger
		set_country_flag = OFN_COG_Congo_Crisis
	}
}

country_event = { 
	id = OFN_COG.15
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.15"}
	title = OFN_COG.15.t
	desc = OFN_COG.15.desc
	picture = GFX_report_event_cog_native_3

	is_triggered_only = yes

	option = {
		name = OFN_COG.15.a
		set_country_flag = OFN_COG_Mobutu_Stronger
		set_country_flag = OFN_COG_Congo_Crisis
	}
}

country_event = { 
	id = OFN_COG.16
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.16"}
	title = OFN_COG.16.t
	desc = OFN_COG.16.desc
	picture = GFX_report_event_central_africa_lumumba

	is_triggered_only = yes

	option = {
		name = OFN_COG.16.a
		COG = {
			create_country_leader = {
				name = "Collapsed Authority"
				desc = "POLITICS_COG_COLLAPSED_AUTHORITY_DESC"
				picture = "Portrait_Central_Africa_OFN_Collapsed_Authority.dds"
				expire = "1999.1.23"
				ideology = authoritarian_democrat_subtype
				traits = { }
			}
			leave_faction = yes
		}
		USA = {
			end_puppet = COG
		}
		hidden_effect = {
			CFS = {
				set_capital = 1168

				set_state_owner = 1168
				set_state_controller = 1168
				add_state_core = 1168

				set_state_owner = 1172
				set_state_controller = 1172
				add_state_core = 1172

				set_state_owner = 1173
				set_state_controller = 1173
				add_state_core = 1173

				set_state_owner = 1174
				set_state_controller = 1174
				add_state_core = 1174

				declare_war_on = {
				    target = COG
				    type = annex_everything
				}
			}
			CPR = {
				set_state_owner = 718
				set_state_controller = 718
				add_state_core = 718

				set_state_owner = 1170
				set_state_controller = 1170
				add_state_core = 1170

				set_state_owner = 1175
				set_state_controller = 1175
				add_state_core = 1175

				set_state_owner = 1176
				set_state_controller = 1176
				add_state_core = 1176

				set_state_owner = 1177
				set_state_controller = 1177
				add_state_core = 1177

				set_state_owner = 1178
				set_state_controller = 1178
				add_state_core = 1178

				set_state_owner = 1180
				set_state_controller = 1180
				add_state_core = 1180

				declare_war_on = {
				    target = COG
				    type = annex_everything
				}

				declare_war_on = {
				    target = CFS
				    type = annex_everything
				}
			}
			KTG = {
				set_state_owner = 1056
				set_state_controller = 1056
				add_state_core = 1056

				set_state_owner = 1057
				set_state_controller = 1057
				add_state_core = 1057

				declare_war_on = {
				    target = COG
				    type = annex_everything
				}
			}
			KSI = {
				set_state_owner = 1055
				set_state_controller = 1055
				add_state_core = 1055

				set_state_owner = 1169
				set_state_controller = 1169
				add_state_core = 1169

				declare_war_on = {
				    target = CFS
				    type = annex_everything
				}

				declare_war_on = {
				    target = COG
				    type = annex_everything
				}
			}
			AZD = {
				if = {
					limit = {
						COG = {
						    controls_state = 1165
						}
					}
					transfer_state = 1165
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1171
						}
					}
					transfer_state = 1171
				}
				if = {
					limit = {
						COG = {
							controls_state = 1184
						}
					}
					transfer_state = 1184
				}
				if = {
					limit = {
						COG = {
							controls_state = 538
						}
					}
					transfer_state = 538
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1181
						}
					}
					transfer_state = 1181
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1182
						}
					}
					transfer_state = 1182
				}
				if = {
					limit = {
						COG  = {
						    controls_state = 1180
						}
					}
					transfer_state = 1180
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1183
						}
					}
					transfer_state = 1183
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1185
						}
					}
					transfer_state = 1185
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1185
						}
					}
					transfer_state = 1185
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1178
						}
					}
					transfer_state = 1178
				}
				if = {
					limit = {
						COG = {
						    controls_state = 718
						}
					}
					transfer_state = 718
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1170
						}
					}
					transfer_state = 1170
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1177
						}
					}
					transfer_state = 1177
				}
				if = {
					limit = {
						COG = {
						    controls_state = 1176
						}
					}
					transfer_state = 1176
				}
				inherit_technology = PREV
				load_oob = AZD_civil_war

				declare_war_on = {
				    target = CFS
				    type = annex_everything
				}

				declare_war_on = {
				    target = CPR
				    type = annex_everything
				}

				declare_war_on = {
				    target = COG
				    type = annex_everything
				}
			}
			COG = {
				every_unit_leader = {
					limit = { 
						has_id = 181001
					}
					set_nationality = AZD
				}
				every_unit_leader = {
					limit = { 
						has_id = 181002
					}
					set_nationality = CFS
				}
			}
			if = {
				limit = {
					has_country_flag = OFN_COG_Lumumba_Stronger
				}
				CFS = {
					add_equipment_to_stockpile = {
						type = infantry_equipment_1
						amount = 5000
					}
					load_oob = CFS_civil_war_extras
					load_oob = CFS_civil_war_extras
					load_oob = CFS_civil_war_extras
				}
				CPR = {
					add_equipment_to_stockpile = {
						type = infantry_equipment_1
						amount = 5000
					}
					load_oob = CPR_civil_war_extras
					load_oob = CPR_civil_war_extras
					load_oob = CPR_civil_war_extras
				}
				else_if = {
					limit = {
						has_country_flag = OFN_COG_Mobutu_Stronger
					}
					AZD = {
						add_equipment_to_stockpile = {
							type = infantry_equipment_1
							amount = 5000
						}
						load_oob = AZD_civil_war_extras
						load_oob = AZD_civil_war_extras
						load_oob = AZD_civil_war_extras
					}
				}
			}
			country_event = {
			 	id = OFN_AFR_DECOL_COG.8
			 	days = 10
			}
		}
	}
}

country_event = { 
	id = OFN_COG.50 #Germany reacts to asset seizure by OFN COG
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.50"}
	title = OFN_COG.50.t
	desc = OFN_COG.50.desc
	picture = GFX_report_event_central_africa_abrams_2

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.50.a
		add_political_power = -10
		add_opinion_modifier = {
			target = COG
			modifier = OFN_COG_seized_african_assets
		}
	}
}

country_event = { 
	id = OFN_COG.51 #Japan reacts to asset seizure by OFN COG
	immediate = {log = "[GetDateText]: [Root.GetName]: event OFN_COG.51"}
	title = OFN_COG.51.t
	desc = OFN_COG.51.desc
	picture = GFX_report_event_central_africa_abrams_2

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG.51.a
		add_political_power = -10
		add_opinion_modifier = {
			target = COG
			modifier = OFN_COG_seized_african_assets
		}
	}
}

country_event = { 
	id = OFN_COG_flavor.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event OFN_COG_flavor.1" }
	title = OFN_COG_flavor.1.t
	desc = OFN_COG_flavor.1.desc
	picture = GFX_report_event_usa_drinking_businessmen
	
	trigger = {
		country_exists = COG
		if = {
			limit = { country_exists = COG }
			COG = {
				NOT = { has_country_flag = OFN_COG_Congo_Crisis }
				is_puppet_of = USA
			}
		}
	}

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = OFN_COG_flavor.1.a
	}
}