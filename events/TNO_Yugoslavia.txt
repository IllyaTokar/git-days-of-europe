﻿add_namespace = YUG

#Debug Spawn Event#
country_event = {
	id = YUG.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event YUG.1"}
	title = YUG.1.t
	desc = YUG.1.desc

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = YUG.1.a
		YUG = {
			transfer_state = 1273
			transfer_state = 108 
			transfer_state = 1274
			add_state_core = 1273
			add_state_core = 108
			add_state_core = 1274
			add_state_core = 1272
			add_state_core = 1275
			add_state_core = 107
		declare_war_on = {
			target = SER
			type = annex_everything
		}
	}
}