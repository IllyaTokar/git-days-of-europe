﻿add_namespace = MCW
add_namespace = MCW_DBG

### DEBUG EVENTS ###
#Restore 1962 Moskowien
country_event = {
	id = MCW_DBG.1
	title = "Restore 1962 Moskowien"
	desc = "Moskowien Debug"
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW_DBG.1"}

	option = {
		name = "Ok"

		MCW = {
			set_cosmetic_tag = MCW_GER
			clr_global_flag = MCW_collapse
			clr_global_flag = MCW_2SS_German_Pref
			clr_global_flag = MCW_2SS_Russian_Pref
			clr_global_flag = MCW_2SS_peace
			clr_global_flag = MCW_2SS_war
			clr_global_flag = MCW_3SS
			transfer_state = 217
			transfer_state = 224
			transfer_state = 236
			transfer_state = 237
			transfer_state = 238
			transfer_state = 239
			transfer_state = 240
			transfer_state = 248
			transfer_state = 401
			transfer_state = 1314
			transfer_state = 205
			transfer_state = 208
			transfer_state = 220
			transfer_state = 222
			transfer_state = 223
			transfer_state = 241
			transfer_state = 242
			transfer_state = 243
			transfer_state = 244
			transfer_state = 245
			transfer_state = 246
			transfer_state = 247
			transfer_state = 253
			transfer_state = 254
			transfer_state = 255
			transfer_state = 257
			transfer_state = 258
			transfer_state = 263
			transfer_state = 264
			transfer_state = 265
			transfer_state = 195
			transfer_state = 209
			transfer_state = 210
			transfer_state = 219
			transfer_state = 260
		}
	}
}

#3SS - Russian = German Preference
country_event = {
	id = MCW_DBG.2
	title = "3SS - Russian = German Preference"
	desc = "Moskowien Debug"
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW_DBG.2"}

	option = {
		name = "Ok"
		set_global_flag = MCW_3SS
		VLG = {
			transfer_state = 217
			transfer_state = 224
			transfer_state = 236
			transfer_state = 237
			transfer_state = 238
			transfer_state = 239
			transfer_state = 240
			transfer_state = 248
			transfer_state = 401
			transfer_state = 1314

			hidden_effect = {
				add_state_core = 217
				add_state_core = 224
				add_state_core = 236
				add_state_core = 237
				add_state_core = 238
				add_state_core = 239
				add_state_core = 240
				add_state_core = 248
				add_state_core = 401
				add_state_core = 1314
			}

			country_event = VLG.3
			load_oob = "VLG_3SS"
		}

		MOS = {
			transfer_state = 205
			transfer_state = 208
			transfer_state = 220
			transfer_state = 222
			transfer_state = 223
			transfer_state = 241
			transfer_state = 242
			transfer_state = 243
			transfer_state = 244
			transfer_state = 245
			transfer_state = 246
			transfer_state = 247
			transfer_state = 253
			transfer_state = 254
			transfer_state = 255
			transfer_state = 257
			transfer_state = 258
			transfer_state = 263
			transfer_state = 264
			transfer_state = 265

			hidden_effect = {
				country_event = MOS.1
				add_state_core = 205
				add_state_core = 208
				add_state_core = 220
				add_state_core = 222
				add_state_core = 223
				add_state_core = 241
				add_state_core = 242
				add_state_core = 243
				add_state_core = 244
				add_state_core = 245
				add_state_core = 246
				add_state_core = 247
				add_state_core = 253
				add_state_core = 254
				add_state_core = 255
				add_state_core = 257
				add_state_core = 258
				add_state_core = 263
				add_state_core = 264
				add_state_core = 265
			}

			country_event = MOS.1
			load_oob = "MOS_3SS"
		}
		PET = {
			transfer_state = 195
			transfer_state = 209
			transfer_state = 210
			transfer_state = 219
			transfer_state = 260

			hidden_effect = {
				country_event = PET.1
				add_state_core = 195
				add_state_core = 209
				add_state_core = 210
				add_state_core = 219
				add_state_core = 260
			}

			load_oob = "PET_3SS"
		}
	}
}

#2SS - Russian > German Preference
country_event = {
	id = MCW_DBG.3
	title = "2SS - Russian > German Preference"
	desc = "Moskowien Debug"
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW_DBG.3"}

	option = {
		name = "Ok"

		VLG = {
			transfer_state = 217
			transfer_state = 224
			transfer_state = 236
			transfer_state = 237
			transfer_state = 238
			transfer_state = 239
			transfer_state = 240
			transfer_state = 248
			transfer_state = 401
			transfer_state = 1314
			transfer_state = 195
			transfer_state = 209
			transfer_state = 210

			hidden_effect = {
				country_event = { id = VLG.2 }
				add_state_core = 217
				add_state_core = 224
				add_state_core = 236
				add_state_core = 237
				add_state_core = 238
				add_state_core = 239
				add_state_core = 240
				add_state_core = 248
				add_state_core = 401
				add_state_core = 1314
				add_state_core = 195
				add_state_core = 209
				add_state_core = 210
			}

			#load_oob = "VLG_2SS_Russian_Pref"
			set_global_flag = MCW_2SS_Russian_Pref
			#country_event = { id = MCW.109 days = 100 }
		}

		MOS = {
			transfer_state = 205
			transfer_state = 208
			transfer_state = 220
			transfer_state = 222
			transfer_state = 223
			transfer_state = 241
			transfer_state = 242
			transfer_state = 243
			transfer_state = 244
			transfer_state = 245
			transfer_state = 246
			transfer_state = 247
			transfer_state = 253
			transfer_state = 254
			transfer_state = 255
			transfer_state = 257
			transfer_state = 258
			transfer_state = 263
			transfer_state = 264
			transfer_state = 265
			transfer_state = 209
			transfer_state = 210
			transfer_state = 219
			transfer_state = 260

			hidden_effect = {
				country_event = MOS.1
				add_state_core = 205
				add_state_core = 208
				add_state_core = 220
				add_state_core = 222
				add_state_core = 223
				add_state_core = 241
				add_state_core = 242
				add_state_core = 243
				add_state_core = 244
				add_state_core = 245
				add_state_core = 246
				add_state_core = 247
				add_state_core = 253
				add_state_core = 254
				add_state_core = 255
				add_state_core = 257
				add_state_core = 258
				add_state_core = 263
				add_state_core = 264
				add_state_core = 265
				add_state_core = 195
				add_state_core = 209
				add_state_core = 210
				add_state_core = 219
				add_state_core = 260
			}

			load_oob = "MOS_2SS_Russian_Pref"
		}
	}
}

#2SS - Russian < German Preference
country_event = {
	id = MCW_DBG.4
	title = "2SS - Russian > German Preference"
	desc = "Moskowien Debug"
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW_DBG.4"}

	option = {
		name = "Ok"

		VLG = {
			country_event = { id = VLG.2 }
			transfer_state = 217
			transfer_state = 224
			transfer_state = 236
			transfer_state = 237
			transfer_state = 238
			transfer_state = 239
			transfer_state = 240
			transfer_state = 248
			transfer_state = 401
			transfer_state = 1314
			transfer_state = 195
			transfer_state = 209
			transfer_state = 210
			transfer_state = 219
			transfer_state = 260

			hidden_effect = {
				add_state_core = 217
				add_state_core = 224
				add_state_core = 236
				add_state_core = 237
				add_state_core = 238
				add_state_core = 239
				add_state_core = 240
				add_state_core = 248
				add_state_core = 401
				add_state_core = 1314
				add_state_core = 195
				add_state_core = 209
				add_state_core = 210
				add_state_core = 219
				add_state_core = 260
			}

			#load_oob = "VLG_2SS_German_Pref"
			set_global_flag = MCW_2SS_German_Pref
			#country_event = { id = MCW.109 days = 100 }
		}

		MOS = {
			transfer_state = 205
			transfer_state = 208
			transfer_state = 220
			transfer_state = 222
			transfer_state = 223
			transfer_state = 241
			transfer_state = 242
			transfer_state = 243
			transfer_state = 244
			transfer_state = 245
			transfer_state = 246
			transfer_state = 247
			transfer_state = 253
			transfer_state = 254
			transfer_state = 255
			transfer_state = 257
			transfer_state = 258
			transfer_state = 263
			transfer_state = 264
			transfer_state = 265

			hidden_effect = {
				country_event = MOS.1
				add_state_core = 205
				add_state_core = 208
				add_state_core = 220
				add_state_core = 222
				add_state_core = 223
				add_state_core = 241
				add_state_core = 242
				add_state_core = 243
				add_state_core = 244
				add_state_core = 245
				add_state_core = 246
				add_state_core = 247
				add_state_core = 253
				add_state_core = 254
				add_state_core = 255
				add_state_core = 257
				add_state_core = 258
				add_state_core = 263
				add_state_core = 264
				add_state_core = 265
			}

			load_oob = "MOS_2SS_German_Pref"
		}
	}
}

#Collapse - Russian Pref > 5 || German Pref > 5
country_event = { 
	id = MCW_DBG.5
	title = "Collapse - Russian Pref > 5 || German Pref > 5"
	desc = "Moskowien Debug"
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW_DBG.5"}

	option = {
		name = "Ok"
		
		set_global_flag = MCW_collapse

		PSK = {
			add_state_core = 263
			transfer_state = 263
			add_state_core = 209
			transfer_state = 209
			add_state_core = 210
			transfer_state = 210
			add_state_core = 246
			transfer_state = 246
		}
		SMO = {
			add_state_core = 242
			transfer_state = 242
			add_state_core = 224
			transfer_state = 224
			add_state_core = 205
			transfer_state = 205
			add_state_core = 243
			transfer_state = 243
			add_state_core = 241
			transfer_state = 241
		}
		ORL = {
			add_state_core = 220
			transfer_state = 220
			add_state_core = 258
			transfer_state = 258
			add_state_core = 223
			transfer_state = 223
			add_state_core = 222
			transfer_state = 222
		}
		VNH = {
			add_state_core = 265
			transfer_state = 265
			add_state_core = 260
			transfer_state = 260
			add_state_core = 240
			transfer_state = 240
		}
		VLG = {
			add_state_core = 239
			transfer_state = 239
			add_state_core = 217
			transfer_state = 217
			add_state_core = 245
			transfer_state = 245
			add_state_core = 401
			transfer_state = 401
			add_state_core = 236
			transfer_state = 236
			add_state_core = 237
			transfer_state = 237
			add_state_core = 238
			transfer_state = 238
		}
		PEN = {
			add_state_core = 254
			transfer_state = 254
			add_state_core = 255
			transfer_state = 255
			add_state_core = 257
			transfer_state = 257
		}

		MOS = {
			add_state_core = 253
			transfer_state = 253
			add_state_core = 248
			transfer_state = 248
			add_state_core = 1314
			transfer_state = 1314
			add_state_core = 219
			transfer_state = 219
		}
		PET = {
			add_state_core = 208
			transfer_state = 208
			add_state_core = 195
			transfer_state = 195
			add_state_core = 264
			transfer_state = 264
			add_state_core = 244
			transfer_state = 244
			add_state_core = 247
			transfer_state = 247
			set_politics = { ruling_party = national_socialism }
			set_country_flag = PET_moskowien_collapse
			add_ideas = PET_reconquest_of_moskowien
		}
		BBB = {
			add_state_core = 401
			transfer_state = 401
			declare_war_on = { target = VLG type = annex_everything }
		}
		RSB = {
			add_state_core = 264
			transfer_state = 264
			declare_war_on = { target = PET type = annex_everything }
		}
		BSB = {
			add_state_core = 253
			transfer_state = 253
			declare_war_on = { target = MOS type = annex_everything }
			country_event = BSB.1
		}
	}
}

### INTRO EVENTS ###
## Moskowien intro

country_event = { # intro event
	id = MCW.1
	title = MCW.1.t
	desc = MCW.1.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.1.a
	}
}

country_event = { # Germany demands reintegration into RK Moskowien
	id = MCW.2
	title = MCW.2.t
	desc = MCW.2.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.2"}

	option = { # Yes
		name = MCW.2.a
		ai_chance = { factor = 2 }
		GER = {
			country_event = { id = goering.1005 days = 1 }
		}
	}
	option = { # No
		name = MCW.2.a
		ai_chance = { factor = 1 }
		GER = {
			country_event = { id = goering.1006 days = 1 }
		}
	}
}

country_event = { # partisans destroy factory
	id = MCW.3
	title = MCW.3.t
	desc = MCW.3.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.3"}

	option = {
		name = MCW.3.a
		random_owned_controlled_state = {
			limit = { arms_factory > 1 }
			damage_building = {
				type = arms_factory
				damage = 2
			}
		}
	}
}

country_event = { #weapon shipment intercepted
	id = MCW.4
	title = MCW.4.t
	desc = MCW.4.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.4"}

	option = {
		name = MCW.4.a
		add_equipment_to_stockpile = {
			type = infantry_equipment
			amount = 5000
		}
		add_equipment_to_stockpile = {
			type = anti_tank_equipment
			amount = 1000
		}
		hidden_effect = {
			GER = { country_event = { id = goering.1016 days = 3 }}
		}
	}
}

country_event = { # partisans destroy factories
	id = MCW.5
	title = MCW.5.t
	desc = MCW.5.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.5"}

	option = {
		name = MCW.5.a
		random_owned_controlled_state = {
			limit = { arms_factory > 3 }
			damage_building = {
				type = arms_factory
				damage = 4
			}
		}
	}
}

country_event = { # modernisation time
	id = MCW.7
	title = MCW.7.t
	desc = MCW.7.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.3"}

	option = {
		name = MCW.7.a
		set_temp_variable = { GER_academic_base_monthly_change = GER.academic_base_monthly_change }
		set_temp_variable = { GER_agriculture_monthly_change = GER.agriculture_monthly_change }
		set_temp_variable = { GER_industrial_equipment_monthly_change = GER.industrial_equipment_monthly_change }
		
		if = {
			limit = { check_variable = { academic_base_counter < GER.academic_base_counter }}
			multiply_temp_variable = { GER_academic_base_monthly_change = 0.25 }
			add_to_variable = { academic_base_monthly_change = GER_academic_base_monthly_change }
		}
		if = {
			limit = { check_variable = { agriculture_counter < GER.agriculture_counter }}
			multiply_temp_variable = { GER_agriculture_monthly_change = 0.25 }
			add_to_variable = { agriculture_monthly_change = GER_agriculture_monthly_change }
		}
		if = {
			limit = { check_variable = { industrial_equipment_counter < GER.industrial_equipment_counter }}
			multiply_temp_variable = { GER_industrial_equipment_monthly_change = 0.25 }
			add_to_variable = { industrial_equipment_monthly_change = GER_industrial_equipment_monthly_change }
		}
	}
}

country_event = { #Partisans ruin factories
	id = MCW.9
	title = MCW.9.t
	desc = MCW.9.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.9"}

	option = {
		name = MCW.9.a
		219 = {
			damage_building = {
				type = arms_factory
				damage = 5
			}
		}
		260 = {
			damage_building = {
				type = arms_factory
				damage = 1
			}
		}
		hidden_effect = {
			country_event = { id = MCW.10 days = 40 }
		}
	}
}

country_event = { #Partisans ruin infrastructure
	id = MCW.10
	title = MCW.10.t
	desc = MCW.10.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.10"}

	option = {
		name = MCW.10.a
		208 = {
			damage_building = {
				type = infrastructure
				damage = 2
			}
		}
		209 = {
			damage_building = {
				type = infrastructure
				damage = 2
			}
		}
		210 = {
			damage_building = {
				type = infrastructure
				damage = 2
			}
		}
		hidden_effect = {
			country_event = { id = MCW.11 days = 40 }
		}
	}
}

country_event = { # Moskowien at peace
	id = MCW.11
	title = MCW.11.t
	desc = MCW.11.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.11"}

	option = {
		name = MCW.11.a
		remove_ideas = MCW_russian_partisans
	}
}

country_event = { # Ukraine wants us to take in Cossacks
	id = MCW.13
	title = MCW.13.a
	desc = MCW.13.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.13"}

	option = { # Why would we accept?
		name = MCW.13.a
		hidden_effect = {
			country_event = { id = UKR.21 days = 3 }
		}
	}
}

country_event = { # Ukraine wants us to take in Cossacks
	id = MCW.14
	title = MCW.14.a
	desc = MCW.14.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event MCW.14"}

	option = { # Why would we accept?
		name = MCW.14.a
		ai_chance = { factor = 0 }
		hidden_effect = {
			country_event = { id = UKR.21 days = 3 }
		}
	}
	option = { # when you put it like that
		name = MCW.14.b
		ai_chance = { factor = 1 }
		add_to_variable = { money_reserves = 500 }
		hidden_effect = {
			country_event = { id = UKR.22 days = 3 }
		}
	}
}

country_event = { # intro to the council function
	id = MCW.101
	title = MCW.101.t
	desc = MCW.101.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.101.a
		
	}
}

country_event = { # intro to warlord burdens
	id = MCW.102
	title = MCW.102.t
	desc = MCW.102.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.102.a
		
	}
}

country_event = { # industry end
	id = MCW.103
	title = MCW.103.t
	desc = MCW.103.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.103.a
		
	}
}

country_event = { # defense of moskowien
	id = MCW.104
	title = MCW.104.t
	desc = MCW.104.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.104.a
		
	}
}

country_event = { # germany is falling apart, council needs to do something
	id = MCW.105
	title = MCW.105.t
	desc = MCW.105.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.105.a
		hidden_effect = {
			country_event = {
				id = MCW.113
				days = 5
			}
			country_event = {
				id = MCW.114
				days = 14
			}
		}
	}
}

country_event = { # 2SS happens
	id = MCW.106
	title = MCW.106.t
	desc = MCW.106.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.106.a
		#news_event = {
    		#id = WORLD.355
    		#days = 3
		#}
		
		hidden_effect = {
			MCW = {
				add_named_threat = { 
					threat = -5
					name = threat.24.t
				}			
				TNO_World_Tension_subtract_5 = yes	
			}
		}

		VLG = {
			transfer_state = 217
			transfer_state = 224
			transfer_state = 236
			transfer_state = 237
			transfer_state = 238
			transfer_state = 239
			transfer_state = 240
			transfer_state = 248
			transfer_state = 401
			transfer_state = 1314

			hidden_effect = {
				country_event = { id = VLG.2 }
				add_state_core = 217
				add_state_core = 224
				add_state_core = 236
				add_state_core = 237
				add_state_core = 238
				add_state_core = 239
				add_state_core = 240
				add_state_core = 248
				add_state_core = 401
				add_state_core = 1314
			}
		}

		MOS = {
			transfer_state = 205
			transfer_state = 208
			transfer_state = 220
			transfer_state = 222
			transfer_state = 223
			transfer_state = 241
			transfer_state = 242
			transfer_state = 243
			transfer_state = 244
			transfer_state = 245
			transfer_state = 246
			transfer_state = 247
			transfer_state = 253
			transfer_state = 254
			transfer_state = 255
			transfer_state = 257
			transfer_state = 258
			transfer_state = 263
			transfer_state = 264
			transfer_state = 265

			hidden_effect = {
				country_event = MOS.1
				add_state_core = 205
				add_state_core = 208
				add_state_core = 220
				add_state_core = 222
				add_state_core = 223
				add_state_core = 241
				add_state_core = 242
				add_state_core = 243
				add_state_core = 244
				add_state_core = 245
				add_state_core = 246
				add_state_core = 247
				add_state_core = 253
				add_state_core = 254
				add_state_core = 255
				add_state_core = 257
				add_state_core = 258
				add_state_core = 263
				add_state_core = 264
				add_state_core = 265
			}
		}

		# if = { #3ss
		# 	limit = {
		# 		check_variable = {
    	# 			var = MCW_russ_pref
    	# 			value = MCW_german_pref
    	# 			compare = equals
		# 		}
		# 	}
		# 	PET = {
		# 		transfer_state = 195
		# 		transfer_state = 209
		# 		transfer_state = 210
		# 		transfer_state = 219
		# 		transfer_state = 260

		# 		hidden_effect = {
		# 			add_state_core = 195
		# 			add_state_core = 209
		# 			add_state_core = 210
		# 			add_state_core = 219
		# 			add_state_core = 260
		# 			country_event = PET.1
		# 		}

		# 		load_oob = "PET_3SS"
		# 	}
		# 	VLG = {
		# 		country_event = VLG.3
		# 		load_oob = "VLG_3SS"
		# 	}
		# 	MOS = {
		# 		country_event = MOS.1
		# 		load_oob = "MOS_3SS"
		# 	}
		# }
		random_list = {
			50 = {
				set_global_flag = MCW_2SS_Russian_Pref
				VLG = {
					transfer_state = 195
					transfer_state = 209
					transfer_state = 210
	
					hidden_effect = {
						add_state_core = 195
						add_state_core = 209
						add_state_core = 210
					}
	
	
					#load_oob = "VLG_2SS_Russian_Pref"
					
	
					#country_event = { id = MCW.109 days = 100 }
				}
				MOS = {
					#transfer_state = 195
					transfer_state = 209
					transfer_state = 210
					transfer_state = 219
					transfer_state = 260
	
					hidden_effect = {
						add_state_core = 195
						add_state_core = 209
						add_state_core = 210
						add_state_core = 219
						add_state_core = 260
					}
	
					load_oob = "MOS_2SS_Russian_Pref" 
				}
			}
			50 = {
				set_global_flag = MCW_2SS_German_Pref
			
				VLG = {
					transfer_state = 195
					transfer_state = 209
					transfer_state = 210
					transfer_state = 219
					transfer_state = 260
	
					hidden_effect = {
						add_state_core = 195
						add_state_core = 209
						add_state_core = 210
						add_state_core = 219
						add_state_core = 260
					}
	
					#load_oob = "VLG_2SS_German_Pref"
					
	
					#country_event = { id = MCW.109 days = 100 }
				}
	
				MOS = { load_oob = "MOS_2SS_German_Pref" }
			}
		}
	}
}

country_event = { # russian take over
	id = MCW.107
	title = MCW.107.t
	desc = MCW.107.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.107.a
		create_country_leader = {
			name = "Russie McCollab"
			desc = ""
			picture = ""
			expire = "1999.1.1"
			ideology = national_socialism_subtype
			traits = {
		
			}
		}
	}
}

country_event = { # german take over
	id = MCW.108
	title = MCW.108.t
	desc = MCW.108.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.108.a
		create_country_leader = {
			name = "Germany McNazi"
			desc = ""
			picture = ""
			expire = "1999.1.1"
			ideology = national_socialism_subtype
			traits = {
		
			}
		}
	}
}

country_event = { # war in 2SS
	id = MCW.109
	title = MCW.109.t
	desc = MCW.109.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.109.a
		declare_war_on = {
    		target = MOS
    		type = annex_everything
		}
		clr_global_flag = MCW_2SS_peace
		set_global_flag = MCW_2SS_war
	}
}

country_event = { # worries about fatherland
	id = MCW.110
	title = MCW.110.t
	desc = MCW.110.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.110.a
		
	}
}

country_event = { # quick phonecall
	id = MCW.111
	title = MCW.111.t
	desc = MCW.111.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.111.a
		country_event = {
    		id = MCW.112
    		days = 5
		}
	}
}

country_event = { # kasche dips
	id = MCW.112
	title = MCW.112.t
	desc = MCW.112.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.112.a
		retire_country_leader = yes
		create_country_leader = {
			name = "No Authority"
			desc = "POLITICS_MOSKOWIEN_NO_AUTHORITY_DESC"
			picture = "Portrait_Moskowien_No_Authority.dds"
			expire = "1990.1.1"
			ideology = national_socialism_subtype
			traits = { utterly_abandoned }
		}
		remove_ideas = MCW_apathetic_rule
		swap_ideas = {
     		remove_idea = MCW_unified_ostvolk_council
     		add_idea = MCW_council_in_chaos
		}
		hidden_effect = {
			every_country = {
				news_event = { id = WORLD.579 days = 10 }
			}
			random_list = {
				20 = {
					country_event = {
						id = MCW.201
						days = 10
					}
				}
				70 = {
					country_event = {
						id = MCW.106
						days = 10
					}
				}
			}	
		}
	}
}

country_event = { # kasche has a freak out
	id = MCW.113
	title = MCW.113.t
	desc = MCW.113.desc
	picture = GFX_report_event_russia_moscow_celebration

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.113.a
		
	}
}

country_event = { # two state pops up
	id = MCW.114
	title = MCW.114.t
	desc = MCW.114.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.114.a
		
	}
}

country_event = { # Moskowien Collapse #1
	id = MCW.201
	title = MCW.201.t
	desc = MCW.201.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.201.a
		set_global_flag = MCW_collapse
		PSK = {
			add_state_core = 263
			transfer_state = 263
			add_state_core = 209
			transfer_state = 209
			add_state_core = 210
			transfer_state = 210
			add_state_core = 246
			transfer_state = 246
			inherit_technology = MCW
			load_oob = "PSK_Collapse"
		}
		
		country_event = {
    		id = MCW.202
    		days = 2
		}
	}
}

country_event = { # Moskowien Collapse #2
	id = MCW.202
	title = MCW.202.t
	desc = MCW.202.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.202.a
		SMO = {
			add_state_core = 242
			transfer_state = 242
			add_state_core = 224
			transfer_state = 224
			add_state_core = 205
			transfer_state = 205
			add_state_core = 243
			transfer_state = 243
			add_state_core = 241
			transfer_state = 241
			inherit_technology = MCW
			load_oob = "SMO_Collapse"
		}
		country_event = {
    		id = MCW.203
    		days = 2
		}
	}
}

country_event = { # Moskowien Collapse #3
	id = MCW.203
	title = MCW.203.t
	desc = MCW.203.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.203.a
		ORL = {
			add_state_core = 220
			transfer_state = 220
			add_state_core = 258
			transfer_state = 258
			add_state_core = 223
			transfer_state = 223
			add_state_core = 222
			transfer_state = 222
			inherit_technology = MCW
			load_oob = "ORL_Collapse"
		}
		
		country_event = {
    		id = MCW.204
    		days = 2
		}
	}
}

country_event = { # Moskowien Collapse #4
	id = MCW.204
	title = MCW.204.t
	desc = MCW.204.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.204.a
		VNH = {
			add_state_core = 265
			transfer_state = 265
			add_state_core = 260
			transfer_state = 260
			add_state_core = 240
			transfer_state = 240
			inherit_technology = MCW
			load_oob = "VNH_Collapse"
		}
		
		country_event = {
    		id = MCW.205
    		days = 2
		}
	}
}

country_event = { # Moskowien Collapse #5
	id = MCW.205
	title = MCW.205.t
	desc = MCW.205.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.205.a
		VLG = {
			add_state_core = 239
			transfer_state = 239
			add_state_core = 217
			transfer_state = 217
			add_state_core = 245
			transfer_state = 245
			add_state_core = 401
			transfer_state = 401
			add_state_core = 236
			transfer_state = 236
			add_state_core = 237
			transfer_state = 237
			add_state_core = 238
			transfer_state = 238
			inherit_technology = MCW
			load_oob = "VLG_Collapse"
		}
		
		country_event = {
    		id = MCW.206
    		days = 2
		}
	}
}

country_event = { # Moskowien Collapse #6
	id = MCW.206
	title = MCW.206.t
	desc = MCW.206.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.206.a
		PEN = {
			add_state_core = 254
			transfer_state = 254
			add_state_core = 255
			transfer_state = 255
			add_state_core = 257
			transfer_state = 257
			inherit_technology = MCW
			load_oob = "PEN_Collapse"
		}
		
		country_event = {
    		id = MCW.207
    		days = 2
		}
	}
}

country_event = { # Moskowien Collapse #7
	id = MCW.207
	title = MCW.207.t
	desc = MCW.207.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.207.a
		MOS = {
			add_state_core = 253
			transfer_state = 253
			add_state_core = 248
			transfer_state = 248
			add_state_core = 1314
			transfer_state = 1314
			add_state_core = 219
			transfer_state = 219
			inherit_technology = MCW
			load_oob = "MOS_Collapse"
		}
		
		country_event = {
    		id = MCW.208
    		days = 2
		}
	}
}

country_event = { # Moskowien Collapse #8
	id = MCW.208
	title = MCW.208.t
	desc = MCW.208.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.208.a
		PET = {
			set_country_flag = PET_moskowien_collapse

			add_state_core = 208
			transfer_state = 208
			add_state_core = 195
			transfer_state = 195
			add_state_core = 264
			transfer_state = 264
			add_state_core = 244
			transfer_state = 244
			add_state_core = 247
			transfer_state = 247
			inherit_technology = MCW
			load_oob = "PET_Collapse"
			
			set_politics = {
				ruling_party = national_socialism
			}
			
			country_event = {
    		id = MCW.209
    		days = 2
			}
		}
	}
}

country_event = { # Moskowien Collapse #9
	id = MCW.209
	title = MCW.209.t
	desc = MCW.209.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.209.a
		BBB = {
			add_state_core = 401
			transfer_state = 401
			inherit_technology = MCW
			load_oob = "BBB_Collapse"
			
			declare_war_on = {
    			target = VLG
    			type = annex_everything
			}
			
			country_event = {
    		id = MCW.210
    		days = 2
			}
		}
	}
}

country_event = { # Moskowien Collapse #10
	id = MCW.210
	title = MCW.210.t
	desc = MCW.210.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.210.a
		RSB = {
			add_state_core = 264
			transfer_state = 264
			inherit_technology = MCW
			load_oob = "RSB_Collapse"
			
			declare_war_on = {
    			target = PET
    			type = annex_everything
			}
			
			country_event = {
    		id = MCW.211
    		days = 2
			}
		}
	}
}

country_event = { # Moskowien Collapse #11
	id = MCW.211
	title = MCW.211.t
	desc = MCW.211.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.211.a
		BSB = {
			add_state_core = 253
			transfer_state = 253
			inherit_technology = MCW
			load_oob = "BSB_Collapse"
			
			declare_war_on = {
    			target = MOS
    			type = annex_everything
			}
			country_event = BSB.1
		}
	}
}

country_event = { # 1st tree
	id = MCW.100
	title = MCW.100.t
	desc = MCW.100.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.100.a
		#load_focus_tree = MCW_russias_hell
	}
}

country_event = { # 2nd tree
	id = MCW.200
	title = MCW.200.t
	desc = MCW.200.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.200.a
		#load_focus_tree = MCW_its_legit_2_focuses_what_the_fuck
	}
}

country_event = { # scary state solution
	id = MCW.300
	title = MCW.300.t
	desc = MCW.300.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		name = MCW.300.a
		set_global_flag = MCW_3SS
		VLG = {
			transfer_state = 195
			transfer_state = 209
			transfer_state = 210
			transfer_state = 217
			transfer_state = 224
			transfer_state = 236
			transfer_state = 237
			transfer_state = 238
			transfer_state = 239
			transfer_state = 240
			transfer_state = 248
			transfer_state = 401
			transfer_state = 1314

			hidden_effect = {
				add_state_core = 195
				add_state_core = 209
				add_state_core = 210
				add_state_core = 217
				add_state_core = 224
				add_state_core = 236
				add_state_core = 237
				add_state_core = 238
				add_state_core = 239
				add_state_core = 240
				add_state_core = 248
				add_state_core = 401
				add_state_core = 1314
			}
		}
		MOS = {
			hidden_effect = {
				add_state_core = 219
				transfer_state = 219
			}
			change_tag_from = MCW
			annex_country = {
				target = MCW
				transfer_troops = no
			}
		}
	}
}

country_event = { # scary state solution, now with joint
	id = MCW.400
	title = MCW.400.t
	desc = MCW.400.desc
	picture = GFX_report_event_MAD_palace

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event GFX_report_event_MAD_palace"}

	option = {
		set_global_flag = MCW_3SS
		name = MCW.400.a
		
		VLG = {
		add_state_core = 239
		transfer_state = 239
		add_state_core = 401
		transfer_state = 401
		add_state_core = 217
		transfer_state = 217
		add_state_core = 236
		transfer_state = 236
		add_state_core = 237
		transfer_state = 237
		add_state_core = 238
		transfer_state = 238
		add_state_core = 195
		transfer_state = 195
		add_state_core = 248
		transfer_state = 248
		add_state_core = 209
		transfer_state = 209
		add_state_core = 210
		transfer_state = 210
		add_state_core = 240
		transfer_state = 240
		add_state_core = 1314
		transfer_state = 1314
		add_state_core = 224
		transfer_state = 224
		}
		MOS = {
			add_state_core = 219
			transfer_state = 219
		}
		MOS = { change_tag_from = MCW }
		MOS = {
			country_event = MOS.1
			annex_country = {
				target = MCW
				transfer_troops = no
			}
		}
		PET = {
			add_state_core = 209
			transfer_state = 209
			add_state_core = 210
			transfer_state = 210
			add_state_core = 260
			transfer_state = 260
			add_state_core = 219
			transfer_state = 219
			add_state_core = 195
			transfer_state = 195
			set_politics = { ruling_party = despotism }
		}
	}
}