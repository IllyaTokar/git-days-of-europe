﻿add_namespace = MAN

country_event = { #Intro
	id = MAN.0
	immediate = {log = "[GetDateText]: [Root.GetName]: event MAN.0"}
 	title = MAN.0.t
 	desc = MAN.0.d
 	picture = GFX_report_event_generic_military_parade

	is_triggered_only = yes
 	fire_only_once = yes

 	option = {
 		name = MAN.0.a
        hidden_effect = {
            every_country = { news_event = { id = WORLD.16508 days = 2114 } }
        }
    }
 }
