﻿add_namespace = ledger

# Test Event 1
country_event = {
	id = ledger.1
	
	hidden = yes

	is_triggered_only = yes
	
	option = {
		get_same_ideology = yes
	}
}

add_namespace = generic_debug

country_event = {
	id = generic_debug.42
	title = generic_debug.42.t
	desc = generic_debug.42.d
	
	picture = GFX_report_event_generic_factory

	is_triggered_only = yes

	immediate = {
		create_intelligence_agency = yes
	}

	option = {
		name = excellent
	}
}