﻿add_namespace = serbia

country_event = { #anti palme
	id = serbia.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.1"}
	title = serbia.1.t
	desc = serbia.1.d
	picture = GFX_report_event_austin_cambridge

	is_triggered_only = yes

	option = {
		name = serbia.1.a
		add_popularity = { ideology = authoritarian_democrat popularity = 0.50 }
		add_popularity = { ideology = social_conservative popularity = 0.10 }
		add_popularity = { ideology = socialist popularity = 0.20 }
		add_popularity = { ideology = communist popularity = 0.10 }
		set_politics = { ruling_party = authoritarian_democrat }
		create_country_leader = {
			name = "Josip Broz Tito"
			#desc = "POLITICS_LYNDON_B_JOHNSON_DESC"
			picture = "Joseph_Tito.tga"
			expire = "1994.4.20"
			ideology = authoritarian_democrat_subtype
			traits = {
			}
		}
		load_oob = "SER_1962"
		load_oob = "SER_1962"
		transfer_state = 105
		transfer_state = 745
		transfer_state = 104
		transfer_state = 969
		overlord = { end_puppet = ROOT }
		leave_faction = yes
		ITA = {
			annex_country = { target = CRO transfer_troops = yes }
		}
		HUN = {
			transfer_state = 76
		}
	}

	option = {
		set_party_name = {
			ideology = authoritarian_democrat
			name = SER_authoritarian_democrat_party
			long_name = SER_authoritarian_democrat_party_long
		}
	}
}

#country_event = { #Goering sends us guns
	#	id = serbia.2
	#	title = serbia.2.t
	#	desc = serbia.2.d
	#	picture = GFX_report_event_austin_cambridge
	#
	#	fire_only_once = yes
	#	is_triggered_only = yes
	#
	#	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.2"}
	#
	#	option = {
		#		name = serbia.2.a
		#		add_equipment_to_stockpile = {
			#			type = infantry_equipment
			#			amount = 5500
			#			producer = GER
			#		}
		#		add_equipment_to_stockpile = {
			#			type = support_equipment_1
			#			amount = 500
			#		}
		#	}
	#}
#
#country_event = { # Attack Romania with Germany?
	#	id = serbia.3
	#	title = serbia.3.t
	#	desc = serbia.3.d
	#	picture = GFX_report_event_austin_cambridge
	#
	#	fire_only_once = yes
	#	is_triggered_only = yes
	#
	#	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.3"}
	#
	#	option = { #yes
		#		name = serbia.3.a
		#		ai_chance = { factor = 30 }
		#		hidden_effect = {
			#			GER = {
				#				country_event = { id = goering.1039 days = 3 }
				#			}
			#		}
		#	}
	#	option = { #no
		#		name = serbia.3.b
		#		ai_chance = { factor = 70 }
		#		hidden_effect = {
			#			GER = {
				#				country_event = { id = goering.1040 days = 3 }
				#			}
			#		}
		#	}
	#}

country_event = { # New tricks
	id = serbia.4
	title = serbia.4.t
	desc = serbia.4.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.4"}

	option = {
		name = serbia.4.a
	}
}

#country_event = { #Goering calls us to arms!
	#	id = serbia.5
	#	title = serbia.5.t
	#	desc = serbia.5.d
	#	picture = GFX_report_event_austin_cambridge
	#
	#	fire_only_once = yes
	#	is_triggered_only = yes
	#
	#	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.5"}
	#
	#	option = { # Too late to pull back
		#		name = serbia.5.a
		#		ai_chance = { factor = 30 }
		#		GER = {
			#			set_country_flag = SER_ally
			#		}
		#		declare_war_on = {
			#			target = ROM
			#			type = annex_everything
			#		}
		#	}
	#	option = { # On second thought
		#		name = serbia.5
		#		ai_chance = { factor = 70 }
		#		hidden_effect = {
			#			GER = {
				#				country_event = { id = goering.1049 days = 3 }
				#			}
			#		}
		#	}
	#}

country_event = { # Serbia Intro
	id = serbia.6
	title = serbia.6.t
	desc = serbia.6.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.6"}

	option = {
		name = serbia.6.a
		add_political_power = 5
	}
}

country_event = { # Propoganda efforts expanded by the government
	id = serbia.7
	title = serbia.7.t
	desc = serbia.7.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.7"}

	option = {
		name = serbia.7.a
		add_political_power = 50
		add_stability = 0.05
	}
}

country_event = { # Massive reprisals against rebellious serbs, its sorta working ish
	id = serbia.8
	title = serbia.8.t
	desc = serbia.8.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.8"}

	option = {
		name = serbia.8.a
		add_political_power = 50
		add_stability = 0.05
		108 = { add_manpower = -632  }
		1275 = { add_manpower = -2254 }
		107 = { add_manpower = -1694 }
	}
}

country_event = { # Germany increases the occupation costs, people protest
	id = serbia.9
	title = serbia.9.t
	desc = serbia.9.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.9"}

	option = {
		name = serbia.9.a
		add_stability = -0.05
		add_war_support = -0.5
		hidden_effect = {
			country_event = {
				id = serbia.10
				days = 10
			}
		}
	}
}

country_event = { # German Garrison kills the protestors, it escalates and turns into a clusterfuck
	id = serbia.10
	title = serbia.10.t
	desc = serbia.10.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.10"}

	option = {
		name = serbia.10.a
		add_political_power = -50
		add_stability = -0.07
		add_war_support = -0.7
		107 = {
			add_manpower = -6250
			damage_building = {
				type = industrial_complex
				damage = 1
			}
			damage_building = {
				type = infrastructure
				damage = 1
			}
		}
		hidden_effect = {
			country_event = {
				id = serbia.11
				days = 30
			}
		}
	}
}

country_event = { # Retaliatory attacks from the partisans
	id = serbia.11
	title = serbia.11.t
	desc = serbia.11.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.11"}

	option = {
		name = serbia.11.a
		add_manpower = -1500
		random_owned_controlled_state = {
			damage_building = {
				type = infrastructure
				damage = 2
			}
		}
		random_owned_controlled_state = {
			damage_building = {
				type = arms_factory
				damage = 1
			}
		}
		random_owned_controlled_state = {
			damage_building = {
				type = industrial_complex
				damage = 1
			}
		}
	}
}

country_event = { # Things are calming down a bit in Serbia
	id = serbia.12
	title = serbia.12.t
	desc = serbia.12.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.12"}

	option = {
		name = serbia.12.a
		add_stability = 0.10
		add_war_support = 0.10
	}
}

country_event = { # Intelligence Report on the Chetniks
	id = serbia.13
	title = serbia.13.t
	desc = serbia.13.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.13"}

	option = {
		name = serbia.13.a
		add_political_power = 5
	}
}

country_event = { # Intelligence report on Tito and pals
	id = serbia.14
	title = serbia.14.t
	desc = serbia.14.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.14"}

	option = {
		name = serbia.14.a
		add_political_power = 5
	}
}

country_event = { # We do an assassination attempt on Tito and fail
	id = serbia.15
	title = serbia.15.t
	desc = serbia.15.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.15"}

	option = {
		name = serbia.15.a
		add_stability = -0.02
	}
}

country_event = { # Tensions with the Chetniks are rising since we just ended our collaberation agreements with them
	id = serbia.16
	title = serbia.16.t
	desc = serbia.16.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.16"}

	option = {
		name = serbia.16.a
		add_political_power = 5
	}
}

country_event = { # We stab the chetniks working for us in the back, the rest of them flee to hiding places
	id = serbia.17
	title = serbia.17.t
	desc = serbia.17.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.17"}

	option = {
		name = serbia.17.a
		add_political_power = 20
		add_manpower = -5000
	}
}

country_event = { # Leaders of Chetnik movements are tracked and marked for assassination
	id = serbia.18
	title = serbia.18.t
	desc = serbia.18.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.18"}

	option = {
		name = serbia.18.a
	}
}

country_event = { # New informants are secured within the ranks of the communist partisans, info on Titos location
	id = serbia.19
	title = serbia.19.t
	desc = serbia.19.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.19"}

	option = {
		name = serbia.19.a
		load_oob = GMS_Garrison
	}
}

country_event = { # German civil war just started, calm before the storm
	id = serbia.20
	title = serbia.20.t
	desc = serbia.20.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = { log = "[GetDateText]: [Root.GetName]: event serbia.20" }

	option = {
		name = serbia.20.a

		remove_ideas = SRB_German_Controlled_Economy
		hidden_effect = { country_event = { id = serbia.21 days = 5 } }
	}
}

country_event = { # German Garrison becomes more parinoid
	id = serbia.21
	title = serbia.21.t
	desc = serbia.21.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = { log = "[GetDateText]: [Root.GetName]: event serbia.21" }

	option = {
		name = serbia.21.a

		hidden_effect = { country_event = { id = serbia.22 days = 5 } }
	}
}

country_event = { # Garrison attempts to coup collab government, collabs unite with partisans, Tito temporaraly in charge and war vs Garrison starts
	id = serbia.22
	title = serbia.22.t
	desc = serbia.22.d
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.22"}

	option = {
		name = serbia.22.a
		set_global_flag = SER_TestGarrison
		#load_focus_tree = SER_Freedom
		remove_ideas = {
			SER_Refugee_Crisis
			SER_The_Occupation_Regime
			SER_Widespread_Insurrection
		}
		hidden_effect = {
			set_popularities = {
				communist = 17
				socialist = 2
				social_democrat = 7
				social_liberal = 18
				social_conservative = 9
				authoritarian_democrat = 32
				despotism = 15
				fascism = 0
				national_socialism = 0
				ultranational_socialism = 0
				burgundian_system = 0
			}
		}
		set_politics = {
			ruling_party = authoritarian_democrat
			elections_allowed = no
		}
		remove_all_ministers = yes
		add_ideas = {
			SER_Aleksandar_Rankovic_hog
			SER_Nikola_Kavaja_sec
			SER_Borislav_Jovic_eco
			SER_Marko_Nikezic_for

			SER_National_Liberation_Army
			SER_SOPO_Terrorism
			SER_Generational_Divide
			SER_Refugee_Question
		}
		load_oob = SER_Partisans
		set_division_template_lock = {
			division_template = "Communist Partisans"
			is_locked = yes
		}
		set_division_template_lock = {
			division_template = "Chetnik Partisans"
			is_locked = yes
		}
		GMS = {
			transfer_state = 1272
			transfer_state = 1274
			transfer_state = 107
			add_state_core = 1272
			add_state_core = 1274
			add_state_core = 107
			load_oob = GMS_Garrison
			declare_war_on = {
				target = SER
				type = annex_everything
			}
		}
		every_country = {
			news_event = { id = WORLD.566 days = 1 }
		}
	}
}

country_event = { # F for Aleksandar
	id = serbia.23
	title = serbia.23.t
	desc = serbia.23.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.23"}

	option = {
		name = serbia.23.a
		add_stability = 0.1
		add_popularity = {
			ideology = communist
			popularity = -0.1
		}
		hidden_effect = {
			#country_event = { id = serbia.30 days = 7 }
			# haha no
		}
	}
}

country_event = { # Lay the trap
	id = serbia.24
	title = serbia.24.t
	desc = serbia.24.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.4"}

	option = {
		name = serbia.24.a
		add_war_support = 0.08
	}
}

country_event = { # Hang the bastard
	id = serbia.25
	title = serbia.25.t
	desc = serbia.25.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.5"}

	option = { # The communists coup
		name = serbia.25.a
		add_stability = 0.1
		add_popularity = {
			ideology = authoritarian_democrat
			popularity = -0.1
		}
		add_popularity = {
			ideology = communist
			popularity = 0.06
		}
		set_politics = {
			ruling_party = communist
			elections_allowed = no
		}
		hidden_effect = {
			#country_event = { id = serbia.30 days = 7 }
			# haha no
		}
	}
	option = { # The chetniks coup
		name = serbia.25.b
		add_stability = 0.1
		add_popularity = {
			ideology = authoritarian_democrat
			popularity = -0.1
		}
		add_popularity = {
			ideology = despotism
			popularity = 0.06
		}
		set_politics = {
			ruling_party = despotism
			elections_allowed = no
		}
		hidden_effect = {
			#country_event = { id = serbia.29 days = 7 }
			# haha no
		}
	}
}

country_event = { # The partisans split with the collabs, collabs go authdem
	id = serbia.26
	title = serbia.26.t
	desc = serbia.26.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.26"}

	option = {
		name = serbia.26.a
		add_popularity = {
			ideology = fascism
			popularity = -0.13
		}
		add_popularity = {
			ideology = authoritarian_democrat
			popularity = 0.13
		}
		set_politics = {
			ruling_party = authoritarian_democrat
			elections_allowed = no
		}
		remove_ideas = SRB_Partisans_allied
		delete_unit_template_and_units = { division_template = "Communist Partisans"}
		delete_unit_template_and_units = { division_template = "Chetnik Partisans"}
	}
}

country_event = { # Scour the mountains
	id = serbia.27
	title = serbia.27.t
	desc = serbia.27.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.27"}

	option = {
		name = serbia.27.a
		add_tech_bonus = {
			bonus = 1
			uses = 1
			category = mountaineers_tech
		}
	}
}

country_event = { # F for Draza
	id = serbia.28
	title = serbia.28.t
	desc = serbia.28.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.28"}

	option = {
		name = serbia.28.a
		add_stability = 0.1
		add_popularity = {
			ideology = despotism
			popularity = -0.1
		}
		hidden_effect = {
			#country_event = { id = serbia.29 days = 7 }
			# haha no
		}
	}
}

country_event = { # The communists consolidate
	id = serbia.29
	title = serbia.29.t
	desc = serbia.29.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.29"}

	option = {
		name = serbia.29.a
		add_stability = -0.05
		add_popularity = {
			ideology = communist
			popularity = 0.04
		}
		1275 = {
			set_demilitarized_zone = yes
		}
		108 = {
			set_demilitarized_zone = yes
		}
		hidden_effect = {
			country_event = { id = serbia.31 days = 29 }
		}
		hidden_effect = {
			GMS = {
				set_cosmetic_tag = GMS_SER
				set_politics = {
					ruling_party = communist
					elections_allowed = no
				}
				set_popularities = {
					communist = 35
					ultranational_socialism = 5
					socialist = 7
					social_democrat = 14
					social_liberal = 18
					market_liberal = 0
					social_conservative = 18
					authoritarian_democrat = 3
					despotism = 0
					fascism = 0
					burgundian_system = 0
				}
			}
			create_country_leader = {
				name = "Aleksandar Ranković"
				picture = "gfx/leaders/SER/Portrait_Aleksandar_Rankovic.dds"
				expire = "1965.1.1"
				ideology = communist_subtype
				traits = {
					#
				}
			}
		}
	}
}

country_event = { # The chetniks consolidate
	id = serbia.30
	title = serbia.30.t
	desc = serbia.30.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.30"}

	option = {
		name = serbia.30.a
		add_stability = -0.05
		add_popularity = {
			ideology = despotism
			popularity = 0.04
		}
		hidden_effect = {
			country_event = { id = serbia.32 days = 29 }
		}
		1275 = {
			set_demilitarized_zone = yes
		}
		108 = {
			set_demilitarized_zone = yes
		}
		hidden_effect = {
			GMS = {
				set_cosmetic_tag = GMS_SER
				set_politics = {
					ruling_party = despotism
					elections_allowed = no
				}
				set_popularities = {
					communist = 0
					ultranational_socialism = 5
					socialist = 7
					social_democrat = 14
					social_liberal = 18
					market_liberal = 0
					social_conservative = 18
					authoritarian_democrat = 3
					despotism = 35
					fascism = 0
					burgundian_system = 0
				}
				create_country_leader = {
					name = "Draža Mihailović"
					picture = "gfx/leaders/SER/Portrait_Draza_Mihailovic.dds"
					expire = "1965.1.1"
					ideology = despotism_subtype
					traits = {
						#
					}
				}
			}
		}
	}
}

country_event = { # The communists rise up
	id = serbia.31
	title = serbia.31.t
	desc = serbia.31.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.31"}

	option = {
		name = serbia.31.a
		if = {
			limit = { has_government = despotism }
			add_popularity = {
				ideology = despotism
				popularity = 0.15
			}
		}
		else = {
			add_popularity = {
				ideology = authoritarian_democrat
				popularity = 0.15
			}
		}
		add_popularity = {
			ideology = authoritarian_democrat
			popularity = 0.15
		}
		GMS = {
			set_cosmetic_tag = GMS_SER
			declare_war_on = {
				target = SER
				type = annex_everything
			}
			transfer_state = 1275
			transfer_state = 108
			load_oob = SER_revolt
			add_timed_idea = {
				idea = SER_chaos_once_again
				days = 70
			}
		}
		every_country = {
			news_event = { id = WORLD.567 days = 1 }
		}
	}
	option = { # Switch to them
		trigger = {
			is_ai = no
		}
		if = {
			limit = { has_government = despotism }
			add_popularity = {
				ideology = despotism
				popularity = 0.15
			}
		}
		else = {
			add_popularity = {
				ideology = authoritarian_democrat
				popularity = 0.15
			}
		}
		add_popularity = {
			ideology = authoritarian_democrat
			popularity = 0.15
		}
		GMS = {
			set_cosmetic_tag = GMS_SER
			declare_war_on = {
				target = SER
				type = annex_everything
			}
			transfer_state = 1275
			transfer_state = 108
			load_oob = SER_revolt
			add_timed_idea = {
				idea = SER_chaos_once_again
				days = 70
			}
			change_tag_from = SER
		}
		every_country = {
			news_event = { id = WORLD.567 days = 1 }
		}
	}
}

country_event = { # The chetniks rise up
	id = serbia.32
	title = serbia.32.t
	desc = serbia.32.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.32"}

	option = {
		name = serbia.32.a
		if = {
			limit = { has_government = communist }
			add_popularity = {
				ideology = communist
				popularity = 0.15
			}
		}
		else = {
			add_popularity = {
				ideology = authoritarian_democrat
				popularity = 0.15
			}
		}
		hidden_effect = {
			GMS = {
				declare_war_on = {
					target = SER
					type = annex_everything
				}
				transfer_state = 1275
				transfer_state = 108
				load_oob = SER_revolt
				add_timed_idea = {
					idea = SER_chaos_once_again
					days = 70
				}
			}
		}
		every_country = {
			news_event = { id = WORLD.567 days = 1 }
		}
	}
	option = {
		trigger = {
			is_ai = no
		}
		if = {
			limit = { has_government = communist }
			add_popularity = {
				ideology = communist
				popularity = 0.15
			}
		}
		else = {
			add_popularity = {
				ideology = authoritarian_democrat
				popularity = 0.15
			}
		}
		hidden_effect = {
			GMS = {
				declare_war_on = {
					target = SER
					type = annex_everything
				}
				transfer_state = 1275
				transfer_state = 108
				load_oob = SER_revolt
				add_timed_idea = {
					idea = SER_chaos_once_again
					days = 70
				}
			}
			change_tag_from = SER
		}
		every_country = {
			news_event = { id = WORLD.567 days = 1 }
		}
	}
}

country_event = { # Debug test
	id = serbia.33
	title = serbia.33.t
	desc = serbia.33.desc
	picture = GFX_report_event_austin_cambridge

	fire_only_once = yes
	is_triggered_only = yes

	immediate = {log = "[GetDateText]: [Root.GetName]: event serbia.33"}

	option = {
		name = serbia.33.a
		set_cosmetic_tag = GMS_SER
	}
}

country_event = {
	id = serbia.34
	title = serbia.34.t
	desc = serbia.34.desc

	fire_only_once = yes
	is_triggered_only = yes

	immediate = { log = "[GetDateText]: [Root.GetName]: event serbia.34" }

	option = {
		name = serbia.34.a
		remove_ideas = {
			SER_SOPO_Terrorism
			SER_Generational_Divide
		}
		set_popularities = {
			communist = 21
			socialist = 8
			social_democrat = 47
			social_liberal = 15
			social_conservative = 9
		}
		set_politics = {
			ruling_party = social_democrat
			elections_allowed = no
		}
		remove_all_ministers = yes
		add_ideas = {
			SER_Mihajlo_Markovic_hog
			SER_Sejdo_Bajramovic_sec
			SER_Dragutin_Zelenovic_eco
			SER_Borka_Vucic_for

			SER_Resurgent_Nationalism
			SER_SPS_Dominance
		}
	}
	option = {
		name = serbia.34.b
		remove_ideas = {
			SER_SOPO_Terrorism
			SER_Generational_Divide
		}
		set_popularities = {
			social_democrat = 18
			social_liberal = 10
			social_conservative = 9
			authoritarian_democrat = 17
			despotism = 46
		}
		set_politics = {
			ruling_party = despotism
			elections_allowed = no
		}
		remove_all_ministers = yes
		add_ideas = {
			SER_Stojiljko_Kajevic_hog
			SER_Andro_Loncaric_sec
			SER_Dragisa_Kasikovic_eco
			SER_Velimir_Piletic_for

			SER_The_Return_of_the_Kingdom
		}
	}
	option = {
		name = serbia.34.c
		remove_ideas = {
			SER_SOPO_Terrorism
			SER_Generational_Divide
		}
		set_popularities = {
			communist = 65
			socialist = 21
			social_democrat = 14
		}
		set_politics = {
			ruling_party = communist
			elections_allowed = no
		}
		remove_all_ministers = yes
		add_ideas = {
			SER_Dobrica_Cosic_hog
			SER_Nikola_Ljubicic_sec
			SER_Petar_Stambolic_eco
			SER_Milos_Minic_for

			SER_Reactionary_Remnants
			SER_Yugoslav_Rebirth
		}
	}
}

country_event = {
	id = serbia.35
	title = serbia.35.t
	desc = serbia.35.desc

	fire_only_once = yes
	is_triggered_only = yes

	immediate = { log = "[GetDateText]: [Root.GetName]: event serbia.35" }

	option = {
		name = serbia.35.a
		remove_ideas = {
			SER_National_Liberation_Army
			SER_SOPO_Terrorism
			SER_Generational_Divide
			SER_Refugee_Question
		}
		set_popularities = {
			social_democrat = 4
			social_liberal = 8
			social_conservative = 8
			authoritarian_democrat = 23
			despotism = 26
			fascism = 31
		}
		set_politics = {
			ruling_party = fascism
			elections_allowed = no
		}
		remove_all_ministers = yes
		add_ideas = {
			SER_Dimitrije_Ljotic_hog
			SER_Kosta_Musicki_sec
			SER_Ognjen_Kuzmanovic_eco
			SER_Velibor_Jonic_for

			SER_Refugee_Crisis
			SER_The_Occupation_Regime
			SER_Widespread_Insurrection
		}
	}
}

country_event = {
	id = serbia.36
	title = serbia.36.t
	desc = serbia.36.desc

	fire_only_once = yes
	is_triggered_only = yes

	immediate = { log = "[GetDateText]: [Root.GetName]: event serbia.36" }

	option = {
		name = serbia.36.a
		set_cosmetic_tag = SER_PEL
		remove_ideas = {
			SER_National_Liberation_Army
			SER_SOPO_Terrorism
			SER_Generational_Divide
			SER_Refugee_Question
		}
		set_popularities = {
			national_socialism = 65
			fascism = 20
			burgundian_system = 15
		}
		set_politics = {
			ruling_party = national_socialism
			elections_allowed = no
		}
		create_country_leader = {
			name = "Sepp Janko"
			desc = "POLITICS_SEPP_JANKO_DESC"
			picture = "gfx/leaders/SER/Portrait_GMS_Sepp_Janko.dds"
			expire = "1985.1.1"
			ideology = national_socialism_subtype
			traits = {
			}
		}
		set_party_name = {
			ideology = national_socialism
			name = GMS_NSDAP_national_socialism_party
			long_name = GMS_NSDAP_national_socialism_party_long
		}
		remove_all_ministers = yes
		add_ideas = {
			GMS_Sepp_Janko_hog
			GMS_Emanuel_Schafer_sec
			GMS_Franz_Neuhausen_eco
			GMS_Hermann_Neubacher_for
		}
	}
}
