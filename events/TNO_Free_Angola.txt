add_namespace = ANG_Free
add_namespace = ANG_Free_Debug

country_event = {
	id = ANG_Free_Debug.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event ANG_Free_Debug.1"}
	title = ANG_Free_Debug.1.t
	desc = ANG_Free_Debug.desc
	picture = GFX_none_defined

	is_triggered_only = yes
	hidden = no
	fire_only_once = yes

	option = {
		name = ANG_Free_Debug.1.a
		#ANG = {
		#	load_focus_tree = {
		#		tree = TNO_ANG_portugal_war
		#		keep_completed = yes
		#	}
		#}
	}
}

country_event = {
	id = ANG_Free.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event ANG_Free.1"}
	title = ANG_Free.1.t
	desc = ANG_Free.1.desc
	picture = GFX_none_defined

	is_triggered_only = yes
	hidden = no
	fire_only_once = yes

	option = {
		name = ANG_Free.1.a
	}
}
