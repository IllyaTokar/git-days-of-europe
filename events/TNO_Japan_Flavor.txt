﻿add_namespace = jap_newflavour

#PM Intro Events

country_event = { #Overall YSK/Japan Introduction
	id = jap_newflavour.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.1"}
	title = jap_newflavour.1.t
	desc = jap_newflavour.1.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.1.a
		country_event = { id = jap_newflavour.2 days = 2 }
	}
}

country_event = { #Ikeda Introduction
	id = jap_newflavour.2
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.2"}
	title = jap_newflavour.2.t
	desc = jap_newflavour.2.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.2.a
		country_event = { id = jap_newflavour.3 days = 4 }
	}
}

country_event = { #Takagi Introduction
	id = jap_newflavour.3
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.3"}
	title = jap_newflavour.3.t
	desc = jap_newflavour.3.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.3.a
		country_event = { id = jap_newflavour.4 days = 4 }
	}
}

country_event = { #Kaya Introduction
	id = jap_newflavour.4
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.4"}
	title = jap_newflavour.4.t
	desc = jap_newflavour.4.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.4.a
		country_event = { id = jap_newflavour.5 days = 2 }
	}
}

country_event = { #Kido Introduction
	id = jap_newflavour.5
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.5"}
	title = jap_newflavour.5.t
	desc = jap_newflavour.5.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.5.a
		country_event = { id = jap_newflavour.6 days = 2 }
	}
}

country_event = { #Konoe Introduction
	id = jap_newflavour.6
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.6"}
	title = jap_newflavour.6.t
	desc = jap_newflavour.6.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.6.a
		country_event = { id = jap_newflavour.7 days = 2 }
	}
}

country_event = { #Tanaka Introduction
	id = jap_newflavour.7
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.7"}
	title = jap_newflavour.7.t
	desc = jap_newflavour.7.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.7.a
		country_event = { id = jap_newflavour.9 days = 2 }
	}
}

country_event = { #Kishi Introduction
	id = jap_newflavour.9
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.9"}
	title = jap_newflavour.9.t
	desc = jap_newflavour.9.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.9.a
	}
}

#Ino-era flavour

country_event = { #Colonel Event 1
	id = jap_newflavour.10
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.10"}
	title = jap_newflavour.10.t
	desc = jap_newflavour.10.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.10.a
		country_event = { id = jap_newflavour.11 days = 2 }
	}
}

country_event = { #Colonel Event 2
	id = jap_newflavour.11
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.11"}
	title = jap_newflavour.11.t
	desc = jap_newflavour.11.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.11.a
		add_political_power = -5
	}
}

country_event = { #Yasuda Bank Loan
	id = jap_newflavour.12
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.12"}
	title = jap_newflavour.12.t
	desc = jap_newflavour.12.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.12.a
		JAP_HoP_support_increase_small = yes
	}
}

country_event = { #Liberal Politician
	id = jap_newflavour.13
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.13"}
	title = jap_newflavour.13.t
	desc = jap_newflavour.13.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.13.a
		JAP_Takagi_increase_power_small = yes
		JAP_PA_support_decrease_small = yes
	}
}

country_event = { #Decadent Luxury
	id = jap_newflavour.14
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.14"}
	title = jap_newflavour.14.t
	desc = jap_newflavour.14.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.14.a
	}
}

country_event = { #Minamata 1
	id = jap_newflavour.15
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.15"}
	title = jap_newflavour.15.t
	desc = jap_newflavour.15.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.15.a
		country_event = { id = jap_newflavour.16 days = 2 }
	}
}

country_event = { #Minamata 2
	id = jap_newflavour.16
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.16"}
	title = jap_newflavour.16.t
	desc = jap_newflavour.16.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.16.a
		country_event = { id = jap_newflavour.17 days = 2 }
	}
}

country_event = { #Minamata 3
	id = jap_newflavour.17
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.17"}
	title = jap_newflavour.17.t
	desc = jap_newflavour.17.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.17.a
		country_event = { id = jap_newflavour.18 days = 2 }
	}
}

country_event = { #Minamata 4
	id = jap_newflavour.18
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.18"}
	title = jap_newflavour.18.t
	desc = jap_newflavour.18.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.18.a
		country_event = { id = jap_newflavour.19 days = 2 }
	}
	option = {
		name = jap_newflavour.18.b
		country_event = { id = jap_newflavour.20 days = 2 }
	}
}

country_event = { #Minamata 5
	id = jap_newflavour.19
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.19"}
	title = jap_newflavour.19.t
	desc = jap_newflavour.19.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.19.a
	}
}

country_event = { #Minamata 6
	id = jap_newflavour.20
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.20"}
	title = jap_newflavour.20.t
	desc = jap_newflavour.20.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.20.a
		country_event = { id = jap_newflavour.21 days = 2 }
	}
}

country_event = { #Minamata 7
	id = jap_newflavour.21
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.21"}
	title = jap_newflavour.21.t
	desc = jap_newflavour.21.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.21.a
	}
}

country_event = { #Student demonstrators
	id = jap_newflavour.22
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.22"}
	title = jap_newflavour.22.t
	desc = jap_newflavour.22.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.22.a
		JAP_PA_support_decrease_small = yes
		add_political_power = -20
		add_stability = -0.1
	}
}

country_event = { #Jay Gatsby
	id = jap_newflavour.23
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.23"}
	title = jap_newflavour.23.t
	desc = jap_newflavour.23.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.23.a
	}
}

country_event = { #Manor
	id = jap_newflavour.24
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.24"}
	title = jap_newflavour.24.t
	desc = jap_newflavour.24.d

	fire_only_once = yes
	is_triggered_only = yes


	option = {
		name = jap_newflavour.24.a
	}
}

country_event = { #Pacific Ocean
	id = jap_newflavour.25
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.25"}
	title = jap_newflavour.25.t
	desc = jap_newflavour.25.d

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = jap_newflavour.25.a
	}
}

##### Japanese MCW Diplo Crisis Events ####

country_event = { ###this event needs a proper trigger
	id = jap_newflavour.26
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.26"}
	title = jap_newflavour.26.t
	desc = jap_newflavour.26.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.26.a
		country_event = { id = jap_newflavour.27 days = 1 }
	}
}

country_event = { 
	id = jap_newflavour.27
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.27"}
	title = jap_newflavour.27.t
	desc = jap_newflavour.27.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.27.a
		GER = {
		country_event = { id = jap_newflavour.28 days = 1 }
		}
	}
}

country_event = { #german event
	id = jap_newflavour.28
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.28"}
	title = jap_newflavour.28.t
	desc = jap_newflavour.28.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.28.a
	}
}

country_event = { 
	id = jap_newflavour.29
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.29"}
	title = jap_newflavour.29.t
	desc = jap_newflavour.29.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.29.a
		country_event = { id = jap_newflavour.32 days = 1 }
	}
	option = {
		name = jap_newflavour.30.b
		country_event = { id = jap_newflavour.30 days = 1 }
	}
}

country_event = { 
	id = jap_newflavour.30
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.30"}
	title = jap_newflavour.30.t
	desc = jap_newflavour.30.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.30.a
		country_event = { id = jap_newflavour.31 days = 1 }
	}
}

country_event = { 
	id = jap_newflavour.31
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.31"}
	title = jap_newflavour.31.t
	desc = jap_newflavour.31.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.31.a
	}
}

country_event = { 
	id = jap_newflavour.32
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.32"}
	title = jap_newflavour.32.t
	desc = jap_newflavour.32.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.32.a
		GER = {
		country_event = { id = jap_newflavour.33 days = 1 }
		}
	}
}

country_event = { #german event #might want to make sure this event delivers to japan
	id = jap_newflavour.33
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.33"}
	title = jap_newflavour.33.t
	desc = jap_newflavour.33.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.33.a
		country_event = { id = jap_newflavour.34 days = 1 }
	}
	option = {
		name = jap_newflavour.33.b
		GER = {
		country_event = { id = jap_newflavour.35 days = 1 }
		}
	}
}

country_event = { 
	id = jap_newflavour.34
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.34"}
	title = jap_newflavour.34.t
	desc = jap_newflavour.34.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.34.a
	}
}

country_event = { #german event
	id = jap_newflavour.35
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.35"}
	title = jap_newflavour.35.t
	desc = jap_newflavour.35.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.35.a
		country_event = { id = jap_newflavour.36 days = 1 }
	}
}

country_event = { 
	id = jap_newflavour.36
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.36"}
	title = jap_newflavour.36.t
	desc = jap_newflavour.36.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.36.a
		GER = {
		country_event = { id = jap_newflavour.37 days = 1 }
		}
	}
	option = {
		name = jap_newflavour.36.b
		GER = {
		country_event = { id = jap_newflavour.38 days = 1 }
		}
	}
}

country_event = { #german event
	id = jap_newflavour.37
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.37"}
	title = jap_newflavour.37.t
	desc = jap_newflavour.37.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.37.a
	}
}

country_event = { #german event
	id = jap_newflavour.38
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.38"}
	title = jap_newflavour.38.t
	desc = jap_newflavour.38.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.38.a
		country_event = { id = jap_newflavour.39 days = 1 }
	}
}

country_event = { 
	id = jap_newflavour.39
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.39"}
	title = jap_newflavour.39.t
	desc = jap_newflavour.39.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.39.a
		country_event = { id = jap_newflavour.40 days = 1 }
	}
}

country_event = { #german event? idk 
	id = jap_newflavour.40
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.40" }
	title = jap_newflavour.40.t
	desc = jap_newflavour.40.d

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.40.a
	}
}

#Misc flavor

country_event = {
	id = jap_newflavour.41
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.41" }
	title = jap_newflavour.41.t
	desc = jap_newflavour.41.d
	picture = GFX_report_event_BRG_boats_1

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.41.a
	}
}

country_event = {
	id = jap_newflavour.42
	immediate = {log = "[GetDateText]: [Root.GetName]: event jap_newflavour.42" }
	title = jap_newflavour.42.t
	desc = jap_newflavour.42.d
	picture = GFX_report_event_japan_tokyo_port

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = jap_newflavour.42.a
	}
}