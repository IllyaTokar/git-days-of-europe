﻿add_namespace = COR

country_event = { #Correntia Victorious
	id = COR.1
	immediate = {log = "[GetDateText]: [Root.GetName]: event COR.1"}
	title = COR.1.t
	desc = COR.1.d
	picture = GFX_report_event_dead_soldiers
	
	trigger = {
		tag = COR
		owns_state = 1092
		owns_state = 269
		owns_state = 1109
	}
	
	fire_only_once = yes

	option = { 
		name = COR.1.a
		hidden_effect = {
			news_event = { id = WORLD.936 days = 1 }
		}
	}
}