﻿ #############################################
## Syonan Events - Written by Butteryicarus  ###
##############################################

add_namespace = syo

country_event = { #Intro
	id = syo.0
	immediate = {log = "[GetDateText]: [Root.GetName]: event syo.0"}
 	title = syo.0.t
 	desc = syo.0.d
 	picture = GFX_report_event_generic_military_parade

	is_triggered_only = yes
 	fire_only_once = yes

 	option = {
 		name = syo.0.a
        hidden_effect = {
            every_country = { news_event = { id = WORLD.16500 days = 364 } }
            every_country = { news_event = { id = WORLD.16514 days = 1695 } }
        }
    }
 }
