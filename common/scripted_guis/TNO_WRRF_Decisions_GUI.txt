scripted_gui = {
	TNO_WRRF_Tangle_Decisions_GUI = {
		context_type = decision_category
		window_name = "TNO_WRRF_Tangle_Decisions_GUI"
		
		visible = {
			always = yes
		}
		
		triggers = {
			
		}
		
		effects = {
			
		}

		properties = {
			WRRF_Tangle_Dot = {
				x = WRRFTangle_X
				y = WRRFTangle_Y
			}
		}
	}
}