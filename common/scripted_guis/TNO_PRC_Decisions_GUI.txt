scripted_gui = {
	TNO_SVO_Army_Decisions_GUI = {
		context_type = decision_category
		window_name = "SVO_Army_Decisions_GUI"
		
		triggers = {
			SVO_1_Name_click_enabled = {
				SVO_Modifier_1_grey_trigger = yes
			}
			SVO_1_Modifier_click_enabled = {
				SVO_Modifier_1_grey_trigger = yes
			}
			SVO_1_Interact_Plus_click_enabled = {
				SVO_Modifier_1_button_trigger = yes
				SVO_Modifier_1_grey_trigger = yes
			}
			SVO_1_Interact_Minus_click_enabled = {
				SVO_Modifier_1_button_trigger = yes
				SVO_Modifier_1_grey_trigger = yes
			}
			SVO_2_Name_click_enabled = {
				SVO_Modifier_2_grey_trigger = yes
			}
			SVO_2_Modifier_click_enabled = {
				SVO_Modifier_2_grey_trigger = yes
			}
			SVO_2_Interact_Plus_click_enabled = {
				SVO_Modifier_2_button_trigger = yes
				SVO_Modifier_2_grey_trigger = yes
			}
			SVO_2_Interact_Minus_click_enabled = {
				SVO_Modifier_2_button_trigger = yes
				SVO_Modifier_2_grey_trigger = yes
			}

			SVO_3_Name_click_enabled = {
				SVO_Modifier_3_grey_trigger = yes
			}
			SVO_3_Modifier_click_enabled = {
				SVO_Modifier_3_grey_trigger = yes
			}
			SVO_3_Interact_Plus_click_enabled = {
				SVO_Modifier_3_button_trigger = yes
				SVO_Modifier_3_grey_trigger = yes
			}
			SVO_3_Interact_Minus_click_enabled = {
				SVO_Modifier_3_button_trigger = yes
				SVO_Modifier_3_grey_trigger = yes
			}
			SVO_4_Name_click_enabled = {
				SVO_Modifier_4_grey_trigger = yes
			}
			SVO_4_Modifier_click_enabled = {
				SVO_Modifier_4_grey_trigger = yes
			}
			SVO_4_Interact_Plus_click_enabled = {
				SVO_Modifier_4_button_trigger = yes
				SVO_Modifier_4_grey_trigger = yes
			}
			SVO_4_Interact_Minus_click_enabled = {
				SVO_Modifier_4_button_trigger = yes
				SVO_Modifier_4_grey_trigger = yes
			}

			SVO_5_Name_click_enabled = {
				SVO_Modifier_5_grey_trigger = yes
			}
			SVO_5_Modifier_click_enabled = {
				SVO_Modifier_5_grey_trigger = yes
			}
			SVO_5_Interact_Plus_click_enabled = {
				SVO_Modifier_5_button_trigger = yes
				SVO_Modifier_5_grey_trigger = yes
			}
			SVO_5_Interact_Minus_click_enabled = {
				SVO_Modifier_5_button_trigger = yes
				SVO_Modifier_5_grey_trigger = yes
			}
			SVO_6_Name_click_enabled = {
				SVO_Modifier_6_grey_trigger = yes
			}
			SVO_6_Modifier_click_enabled = {
				SVO_Modifier_6_grey_trigger = yes
			}
			SVO_6_Interact_Plus_click_enabled = {
				SVO_Modifier_6_button_trigger = yes
				SVO_Modifier_6_grey_trigger = yes
			}
			SVO_6_Interact_Minus_click_enabled = {
				SVO_Modifier_6_button_trigger = yes
				SVO_Modifier_6_grey_trigger = yes
			}

			SVO_7_Name_click_enabled = {
				SVO_Modifier_7_grey_trigger = yes
			}
			SVO_7_Modifier_click_enabled = {
				SVO_Modifier_7_grey_trigger = yes
			}
			SVO_7_Interact_Plus_click_enabled = {
				SVO_Modifier_7_button_trigger = yes
				SVO_Modifier_7_grey_trigger = yes
			}
			SVO_7_Interact_Minus_click_enabled = {
				SVO_Modifier_7_button_trigger = yes
				SVO_Modifier_7_grey_trigger = yes
			}
			SVO_8_Name_click_enabled = {
				SVO_Modifier_8_grey_trigger = yes
			}
			SVO_8_Modifier_click_enabled = {
				SVO_Modifier_8_grey_trigger = yes
			}
			SVO_8_Interact_Plus_click_enabled = {
				SVO_Modifier_8_button_trigger = yes
				SVO_Modifier_8_grey_trigger = yes
			}
			SVO_8_Interact_Minus_click_enabled = {
				SVO_Modifier_8_button_trigger = yes
				SVO_Modifier_8_grey_trigger = yes
			}
		}
		
		effects = {
			SVO_1_Interact_Plus_click = {
				if = {
					limit = { command_power > 19.99 }
					add_command_power = -20
					set_variable = { SVO_Exclusive_1_2_Frame = 2 }
					add_to_variable = { SVO_RA_army_armor_attack_factor = 0.05 }
					add_to_variable = { SVO_RA_army_infantry_attack_factor = -0.025 }
					SVO_update_dynamic_modifier_values = yes
					country_event = { id = SVO_debug.2 days = 60 }
				}
			}
			SVO_1_Interact_Minus_click = {
				army_experience = 20
				set_variable = { SVO_Exclusive_1_2_Frame = 2 }
				add_to_variable = { SVO_RA_army_armor_attack_factor = -0.05 }
				SVO_update_dynamic_modifier_values = yes
				country_event = { id = SVO_debug.2 days = 60 }
			}

			SVO_2_Interact_Plus_click = {
				if = {
					limit = { command_power > 19.99 }
					add_command_power = -20
					set_variable = { SVO_Exclusive_1_2_Frame = 3 }
					add_to_variable = { SVO_RA_army_infantry_attack_factor = 0.05 }
					add_to_variable = { SVO_RA_army_armor_attack_factor = -0.025 }
					SVO_update_dynamic_modifier_values = yes
					country_event = { id = SVO_debug.2 days = 60 }
				}
			}
			SVO_2_Interact_Minus_click = {
				army_experience = 20
				set_variable = { SVO_Exclusive_1_2_Frame = 3 }
				add_to_variable = { SVO_RA_army_infantry_attack_factor = -0.05 }
				SVO_update_dynamic_modifier_values = yes
				country_event = { id = SVO_debug.2 days = 60 }
			}

			SVO_3_Interact_Plus_click = {
				if = {
					limit = { command_power > 19.99 }
					add_command_power = -20
					set_variable = { SVO_Exclusive_3_4_Frame = 2 }
					add_to_variable = { SVO_RA_army_armor_speed_factor = 0.05 }
					add_to_variable = { SVO_RA_army_infantry_defence_factor = -0.025 }
					SVO_update_dynamic_modifier_values = yes
					country_event = { id = SVO_debug.3 days = 60 }
				}
			}
			SVO_3_Interact_Minus_click = {
				army_experience = 20
				set_variable = { SVO_Exclusive_3_4_Frame = 2 }
				add_to_variable = { SVO_RA_army_armor_speed_factor = -0.05 }
				SVO_update_dynamic_modifier_values = yes
				country_event = { id = SVO_debug.3 days = 60 }
			}

			SVO_4_Interact_Plus_click = {
				if = {
					limit = { command_power > 19.99 }
					add_command_power = -20
					set_variable = { SVO_Exclusive_3_4_Frame = 3 }
					add_to_variable = { SVO_RA_army_infantry_defence_factor = 0.05 }
					add_to_variable = { SVO_RA_army_armor_speed_factor = -0.025 }
					SVO_update_dynamic_modifier_values = yes
					country_event = { id = SVO_debug.3 days = 60 }
				}
			}
			SVO_4_Interact_Minus_click = {
				army_experience = 20
				set_variable = { SVO_Exclusive_3_4_Frame = 3 }
				add_to_variable = { SVO_RA_army_infantry_defence_factor = -0.05 }
				SVO_update_dynamic_modifier_values = yes
				country_event = { id = SVO_debug.3 days = 60 }
			}

			SVO_5_Interact_Plus_click = {
				if = {
					limit = { command_power > 19.99 }
					add_command_power = -20
					set_variable = { SVO_Exclusive_5_6_Frame = 2 }
					add_to_variable = { SVO_RA_army_org = 0.05 }
					add_to_variable = { SVO_RA_training_time_army = -0.025 }
					SVO_update_dynamic_modifier_values = yes
					country_event = { id = SVO_debug.4 days = 60 }
				}
			}
			SVO_5_Interact_Minus_click = {
				army_experience = 20
				set_variable = { SVO_Exclusive_5_6_Frame = 2 }
				add_to_variable = { SVO_RA_army_org = -0.05 }
				SVO_update_dynamic_modifier_values = yes
				country_event = { id = SVO_debug.4 days = 60 }
			}

			SVO_6_Interact_Plus_click = {
				if = {
					limit = { command_power > 19.99 }
					add_command_power = -20
					set_variable = { SVO_Exclusive_5_6_Frame = 3 }
					add_to_variable = { SVO_RA_training_time_army = -0.05 }
					add_to_variable = { SVO_RA_army_org = -0.025 }
					SVO_update_dynamic_modifier_values = yes
					country_event = { id = SVO_debug.4 days = 60 }
				}
			}
			SVO_6_Interact_Minus_click = {
				army_experience = 20
				set_variable = { SVO_Exclusive_5_6_Frame = 3 }
				add_to_variable = { SVO_RA_training_time_army = 0.05 }
				SVO_update_dynamic_modifier_values = yes
				country_event = { id = SVO_debug.4 days = 60 }
			}
			
			SVO_7_Interact_Plus_click = {
				if = {
					limit = { command_power > 19.99 }
					add_command_power = -20
					set_variable = { SVO_Exclusive_7_8_Frame = 2 }
					add_to_variable = { SVO_RA_army_sub_unit_air_assault_attack_factor = 0.05 }
					add_to_variable = { SVO_RA_experience_gain_army_factor = -0.025 }
					SVO_update_dynamic_modifier_values = yes
					country_event = { id = SVO_debug.5 days = 60 }
				}
			}
			SVO_7_Interact_Minus_click = {
				army_experience = 20
				set_variable = { SVO_Exclusive_7_8_Frame = 2 }
				add_to_variable = { SVO_RA_army_sub_unit_air_assault_attack_factor = -0.05 }
				SVO_update_dynamic_modifier_values = yes
				country_event = { id = SVO_debug.5 days = 60 }
			}

			SVO_8_Interact_Plus_click = {
				if = {
					limit = { command_power > 19.99 }
					add_command_power = -20
					set_variable = { SVO_Exclusive_7_8_Frame = 3 }
					add_to_variable = { SVO_RA_experience_gain_army_factor = 0.05 }
					add_to_variable = { SVO_RA_army_sub_unit_air_assault_attack_factor = -0.025 }
					SVO_update_dynamic_modifier_values = yes
					country_event = { id = SVO_debug.5 days = 60 }
				}
			}
			SVO_8_Interact_Minus_click = {
				army_experience = 20
				set_variable = { SVO_Exclusive_7_8_Frame = 3 }
				add_to_variable = { SVO_RA_experience_gain_army_factor = -0.05 }
				SVO_update_dynamic_modifier_values = yes
				country_event = { id = SVO_debug.5 days = 60 }
			}
		}

		properties = {
			SVO_1_2_Exclusive_Icon = { frame = SVO_Exclusive_1_2_Frame }
			SVO_3_4_Exclusive_Icon = { frame = SVO_Exclusive_3_4_Frame }
			SVO_5_6_Exclusive_Icon = { frame = SVO_Exclusive_5_6_Frame }
			SVO_7_8_Exclusive_Icon = { frame = SVO_Exclusive_7_8_Frame }
			SVO_Modernization_ProgressBar = { frame = SVO_Army_Modernization }
		}
	}

	#Estates
	SVO_ownership_display = {
		window_name = "SVO_State_Container"
		parent_window_token = selected_state_view
		context_type = selected_state_context
		visible = {
			ROOT = {
				original_tag = SVO
			}
			THIS = {
				owner = { tag = SVO }
			}
		}
		properties = {
			#SVO_remove_status = { image = "[GetSVO_remove_status]" }
		}
		effects = {
			SVO_remove_status_click = {	
				if = { 
					limit = { check_variable = { THIS.SVO_Owner = -1 } }
						add_to_variable = { SVO.SVO_Army_Power = -10 }
						add_to_variable = { SVO.SVO_Army_Loyalty = -20 }
				}
				
				if = { 
					limit = { check_variable = { THIS.SVO_Owner = 1 } }
					
					add_to_variable = { SVO.SVO_Civilian_Power = -10 }
					add_to_variable = { SVO.SVO_Civilian_Loyalty = -20 }
				}
				
				set_variable = { THIS.SVO_Owner = 0 }
				SVO_apply_relationship_effect = yes
				
			}
		
			SVO_add_civ_status_click = {
				if = { 
					limit = {
						check_variable = { THIS.SVO_Owner = 0 }
						SVO = { NOT = { has_country_flag = SVO_destroyed_citzenry } }
						OR = {
							AND = {
								NOT = { is_core_of = SVO }
								SVO = { has_country_flag = SVO_can_give_non_cores }
							}
							is_core_of = SVO
						}
					}
					set_variable = { THIS.SVO_Owner = 1 }
					
					add_to_variable = { SVO.SVO_Civilian_Power = 10 }
					add_to_variable = { SVO.SVO_Civilian_Loyalty = 10 }
					SVO_apply_relationship_effect = yes
				}
			}
	
			SVO_add_mil_status_click = {
				if = { 
					limit = {
						check_variable = { THIS.SVO_Owner = 0 }
						SVO = { NOT = { has_country_flag = SVO_destroyed_military } }
						OR = {
							AND = {
								NOT = { is_core_of = SVO }
								SVO = { has_country_flag = SVO_can_give_non_cores }
							}
							is_core_of = SVO
						}
					}
					set_variable = { THIS.SVO_Owner = -1 }
					add_to_variable = { SVO.SVO_Army_Power = 10 }
					add_to_variable = { SVO.SVO_Army_Loyalty = 10 }
					SVO_apply_relationship_effect = yes
				}
			}
		}
	}
}