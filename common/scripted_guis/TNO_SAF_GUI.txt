scripted_gui = {
	TNO_SAF_GUI = {
		context_type = decision_category
		parent_window_token = top_bar
		window_name = "TNO_SAF_Decisions_GUI"

		visible = {
			always = yes
		}
		
		triggers = {
		
		}

		effects = {

		}
		
		properties = {
			SAF_GUI_State1 ={
				image = "[GetSAFGUIState1]"
			}
			SAF_GUI_State2 ={
				image = "[GetSAFGUIState2]"
			}
			SAF_GUI_State3 ={
				image = "[GetSAFGUIState3]"
			}
			SAF_GUI_State4 ={
				image = "[GetSAFGUIState4]"
			}
			SAF_GUI_State5 ={
				image = "[GetSAFGUIState5]"
			}
		}
	}
}