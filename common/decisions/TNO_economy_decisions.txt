economy_test_category = {
	#basically everything's cool until you can't repay your debts
	#i.e. you start with a negative budget
	economic_crisis = {                           # This is our mission id
		allowed = { always = yes }
		available = {                           # Available here means as much as goal because:
		    custom_trigger_tooltip = {
		    	tooltip = econ_positive_budget
		   		check_variable = { total_budget > 0 }
		    }
		}
		activation = {                          # The mission appears and starts counting down under these circumstances:
		    check_variable = { total_budget < 0 }
		    NOT = { has_country_flag = bankrupt_nation }
			econ_can_use_economy_system = yes
		}
		is_good = no                          # This mission is color coded to be a positive thing (not a crisis)
		days_mission_timeout = 60              # How many days before the mission fails?
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout economic_crisis"
			hidden_effect = { update_economy_tab = yes }
		    if = { #ok this is not good
		    	limit = { NOT = { has_variable = econ_crisis } }
		    	set_variable = { econ_crisis = 1 }

				add_stability = -0.05
				add_political_power = -100

		    	custom_effect_tooltip = econ_decrease_GDP
				custom_effect_tooltip = econ_debt_rate_50_percent_increase
		    	multiply_variable = { interest_rates = 1.5 }
		    	multiply_variable = { GDP = 0.95 }
		    }
		    else_if = { #ok this is really not good
		    	limit = { check_variable = { econ_crisis = 1 } }
		    	add_to_variable = { econ_crisis = 1 }

		    	add_stability = -0.05
				add_political_power = -150

		    	custom_effect_tooltip = econ_decrease_GDP
				custom_effect_tooltip = econ_debt_rate_50_percent_increase
		    	multiply_variable = { interest_rates = 1.5 }
		    	multiply_variable = { GDP = 0.9 }
		    }
		    else_if = { #oh god bls help
		    	limit = { check_variable = { econ_crisis = 2 } }
		    	add_to_variable = { econ_crisis = 1 }

		    	add_stability = -0.10
				add_political_power = -200

		    	custom_effect_tooltip = econ_decrease_GDP
				custom_effect_tooltip = econ_debt_rate_50_percent_increase
		    	multiply_variable = { interest_rates = 1.5 }
		    	multiply_variable = { GDP = 0.85 }
		    }
		    else_if = { #your country is fucking bankrupt you idiot
		    	limit = { check_variable = { econ_crisis = 3 } }
		    	clear_variable = econ_crisis

		    	add_ideas = bankrupt_military
		    	add_ideas = bankrupt_civilian

		    	#eventually this bankrupcy thing goes away
		    	hidden_effect = {
		    		country_event = { id = econ.7 days = 547 random = 8760 } #eventually
		    		set_country_flag = bankrupt_nation
		    	}

		    	custom_effect_tooltip = econ_decrease_GDP
		    	custom_effect_tooltip = econ_debt_rate_100_percent_increase
		    	custom_effect_tooltip = econ_default_debt
		    	set_variable = { national_debt = 0 }
		    	multiply_variable = { interest_rates = 2.0 }
		    	multiply_variable = { GDP = 0.75 }
		    }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision economic_crisis"
			add_stability = -0.05
			add_political_power = -100
		    clear_variable = econ_crisis
		}
	}

	econ_emergency_taxes = {
		visible = { check_variable = { total_budget < 0 } }
		custom_cost_trigger = { always = yes } #no cost
		days_re_enable = 700
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision econ_emergency_taxes"
			add_political_power = -150
			add_stability = -0.03
			tno_tax_rate_improve = yes
			hidden_effect = {
				country_event = { id = econ.10 days = 365 } #reset in a year
				update_economy_tab = yes
			}
		}
	}

	econ_emergency_military_austerity = {
		visible = { check_variable = { total_budget < 0 } }
		custom_cost_trigger = { always = yes } #no cost
		days_re_enable = 700
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision econ_emergency_military_austerity"
			add_political_power = -150
			add_stability = -0.03
			remove_ideas = military_budget_boost
			if = {
				limit = { has_idea = military_budget_cuts }
				swap_ideas = {
					add_idea = military_budget_cuts_heavy
					remove_idea = military_budget_cuts
				}
			}
			else = {
				add_ideas = military_budget_cuts_heavy
			}
			hidden_effect = {
				country_event = { id = econ.8 days = 365 } #reset in a year
				update_economy_tab = yes
			}
		}
	}

	econ_emergency_civilian_austerity = {
		visible = {  check_variable = { total_budget < 0 } }
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision econ_emergency_civilian_austerity"
			add_political_power = -150
			add_stability = -0.03
			remove_ideas = civilian_budget_boost
			if = {
				limit = { has_idea = civilian_budget_cuts }
				swap_ideas = {
					add_idea = civilian_budget_cuts_heavy
					remove_idea = civilian_budget_cuts
				}
			}
			else = {
				add_ideas = civilian_budget_cuts_heavy
			}
			hidden_effect = {
				country_event = { id = econ.9 days = 365 } #reset in a year
				update_economy_tab = yes
			}
		}
	}

	econ_use_liquid_reserves = {
		visible = {
			check_variable = { total_budget < 0 } 
			check_variable = { money_reserves > 0 }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision econ_use_liquid_reserves"
			custom_effect_tooltip = econ_use_liquid_reserves_tt
			subtract_from_variable = { national_debt = money_reserves }
			if = {
				limit = { check_variable = { national_debt < 0 } }
				set_variable = { money_reserves = national_debt }
				multiply_variable = { money_reserves = -1 }
			}
			else = {
				set_variable = { money_reserves = 0 }
			}
			hidden_effect = {
				update_economy_tab = yes
			}
		}
	}
}