MAD_cat_die_schwarze_pest = {
	MAD_quarantine_toamasina = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			944 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			944 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 944 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			944 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	MAD_quarantine_antananarivo = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			945 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			945 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 945 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			945 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	MAD_quarantine_toliara = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			946 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			946 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 946 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			946 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	MAD_quarantine_mahajanga = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			947 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			947 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 947 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			947 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	MAD_quarantine_antsiranana = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			543 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			543 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 543 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			543 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	MAD_quarantine_ivakoany_massif = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			1041 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			1041 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 1041 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			1041 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	MAD_quarantine_tolanaro = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			1042 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			1042 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 1042 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			1042 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	MAD_quarantine_morondava = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			1043 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			1043 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 1043 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			1043 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	MAD_quarantine_reunion = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			706 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			706 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 706 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			706 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	MAD_quarantine_mauritius = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			707 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
			}
		}

		visible = {
			707 = {
				check_variable = { MAD_plague_percent_population > 0.2 }
				is_demilitarized_zone = no
			}
			has_completed_focus = MAD_regional_quarantine_areas
		}

		highlight_states = { state = 707 }

		on_map_mode = map_only

		cost = 25

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 2
		}

		complete_effect = {
			707 = {
				set_state_flag = MAD_state_quarantined
				set_demilitarized_zone = yes
			}
		}
	}

	#########################################################################################################

	MAD_lift_quarantine_toamasina = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			944 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			944 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 944 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			944 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	MAD_lift_quarantine_antananarivo = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			945 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			945 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 945 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			945 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	MAD_lift_quarantine_toliara = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			946 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			946 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 946 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			946 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	MAD_lift_quarantine_mahajanga = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			947 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			947 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 947 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			947 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	MAD_lift_quarantine_antsiranana = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			543 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			543 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 543 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			543 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	MAD_lift_quarantine_ivakoany_massif = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			1041 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			1041 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 1041 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			1041 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	MAD_lift_quarantine_tolanaro = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			1042 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			1042 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 1042 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			1042 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	MAD_lift_quarantine_morondava = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			1043 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			1043 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 1043 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			1043 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	MAD_lift_quarantine_reunion = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			706 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			706 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 706 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			706 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	MAD_lift_quarantine_mauritius = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			707 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
		}

		visible = {
			707 = {
				check_variable = { MAD_plague_percent_population < 0.05 }
				is_demilitarized_zone = yes
			}
			#has_completed_focus = MAD_regional_lift_quarantine_areas
		}

		highlight_states = { state = 707 }

		on_map_mode = map_only

		cost = 0

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			707 = {
				clr_state_flag = MAD_state_quarantined
				set_demilitarized_zone = no
			}
			add_political_power = 50
		}
	}

	#########################################################################################################

	MAD_send_medical_aid_to_toamasina = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			944 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			944 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 944 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			944 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	MAD_send_medical_aid_to_antananarivo = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			945 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			945 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 945 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			945 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	MAD_send_medical_aid_to_toliara = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			946 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			946 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 946 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			946 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	MAD_send_medical_aid_to_mahajanga = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			947 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			947 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 947 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			947 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	MAD_send_medical_aid_to_antsiranana = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			543 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			543 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 543 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			543 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	MAD_send_medical_aid_to_ivakoany_massif = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			1041 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			1041 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 1041 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			1041 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	MAD_send_medical_aid_to_tolanaro = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			1042 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			1042 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 1042 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			1042 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	MAD_send_medical_aid_to_morondava = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			1043 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			1043 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 1043 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			1043 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	MAD_send_medical_aid_to_reunion = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			706 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			706 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 706 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			706 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	MAD_send_medical_aid_to_mauritius = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
			707 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			custom_trigger_tooltip = {
				tooltip = MAD_need_enough_crates_medical_aid		
			    check_variable = {
					MAD_plague_number_aid > 0
				}
			}
		}

		visible = {
			707 = {
				check_variable = { MAD_plague_percent_population > 0 }
			}
			has_completed_focus = MAD_time_for_action
		}

		highlight_states = { state = 707 }

		on_map_mode = map_only

		cost = 10

		fire_only_once = no

		cancel_if_not_visible = yes

		days_remove = 5

		ai_will_do = {
			factor = 1
		}

		complete_effect = {
			subtract_from_variable = { MAD_plague_number_aid = 1 }
		}

		remove_effect = {
			707 = {
				subtract_from_variable = { MAD_plague_percent_population = 0.1 }
				clamp_variable = {
					var = MAD_plague_percent_population
				    min = 0
				    max = 1
				}
			}
		}
	}

	#########################################################################################################

	MAD_gather_more_medical_aid = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
		}

		visible = {
			NOT = { has_completed_focus = MAD_we_need_everything }
		}

		on_map_mode = map_only

		cost = 50

		fire_only_once = no

		cancel_if_not_visible = no

		days_remove = 15

		ai_will_do = {

			factor = 1

			modifier = {
				add = 1
				check_variable = { MAD_plague_number_aid < 5 }
			}

			modifier = {
				add = -1
				has_political_power < 150
			}

			modifier = {
				add = -2
				check_variable = { MAD_plague_percent_global = 0.00 }
			}
		}

		remove_effect = {
			add_to_variable = { MAD_plague_number_aid = 2 }
		}
	}

	MAD_gather_more_medical_aid_2 = {
		# icon = eng_blackshirt_march

		allowed = {
	        original_tag = MAD
	    }

		available = {
		}

		visible = {
			has_completed_focus = MAD_we_need_everything
		}

		on_map_mode = map_only

		cost = 50

		fire_only_once = no

		cancel_if_not_visible = no

		days_remove = 15

		ai_will_do = {

			factor = 1

			modifier = {
				add = 1
				check_variable = { MAD_plague_number_aid < 5 }
			}

			modifier = {
				add = -1
				has_political_power < 150
			}

			modifier = {
				add = -2
				check_variable = { MAD_plague_percent_global = 0.00 }
			}

		}

		remove_effect = {
			add_to_variable = { MAD_plague_number_aid = 4 }
		}
	}
}