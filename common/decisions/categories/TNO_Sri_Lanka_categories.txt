#Economy
SRI_OFN_economy_category = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	visible = {
		has_country_flag = SRI_is_ofn_admin
		original_tag = SRI
		#has_completed_focus = SRI_begin_economic_development
	}
}
