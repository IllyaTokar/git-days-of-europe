CHI_gov_legitimacy_category = {
	icon = decision_category_generic_political_actions

	allowed = {
		original_tag = CHI
	}
	visible = {
	}
	visible_when_empty = yes
	priority = {
		base = 15
	}
}

CHI_tech_modernisation_category = {
	icon = decision_category_generic_political_actions
	
	allowed = {
		original_tag = CHI 
	}
	visible = {
		has_completed_focus = CHI_Technology
		NOT = { has_country_flag = CHI_tech_modernisation_complete }
	}
}

CHI_education_modernisation_category = {
	icon = decision_category_generic_political_actions
	
	allowed = {
		original_tag = CHI 
	}
	visible = {
		has_completed_focus = CHI_Education
		NOT = { has_country_flag = CHI_edu_modernisation_complete }
	}
}

CHI_talk_with_long_yun_category = {
	icon = military_operation
	picture = GFX_decision_cat_picture_chi_infiltration
	allowed = {
		original_tag = CHI
	}
	visible = { 
		NOT = { has_global_flag = CHI_Western_Insurrection_Crisis }
		has_country_flag = CHI_Talks_with_Long_Yun
	}
}

CHI_Western_Insurrection_category = {
	icon = military_operation
	picture = GFX_decision_cat_picture_chi_infiltration
	allowed = {
		original_tag = CHI
	}
	visible = { 
		has_global_flag = CHI_Western_Insurrection_Crisis
	}
}

CHI_Industry_Resources = {
	icon = generic_industry
	#picture = 
	allowed = {
		original_tag = CHI
	}
	visible = {
		has_completed_focus = CHI_Industrialization
	}
	visible_when_empty = yes
}

CHI_Resource_Exploitation = {
	icon = generic_industry
	#picture = 
	allowed = {
		original_tag = CHI
	}
	visible = {
		has_completed_focus = CHI_Digging_Deep
	}
	visible_when_empty = yes
}