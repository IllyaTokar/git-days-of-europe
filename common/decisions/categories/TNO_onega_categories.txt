VTY_deal_with_radicals = {
	icon = GFX_decision_generic_break_treaty

	picture = GFX_VTY_political_fighting

	allowed  = {
		tag = VTY
	}
	visible = {
		has_country_flag = VTY_deal_with_radicals
		OR = {
			has_idea = VTY_politics_1
			has_idea = VTY_politics_2
			has_idea = VTY_politics_3
			has_idea = VTY_politics_4
		}
	}
}

VTY_deal_with_bandits = {
	icon = GFX_decision_generic_ignite_civil_war

	picture = GFX_VTY_secure_the_front

	allowed  = {
		tag = VTY
	}
	visible = {
		has_country_flag = VTY_deal_with_bandits
		OR = {
			has_idea = VTY_bandits_1
			has_idea = VTY_bandits_2
			has_idea = VTY_bandits_3
			has_idea = VTY_bandits_4
		}
	}
}

VTY_anti_commie_guard = {
	icon = GFX_decision_generic_prepare_civil_war


	allowed  = {
		tag = VTY
	}
	visible = {
		has_country_flag = VTY_anti_commie_guard
		WRS = {
			exists = yes
		}
	}
}

VTY_communal_industries = {
	icon = GFX_decision_generic_nationalism

	picture = GFX_VTY_fishing

	allowed = {
		tag = VTY
	}

	visible = {
		has_country_flag = VTY_communal_industries
	}
}

VTY_modernization = {
	icon = GFX_decision_category_generic_industry

	picture = GFX_SIB_decision_legacy_of_the_siberian_plan

	allowed = {
		tag = VTY
	}

	visible = {
		has_country_flag = VTY_modernization
	}
}

VTY_faction = {
	icon = GFX_decision_category_generic_foreign_policy

	picture = GFX_decision_RUS_reunification

	allowed = {
		tag = VTY
	}

	visible = {
		has_country_flag = VTY_faction
	}
}
