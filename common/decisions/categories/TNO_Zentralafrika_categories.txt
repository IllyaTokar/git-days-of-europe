COG_hunting_season = {
	icon = decision_category_generic_political_actions
	visible_when_empty = yes

	picture = GFX_decision_COG_hunting_season

	allowed = {
		tag = COG
	}
	visible = {
		has_country_leader = {
		    name = "Siegfried Müller"
		    ruling_only = yes
		}
		has_war = no
		has_completed_focus = COG_the_safari_beckons
	}
}

COG_internal_relations = {
	icon = decision_category_generic_political_actions
	visible_when_empty = yes

	picture = GFX_decision_COG_internal_relations

	allowed = {
		original_tag = COG
	}
	visible = {
		has_country_leader = {
		    name = "Siegfried Müller"
		    ruling_only = yes
		}
	}
}

COG_investor_favors = {
	icon = decision_category_generic_political_actions
	visible_when_empty = yes

	picture = GFX_decision_COG_internal_relations

	allowed = {
		original_tag = COG
	}
	visible = {
		has_completed_focus = COG_a_corporate_empire
		NOT = { has_country_flag = COG_Investors_Gone }
		has_country_leader = {
		    name = "Siegfried Müller"
		    ruling_only = yes
		}
	}
}

COG_welcome_in_the_heart_of_afrika = {
	icon = decision_category_generic_political_actions
	visible_when_empty = yes

	picture = GFX_decision_COG_welcome_in_the_heart_of_afrika

	allowed = {
		original_tag = COG
	}
	visible = {
		has_country_leader = {
		    name = "Siegfried Müller"
		    ruling_only = yes
		}
		has_war = no
		NOT = { has_global_flag = german_civil_war }
	}
}

COG_the_mercs = {
	icon = decision_category_generic_political_actions
	visible_when_empty = yes

	picture = GFX_decision_COG_the_mercs

	allowed = {
		original_tag = COG
	}
	visible = {
		has_country_leader = {
		    name = "Siegfried Müller"
		    ruling_only = yes
		}
		has_country_flag = COG_has_extended_mercenary_recruitment_flag
		has_war = yes
	}
}

COG_resource_buying_category = {
	icon = GFX_decision_category_generic_prospect_for_resources

	allowed = {
		tag = COG
		NOT = { has_cosmetic_tag = COG_USA }
	}
}
