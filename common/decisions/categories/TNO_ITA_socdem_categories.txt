ita_agip_reinvestment = {
	icon = generic_prospect_for_resources

	picture = GFX_decision_USA_decision_political_situation
	allowed = {
		tag = ITA
	}
	available = {
		has_country_flag = ita_reinvest_in_agip
	}
}
