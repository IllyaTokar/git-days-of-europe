UDM_Black_Market_Purchase_Foreign = {

	icon = decision_black_market_money

	picture = GFX_BRT_decision_black_market_foreign_purchase

	#allowed = {
		#	has_country_flag = udm_sphere_of_influence
		#}

	visible = {
		has_country_flag = udm_sphere_of_influence
		UDM = {
			exists = yes
		}
	}
}

UDM_Black_Market_Order_Received = {

	icon = decision_category_generic_foreign_policy

	picture = GFX_BRT_decision_black_market_order_received

	allowed = {
		tag = UDM
	}

}

UDM_Expenditures = {

	icon = decision_category_generic

	picture = GFX_BRT_decision_black_market_order_received

	allowed = {
		tag = UDM
	}

}

UDM_Defense_And_Influence = {

	icon = decision_category_military_operation

	picture = GFX_BRT_decision_black_market_order_received

	allowed = {
		tag = UDM
	}

}
