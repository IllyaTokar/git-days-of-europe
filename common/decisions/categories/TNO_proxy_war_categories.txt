####Proxy war
####War

GER_south_african_war_category = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_GER_decision_political_situation

	allowed = {
		original_tag = GER
	}
	visible = {
		has_global_flag = south_african_war
		has_global_flag = german_civil_war_end
		NOT = {
			has_global_flag = german_civil_war_bormannvic
			has_global_flag = german_civil_war_speervic
		}
	}
}






USA_south_african_war_category = {
	icon = decision_category_generic_political_actions


	allowed = {
		original_tag = USA
	}

	visible = {
		has_country_flag = USA_In_The_SAF_WAR
		has_global_flag = south_african_war_happening
	}
}

USA_south_african_war_domestic_category = {
	icon = decision_category_generic_political_actions


	allowed = {
		original_tag = USA
	}

	visible = {
		has_country_flag = USA_In_The_SAF_WAR
		has_global_flag = south_african_war_happening
	}
}






ITA_arab_war_category = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = ITA
	}

	visible = {
		has_global_flag = arab_war_ongoing
		ITA = {
			has_country_flag = ITA_entered_arab_war
		}
	}
}

GER_arab_war_category = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = GER
	}

	visible = {
		has_global_flag = arab_war_ongoing
		has_country_flag = GER_entered_arab_war
	}
}

JAP_arab_war_category = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}

	visible = {
		has_global_flag = arab_war_ongoing
		JAP = {
			has_country_flag = JAP_entered_arab_war
		}
	}
}

JAP_mongolian_war_category = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_category_VIN_Vietcong_Pic

	allowed = {
		original_tag = JAP
	}

	visible = {
		has_country_flag = JAP_mongolian_war
		MON = {
			exists = yes
			has_war_with = MEN
		}
	}
}

USA_IWI_category = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_category_VIN_Vietcong_Pic

	allowed = {
		tag = USA
	}

	visible = {
		RAJ = {
			exists = yes
			has_war_with = BEN
		}
	}
}
