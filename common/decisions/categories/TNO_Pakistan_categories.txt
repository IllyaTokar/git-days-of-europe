PAK_Pakistani_Insurgency_Cat = {
	icon = decision_generic_ignite_civil_war
	picture = GFX_decision_PAK_Pakistani_Insurgency
	allowed = {
		tag = PAK
		has_capitulated = yes
	}
	allowed = {
		
		tag = PAK
	}
	visible = {
		has_capitulated = yes
	}
}