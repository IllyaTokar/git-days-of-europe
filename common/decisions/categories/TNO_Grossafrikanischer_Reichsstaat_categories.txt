MZB_REICHSTAAT_stabilisierung_des_reichstaates = {
	icon = decision_category_military_operation
	visible_when_empty = yes

	picture = GFX_decision_MZB_REICHSTAAT_stabilisierung_des_reichstaates
	
	allowed = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_war = no
	}
	
	visible = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_war = no
	}
}

MZB_REICHSTAAT_system_of_efficiency = {
	icon = decision_category_military_operation
	visible_when_empty = yes

	picture = GFX_decision_MZB_REICHSTAAT_stabilisierung_des_reichstaates
	
	allowed = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_war = no
	}
	
	visible = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_completed_focus = MZB_strict_work_quotas
		has_war = no
	}
}

MZB_REICHSTAAT_crisis_in_sudafrika = {
	icon = decision_category_military_operation
	visible_when_empty = yes

	picture = GFX_decision_MZB_REICHSTAAT_die_ostafrikanishen_ss_einsatzgruppen
	
	allowed = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_war = no
	}
	
	visible = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_war = no
		country_exists = SFG
	}
}

MZB_REICHSTAAT_die_grossafrikanishen_ss_einsatzgruppen = {
	icon = decision_category_military_operation
	visible_when_empty = yes

	picture = GFX_decision_MZB_REICHSTAAT_die_ostafrikanishen_ss_einsatzgruppen
	
	allowed = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_war = no
		has_completed_focus = MZB_gather_our_strength
	}
	
	visible = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_war = no
		has_completed_focus = MZB_gather_our_strength
	}
}

MZB_REICHSTAAT_die_organisation_werwolf = {
	icon = decision_category_military_operation
	visible_when_empty = yes

	picture = GFX_decision_MZB_REICHSTAAT_die_organisation_werwolf
	
	allowed = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_war = no
		has_completed_focus = MZB_werewolves_against_the_silver
	}
	
	visible = {
		OR = {
			has_cosmetic_tag = MZB_REICHSTAAT
			has_cosmetic_tag = MZB_REICHSTAAT_2
			has_cosmetic_tag = MZB_REICHSTAAT_3
		}
		has_war = no
		has_completed_focus = MZB_werewolves_against_the_silver
	}
}

MZB_REICHSTAAT_african_cabal = {
	icon = decision_category_military_operation
	visible_when_empty = yes
	picture = GFX_decision_MZB_REICHSTAAT_african_cabal
	
	visible = {
		tag = GER
		has_country_flag = GER_can_interact_with_cabal
	}
}