

MCS_reform_decisions = {
    icon = decision_category_generic_political_actions
	picture = GFX_decision_USA_decision_political_situation

    allowed = {
		original_tag = USA
	}
    
    visible = {
		original_tag = USA
        has_country_flag = USA_MCS_reform_active
	}
}