RUS_race_for_the_urals = {
	icon = GFX_decision_category_power_struggle

	picture = GFX_RUS_warlord_development

	allowed = {
		OR = {
			is_central_russian_nation = yes
			is_west_siberian_nation = yes
		}
	}
	visible = {
		has_country_flag = BEGUN_SOUTHERN_URALS_CRISIS
		NOT = {
			has_country_flag = RUS_SOUTHERN_URALS_CRISIS_OVER
		}
		NOT = {
			AND = {	
				has_war = no
				AND = {
					ORS = {
						exists = no
					}
					ORE = {
						exists = no
					}
					URL = {
						exists = no
					}
					BAS = {
						exists = no
					}
				}
			}
		}
	}
	priority = {
		base = 180
	}
}

RUS_race_for_kazakhstan = {
	icon = GFX_decision_category_power_struggle

	picture = GFX_RUS_warlord_development

	allowed = {
		is_russian_nation = yes
	}
	visible = {
		has_country_flag = BEGUN_KAZAKH_CRISIS
	}
	priority = {
		base = 180
	}
}
