KHA_power_struggle_category = {

	icon = generic_political_discourse
	picture = GFX_ORE_category_political_situation

	allowed = {
		original_tag = KHA
	}
	
	visible = {
		NOT = {
			has_country_flag = KHA_NO_MORE_ANARCHISM
		}
	}
	
	priority = 300
}
KHA_voting_category = {

	icon = generic_democracy
	picture = GFX_VTY_political_fighting
	visible_when_empty = yes

	allowed = {
		original_tag = KHA
	}
	
	visible = {
		has_country_flag = KHA_debate_ongoing
		NOT = {
			has_country_flag = KHA_NO_MORE_ANARCHISM
		}
	}
	
	priority = 300
}
KHA_build_a_nuke_workshop = {

	icon = generic_prospect_for_resources
	picture = GFX_decision_ITA_progetta_alfa

	allowed = {
		original_tag = KHA
	}

	available = {
		always = yes
	}
	
	visible = {
		always = yes
	}
	
	priority = 300
}