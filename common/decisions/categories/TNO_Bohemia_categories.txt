CZE_ai_ss_path = {
	allowed = {
		tag = ZZZ
	}
	visible = {
		CZE = { has_country_flag = CZE_player_garrison }
		NOT = {
			OR = {
				has_global_flag = CZE_garrison_victory
				has_global_flag = CZE_ss_victory
			}
		}
	}
}

CZE_ai_garrison_path = {
	allowed = {
		tag = ZZZ
	}
	visible = {
		CZE = { has_country_flag = CZE_player_ss }
		NOT = {
			OR = {
				has_global_flag = CZE_garrison_victory
				has_global_flag = CZE_ss_victory
			}
		}
	}
}

CZE_power_struggle = {
	allowed = {
		tag = CZE
	}
	visible = {
		has_completed_focus = CZE_now_is_the_time
		NOT = {
			OR = {
				has_global_flag = CZE_garrison_victory
				has_global_flag = CZE_ss_victory
			}
		}
	}
}

CZE_czech_guard_category = {
	allowed = {
		tag = CZE
	}
	visible = {
		has_completed_focus = CZE_the_czech_guard
		NOT = {
			has_completed_focus = CZE_the_lidice_massacre
		}
	}
}