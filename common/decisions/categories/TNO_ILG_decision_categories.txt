ILG_Banditry_Decisions = {
	icon = generic_prospect_for_resources

	picture = GFX_decision_USA_decision_political_situation
	allowed = {
		tag = ILG
	}
	available = {
		
		has_country_flag = ILG_Banditry_Decisions
	}
}
