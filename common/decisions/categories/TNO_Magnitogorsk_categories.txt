BAS_subjects_category = {
	icon = generic_operation
	priority = 100
	picture = GFX_URL_decision_Ural_Guard
	allowed = { original_tag = BAS }
	visible = {
		has_country_flag = subjects_aquired
		NOT = { has_global_flag = BAS_NKVD_coup }
	}
}
BAS_target_category = {
	icon = generic_operation
	priority = 100
	picture = GFX_URL_decision_Ural_Guard
	allowed = { original_tag = BAS }
	visible = {
		has_country_flag = target_acquired
		NOT = { has_global_flag = BAS_NKVD_coup }
	}
}
BAS_army_professianalism_category = {
	icon = generic_operation
	priority = 200
	picture = GFX_decision_USA_decision_political_situation
	allowed = { original_tag = BAS }
	visible = {
		has_completed_focus = BAS_the_black_mountain_awakes
	}
}
BAS_lysenko_sanity_category = {
	icon = generic_operation
	priority = 200
	picture = GFX_BAS_category_lysenko_sanity
	allowed = { original_tag = BAS }
	visible_when_empty = yes
	visible = {
		NOT = {
			has_global_flag = BAS_NKVD_coup
		}
	}
}

nkvd_loyalty_category = {
	icon = generic_operation
	priority = 300
	picture = GFX_BAS_category_KNVD_recruitment
	allowed = { original_tag = BAS }
	visible = {		
		NOT = { has_global_flag = BAS_NKVD_coup }
	}
}

NKVD_Consolidating_Support =  {
	icon = generic_operation
	priority = 100
	picture = GFX_BAS_category_KNVD_recruitment
	allowed = {
		original_tag = BAS
	}
	visible = {
		has_country_flag = BAS_gains_Flag
	}
}

NKVD_Recruiting_Drive =  {
	icon = generic_operation
	priority = 100
	picture = GFX_BAS_category_KNVD_recruitment
	allowed = {
		original_tag = BAS
	}
	visible = {
		has_country_flag = BAS_dirlewanger_issue_flag
	}
}
