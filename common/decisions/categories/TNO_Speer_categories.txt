SGR_reich_in_crisis_category = {

	icon = decision_category_slave_revolt
	picture = GFX_GER_category_slave_revolt

	visible_when_empty = yes

	allowed = {
		original_tag = GER
	}

	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
			has_country_flag = SGR_phase3
			NOT = {
				OR = {
					has_global_flag = SGR_Slave_Revolt
					has_country_flag = SGR_Slave_Revolt_Concluded
				}
			}
		}
	}
	
	visible = {
		has_global_flag = german_civil_war_speervic
		has_country_flag = SGR_phase3
		NOT = {
			OR = {
				has_global_flag = SGR_Slave_Revolt
				has_country_flag = SGR_Slave_Revolt_Concluded
			}
		}
	}
	
	priority = 1000
}

SGR_state_of_the_reich_category = {

	icon = ger_reichskommissariats

	scripted_gui = TNO_Germany_SGR_Decisions
	visible_when_empty = yes

	allowed = {
		original_tag = GER
	}

	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
			NOT = { has_country_flag = SGR_state_reich_disable }
		}
	}
	
	visible = {
		has_global_flag = german_civil_war_speervic
		NOT = { has_country_flag = SGR_state_reich_disable }
	}
	
	priority = 300
}

SGR_Zollverein_members_investment_category = {

	icon = decision_black_market_money
	picture = GFX_GER_decision_category_zollverein 
	visible_when_empty = yes
	
	allowed = { THIS = {exists = yes} }

	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
		}
	}

	visible = {
		OR = {
			has_idea = SGR_zollverein_basic
			has_idea = SGR_zollverein_germany
		}
		NOT = {has_global_flag = SGR_natsoc_collapse}
	}

	priority = 300
}

SGR_South_African_War_category = {

	icon = decision_category_border_war
	picture = GFX_SAF_decision_category_war
	visible_when_empty = yes
	
	allowed = { tag = GER }

	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
			OR = {
				has_global_flag = south_african_war
				has_country_flag = SGR_SAF_debug
			}
			has_global_flag = german_civil_war_speervic
			NOT = {has_global_flag = SGR_natsoc_collapse}
			NOT = {has_country_flag = SGR_SAW_withdrawal}
			has_completed_focus = GER_the_southern_war
			NOT = {has_completed_focus = GER_bear_no_responsibility}
		}
	}

	visible = {
		OR = {
			has_global_flag = south_african_war
			has_country_flag = SGR_SAF_debug
		}
		has_global_flag = german_civil_war_speervic
		NOT = {has_global_flag = SGR_natsoc_collapse}
		NOT = {has_country_flag = SGR_SAW_withdrawal}
		has_completed_focus = GER_the_southern_war
		NOT = {has_completed_focus = GER_bear_no_responsibility}
	}

	priority = 400
}


SGR_collab_regimes_category = {

	visible_when_empty = no
	
	allowed = {
		original_tag = GER
	}

	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
		}
	}
	
	visible = {
		has_country_flag = SGR_collab_regimes
	}
	
	icon = decision_category_military_operation
	picture = GFX_GER_decision_map
	priority = 100
}

SGR_reichslander_category = {

	visible_when_empty = no
	
	allowed = {
		original_tag = GER
	}

	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
		}
	}
	
	visible = {
		has_country_flag = SGR_reichslander
	}
	
	icon = decision_category_military_operation
	picture = GFX_GER_decision_map
	priority = 100
}

	
SGR_science_competition_category = {

	#icon = decision_drug_trade_increase
	
	#picture = 

	allowed = {
		original_tag = GER
	}

	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
		}
	}
	
	visible = {
		has_completed_focus = GER_Supremacy_or_Bust
	}
	
}

SGR_mauritius_reunion_operation = {
	
    icon = decision_category_mtg_naval_treaties
	
	#picture = 
	
    allowed = {
		tag = GER
	}

	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
		}
	}
	
	visible = {
		has_country_flag = sgr_operation_mauritius_reunion
	}
}

GER_SGR_post_embargo_trading = {
	
    icon = GFX_decision_category_generic_foreign_policy
	
	picture = GFX_SGR_decision_usa_ger_trade
	
    allowed = {
		OR = {
			tag = GER
			tag = USA
		}
	}

	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
			NOT = {
				has_global_flag = SGR_natsoc_collapse
			}
			
			GER = {
				has_country_flag = SGR_embargo_lifted
			}
			
			#USA = {
			#	has_country_flag = SGR_embargo_lifted
			#}
			
			NOT = {
				has_country_flag = sgr_pet_finished
			}
		}
	}
	
	visible = {
		has_global_flag = german_civil_war_speervic
		NOT = {
			has_global_flag = SGR_natsoc_collapse
		}
		
		GER = {
			has_country_flag = SGR_embargo_lifted
		}
		
		#USA = {
		#	has_country_flag = SGR_embargo_lifted
		#}
		
		NOT = {
			has_country_flag = sgr_pet_finished
		}
	}
}

SGR_ircw_category = {
	icon = border_conflicts
	picture = GFX_decision_cat_icw_usa

	allowed = { original_tag = GER }
	available = { hidden_trigger = { has_country_flag = GER_iran_intervention } }
	visible = {
		country_exists = PER
		has_country_flag = GER_iran_intervention
	}

	priority = 100
}

SGR_escalation_decision_category = {
	
	icon = decision_category_slave_revolt
	
	priority = 500
	
	picture = GFX_GER_category_slave_revolt

	visible_when_empty = yes
	
    allowed = {
		tag = GER
	}
	available = {
		hidden_trigger = {
			has_global_flag = german_civil_war_speervic
			has_global_flag = SGR_Slave_Revolt
			NOT = {
				OR = {
					has_country_flag = SGR_SR_bad_end
					has_country_flag = SGR_SR_good_end
					has_country_flag = SGR_Slave_Revolt_Concluded
				}
			}
		}
	}
	
	visible = {
		has_global_flag = SGR_Slave_Revolt
		NOT = {
			OR = {
				has_country_flag = SGR_SR_bad_end
				has_country_flag = SGR_SR_good_end
				has_country_flag = SGR_Slave_Revolt_Concluded
			}
		}
	}
	
}

SGR_pacific_war_decision_category = {
	picture = GFX_decision_VIN_Vietcong
	visible_when_empty = yes
	allowed = {
		tag = GER
	}
	available = {
		hidden_trigger = {
			has_country_flag = SGR_created_RND
			has_global_flag = german_civil_war_speervic
		}
	}

	visible = {
		has_country_flag = SGR_created_RND
		has_global_flag = german_civil_war_speervic
		INS = {
			has_war_with = FRI
		}
	}
}