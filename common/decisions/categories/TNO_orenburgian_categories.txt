ita_general_improvement = {
	icon = GFX_decision_category_generic_prospect_for_resources

	picture = GFX_ORE_category_projects

	allowed  = {
		tag = ORE
	}
}

ORE_category_political_situation = {
	icon = GFX_decision_category_generic_political_actions

	picture = GFX_ORE_category_political_situation

	allowed  = {
		tag = ORE
	}
	visible = {
		NOT = {
			has_country_flag = ORE_chairman_chosen
		}
	}
}

ORE_category_centralization_gamble = {
	icon = GFX_decision_category_generic_political_actions

	picture = GFX_ORE_decisions_centralization_battle

	allowed  = {
		tag = ORE
	}	
}

ORE_category_battle_of_the_urals = {
	icon = GFX_decision_category_generic_political_actions

	picture = GFX_URL_decision_Ural_Guard

	allowed  = {
		tag = ORE
	}
}
