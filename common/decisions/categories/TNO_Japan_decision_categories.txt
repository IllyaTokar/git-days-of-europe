#JAP_society_check = {
#	allowed = {
#		original_tag = JAP 
#	}
#	visible_when_empty = yes
#
#	priority = 85
#}
JAP_debug_decisions = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}

	visible = {
		is_debug = yes
		is_ai = no
	}

	visible_when_empty = yes
	
	priority = 500
}


JAP_the_dlc_investigation = {
	icon = GFX_decision_category_generic_crisis

	picture = GFX_JAP_decision_dai_li_conspiracy

	visible = {
		has_country_flag = JAP_dlc_investigation_activated
		NOT = {
			has_variable = Jap_DLC_solved
		}
	}

	visible_when_empty = yes
}

JAP_public_relations = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills
	allowed = {
		original_tag = JAP
	}
	
	visible = {
		has_country_flag = JAP_dlc_investigation_activated
		NOT = {
			has_variable = Jap_DLC_solved
		}
	}
}
JAP_Urbanization = {
	icon = decision_category_generic_political_actions
	
	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_Tanaka_Prime_Minister
	}
}

JAP_investor_confidence = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = investor_confidence_schemes
	}
}

JAP_debt_is_just_numbers = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = just_numbers
	}
}

JAP_bail_out_the_zaibatsus = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = bail_zaibatsus
	}
}

JAP_the_public_works_campaign = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = public_works
	}
}

Jap_kaya_hardliners_making_demands_for_us_decisions = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = japan_kaya_hardliners_making_demands_of_us_flag
	}
}

JAP_kaya_sweat_into_munitions = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = sweat_into_munitions
	}
}
JAP_kaya_military_rivalry = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_completed_focus = JAP_the_militaries_friendly_rivalry	
	}
}
JAP_kido_Seijikai = {
	icon = decision_category_generic_political_Actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_Dai_Nihon_Seijikai
	}
}
JAP_kido_Upward_Approach = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_Elitist_Approach
	}
}
JAP_takagi_kun = {
	icon = decision_category_generic_political_actions
	
	picture = GFX_decision_cat_generic_mefo_bills
	
	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_takagi1
	}
}
JAP_takagi_electrify_tokyo = {
	icon = decision_category_generic_crisis
	
	#picture = GFX_
	
	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_tokyo_electrifying
	}
}
JAP_Kyoei_Doctrine = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_tree_kyoei
	}
}
JAP_Hondo_Kara_Doctrine = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_tree_hondo_kara
	}
}
JAP_Dai_Toa_Doctrine = {
	icon = decision_category_generic_political_Actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_tree_dai_toa
	}
}
JAP_Econ_Wars_Zaibatsu = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_Economic_Wars
		has_country_flag = JAP_Gov_Support_Zaibatsu
	}
}
JAP_Econ_Wars_Keiretsu = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_Economic_Wars
		has_country_flag = JAP_Gov_Support_Keiretsu
	}
}
JAP_Western_Insurrection = {
	icon = decision_category_generic_crisis

	picture = GFX_decision_JAP_western_insurrection

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_global_flag = CHI_Western_Insurrection_Crisis
		NOT = { has_global_flag = JAP_Western_Insurrection_Intervention }
	}
}
JAP_Ikeda_Coalition = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_JAP_decision_political_situation

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_Ikeda_Prime_Minister
	}
	priority = {
		base = 15
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_China = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploChinaInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Guangdong = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploGuangdongInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Guangxi = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploGuangxiInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Guizhou = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploGuizhouInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Manchuria = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploManchuriaInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Mengjiang = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploMengjiangInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Shaanxi = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploShaanxiInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Sichuan = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploSichuanInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_India = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploIndiaInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Bengal = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploBengalInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Nepal = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploNepalInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Bhutan = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploBhutanInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Afghan = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploAfghanInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Xikang = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploXikangInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Xinjiang = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploXinjiangInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Hui = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploHuiInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Ma = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploMaInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Tibet = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploTibetInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Russia = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploRussiaInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Burma = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploBurmaInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Yunnan = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploYunnanInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Thailand = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploThailandInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Phil = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploPhilInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Indonesia = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploIndonesiaInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Shounan = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploShounanInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Vietnam = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploVietnamInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Laos = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploLaosInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Sphere_Diplo_Cambodia = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_cat_generic_mefo_bills

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JapanDiploCambodiaInteractionsVisible
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_Oil_Crisis = {
	icon = decision_category_generic_political_actions

	picture = GFX_JAP_decision_middle_east_intervention

	allowed = {
		original_tag = JAP
	}
	visible = {
		has_country_flag = JAP_OIL_CRISIS
	}
	priority = {
		base = 100
	}

	visible_when_empty = yes
}

JAP_blockade_iran = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_JAP_decision_political_situation

	visible_when_empty = yes
	
	allowed = {
		original_tag = JAP
	}
	visible = {
		original_tag = JAP
		has_country_flag = JAP_enable_blockade
	}
}

JAP_indo_war = {
	icon = decision_category_generic_political_actions

	picture = GFX_decision_JAP_decision_political_situation
	
	visible_when_empty = yes
	
	allowed = {
		original_tag = JAP
		
	}
	visible = {
		original_tag = JAP
		has_global_flag = INS_war 
	}
}

JAP_kaya_influence_fight = {
	icon = decision_category_generic_political_actions
	picture = GFX_decision_JAP_decision_political_situation

	allowed = {
		original_tag = JAP 
	}

	priority = {
		base = 15
	}

	visible_when_empty = yes
	visible = { has_country_flag = JAP_Kaya_Prime_Minister }
}