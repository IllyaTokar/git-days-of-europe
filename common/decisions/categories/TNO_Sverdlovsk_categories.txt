TOB_Guardians_Of_The_Skies = {
	icon = GFX_decision_category_generic_political_actions
	picture = GFX_FAV_decision_deployment

	allowed = {
		tag = TOB
	}
	visible = {
		has_completed_focus = TOB_an_expanded_deal
		country_exists = FAV
	}
	visible_when_empty = yes
	priority = {
		base = 50
	}
}

TOB_Not_So_Red_Army = {
	icon = GFX_decision_category_military_operation
	picture = GFX_FAV_decision_diplomacy

	allowed = {
		tag = TOB
	}
	visible = {
		has_country_flag = TOB_NOT_SO_RED_ARMY
		#always = yes
	}

	visible_when_empty = yes
	priority = {
		base = 60
	}
}

TOB_sverdlovsk_regional_category = {
	icon = GFX_decision_category_generic_political_actions
	picture = GFX_VTY_political_fighting

	allowed = {
		tag = TOB
	}

	visible = {
		has_completed_focus = TOB_the_status_of_the_district
	}

	visible_when_empty = yes
	priority = {
		base = 60
	}
}

TOB_election_results_category = {
	icon = GFX_decision_category_generic_political_actions

	picture = GFX_decision_RUS_decision_political_situation

	allowed = {
		original_tag = TOB
	}
	visible = {
		has_country_flag = RUS_ELECTION_RESULTS_SHOW
	}
	visible_when_empty = yes
	priority = {
		base = 110
	}
}

TOB_democratic_politics_category = {
	icon = GFX_decision_category_generic_political_actions

	picture = GFX_decision_RUS_decision_political_situation

	allowed = {
		original_tag = TOB
	}
	visible = {
		has_country_flag = RUS_POLLS_ENABLE
	}
	visible_when_empty = yes
	priority = {
		base = 105
	}
}