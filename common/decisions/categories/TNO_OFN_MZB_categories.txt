MZB_USA_eastern_african_developments = {
	icon = decision_category_military_operation
	visible_when_empty = yes

	picture = GFX_decision_OFN_mandate_category_MZB
	
	allowed = {
		tag = MZB
		has_war = no
	}
	
	visible = {
		has_cosmetic_tag = MZB_USA
		has_war = no
	}
}