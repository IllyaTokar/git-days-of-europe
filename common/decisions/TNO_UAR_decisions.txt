UAR_unification_category = {
    uar_invite_yemen = {
          visible = {
              original_tag = UAR
			  country_exists = NYM
          }
          available = {
              original_tag = UAR
			  country_exists = NYM
			  NYM = {
				has_war = no
			  }
          }
		ai_will_do = {
			factor = 100
		}
		cost = 10
		fire_only_once = yes 
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision uar_take_syria"
			annex_country = {
				target = NYM
				transfer_troops = yes
			}
        }
    }
	uar_invite_egypt = {
          visible = {
              original_tag = UAR
			  country_exists = EGB
          }
          available = {
              original_tag = UAR
			  country_exists = EGB
			   EGB = {
				has_war = no
			  }
          }
		ai_will_do = {
			factor = 100
		}
		cost = 10
		fire_only_once = yes 
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision uar_take_syria"
			annex_country = {
				target = EGB
				transfer_troops = yes
			}
        }
    }
	uar_invite_levant = {
          visible = {
              original_tag = UAR
			  country_exists = JOR
          }
          available = {
              original_tag = UAR
			  country_exists = JOR
			  JOR = {
				has_war = no
			  }
          }
		ai_will_do = {
			factor = 100
		}
		cost = 10
		fire_only_once = yes 
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision uar_take_syria"
			annex_country = {
				target = JOR
				transfer_troops = yes
			}
        }
    }
}