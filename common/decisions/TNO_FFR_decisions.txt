FFR_Elections = {
Organize_FFR_Election = {
		allowed = { tag = FFR }
		      activation = {                       
              has_country_flag = FFR_Elections                
				}
			
				fire_only_once = yes
				available = {                           
                has_country_flag = FFR_Elections_GO                     
          }
			 is_good = no    
			 days_mission_timeout = 365            
		timeout_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout Organize_FFR_Election"
               set_country_flag = FFR_no_elections              
          }
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision Organize_FFR_Election"
               country_event = { id = free_france.7 days = 14 }         
          }
	
	
	
	}
Organize_FFR_Election_real = {

activation = {                       
              has_country_flag = FFR_Elections                
				}
fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision Organize_FFR_Election_real"
               set_country_flag = FFR_Elections_GO
          }
		}
}

