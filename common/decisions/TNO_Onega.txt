VTY_deal_with_radicals = {
	VTY_assassinate = {
		allowed = {
			tag = VTY
		}
		cost = 50
		days_re_enable = 45
		days_remove = 30
		available = {
			tag = VTY
			has_stability < 0.75
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_assassinate"
			add_stability = 0.05
			VTY = { #assassinate radical
				country_event = { days = 1 id = VTY.4 }
			}
		}
		ai_will_do = {
			factor = 40
		}
	}
	VTY_protests = {
		allowed = {
			tag = VTY
		}
		cost = 40
		days_re_enable = 180
		days_remove = 60
		available = {
			tag = VTY
			has_stability < 0.50
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_protests"
			add_stability = 0.05
			VTY = { #disperse protestors
				country_event = { days = 1 id = VTY.3 }
			}
		}
		modifier = {
			stability_weekly = -0.005
			political_power_cost = 0.1
		}
		ai_will_do = {
			factor = 40
		}
	}
	VTY_enforce_order_1 = {
		allowed = {
			tag = VTY
		}
		visible = {
			has_idea = VTY_politics_1
		}
		cost = 100
		days_remove = 30

		available = {
			tag = VTY
			has_idea = VTY_politics_1
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_enforce_order_1"
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = VTY_politics_1
				add_idea = VTY_politics_2
			}
			VTY = { #enforce order 1
				country_event = { days = 1 id = VTY.5 }
			}
		}
		modifier = {
			political_power_cost = 0.3
		}
		ai_will_do = {
			factor = 20
		}
	}

	VTY_enforce_order_2 = {
		allowed = {
			tag = VTY
		}
		visible = {
			has_idea = VTY_politics_2
		}
		cost = 100
		days_remove = 30

		available = {
			tag = VTY
			has_idea = VTY_politics_2
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_enforce_order_2"
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = VTY_politics_2
				add_idea = VTY_politics_3
			}
			VTY = { #enforce order 2
				country_event = { days = 1 id = VTY.6 }
			}
		}
		modifier = {
			political_power_cost = 0.3
		}
		ai_will_do = {
			factor = 20
		}
	}

	VTY_enforce_order_3 = {
		allowed = {
			tag = VTY
		}
		visible = {
			has_idea = VTY_politics_3
		}
		cost = 100
		days_remove = 30

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_enforce_order_3"
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = VTY_politics_3
				add_idea = VTY_politics_4
			}
			VTY = { #enforce order 3
				country_event = { days = 1 id = VTY.7 }
			}
		}
		modifier = {
			political_power_cost = 0.3
		}
		ai_will_do = {
			factor = 20
		}
	}
}

VTY_deal_with_bandits = {
	VTY_assault = {
		allowed = {
			tag = VTY
		}
		cost = 50
		days_re_enable = 75
		days_remove = 45
		available = {
			tag = VTY
		}
		modifier = {
			political_power_cost = 0.1
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_assault"
			add_manpower = -300
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = 500
				producer = VTY
			}
			add_war_support = 0.05
			VTY = { #bandit cache
				country_event = { days = 1 id = VTY.8 }
			}
		}
		ai_will_do = {
			factor = 10
		}
	}
	VTY_deploy = {
		allowed = {
			tag = VTY
		}
		cost = 40
		days_re_enable = 75
		days_remove = 50
		available = {
			tag = VTY
			has_stability < 0.65
		}

		modifier = {
			political_power_cost = 0.1
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_deploy"
			add_manpower = -150
			add_war_support = 0.05
			add_stability = 0.05
			VTY = { #protect railroad
				country_event = { days = 1 id = VTY.9 }
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_destroy_1 = {
		allowed = {
			tag = VTY
		}
		visible = {
			has_idea = VTY_bandits_1
		}
		cost = 100
		days_re_enable = 75
		days_remove = 30
		available = {
			tag = VTY
			has_idea = VTY_bandits_1
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_destroy_1"
			add_manpower = -300
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = VTY_bandits_1
				add_idea = VTY_bandits_2
			}
			VTY = { #destroy bandits 1
				country_event = { days = 1 id = VTY.10 }
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	VTY_destroy_2 = {
		allowed = {
			tag = VTY
		}
		visible = {
			has_idea = VTY_bandits_2
		}
		cost = 100
		days_re_enable = 75
		days_remove = 30
		available = {
			tag = VTY
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_destroy_2"
			add_manpower = -600
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = VTY_bandits_2
				add_idea = VTY_bandits_3
			}
			VTY = { #destroy bandits 1
				country_event = { days = 1 id = VTY.16 }
			}
		}
		ai_will_do = {
			factor = 1
		}
	}

	VTY_destroy_3 = {
		allowed = {
			tag = VTY
		}
		visible = {
			has_idea = VTY_bandits_3
		}
		cost = 100
		days_re_enable = 75
		days_remove = 30
		available = {
			tag = VTY

		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_destroy_3"
			add_manpower = -1000
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = VTY_bandits_3
				add_idea = VTY_bandits_4
			}
			VTY = { #destroy bandits 3
				country_event = { days = 1 id = VTY.17 }
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}

VTY_anti_commie_guard = { #vologda volunteers
	VTY_guard_VOL = {
		allowed = {
			tag = VTY
		}
		visible = {
			WRS = {
				exists = yes
			}
			VOL = {
				exists = yes
			}
		}
		custom_cost_trigger = {
			has_equipment = { infantry_equipment > 185 }
		}

		fire_only_once = yes

		available = {
			VOL = { has_war_with = WRS }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_guard_VOL"
			VOL = {
				country_event = { id = VTY.2 days = 3 }
			}
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -185
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_guard_NIK = {
		allowed = {
			tag = VTY
		}
		visible = {
			WRS = {
				exists = yes
			}
			NIK = {
				exists = yes
				NOT = {
					has_government = communist
					has_government = socialist
				}
			}
		}
		custom_cost_trigger = {
			has_equipment = { infantry_equipment > 185 }
		}

		fire_only_once = yes

		available = {
			NIK = { has_war_with = WRS }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_guard_NIK"
			NIK = {
				country_event = { id = VTY.2 days = 3 }
			}
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -185
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_guard_KST = { #kostroma volunteers
		allowed = {
			tag = VTY
		}
		visible = {
			WRS = {
				exists = yes
			}
			KST = {
				exists = yes
			}
		}
		custom_cost_trigger = {
			has_equipment = { infantry_equipment > 185 }
		}

		fire_only_once = yes

		available = {
			KST = { has_war_with = WRS }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_guard_KST"
			KST = {
				country_event = { id = VTY.2 days = 3 }
			}
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -185
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_guard_TAR = { #tatar volunteers
		allowed = {
			tag = VTY
		}
		visible = {
			WRS = {
				exists = yes
			}
			TAR = {
				exists = yes
			}
		}
		custom_cost_trigger = {
			has_equipment = { infantry_equipment > 185 }
		}

		fire_only_once = yes

		available = {
			TAR = { has_war_with = WRS }
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_guard_TAR"
			TAR = {
				country_event = { id = VTY.2 days = 3 }
			}
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -185
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_guard_GAY = { #gay volunteers c;
		allowed = {
			tag = VTY
		}
		visible = {
			WRS = {
				exists = yes
			}
			GAY = {
				exists = yes
			}
		}

		fire_only_once = yes

		custom_cost_trigger = {
			has_equipment = { infantry_equipment > 185 }
		}

		available = {
			GAY = { has_war_with = WRS }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_guard_GAY"
			GAY = {
				country_event = { id = VTY.2 days = 3 }
			}
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -185
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_guard_BKR = { #bashkiria volunteers
		allowed = {
			tag = VTY
		}
		visible = {
			WRS = {
				exists = yes
			}
			BKR = {
				exists = yes
			}
		}
		custom_cost_trigger = {
			has_equipment = { infantry_equipment > 185 }
		}

		fire_only_once = yes

		available = {
			BKR = { has_war_with = WRS }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_guard_BKR"
			BKR = {
				country_event = { id = VTY.2 days = 3 }
			}
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -185
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}

VTY_communal_industries = {
	VTY_privatise_fisheries = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
			NOT = {
				owns_state = 214
			}
		}
		cost = 50
		fire_only_once = yes
		days_remove = 80
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_privatise_fisheries"
			add_stability = -0.025
			add_war_support = -0.03
			TNO_worsen_poverty_med = yes
		}
		remove_effect = {
			add_ideas = tno_trade_laws_export_focus
			set_country_flag = VTY_fisheries_privatised
			add_popularity = {
				ideology = communist
				popularity = -0.05
			}
			add_stability = 0.025
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_develop_infrastructure = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
			NOT = {
				owns_state = 214
			}
		}
		fire_only_once = yes
		cost = 75
		days_remove = 100
		available = {
			has_country_flag = VTY_fisheries_privatised
		}
		modifier = {
			consumer_goods_factor = 0.075
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_develop_infrastructure"
		}
		remove_effect = {
			859 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			858 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			TNO_improve_poverty_med = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_foreign_investment = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
			NOT = {
				owns_state = 214
			}
		}
		cost = 65
		days_remove = 50
		available = {
			NOT = { num_of_factories < 14 }
			has_country_flag = VTY_fisheries_privatised
			custom_trigger_tooltip = {
				tooltip = warlord_development_industrial_tt
				any_owned_state = {
					free_building_slots = {
						building = arms_factory
						size > 0
						include_locked = no
					}
				}
			}
		}
		modifier = {
			consumer_goods_factor = 0.05
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_foreign_investment"
		}
		remove_effect = {
			random_list = {
				50 = {
					random_owned_state = {
						limit = {
							free_building_slots = {
								building = arms_factory
								size > 0
								include_locked = no
							}
						}
						add_building_construction = {
							type = arms_factory
							level = 1
							instant_build = yes
						}
					}
				}
				50 = {
					random_owned_state = {
						limit = {
							free_building_slots = {
								building = industrial_complex
								size > 0
								include_locked = no
							}
						}
						add_building_construction = {
							type = industrial_complex
							level = 1
							instant_build = yes
						}
					}
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}

VTY_modernization = {
	VTY_ark_modernization = {
		allowed = {
			tag = VTY
		}
		available = {
			NOT = {
				has_country_flag = VTY_modernizing
			}
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		highlight_states = {
			state = 214
		}
		cost = 60
		fire_only_once = yes
		days_remove = 50
		modifier = {
			consumer_goods_factor = 0.075
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_privatise_fisheries"
			set_country_flag = VTY_modernizing
		}
		remove_effect = {
			clr_country_flag = VTY_modernizing
			TNO_improve_industrial_equipment_low = yes
			214 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			862 = {
				add_building_construction = {
					type = infrastructure
					level = 2
					instant_build = yes
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_ukh_modernization = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_modernizing
			}
		}
		highlight_states = {
			state = 869
		}
		cost = 60
		fire_only_once = yes
		days_remove = 50
		modifier = {
			consumer_goods_factor = 0.075
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_privatise_fisheries"
			set_country_flag = VTY_modernizing
		}
		remove_effect = {
			clr_country_flag = VTY_modernizing
			TNO_improve_industrial_equipment_low = yes
			866 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			869 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			262 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_vol_modernization = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_modernizing
			}
		}
		highlight_states = {
			state = 351
		}
		cost = 60
		fire_only_once = yes
		days_remove = 50
		modifier = {
			consumer_goods_factor = 0.075
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_privatise_fisheries"
			set_country_flag = VTY_modernizing
		}
		remove_effect = {
			clr_country_flag = VTY_modernizing
			TNO_improve_industrial_equipment_low = yes
			857 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			351 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			856 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_kom_modernization = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_modernizing
			}
		}
		highlight_states = {
			state = 397
		}
		cost = 60
		fire_only_once = yes
		days_remove = 50
		modifier = {
			consumer_goods_factor = 0.075
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_privatise_fisheries"
			set_country_flag = VTY_modernizing
		}
		remove_effect = {
			clr_country_flag = VTY_modernizing
			TNO_improve_industrial_equipment_low = yes
			867 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			868 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			397 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_kot_modernization = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_modernizing
			}
		}
		highlight_states = {
			state = 861
		}
		cost = 60
		fire_only_once = yes
		days_remove = 50
		modifier = {
			consumer_goods_factor = 0.075
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_privatise_fisheries"
			set_country_flag = VTY_modernizing
		}
		remove_effect = {
			clr_country_flag = VTY_modernizing
			TNO_improve_industrial_equipment_low = yes
			861 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			860 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			870 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_modernize_research_facilities = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		modifier = {
			consumer_goods_factor = 0.1
		}
		cost = 80
		fire_only_once = yes
		days_remove = 100
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_modernize_research_facilities"
		}
		remove_effect = {
			TNO_improve_research_facilities_med = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_modernize_agriculture = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		modifier = {
			consumer_goods_factor = 0.1
		}
		cost = 80
		fire_only_once = yes
		days_remove = 100
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_modernize_agriculture"
		}
		remove_effect = {
			TNO_improve_agriculture_med = yes
		}
		ai_will_do = {
			factor = 1
		}
	}
}

VTY_faction = {
	VTY_faction_CHU = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_diplomats_sent
			}
		}
		highlight_states = {
			state = 252
		}
		cost = 75
		fire_only_once = yes
		days_remove = 15
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_faction_CHU"
			set_country_flag = VTY_diplomats_sent
		}
		remove_effect = {
			country_event = { id = VTY.52 }
			clr_country_flag = VTY_diplomats_sent
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_faction_TAR = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_diplomats_sent
			}
		}
		highlight_states = {
			state = 249
		}
		cost = 75
		fire_only_once = yes
		days_remove = 15
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_faction_TAR"
			set_country_flag = VTY_diplomats_sent
		}
		remove_effect = {
			country_event = { id = VTY.54 }
			clr_country_flag = VTY_diplomats_sent
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_faction_SAM = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_diplomats_sent
			}
		}
		highlight_states = {
			state = 850
		}
		cost = 75
		fire_only_once = yes
		days_remove = 15
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_faction_SAM"
			set_country_flag = VTY_diplomats_sent
		}
		remove_effect = {
			country_event = { id = VTY.55 }
			clr_country_flag = VTY_diplomats_sent
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_faction_KIR = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_diplomats_sent
			}
		}
		highlight_states = {
			state = 400
		}
		cost = 75
		fire_only_once = yes
		days_remove = 15
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_faction_KIR"
			set_country_flag = VTY_diplomats_sent
		}
		remove_effect = {
			country_event = { id = VTY.56 }
			clr_country_flag = VTY_diplomats_sent
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_faction_GAY = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_diplomats_sent
			}
		}
		highlight_states = {
			state = 399
		}
		cost = 75
		fire_only_once = yes
		days_remove = 15
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_faction_GAY"
			set_country_flag = VTY_diplomats_sent
		}
		remove_effect = {
			country_event = { id = VTY.57 }
			clr_country_flag = VTY_diplomats_sent
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_faction_BKR = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_diplomats_sent
			}
		}
		highlight_states = {
			state = 651
		}
		cost = 75
		fire_only_once = yes
		days_remove = 15
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_faction_BKR"
			set_country_flag = VTY_diplomats_sent
		}
		remove_effect = {
			country_event = { id = VTY.58 }
			clr_country_flag = VTY_diplomats_sent
		}
		ai_will_do = {
			factor = 1
		}
	}
	VTY_faction_PRM = {
		allowed = {
			tag = VTY
		}
		allowed = {
			
			tag = VTY
		}
		visible = {
		}
		available = {
			NOT = {
				has_country_flag = VTY_diplomats_sent
			}
		}
		highlight_states = {
			state = 753
		}
		cost = 75
		fire_only_once = yes
		days_remove = 15
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision VTY_faction_PRM"
			set_country_flag = VTY_diplomats_sent
		}
		remove_effect = {
			country_event = { id = VTY.59 }
			clr_country_flag = VTY_diplomats_sent
		}
		ai_will_do = {
			factor = 1
		}
	}
}
