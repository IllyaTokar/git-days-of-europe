COG_internal_relations = {
	COG_hire_belgians = {
		allowed = { tag = COG }

		cost = 20

		days_re_enable = 20

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_hire_belgians"
			custom_effect_tooltip = Hire_Belgian_Mercenaries_tt
			custom_effect_tooltip = COG_mercs_bel_likes_tt
			add_manpower = 600
			army_experience = 5
			add_to_variable = { cog_bel_loyalty = 0.05 }
			COG_update_merc_variables = yes
		}
	}

	COG_hire_english = {
		allowed = { tag = COG }

		cost = 20

		days_re_enable = 20

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_hire_english"
			custom_effect_tooltip = Hire_Anglo_Mercenaries_tt
			custom_effect_tooltip = COG_mercs_eng_likes_tt
			add_manpower = 100
			add_equipment_to_stockpile = {
				type = support_equipment
				amount = 50
				producer = ENG
			}
			add_equipment_to_stockpile = {
				type = motorized_equipment
				amount = 30
				producer = ENG
			}
			add_to_variable = { cog_eng_loyalty = 0.05 }
			COG_update_merc_variables = yes
		}
	}

	COG_hire_french = {
		allowed = { tag = COG }

		cost = 20

		days_re_enable = 20

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_hire_french"
			custom_effect_tooltip = Hire_French_Mercenaries_tt
			custom_effect_tooltip = COG_mercs_fra_likes_tt
			add_manpower = 250

			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = 250
				producer = FFR
			}
			add_to_variable = { cog_fra_loyalty = 0.05 }
			add_to_variable = { misc_income = 1 }
			COG_update_merc_variables = yes
		}
	}


	COG_hire_natives = {
		allowed = { tag = COG }

		cost = 50

		days_re_enable = 60

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_hire_natives"
			custom_effect_tooltip = Hire_Native_Mercenaries_tt
			custom_effect_tooltip = COG_lower_native_tension_tt
			add_to_variable = { cog_native_tensions = -0.05 }

			add_stability = 0.03
			add_war_support = 0.03
			add_manpower = 300
			add_to_variable = { misc_income = 1 }
			COG_update_merc_variables = yes
		}
	}







	COG_bolster_garrisons = {
		allowed = { tag = COG }

		days_re_enable = 90

		days_remove = 60

		available = {
			has_equipment = { infantry_equipment > 99 }
		}

		cost = 35

		modifier = {
			stability_weekly = -0.005
			weekly_manpower = -75
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_bolster_garrisons"
			custom_effect_tooltip = Suppress_Internal_Dissent_tt
			custom_effect_tooltip = COG_lower_native_tension_lot_tt
			add_to_variable = { cog_native_tensions = -0.1 }
			country_event = { id = COG_focus.999 }
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -100
				}
		}
	}


		COG_hire_pilots = {
			allowed = { tag = COG }

			days_re_enable = 60

			cost = 55
			days_remove = 50

			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Decision COG_hire_pilots"
				custom_effect_tooltip = Encourage_Oil_Well_Production_tt
				custom_effect_tooltip = COG_investors_likes_lot_tt
				custom_effect_tooltip = COG_raise_native_tension_lot_tt
				add_to_variable = { cog_native_tensions = 0.1 }
				add_to_variable = { COG_Investor_Happiness = 0.1 }
				country_event = { id = COG_focus.999 }
			}

			modifier = {
				weekly_manpower = -125
				production_oil_factor = 0.15
				fuel_gain_factor = 0.1
			}
		}


	COG_guard_investments = {
		allowed = { tag = COG }

		days_re_enable = 90

		days_remove = 45

		cost = 50

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_guard_investments"
			custom_effect_tooltip = Police_The_Plantations_tt
			custom_effect_tooltip = COG_investors_likes_lot_tt
			custom_effect_tooltip = COG_raise_native_tension_lot_tt
			add_to_variable = { cog_native_tensions = 0.1 }
			add_to_variable = { COG_Investor_Happiness = 0.1 }
			cog_hoa_update = yes
			country_event = { id = COG_focus.999 }
		}

		modifier = {
			local_resources_factor = 0.15
			weekly_manpower = -150
		}
		remove_effect = {
			539 = {
			add_resource = {
				type = rubber
				amount = 1
				}
			}
		}
	}



	COG_guard_construction = {
		allowed = { tag = COG }

		days_re_enable = 90

		days_remove = 45

		cost = 40

		modifier = {
			weekly_manpower = -100
			production_speed_buildings_factor = 0.1
			consumer_goods_factor = -0.05
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_guard_construction"
			custom_effect_tooltip = Guard_Construction_Projects_tt
			custom_effect_tooltip = COG_investors_likes_tt
			custom_effect_tooltip = COG_raise_native_tension_tt
			add_to_variable = { cog_native_tensions = 0.05 }
			add_to_variable = { COG_Investor_Happiness = 0.05 }
			cog_hoa_update = yes
			country_event = { id = COG_focus.999 }
		}
	}



	COG_guard_mines = {
		allowed = { tag = COG }

		days_re_enable = 90

		cost = 40

		days_remove = 40

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_guard_mines"
			custom_effect_tooltip = Encourage_Mining_Operations_tt
			custom_effect_tooltip = COG_investors_likes_tt
			custom_effect_tooltip = COG_raise_native_tension_tt
			add_to_variable = { cog_native_tensions = 0.05 }
			add_to_variable = { COG_Investor_Happiness = 0.05 }
			cog_hoa_update = yes
			country_event = { id = COG_focus.999 }
		}

		modifier = {
			weekly_manpower = -125
			industrial_capacity_factory = 0.1
		}

		remove_effect = {
			718 = {
			add_resource = {
				type = steel
				amount = 1
				}
			}
		}
	}
	COG_give_equipment_to_the_belgians = {
		allowed = { tag = COG }

		available = {
			has_war = no
			OR = {
				has_equipment = { infantry_equipment > 199  }
			}
		}

		cost = 15
		days_re_enable = 20

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_give_equipment_to_the_belgians"
			custom_effect_tooltip = Sell_Equipment_To_The_Belgians_tt
			custom_effect_tooltip = COG_mercs_bel_likes_lot_tt
			add_to_variable = { cog_bel_loyalty = 0.1}
			COG_update_merc_variables = yes

	 		add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -200
			}
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_give_equipment_to_the_french = {
		allowed = { tag = COG }

		available = {
			has_war = no
			OR = {
				has_equipment = { infantry_equipment > 149 }
			}
		}

		cost = 15
		days_re_enable = 20

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_give_equipment_to_the_belgians"
			custom_effect_tooltip = Sell_Equipment_To_The_French_tt
			custom_effect_tooltip = COG_mercs_fra_likes_lot_tt
			add_to_variable = { cog_fra_loyalty = 0.1}
			COG_update_merc_variables = yes

	 		add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -150
			}
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_give_equipment_to_the_anglo = {
		allowed = { tag = COG }

		available = {
			has_war = no

			has_equipment = { infantry_equipment > 49 }
			has_equipment = { motorized_equipment > 14 }
		}

		fire_only_once = no

		cost = 15
		days_re_enable = 20

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_give_equipment_to_the_belgians"
			custom_effect_tooltip = Sell_Equipment_To_The_English_tt
			custom_effect_tooltip = COG_mercs_eng_likes_lot_tt
			add_to_variable = { cog_eng_loyalty = 0.1 }
			COG_update_merc_variables = yes

	 		add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -50
			}

			add_equipment_to_stockpile = {
				type = motorized_equipment
				amount = -15
			}
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_give_equipment_to_the_natives = {
		allowed = { tag = COG }

		available = {
			has_war = no

			has_equipment = { infantry_equipment > 249 }
		}

		cost = 30
		days_re_enable = 30

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_give_equipment_to_the_belgians"
			custom_effect_tooltip = Sell_Equipment_To_The_Natives_tt
			custom_effect_tooltip = COG_lower_native_tension_lot_tt
			add_to_variable = { cog_native_tensions = -0.1 }
			COG_update_merc_variables = yes

	 		add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = -250
			}
		}

		ai_will_do = {
			factor = 0
		}
	}

    COG_buy_infantry_equipment = {
		allowed = { tag = COG }

		available = {
			has_war = yes
			has_political_power > 10
			has_army_experience > 1
		}

		visible = {
            has_completed_focus = COG_ask_for_more_guns
        }

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_buy_infantry_equipment"
	 		add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = 600
			}
			custom_effect_tooltip = econ_decrease_money_reserves_small
			 add_to_variable = { money_reserves = -10 }
			 add_political_power = -10
			 army_experience = -1
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_buy_panzers = {
		allowed = { tag = COG }

		available = {
			has_war = yes
			country_exists = MZB
			MZB = {
				has_cosmetic_tag = MZB_GER
			}
			has_political_power > 10
			has_army_experience > 5
		}

		visible = {
            has_completed_focus = COG_buy_panzers_to_germania
        }

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_buy_panzers"
	 		add_equipment_to_stockpile = {
				type = MBT_equipment
				amount = 100
			}
			add_to_variable = { money_reserves = -10 }
			custom_effect_tooltip = econ_decrease_money_reserves_small
			add_political_power = -10
			army_experience = -5
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_buy_helicopters = {
		allowed = { tag = COG }

		available = {
			has_war = yes
			country_exists = ANG
			ANG = {
				has_cosmetic_tag = ANG_GER
			}
			has_political_power > 10
			has_army_experience > 7
		}

		visible = {
            has_completed_focus = COG_the_sudwestafrikaner_stockpiles
        }

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_buy_planes"
	 		add_equipment_to_stockpile = {
				type = scout_helicopter_equipment_1
				amount = 100
			}
	 		add_to_variable = { money_reserves = -10 }
			custom_effect_tooltip = econ_decrease_money_reserves_small
			add_political_power = -10
			army_experience = -7
		}

		ai_will_do = {
			factor = 0
		}
	}
}

COG_welcome_in_the_heart_of_afrika = {
	COG_vacation_kollner = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		visible = {
			has_global_flag = man_on_the_moon
		}

		days_remove = 21

		cost = 25
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_kollner"
			country_event = {
				id = COG_misc.1
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 1
	 		}
			custom_effect_tooltip = COG_celebrity_brings_investors_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.2
			}
			add_to_variable = { COG_investor_happiness = 0.03 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_vacation_speer_jr = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		days_remove = 21

		cost = 125
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_speer_jr"
			country_event = {
				id = COG_misc.11
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 2
	 		}
			custom_effect_tooltip = COG_celebrity_brings_investors_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.12
			}
			add_to_variable = { COG_investor_happiness = 0.05 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_vacation_kaselowsky = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		days_remove = 21

		cost = 50
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_schenck"
			country_event = {
				id = COG_misc.5
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 3
	 		}
			custom_effect_tooltip = COG_celebrity_brings_investors_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.6
			}
			add_to_variable = { COG_investor_happiness = 0.02 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_vacation_manstein = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		days_remove = 21

		cost = 75
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_manstein"
			country_event = {
				id = COG_misc.9
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 4
	 		}
			custom_effect_tooltip = COG_celebrity_brings_investors_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.10
			}
			add_to_variable = { COG_investor_happiness = 0.025 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_vacation_nordhoff = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		days_remove = 21

		cost = 75
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_nordhoff"
			country_event = {
				id = COG_misc.3
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 5
	 		}

	 		custom_effect_tooltip = COG_investors_pleased_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.4
			}
			add_to_variable = { COG_investor_happiness = 0.075 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_vacation_von_heydekampf = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		days_remove = 21

		cost = 100
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_von_heydekampf"
			country_event = {
				id = COG_misc.7
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 6
	 		}

	 		custom_effect_tooltip = COG_investors_pleased_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.8
			}
			add_to_variable = { COG_investor_happiness = 0.075 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_vacation_abs = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		days_remove = 21

		cost = 150
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_abs"
			country_event = {
				id = COG_misc.16
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 7
	 		}

	 		custom_effect_tooltip = COG_investors_pleased_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.17
			}
			add_to_variable = { COG_investor_happiness = 0.075 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_vacation_flick = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		days_remove = 21

		cost = 150
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_flick"
			country_event = {
				id = COG_misc.18
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 10
	 		}

	 		custom_effect_tooltip = COG_investors_pleased_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.19
			}
			add_to_variable = { COG_investor_happiness = 0.075 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_vacation_geilenberg = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		days_remove = 21

		cost = 150
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_geilenberg"
			country_event = {
				id = COG_misc.20
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 9
	 		}

	 		custom_effect_tooltip = COG_investors_pleased_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.21
			}
			add_to_variable = { COG_investor_happiness = 0.075 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_vacation_siemens = {
		allowed = { tag = COG }

		available = {
			has_war = no
			check_variable = { cog_vip_id = 0 }
		}

		days_remove = 21

		cost = 150
		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_vacation_siemens"
			country_event = {
				id = COG_misc.22
				days = 7
			}
			set_variable = {
	 			cog_vip_id = 8
	 		}

	 		custom_effect_tooltip = COG_investors_pleased_tt
		}

		remove_effect = {
			country_event = {
				id = COG_misc.23
			}
			add_to_variable = { COG_investor_happiness = 0.075 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
		}

		ai_will_do = {
			factor = 0
		}
	}
}

COG_the_mercs = {
	COG_hire_expert_mercs = {
		allowed = {
		}

		available = {
			has_war = yes
		}

		visible = {
			check_variable = { COG_mercs_experts_units > 0 }
		}
		cost = 25

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_hire_expert_mercs"
			add_to_variable = {
	 			COG_mercs_experts_units = -1
	 		}
	 		random_owned_controlled_state = {
				create_unit = {
					division = "name = \"Elitesöldnereinheit\" division_template = \"Elitesöldnereinheit\" start_experience_factor = 0.8 start_equipment_factor = 0.9"
					owner = ROOT
				}
			}
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_hire_trained_mercs = {
		allowed = {
		}

		available = {
			has_war = yes
		}

		visible = {
			check_variable = { COG_mercs_trained_units > 0 }
		}

		cost = 25

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_hire_trained_mercs"
			add_to_variable = {
	 			COG_mercs_trained_units = -1
	 		}
	 		random_owned_controlled_state = {
				create_unit = {
					division = "name = \"Söldnereinheit\" division_template = \"Söldnereinheit\" start_experience_factor = 0.6 start_equipment_factor = 0.7"
					owner = ROOT
				}
			}
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_hire_untrained_mercs = {
		allowed = {
		}

		available = {
			has_war = yes
		}

		visible = {
			check_variable = { COG_mercs_untrained_units > 0 }
		}

		cost = 25

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_hire_untrained_mercs"
			add_to_variable = {
	 			COG_mercs_untrained_units = -1
	 		}
	 		random_owned_controlled_state = {
				create_unit = {
					division = "name = \"Aushilfssöldnereinheit\" division_template = \"Aushilfssöldnereinheit\" start_experience_factor = 0.4 start_equipment_factor = 0.5"
					owner = ROOT
				}
			}
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_hire_native_bandits = {
		allowed = {
		}

		available = {
			has_war = yes
		}

		visible = {
			check_variable = { COG_mercs_bandits_units > 0 }
		}

		cost = 25

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_hire_native_bandits"
			add_to_variable = {
	 			COG_mercs_bandits_units = -1
	 		}
	 		random_owned_controlled_state = {
				create_unit = {
					division = "name = \"Eingeboreneneinheit \" division_template = \"Eingeboreneneinheit \" start_experience_factor = 0.2 start_equipment_factor = 0.3"
					owner = ROOT
				}
			}
			add_manpower = -500
		}

		ai_will_do = {
			factor = 0
		}
	}
}

COG_hunting_season = {
	COG_retry_gorilla_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_gorilla_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_gorilla_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_gorilla_hunt"
			clr_country_flag = COG_hunt_gorilla_failed
			country_event = COG_hunt.210
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_retry_buffalo_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_buffalo_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_buffalo_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_buffalo_hunt"
			clr_country_flag = COG_hunt_buffalo_failed
			country_event = COG_hunt.220
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_retry_kudu_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_kudu_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_kudu_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_kudu_hunt"
			clr_country_flag = COG_hunt_kudu_failed
			country_event = COG_hunt.230
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_retry_lion_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_lion_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_lion_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_lion_hunt"
			clr_country_flag = COG_hunt_lion_failed
			country_event = COG_hunt.240
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_retry_elephant_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_elephant_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_elephant_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_elephant_hunt"
			clr_country_flag = COG_hunt_elephant_failed
			country_event = COG_hunt.250
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_retry_topi_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_topi_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_topi_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_topi_hunt"
			clr_country_flag = COG_hunt_topi_failed
			country_event = COG_hunt.260
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_retry_cheetah_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_cheetah_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_cheetah_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_cheetah_hunt"
			clr_country_flag = COG_hunt_cheetah_failed
			country_event = COG_hunt.270
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_retry_pangolin_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_pangolin_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_pangolin_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_pangolin_hunt"
			clr_country_flag = COG_hunt_pangolin_failed
			country_event = COG_hunt.280
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_retry_hog_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_hog_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_hog_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_hog_hunt"
			clr_country_flag = COG_hunt_hog_failed
			country_event = COG_hunt.290
		}

		ai_will_do = {
			factor = 0
		}
	}

	COG_retry_giraffe_hunt = {
		allowed = {
		}

		available = {
			has_country_flag = COG_hunt_giraffe_failed
			check_variable = { cog_hunting_id = 0 }
		}

		visible = {
			has_country_flag = COG_hunt_giraffe_failed
		}

		cost = 25
		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_retry_giraffe_hunt"
			clr_country_flag = COG_hunt_giraffe_failed
			country_event = COG_hunt.2110
		}

		ai_will_do = {
			factor = 0
		}
	}
	COG_Muller_is_bored = { #where the elephants at, I just want to hunt
		allowed = { tag = COG }
		available = {
			has_country_flag = COG_finished_the_hunt
		}
		activation = {
			has_country_flag = COG_hunting_season_flag
		}
		is_good = no
		days_mission_timeout = 30
		timeout_effect = {
			subtract_from_variable = { COG_muller_boredom = 0.01 }
		}
		complete_effect = {
			clr_country_flag = COG_finished_the_hunt
		}
	}
}

COG_investor_favors = {
	COG_ask_investors_to_produce_small_arms = {
		allowed = { tag = COG }

		available = {
			has_war = no
		}

		cost = 25

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_produce_small_arms"
			custom_effect_tooltip = Ask_Investors_to_produce_Small_Arms_tt
			subtract_from_variable = { COG_investor_happiness = 0.02 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			custom_effect_tooltip = COG_investors_displeased_tt
			cog_hoa_update = yes
			add_equipment_to_stockpile = {
				type = infantry_equipment
				amount = 500
			}
		}
		days_re_enable = 25

		ai_will_do = {
			factor = 0
		}
	}

	COG_ask_investors_to_produce_motorized_equipment = {
		allowed = { tag = COG }

		available = {
			has_war = no
		}

		cost = 50

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_produce_motorized_equipment"
			custom_effect_tooltip = Ask_Investors_To_Produce_Motorized_Equipment_tt
			subtract_from_variable = { COG_investor_happiness = 0.02 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			custom_effect_tooltip = COG_investors_displeased_tt
			cog_hoa_update = yes
			add_equipment_to_stockpile = {
				type = motorized_equipment
				amount = 50
			}
		}
		days_re_enable = 50

		ai_will_do = {
			factor = 0
		}
	}

	COG_ask_investors_to_produce_tanks = {
		allowed = { tag = COG }

		available = {
			has_war = no
		}

		cost = 50

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_produce_tanks"
			custom_effect_tooltip = Ask_Investors_To_Produce_Tanks
			subtract_from_variable = { COG_investor_happiness = 0.02 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			custom_effect_tooltip = COG_investors_displeased_tt
			cog_hoa_update = yes
			add_equipment_to_stockpile = {
				type = light_tank_equipment
				amount = 20
			}
		}
		days_re_enable = 50

		ai_will_do = {
			factor = 0
		}
	}

	COG_ask_investors_to_increase_steel_production_quotas = {
		allowed = { tag = COG }

		available = {
			has_war = no
			NOT = { has_country_flag = COG_Investors_Gone }
		}

		visible = { has_completed_focus = COG_increase_exploitation }

		cost = 50

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_increase_steel_production_quotas"
			custom_effect_tooltip = Ask_Investors_to_Raise_Steel_Quotas
			subtract_from_variable = { COG_investor_happiness = 0.05 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			custom_effect_tooltip = COG_investors_displeased_tt
			cog_hoa_update = yes
			add_timed_idea = {
				idea = COG_Raised_Steel_Quotas
				days = 180
			}
		}
		days_re_enable = 180

		ai_will_do = {
			factor = 0
		}
	}

	COG_ask_investors_to_increase_rubber_production_quotas = {
		allowed = { tag = COG }

		available = {
			has_war = no
			has_completed_focus = COG_increase_exploitation
			NOT = { has_country_flag = COG_Investors_Gone }
		}

		visible = { has_completed_focus = COG_increase_exploitation }

		cost = 50

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_increase_rubber_production_quotas"
			custom_effect_tooltip = Ask_Investors_to_Raise_Rubber_Quotas
			subtract_from_variable = { COG_investor_happiness = 0.05 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			custom_effect_tooltip = COG_investors_displeased_tt
			cog_hoa_update = yes
			add_timed_idea = {
				idea = COG_Raised_Rubber_Quotas
				days = 180
			}
		}
		days_re_enable = 180

		ai_will_do = {
			factor = 0
		}
	}

	COG_ask_investors_to_contruct_infrastructure = {
		allowed = { tag = COG }

		available = {
			has_war = no
			NOT = { has_country_flag = COG_Investors_Gone }
		}

		cost = 25

		visible = { has_completed_focus = COG_the_road_to_cabinda }

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_contruct_infrastructure"
			subtract_from_variable = { COG_investor_happiness = 0.02 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			custom_effect_tooltip = COG_investors_displeased_tt
			cog_hoa_update = yes
			random_owned_controlled_state = {
				add_building_construction = {
					type = infrastructure
					level = 1
				}
			}
		}
		days_re_enable = 25

		ai_will_do = {
			factor = 0
		}
	}

	COG_ask_investors_to_finance_civil_industry = {
		allowed = { tag = COG }

		available = {
			has_war = no
			has_completed_focus = COG_the_hub_of_afrika
			NOT = { has_country_flag = COG_Investors_Gone }
		}

		cost = 25

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_finance_civil_industry"
			custom_effect_tooltip = Ask_Investors_to_Finance_Civil_Industry
			subtract_from_variable = { COG_investor_happiness = 0.02 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			custom_effect_tooltip = COG_investors_displeased_tt
			cog_hoa_update = yes
			random_owned_controlled_state = {
				add_building_construction = {
					type = industrial_complex
					level = 1
				}
			}
		}
		days_re_enable = 25

		ai_will_do = {
			factor = 0
		}
	}

	COG_ask_investors_to_fund_arms_production = {
		allowed = { tag = COG }

		available = {
			has_war = no
			has_completed_focus = COG_mercenary_industries
			NOT = { has_country_flag = COG_Investors_Gone }
		}

		cost = 25

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_fund_arms_production"
			custom_effect_tooltip = Ask_Investors_to_Fund_Arms_Production
			subtract_from_variable = { COG_investor_happiness = 0.02 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			custom_effect_tooltip = COG_investors_displeased_tt
			cog_hoa_update = yes
			random_owned_controlled_state = {
				add_building_construction = {
					type = arms_factory
					level = 1
				}
			}

		}
		days_re_enable = 25

		ai_will_do = {
			factor = 0
		}
	}

	COG_ask_investors_to_fund_research_project = {
		allowed = { tag = COG }

		available = {
			has_war = no
			NOT = { has_country_flag = COG_Investors_Gone }
		}

		visible = { has_completed_focus = COG_the_global_market }

		cost = 75

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_fund_research_project"
			custom_effect_tooltip = Ask_Investors_to_Fund_Research_Project
			subtract_from_variable = { COG_investor_happiness = 0.05 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			custom_effect_tooltip = COG_investors_displeased_tt
			cog_hoa_update = yes
			add_tech_bonus = {
				uses = 1
				bonus = 0.25
				category = industry
				category = electronics
				category = infantry_weapons
				category = armor
			}
		}
		days_re_enable = 75

		ai_will_do = {
			factor = 0
		}
	}

	COG_lower_civ_count = {
		allowed = { tag = COG }

		available = {
			has_war = no
			NOT = { has_country_flag = COG_Investors_Gone }
		}

		visible = { has_completed_focus = COG_clear_the_jungles }

		cost = 60

		days_remove = 70

		days_re_enable = 70

		fire_only_once = no

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision COG_ask_investors_to_fund_research_project"
			custom_effect_tooltip = Ask_Investors_to_Finance_Our_Developments
			subtract_from_variable = { COG_investor_happiness = 0.1 }
			clamp_variable = {
				var = COG_investor_happiness
				min = 0
				max = 1
			}
			cog_hoa_update = yes
			custom_effect_tooltip = COG_investors_displeased_tt
		}

		modifier = { consumer_goods_factor = -0.15 }

		ai_will_do = {
			factor = 1
		}
	}

}

COG_resource_buying_category = {
    COG_buy_steel_from_germany = {
        visible = {
			NOT = { has_global_flag = german_civil_war }
        }
        available = {
            custom_trigger_tooltip = {
                tooltip = afrika_RK_cash_need_5
                check_variable = {
                    var = money_reserves
                    value = 5
                    compare = greater_than
                }
            }
        }
        ai_will_do = {
           factor = 60 ##unsure if this should be changed or not
        }

        days_remove = 30

	    fire_only_once = no

	    complete_effect = {
            custom_effect_tooltip = afrika_RK_resource_5
		    random_controlled_state = {
                prioritize = { 1163 1162 539 }
			    add_resource = {
				    type = steel
				    amount = 2
				}
				add_to_variable = {
					var = steel_bought
					value = 2
				}
		    }
		    subtract_from_variable = {
			    var = money_reserves
			    value = 5
            }
        }
    }
    COG_buy_aluminium_from_germany = {
        visible = {
			NOT = { has_global_flag = german_civil_war }
        }
        available = {
            custom_trigger_tooltip = {
                tooltip = afrika_RK_cash_need_5
                check_variable = {
                    var = money_reserves
                    value = 5
                    compare = greater_than
                }
            }
        }
        ai_will_do = {
           factor = 60 ##unsure if this should be changed or not
        }

        days_remove = 30

	    fire_only_once = no

	    complete_effect = {
            custom_effect_tooltip = afrika_RK_resource_5
		    random_controlled_state = {
                prioritize = { 1162 1163 539 }
			    add_resource = {
				    type = aluminium
				    amount = 2
				}
				add_to_variable = {
					var = aluminium_bought
					value = 2
				}
		    }
		    subtract_from_variable = {
			    var = money_reserves
			    value = 5
            }
        }
	}
    COG_buy_oil_from_germany = {
        visible = {
			NOT = { has_global_flag = german_civil_war }
        }
        available = {
            custom_trigger_tooltip = {
                tooltip = afrika_RK_cash_need_5
                check_variable = {
                    var = money_reserves
                    value = 5
                    compare = greater_than
                }
            }
        }
        ai_will_do = {
           factor = 60 ##unsure if this should be changed or not
        }

        days_remove = 30

	    fire_only_once = no

	    complete_effect = {
            custom_effect_tooltip = afrika_RK_resource_5
		    random_controlled_state = {
                prioritize = { 1162 1163 539 }
			    add_resource = {
				    type = oil
				    amount = 2
				}
				add_to_variable = {
					var = oil_bought
					value = 2
				}
		    }
		    subtract_from_variable = {
			    var = money_reserves
			    value = 5
            }
        }
	}
    COG_buy_chromium_from_germany = {
        visible = {
			NOT = { has_global_flag = german_civil_war }
        }
        available = {
            custom_trigger_tooltip = {
                tooltip = afrika_RK_cash_need_5
                check_variable = {
                    var = money_reserves
                    value = 5
                    compare = greater_than
                }
            }
        }
        ai_will_do = {
           factor = 60 ##unsure if this should be changed or not
        }

        days_remove = 30

	    fire_only_once = no

	    complete_effect = {
            custom_effect_tooltip = afrika_RK_resource_5
		    random_controlled_state = {
                prioritize = { 1162 1163 539 }
			    add_resource = {
				    type = chromium
				    amount = 2
				}
				add_to_variable = {
					var = chromium_bought
					value = 2
				}
		    }
		    subtract_from_variable = {
			    var = money_reserves
			    value = 5
            }
        }
	}
    COG_buy_rubber_from_germany = {
        visible = {
			NOT = { has_global_flag = german_civil_war }
        }
        available = {
            custom_trigger_tooltip = {
                tooltip = afrika_RK_cash_need_5
                check_variable = {
                    var = money_reserves
                    value = 5
                    compare = greater_than
                }
            }
        }
        ai_will_do = {
           factor = 60 ##unsure if this should be changed or not
        }

        days_remove = 30

	    fire_only_once = no

	    complete_effect = {
            custom_effect_tooltip = afrika_RK_resource_5
		    random_controlled_state = {
                prioritize = { 1162 1163 539 }
			    add_resource = {
				    type = rubber
				    amount = 2
				}
				add_to_variable = {
					var = rubber_bought
					value = 2
				}
		    }
		    subtract_from_variable = {
			    var = money_reserves
			    value = 5
            }
        }
	}
    COG_buy_tungsten_from_germany = { #please fucking work please fucking work
        visible = {
			NOT = { has_global_flag = german_civil_war }
        }
        available = {
            custom_trigger_tooltip = {
                tooltip = afrika_RK_cash_need_5
                check_variable = {
                    var = money_reserves
                    value = 5
                    compare = greater_than
                }
            }
        }
        ai_will_do = {
           factor = 60 ##unsure if this should be changed or not
        }

        days_remove = 30

	    fire_only_once = no

	    complete_effect = {
			custom_effect_tooltip = afrika_RK_resource_5
		    random_controlled_state = {
                prioritize = { 1162 1163 539 }
			    add_resource = {
				    type = tungsten
				    amount = 2
				}
				add_to_variable = {
					var = tungsten_bought
					value = 2
				}
		    }
		    subtract_from_variable = {
			    var = money_reserves
			    value = 5
            }
        }
	}
	COG_resource_stockpile_depletion = {
		allowed = { tag = COG }
		available = { hidden_trigger = { always = no } }
		fire_only_once = yes
		activation = {
			has_global_flag = german_civil_war
			NOT = { has_country_flag = german_resources_gone }
		}
		is_good = no
		days_mission_timeout = 30
		timeout_effect = {
			custom_effect_tooltip = afrika_resource_stockpile_depletion_tooltip
			set_country_flag = german_resources_gone
			hidden_effect = {
				539 = { afrika_resources_from_germany_depleted = yes }
				1163 = { afrika_resources_from_germany_depleted = yes }
				1162 = { afrika_resources_from_germany_depleted = yes }
			}
		}
	}
}
