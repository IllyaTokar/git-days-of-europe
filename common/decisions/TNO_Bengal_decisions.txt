#BEN_ICW_DEC = {
#	RAJ_ICW_dec_disband_azad_hind = {
#	
#		allowed = { is_indian_nation = yes }
#		available = {
#			
#		}
#		cost = 120
#		fire_only_once = yes
#		#days_re_enable = 120
#		ai_will_do = {
#			factor = 30
#		}
#		complete_effect = {
#			log = "[GetDateText]: [Root.GetName]: Decision RAJ_ICW_dec_disband_azad_hind"
#		add_stability = 0.05
#		add_war_support = 0.05
#			}
#
#     		}
#	RAJ_ICW_dec_sphere_help = {
#	
#		allowed = { is_indian_nation = yes }
#		available = {
#		}
#		cost = 100
#		fire_only_once = yes
#		#days_re_enable = 120
#		ai_will_do = {
#			factor = 30
#		}
#		complete_effect = {
#			log = "[GetDateText]: [Root.GetName]: Decision RAJ_ICW_dec_sphere_help"
#			add_equipment_to_stockpile = {
#				type = infantry_equipment
#				amount = 4000
#				producer = JAP
#			}
#     	}
#	}
#	RAJ_ICW_dec_lakshmi_talks = {
#	
#		allowed = { is_indian_nation = yes }
#		available = {
#			
#		}
#		cost = 90
#		fire_only_once = yes
#		#days_re_enable = 120
#		ai_will_do = {
#			factor = 30
#		}
#		complete_effect = {
#			log = "[GetDateText]: [Root.GetName]: Decision RAJ_ICW_dec_lakshmi_talks"
#			custom_effect_tooltip = BEN_sentmercs_tt
#			load_oob = BEN_merchelp
#		}
#    }
#	RAJ_ICW_dec_announce_plan = {
#	
#		allowed = { is_indian_nation = yes }
#		available = {
#			
#		}
#		cost = 70
#		fire_only_once = yes
#		#days_re_enable = 120
#		ai_will_do = {
#			factor = 30
#		}
#		complete_effect = {
#			log = "[GetDateText]: [Root.GetName]: Decision RAJ_ICW_dec_announce_plan"
#			add_war_support = 0.1
#		}
#    }
#	RAJ_ICW_dec_operation_butterfly = {
#	
#		allowed = { is_indian_nation = yes }
#		available = {
#
#		}
#		cost = 30
#		fire_only_once = yes
#		#days_re_enable = 120
#		ai_will_do = {
#			factor = 0
#		}
#		complete_effect = {
#			log = "[GetDateText]: [Root.GetName]: Decision RAJ_ICW_dec_operation_butterfly"
#			declare_war_on = {
#				target = INC
#				type = annex_everything
#			}
#			leave_faction = yes
#			hidden_effect = {
#				load_oob = BEN_civilwar_units
#				add_offsite_building = { type = arms_factory level = 4 } 
#			}
#			custom_effect_tooltip = BEN_icwfacs
#			custom_effect_tooltip = BEN_icwunits
#		}
#    }
#    RAJ_ICW_dec_operation_elephant = {
#		allowed = { is_indian_nation = yes }
#		available = {
#			OR = {
#			RAJ = { exists = no }
#			INC = { exists = no }
#			}
#
#		}
#		cost = 30
#		fire_only_once = yes
#		#days_re_enable = 120
#		ai_will_do = {
#			factor = 0
#		}
#		complete_effect = {
#			log = "[GetDateText]: [Root.GetName]: Decision RAJ_ICW_dec_operation_elephant"
#			declare_war_on = {
#				target = INF
#				type = annex_everything
#			}
#			leave_faction = yes
#			hidden_effect = {
#				load_oob = BEN_civilwar_units
#				add_offsite_building = { type = arms_factory level = 4 } 
#			}
#			custom_effect_tooltip = BEN_icwfacs
#			custom_effect_tooltip = BEN_icwunits
#		}
#    }
#    RAJ_ICW_dec_operation_reconduction = {
#	
#		allowed = { is_indian_nation = yes }
#		available = {
#
#		}
#		cost = 30
#		fire_only_once = yes
#		#days_re_enable = 120
#		ai_will_do = {
#			factor = 0
#		}
#		complete_effect = {
#			log = "[GetDateText]: [Root.GetName]: Decision RAJ_ICW_dec_operation_reconduction"
#			declare_war_on = {
#				target = RAJ
#				type = annex_everything
#			}
#			leave_faction = yes
#			hidden_effect = {
#				load_oob = BEN_civilwar_units 
#				add_offsite_building = { type = arms_factory level = 4 } 
#			}
#			custom_effect_tooltip = BEN_icwfacs
#			custom_effect_tooltip = BEN_icwunits
#		}
#    }
#}
#
#BEN_Bengali_Ethnic_Tensions_Descisions = {
#	BEN_Bengali_Ethnic_Tensions_Create_The_Bengali_Peace_Committee = { #judenrate
#		visible = {
#			always = yes
#		}
#		fire_only_once = yes
#		complete_effect = {
#		custom_effect_tooltip = BEN_Bengali_Ethnic_Tensions_Create_The_Bengali_Peace_Committee_tt
#			hidden_effect = {
#				random_list = {
#					50 = {
#						subtract_from_variable = {
#							var = BEN_Bengali_Ethnic_Tensions
#							value = 10
#						}
#					}
#					50 = {
#						subtract_from_variable = {
#							var = BEN_Bengali_Ethnic_Tensions
#							value = 2
#						}
#					}
#					add_political_power = -100
#				}
#			}
#		}
#	}
#	BEN_Bengali_Ethnic_Tensions_Integrate_The_Bengali_Generals = {
#		visible = {
#			always = yes
#		}
#		fire_only_once = yes
#		complete_effect = {
#			custom_effect_tooltip = BEN_Bengali_Ethnic_Tensions_Integrate_The_Bengali_Generals_tt
#			hidden_effect = {
#				subtract_from_variable = {
#					var = BEN_Bengali_Ethnic_Tensions
#					value = 25
#				}
#				random_list = {
#					50 = {
#						create_corps_commander = {
#							name = "Ziaur Rahman"
#							portrait_path = "Portrait_Azad_Hind_Ziaur_Rahman.dds"
#							traits = { infantry_leader trickster}
#							skill = 2
#							attack_skill = 3
#							defense_skill = 2
#							planning_skill = 1
#							logistics_skill = 1
#							id = 3212
#						}
#						create_corps_commander = {
#							name = "Khaled Mosharraf"
#							portrait_path = "Portrait_Azad_Hind_Khaled_Mosharraf.dds"
#							traits = { fortress_buster organizer }
#							skill = 2
#							attack_skill = 2
#							defense_skill = 2
#							planning_skill = 2
#							logistics_skill = 2
#							id = 3209
#						}
#					}
#				}
#				50 = {
#				}
#			}
#		}
#	}
#	BEN_Bengali_Ethnic_Tensions_Exclude_Any_Who_Serve = {
#		visible = {
#			always = yes
#		}
#		fire_only_once = yes
#		complete_effect = {
#			custom_effect_tooltip = BEN_Bengali_Ethnic_Tensions_Exclude_Any_Who_Serve_tt
#			hidden_effect = {
#				subtract_from_variable = {
#					var = BEN_Bengali_Ethnic_Tensions
#					value = 10
#				}
#				add_manpower = 5000
#			}
#		}
#	}
#}
#
#BEN_Singh_Military_Descisions = {
#	BEN_Singh_Military_Descisions_Improve_Loyalty = {
#		visible = {
#			always = yes
#		}
#		available = {
#			has_political_power > 250
#		}
#		fire_only_once = yes
#		complete_effect = {
#			custom_effect_tooltip = BEN_WIP_tt
#			hidden_effect = {
#				add_to_variable = {
#					var = BEN_Army_Loyalty
#					value = 10
#				}
#				add_political_power = -250
#			}
#		}
#	}
#	BEN_Singh_Military_Descisions_Increase_Independence = {
#		visible = {
#			always = yes
#		}
#		available = {
#			has_political_power > 250
#		}
#		fire_only_once = yes
#		complete_effect = {
#			custom_effect_tooltip = BEN_WIP_tt
#			hidden_effect = {
#				add_to_variable = {
#					var = BEN_Army_Independence
#					value = 10
#				}
#				add_political_power = -250
#			}
#		}
#	}
#	BEN_Singh_Military_Descisions_Decrease_Independence = {
#		visible = {
#			always = yes
#		}
#		available = {
#			has_political_power > 250
#		}
#		fire_only_once = yes
#		complete_effect = {
#			custom_effect_tooltip = BEN_WIP_tt
#			hidden_effect = {
#				subtract_from_variable = {
#					var = BEN_Army_Independence
#					value = 10
#				}
#				add_political_power = -250
#			}
#		}
#	}
#}
#
#BEN_Bose_One_Year_Plans = {
#	BEN_Bose_OYP_Activate_First_Plan = {
#		visible = {
#			has_country_flag = BEN_Bose_OYP_First_Plan
#		}
#		available = {
#			has_country_flag = BEN_Bose_OYP_First_Plan
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		fire_only_once = yes
#		complete_effect = {
#			set_country_flag = BEN_Bose_OYP_First_Plan_activated
#			hidden_effect = {
#				country_event = {
#					id = BEN_oyp.1
#					hours = 5
#				}
#			}
#		}
#	}
#	BEN_Bose_OYP_Activate_Second_Plan = {
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Second_Plan
#		}
#		available = {
#			has_country_flag = BEN_Bose_OYP_Second_Plan
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		fire_only_once = yes
#		complete_effect = {
#			set_country_flag = BEN_Bose_OYP_Second_Plan_activated
#		}
#	}
#	BEN_Bose_OYP_Activate_Third_Plan = {
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Third_Plan
#		}
#		available = {
#			has_country_flag = BEN_Bose_OYP_Third_Plan
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		fire_only_once = yes
#		complete_effect = {
#			set_country_flag = BEN_Bose_OYP_Third_Plan_activated
#		}
#	}
#	BEN_Bose_OYP_Activate_Fourth_Plan = {
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Fourth_Plan
#		}
#		available = {
#			has_country_flag = BEN_Bose_OYP_Fourth_Plan
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		fire_only_once = yes
#		complete_effect = {
#			set_country_flag = BEN_Bose_OYP_Fourth_Plan_activated
#		}
#	}
#	BEN_Bose_OYP_Activate_Fifth_Plan = {
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Fifth_Plan
#		}
#		available = {
#			has_country_flag = BEN_Bose_OYP_Fifth_Plan
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		fire_only_once = yes
#		complete_effect = {
#			set_country_flag = BEN_Bose_OYP_Fifth_Plan_activated
#		}
#	}
#	BEN_Bose_OYP_Activate_Sixth_Plan = {
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Sixth_Plan
#		}
#		available = {
#			has_country_flag = BEN_Bose_OYP_Sixth_Plan
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		fire_only_once = yes
#		complete_effect = {
#			set_country_flag = BEN_Bose_OYP_Sixth_Plan_activated
#		}
#	}
#	BEN_Bose_OYP_Activate_Seventh_Plan = {
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Seventh_Plan
#		}
#		available = {
#			has_country_flag = BEN_Bose_OYP_Seventh_Plan
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		fire_only_once = yes
#		complete_effect = {
#			set_country_flag = BEN_Bose_OYP_Seventh_Plan_activated
#		}
#	}
#	BEN_Bose_OYP_Activate_Eighth_Plan = {
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Eighth_Plan
#		}
#		available = {
#			has_country_flag = BEN_Bose_OYP_Eighth_Plan
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		fire_only_once = yes
#		complete_effect = {
#			set_country_flag = BEN_Bose_OYP_Eighth_Plan_activated
#		}
#	}
#	BEN_Bose_OYP_Activate_Ninth_Plan = {
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Ninth_Plan
#		}
#		available = {
#			has_country_flag = BEN_Bose_OYP_Ninth_Plan
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		fire_only_once = yes
#		complete_effect = {
#			set_country_flag = BEN_Bose_OYP_Ninth_Plan_activated
#		}
#	}
#	##MISSIONS##
#	BEN_Bose_OYP_Complete_First_Plan = {
#		available = {
#			check_variable = { BEN_Bose_OYP_Progress = 100 }
#		}
#		activation = {
#			has_country_flag = BEN_Bose_OYP_First_Plan_activated
#		}
#		visible = {
#			has_country_flag = BEN_Bose_OYP_First_Plan_activated
#		}
#		is_good = yes
#		days_mission_timeout = 365
#		timeout_effect = {
#			add_popularity = {
#				ideology = authoritarian_democrat
#				popularity = -0.05
#			}
#			hidden_effect = {
#				add_popularity = {
#					ideology = social_democrat
#					popularity = 0.05
#				}
#			}
#		}
#		complete_effect = {
#			set_variable = { BEN_Bose_OYP_Progress = 0 }
#			set_country_flag = BEN_Bose_OYP_Second_Plan
#		}
#	}
#	BEN_Bose_OYP_Complete_Second_Plan = {
#		available = {
#			check_variable = { BEN_Bose_OYP_Progress = 100 }
#		}
#		activation = {
#			has_country_flag = BEN_Bose_OYP_Second_Plan_activated
#		}
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Second_Plan_activated
#		}
#		is_good = yes
#		days_mission_timeout = 365
#		timeout_effect = {
#			add_popularity = {
#				ideology = authoritarian_democrat
#				popularity = -0.05
#			}
#			hidden_effect = {
#				add_popularity = {
#					ideology = social_democrat
#					popularity = 0.05
#				}
#			}
#		}
#		complete_effect = {
#			set_variable = { BEN_Bose_OYP_Progress = 0 }
#			set_country_flag = BEN_Bose_OYP_Third_Plan
#		}
#	}
#	BEN_Bose_OYP_Complete_Third_Plan = {
#		available = {
#			check_variable = { BEN_Bose_OYP_Progress = 100 }
#		}
#		activation = {
#			has_country_flag = BEN_Bose_OYP_Third_Plan_activated
#		}
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Third_Plan_activated
#		}
#		is_good = yes
#		days_mission_timeout = 365
#		timeout_effect = {
#			add_popularity = {
#				ideology = authoritarian_democrat
#				popularity = -0.05
#			}
#			hidden_effect = {
#				add_popularity = {
#					ideology = social_democrat
#					popularity = 0.05
#				}
#			}
#		}
#		complete_effect = {
#			set_variable = { BEN_Bose_OYP_Progress = 0 }
#			set_country_flag = BEN_Bose_OYP_Fourth_Plan
#		}
#	}
#	BEN_Bose_OYP_Complete_Fourth_Plan = {
#		available = {
#			check_variable = { BEN_Bose_OYP_Progress = 100 }
#		}
#		activation = {
#			has_country_flag = BEN_Bose_OYP_Fourth_Plan_activated
#		}
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Fourth_Plan_activated
#		}
#		is_good = yes
#		days_mission_timeout = 365
#		timeout_effect = {
#			add_popularity = {
#				ideology = authoritarian_democrat
#				popularity = -0.05
#			}
#			hidden_effect = {
#				add_popularity = {
#					ideology = social_democrat
#					popularity = 0.05
#				}
#			}
#		}
#		complete_effect = {
#			set_variable = { BEN_Bose_OYP_Progress = 0 }
#			set_country_flag = BEN_Bose_OYP_Fifth_Plan
#		}
#	}
#	BEN_Bose_OYP_Complete_Fifth_Plan = {
#		available = {
#			check_variable = { BEN_Bose_OYP_Progress = 100 }
#		}
#		activation = {
#			has_country_flag = BEN_Bose_OYP_Fifth_Plan_activated
#		}
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Fifth_Plan_activated
#		}
#		is_good = yes
#		days_mission_timeout = 365
#		timeout_effect = {
#			add_popularity = {
#				ideology = authoritarian_democrat
#				popularity = -0.05
#			}
#			hidden_effect = {
#				add_popularity = {
#					ideology = social_democrat
#					popularity = 0.05
#				}
#			}
#		}
#		complete_effect = {
#			set_variable = { BEN_Bose_OYP_Progress = 0 }
#			set_country_flag = BEN_Bose_OYP_Sixth_Plan
#		}
#	}
#	BEN_Bose_OYP_Complete_Sixth_Plan = {
#		available = {
#			check_variable = { BEN_Bose_OYP_Progress = 100 }
#		}
#		activation = {
#			has_country_flag = BEN_Bose_OYP_Sixth_Plan_activated
#		}
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Sixth_Plan_activated
#		}
#		is_good = yes
#		days_mission_timeout = 365
#		timeout_effect = {
#			add_popularity = {
#				ideology = authoritarian_democrat
#				popularity = -0.05
#			}
#			hidden_effect = {
#				add_popularity = {
#					ideology = social_democrat
#					popularity = 0.05
#				}
#			}
#		}
#		complete_effect = {
#			set_variable = { BEN_Bose_OYP_Progress = 0 }
#			set_country_flag = BEN_Bose_OYP_Seventh_Plan
#		}
#	}
#	BEN_Bose_OYP_Complete_Seventh_Plan = {
#		available = {
#			check_variable = { BEN_Bose_OYP_Progress = 100 }
#		}
#		activation = {
#			has_country_flag = BEN_Bose_OYP_Seventh_Plan_activated
#		}
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Seventh_Plan_activated
#		}
#		is_good = yes
#		days_mission_timeout = 365
#		timeout_effect = {
#			add_popularity = {
#				ideology = authoritarian_democrat
#				popularity = -0.05
#			}
#			hidden_effect = {
#				add_popularity = {
#					ideology = social_democrat
#					popularity = 0.05
#				}
#			}
#		}
#		complete_effect = {
#			set_variable = { BEN_Bose_OYP_Progress = 0 }
#			set_country_flag = BEN_Bose_OYP_Eighth_Plan
#		}
#	}
#	BEN_Bose_OYP_Complete_Eighth_Plan = {
#		available = {
#			check_variable = { BEN_Bose_OYP_Progress = 100 }
#		}
#		activation = {
#			has_country_flag = BEN_Bose_OYP_Eighth_Plan_activated
#		}
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Eighth_Plan_activated
#		}
#		is_good = yes
#		days_mission_timeout = 365
#		timeout_effect = {
#			add_popularity = {
#				ideology = authoritarian_democrat
#				popularity = -0.05
#			}
#			hidden_effect = {
#				add_popularity = {
#					ideology = social_democrat
#					popularity = 0.05
#				}
#			}
#		}
#		complete_effect = {
#			set_variable = { BEN_Bose_OYP_Progress = 0 }
#			set_country_flag = BEN_Bose_OYP_Ninth_Plan
#		}
#	}
#	BEN_Bose_OYP_Complete_Ninth_Plan = {
#		available = {
#			check_variable = { BEN_Bose_OYP_Progress = 100 }
#		}
#		activation = {
#			has_country_flag = BEN_Bose_OYP_Ninth_Plan_activated
#		}
#		visible = {
#			has_country_flag = BEN_Bose_OYP_Ninth_Plan_activated
#		}
#		is_good = yes
#		days_mission_timeout = 365
#		timeout_effect = {
#			add_popularity = {
#				ideology = authoritarian_democrat
#				popularity = -0.05
#			}
#			hidden_effect = {
#				add_popularity = {
#					ideology = social_democrat
#					popularity = 0.05
#				}
#			}
#		}
#		complete_effect = {
#			set_variable = { BEN_Bose_OYP_Progress = 0 }
#		}
#	}
#}
#
#BEN_Northeast_War_cat = {
#	BEN_NorEastWar_Move_Capital_to_Patna = {
#		visible = {
#			has_country_flag = BEN_NorEastWar_Begun
#			NOT = { has_country_flag = BEN_Patna_Capital }
#			has_country_flag = BEN_can_move_capital
#		}
#		available = {
#			custom_trigger_tooltip = {
#				tooltip = BEN_Kolkata_Capital_tt
#				NOT = { has_country_flag = BEN_Patna_Capital }
#			}
#			has_country_flag = BEN_can_move_capital
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		complete_effect = {
#			set_capital = 1351
#			clr_country_flag = BEN_can_move_capital
#			set_country_flag = BEN_Patna_Capital
#		}
#	}
#	BEN_NorEastWar_Move_Capital_to_Kolkata = {
#		visible = {
#			has_country_flag = BEN_NorEastWar_Begun
#			has_country_flag = BEN_Patna_Capital
#			has_country_flag = BEN_can_move_capital
#		}
#		available = {
#			has_country_flag = BEN_Patna_Capital
#			has_country_flag = BEN_can_move_capital
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		complete_effect = {
#			set_capital = 1349
#			clr_country_flag = BEN_can_move_capital
#			clr_country_flag = BEN_Patna_Capital
#		}
#	}
#	BEN_NorEastWar_Declare_a_State_of_Emergency = {
#		visible = {
#			has_country_flag = BEN_NorEastWar_Begun
#			NOT = { has_country_flag = BEN_State_of_Emergency }
#		}
#		available = {
#			custom_trigger_tooltip = {
#				tooltip = BEN_No_State_of_Emergency_tt
#				NOT = { has_country_flag = BEN_State_of_Emergency }
#			}
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		complete_effect = {
#			custom_effect_tooltip = BEN_Declare_State_of_Emergency_tt
#			hidden_effect = {
#				set_politics = {
#					elections_allowed = no
#				}
#				set_country_flag = BEN_No_Elections
#				set_country_flag = BEN_State_of_Emergency
#			}
#		}
#	}
#	BEN_NorEastWar_Lift_the_State_of_Emergency = {
#		visible = {
#			has_country_flag = BEN_NorEastWar_Begun
#			has_country_flag = BEN_State_of_Emergency
#			has_country_flag = BEN_Can_Lift_State_of_Emergency
#		}
#		available = {
#			has_country_flag = BEN_State_of_Emergency
#			has_country_flag = BEN_Can_Lift_State_of_Emergency
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		complete_effect = {
#			custom_effect_tooltip = BEN_Lift_State_of_Emergency_tt
#			hidden_effect = {
#				set_politics = {
#					elections_allowed = yes
#				}
#				IF = {
#					limit = {
#						date > 1970.3.14
#					}
#					IF = {
#						limit = {
#							OR = {
#								has_country_flag = BEN_Lakshmi1
#								has_country_flag = BEN_Lakshmi2
#							}
#						}
#						country_event = {
#							id = BEN_elections.64
#							days = 5
#						}
#					}
#					IF = {
#						limit = {
#							OR = {
#								has_country_flag = BEN_Bose2
#							}
#						}
#						country_event = {
#							id = BEN_elections.50
#							days = 5
#						}
#					}
#					IF = {
#						limit = {
#							OR = {
#								has_country_flag = BEN_Singh1
#								has_country_flag = BEN_Singh2
#							}
#						}
#						###PUT ELECTIONS HERE WHEN THEYRE MADE##
#					}
#					IF = {
#						limit = {
#							has_country_flag = BEN_Lib2
#						}
#						##PUT ELECTIONS HERE WHEN THEYRE MADE##
#					}
#					IF = {
#						limit = {
#							has_country_flag = BEN_LibSoc2
#						}
#						##PUT ELECTIONS HERE WHEN THEYRE MADE##
#					}
#				}
#				clr_country_flag = BEN_No_Elections
#				clr_country_flag = BEN_State_of_Emergency
#			}
#		}
#	}
#	BEN_NorEastWar_Surrender_to_the_NSCN = {
#		visible = {
#			has_country_flag = BEN_NorEastWar_Begun
#			has_war_with = NGL
#		}
#		available = {
#			has_war_with = NGL
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		complete_effect = {
#			NGL = {
#				country_event = {
#					id = NorEastWar.44
#				}
#			}
#		}
#	}
#	BEN_NorEastWar_Surrender_to_the_Bodo_Mahasabha = {
#		visible = {
#			has_country_flag = BEN_NorEastWar_Begun
#			has_war_with = BDL
#		}
#		available = {
#			has_war_with = BDL
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		complete_effect = {
#			BDL = {
#				country_event = {
#					id = NorEastWar.44
#				}
#			}
#		}
#	}
#	BEN_NorEastWar_Surrender_to_the_ANA = {
#		visible = {
#			has_country_flag = BEN_NorEastWar_Begun
#			has_war_with = ASS
#		}
#		available = {
#			has_war_with = ASS
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		complete_effect = {
#			ASS = {
#				country_event = {
#					id = NorEastWar.44
#				}
#			}
#		}
#	}
#	BEN_NorEastWar_Surrender_to_the_Bengalis = {
#		visible = {
#			has_country_flag = BEN_NorEastWar_Begun
#			OR = {
#				has_war_with = BAN
#				has_war_with = IBG
#			}
#		}
#		available = {
#			OR = {
#				has_war_with = BAN
#				has_war_with = IBG
#			}
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		complete_effect = {
#			IF = {
#				limit = {
#					has_war_with = IBG
#				}
#				IBG = {
#					country_event = {
#						id = NorEastWar.47
#					}
#				}
#			}
#			else = {
#				BAN = {
#					country_event = {
#						id = NorEastWar.50
#					}
#				}
#			}
#		}
#	}
#	BEN_NorEastWar_Give_the_INA_a_Greater_Voice = {
#		visible = {
#			has_country_flag = BEN_NorEastWar_Begun
#			NOT = {
#				has_government = social_democrat
#				has_government = despotism
#			}
#		}
#		available = {
#			NOT = {
#				has_government = social_democrat
#				has_government = despotism
#			}
#		}
#		custom_cost_trigger = { always = yes }
#		ai_will_do = {
#			factor = 1
#		}
#		complete_effect = {
#			remove_all_ministers = yes
#			add_ideas = {
#				BEN_Gurbachan_Singh_Salaria_hog
#				BEN_Gurbaksh_Singh_Dhillon_for
#				BEN_Shah_Nawaz_Khan_eco
#				BEN_Mohan_Singh_sec
#			}
#			hidden_effect = {
#				add_to_variable = { BEN_NorEastWar_INA_Satisfaction = -25 }
#			}
#		}
#	}
#}