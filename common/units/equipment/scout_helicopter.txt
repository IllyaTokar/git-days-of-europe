equipments = {
	scout_helicopter_equipment = {

		can_be_produced = {
			NOT = {
				has_idea = ENG_Across_The_Channel
			}
		}
		
		year = 1945

		is_archetype = yes
		is_buildable = no
		picture = archetype_motorized_equipment
		type = { mechanized }
		group_by = archetype
		
		interface_category = interface_category_land
		
		#Space taken in convoy
		lend_lease_cost = 5
		reliability = 0.8
		
		#Defensive Abilities
		defense = 11
		breakthrough = 15

		#Offensive Abilities
		soft_attack = 0
		hard_attack = 0

		build_cost_ic = 15
		resources = {
			aluminium = 2
		}
		fuel_consumption = 0.6
	}
	
	helicopter_equipment_0 = {
		year = 1945

		archetype = scout_helicopter_equipment
		priority = 100
	}
	
	scout_helicopter_equipment_1 = {
		year = 1960

		archetype = scout_helicopter_equipment
		parent = helicopter_equipment_0
		priority = 100
		
		#Defensive Abilities
		defense = 14
		breakthrough = 25

		#Offensive Abilities
		soft_attack = 15
		hard_attack = 6.5
		
		build_cost_ic = 19
	}
	
	scout_helicopter_equipment_2 = {
		year = 1970

		archetype = scout_helicopter_equipment
		parent = scout_helicopter_equipment_1
		priority = 110
		
		#Defensive Abilities
		defense = 16
		breakthrough = 34

		#Offensive Abilities
		soft_attack = 19.5
		hard_attack = 8.5
		
		build_cost_ic = 21
	}
	
	scout_helicopter_equipment_3 = {
		year = 1980

		archetype = scout_helicopter_equipment
		parent = scout_helicopter_equipment_2
		priority = 120
		
		#Defensive Abilities
		defense = 18
		breakthrough = 45

		#Offensive Abilities
		soft_attack = 25
		hard_attack = 11
		
		build_cost_ic = 24
	}
	
	scout_helicopter_equipment_4 = {
		year = 1990

		archetype = scout_helicopter_equipment
		parent = scout_helicopter_equipment_3
		priority = 130
		
		#Defensive Abilities
		defense = 20
		breakthrough = 59

		#Offensive Abilities
		soft_attack = 33
		hard_attack = 14
		
		build_cost_ic = 27
	}
}