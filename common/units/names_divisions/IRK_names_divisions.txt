﻿# Division template historical names system. Is a new method of naming the divisions based on the names-group assigned to it's template.
# If we run out of all historical names, the names will be assigned in the old way.
#
# Each group has a made up tag. Use it if you want to append more, or replace existing names with the extra txt files (modding).
#
# for_countries - lists all countries that can use it. If empty, or entire tag is missing, all countries in the world can use it.
#
# can_use - is a trigger that locks/unlocks the group under specific circumstances. The trigger is in a country scope.
#
# division_types - is a list of tokens to corresponding unit types. A player can in fact use any group of names for a div.template
#                  however this tag is a helper for an automated choice (for AI, or if the group must switch on it's own, because
#                  for example the current one is no longer available due to the can_use trigger saying so).
#                  In automated choice, the division template must have at least 1 of the following types for it to be chosen.
#
# fallback_name - Is going to be used if we run out of the scripted historical names. If you want to use the old division naming
#                 mechanics to be used for fallbacks, then just skip this option.
#
# unordered - It's a list of historical division names that did not have a number. Regardless if such names happened in history or not
#             this option is available here mainly for a mods.
#
# ordered - Is a list of all historical names. 
#           Numbers must start from 1 and up. 
#           Numbers can't repeat in one scope.
#           If you script the name for this group, at the same number (for example in a mod in another file), the name will be override.
#           All arguments between the brackets must be separated by spaces. Each argument is wrapped in "quotas".
#           1st argument = The name. It must contain either: 
#                          %d (for decimal number placement)
#                          %s (for string number placement - ROMAN numbers like XIV).
#           2nd argument is optional = A localizable text describing this historical division. The text will be visible in the tooltip
#                                      where you choose the historical division name.
#           3rd argument is optional = An URL preferably pointing to the WIKI. It's a future feature that is not currently working in
#                                      the current game version.
IRK_Infantry = {
	name = "Strelkovaya Diviziya"

	for_countries = { IRK }

	can_use = { always = yes }

	division_types = { "infantry" }

	# Number reservation system will tie to another group.
	link_numbering_with = { IRK_Motorized }

	fallback_name = "%dya Strelkovaya Diviziya"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "%dya Strelkovaya Diviziya" }
		2 = { "%dya Strelkovaya Diviziya" }
		3 = { "%dya Strelkovaya Diviziya" }
		4 = { "%dya Strelkovaya Diviziya" }
		5 = { "%dya Strelkovaya Diviziya" }
		6 = { "%dya Strelkovaya Diviziya" }
		7 = { "%dya Strelkovaya Diviziya" }
		8 = { "%dya Strelkovaya Diviziya" }
		9 = { "%dya Strelkovaya Diviziya" }
		10 = { "%dya Strelkovaya Diviziya" }
		11 = { "%dya Strelkovaya Diviziya" }
		12 = { "%dya Strelkovaya Diviziya" }
		13 = { "%dya Strelkovaya Diviziya" }
		14 = { "%dya Strelkovaya Diviziya" }
		15 = { "%dya Strelkovaya Diviziya" }
		16 = { "%dya Strelkovaya Diviziya" }
		17 = { "%dya Strelkovaya Diviziya" }
		18 = { "%dya Strelkovaya Diviziya" }
		19 = { "%dya Strelkovaya Diviziya" }
		20 = { "%dya Strelkovaya Diviziya" }
		21 = { "%dya Strelkovaya Diviziya" }
		22 = { "%dya Strelkovaya Diviziya" }
		23 = { "%dya Strelkovaya Diviziya" }
		24 = { "%dya Strelkovaya Diviziya" }
		25 = { "%dya Strelkovaya Diviziya" }
		26 = { "%dya Strelkovaya Diviziya" }
		27 = { "%dya Strelkovaya Diviziya" }
		28 = { "%dya Strelkovaya Diviziya" }
		29 = { "%dya Strelkovaya Diviziya" }
		30 = { "%dya Strelkovaya Diviziya" }
	}
}

IRK_Guards = {
	name = "Gvardiya Strelkovaya Diviziya"

	for_countries = { IRK }

	can_use = { always = yes }

	division_types = { "infantry" "elite_infantry" }

	# Number reservation system will tie to another group.
	link_numbering_with = { IRK_Motorized }

	fallback_name = "%dya Gvardiya Strelkovaya Diviziya"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "%dya Gvardiya Strelkovaya Diviziya" }
	}
}

IRK_Motorized = {
	name = "Motostrelkovaya Diviziya"

	for_countries = { IRK }

	can_use = { always = yes }

	division_types = { "motorized" }

	# Number reservation system will tie to another group.
	link_numbering_with = { IRK_Infantry }

	fallback_name = "%dya Motostrelkovaya Diviziya"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "%dya Motostrelkovaya Diviziya" }
		2 = { "%dya Motostrelkovaya Diviziya" }
		3 = { "%dya Motostrelkovaya Diviziya" }
		4 = { "%dya Motostrelkovaya Diviziya" }
		5 = { "%dya Motostrelkovaya Diviziya" }
		6 = { "%dya Motostrelkovaya Diviziya" }
		7 = { "%dya Motostrelkovaya Diviziya" }
		8 = { "%dya Motostrelkovaya Diviziya" }
		9 = { "%dya Motostrelkovaya Diviziya" }
		10 = { "%dya Motostrelkovaya Diviziya" }
		11 = { "%dya Motostrelkovaya Diviziya" }
		12 = { "%dya Motostrelkovaya Diviziya" }
		13 = { "%dya Motostrelkovaya Diviziya" }
		14 = { "%dya Motostrelkovaya Diviziya" }
		15 = { "%dya Motostrelkovaya Diviziya" }
		16 = { "%dya Motostrelkovaya Diviziya" }
		17 = { "%dya Motostrelkovaya Diviziya" }
		18 = { "%dya Motostrelkovaya Diviziya" }
		19 = { "%dya Motostrelkovaya Diviziya" }
		20 = { "%dya Motostrelkovaya Diviziya" }
		21 = { "%dya Motostrelkovaya Diviziya" }
		22 = { "%dya Motostrelkovaya Diviziya" }
		23 = { "%dya Motostrelkovaya Diviziya" }
		24 = { "%dya Motostrelkovaya Diviziya" }
		25 = { "%dya Motostrelkovaya Diviziya" }
		26 = { "%dya Motostrelkovaya Diviziya" }
		27 = { "%dya Motostrelkovaya Diviziya" }
		28 = { "%dya Motostrelkovaya Diviziya" }
		29 = { "%dya Motostrelkovaya Diviziya" }
		30 = { "%dya Motostrelkovaya Diviziya" }
	}
}

IRK_NKVD = {
	name = "NKVD Gvardeiskaya Brigada"

	for_countries = { IRK }

	can_use = { always = yes }

	division_types = { "elite_infantry" "infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Antinatsistskaya Narodnaya Brigada"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "%dya NKVD Gvardeiskaya Brigada" }
		2 = { "%dya NKVD Gvardeiskaya Brigada" }
		3 = { "%dya NKVD Gvardeiskaya Brigada" }
		4 = { "%dya NKVD Gvardeiskaya Brigada" }
		5 = { "%dya NKVD Gvardeiskaya Brigada" }
		6 = { "%dya NKVD Gvardeiskaya Brigada" }
		7 = { "%dya NKVD Gvardeiskaya Brigada" }
		8 = { "%dya NKVD Gvardeiskaya Brigada" }
		9 = { "%dya NKVD Gvardeiskaya Brigada" }
		10 = { "%dya NKVD Gvardeiskaya Brigada" }
		11 = { "%dya NKVD Gvardeiskaya Brigada" }
		12 = { "%dya NKVD Gvardeiskaya Brigada" }
		13 = { "%dya NKVD Gvardeiskaya Brigada" }
		14 = { "%dya NKVD Gvardeiskaya Brigada" }
		15 = { "%dya NKVD Gvardeiskaya Brigada" }
		16 = { "%dya NKVD Gvardeiskaya Brigada" }
		17 = { "%dya NKVD Gvardeiskaya Brigada" }
		18 = { "%dya NKVD Gvardeiskaya Brigada" }
		19 = { "%dya NKVD Gvardeiskaya Brigada" }
		20 = { "%dya NKVD Gvardeiskaya Brigada" }
		21 = { "%dya NKVD Gvardeiskaya Brigada" }
		22 = { "%dya NKVD Gvardeiskaya Brigada" }
		23 = { "%dya NKVD Gvardeiskaya Brigada" }
		24 = { "%dya NKVD Gvardeiskaya Brigada" }
		25 = { "%dya NKVD Gvardeiskaya Brigada" }
		26 = { "%dya NKVD Gvardeiskaya Brigada" }
		27 = { "%dya NKVD Gvardeiskaya Brigada" }
		28 = { "%dya NKVD Gvardeiskaya Brigada" }
		29 = { "%dya NKVD Gvardeiskaya Brigada" }
		30 = { "%dya NKVD Gvardeiskaya Brigada" }
	}
}