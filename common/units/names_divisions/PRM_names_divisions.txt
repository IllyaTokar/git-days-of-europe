﻿# Division template historical names system. Is a new method of naming the divisions based on the names-group assigned to it's template.
# If we run out of all historical names, the names will be assigned in the old way.
#
# Each group has a made up tag. Use it if you want to append more, or replace existing names with the extra txt files (modding).
#
# for_countries - lists all countries that can use it. If empty, or entire tag is missing, all countries in the world can use it.
#
# can_use - is a trigger that locks/unlocks the group under specific circumstances. The trigger is in a country scope.
#
# division_types - is a list of tokens to corresponding unit types. A player can in fact use any group of names for a div.template
#                  however this tag is a helper for an automated choice (for AI, or if the group must switch on it's own, because
#                  for example the current one is no longer available due to the can_use trigger saying so).
#                  In automated choice, the division template must have at least 1 of the following types for it to be chosen.
#
# fallback_name - Is going to be used if we run out of the scripted historical names. If you want to use the old division naming
#                 mechanics to be used for fallbacks, then just skip this option.
#
# unordered - It's a list of historical division names that did not have a number. Regardless if such names happened in history or not
#             this option is available here mainly for a mods.
#
# ordered - Is a list of all historical names. 
#           Numbers must start from 1 and up. 
#           Numbers can't repeat in one scope.
#           If you script the name for this group, at the same number (for example in a mod in another file), the name will be override.
#           All arguments between the brackets must be separated by spaces. Each argument is wrapped in "quotas".
#           1st argument = The name. It must contain either: 
#                          %d (for decimal number placement)
#                          %s (for string number placement - ROMAN numbers like XIV).
#           2nd argument is optional = A localizable text describing this historical division. The text will be visible in the tooltip
#                                      where you choose the historical division name.
#           3rd argument is optional = An URL preferably pointing to the WIKI. It's a future feature that is not currently working in
#                                      the current game version.
PRM_Infantry = {
	name = "Arishe Infanteriy-Divizyon"

	for_countries = { PRM }

	can_use = { always = yes }

	division_types = { "infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Arishe Infanteriy-Divizyon"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "%d. Infanteriy-Divizyon 'Fyurer'" }
		2 = { "%d. Infanteriy-Divizyon 'Reykh'" }
		3 = { "%d. Infanteriy-Divizyon 'Vagner'" }
		4 = { "%d. Infanteriy-Divizyon 'Ariyer den Osten'" }
		5 = { "%d. Infanteriy-Divizyon 'Erloykhtun'" }
		6 = { "%d. Infanteriy-Divizyon 'Afshtig'" }
		7 = { "%d. Infanteriy-Divizyon 'Boryaner'" }
		8 = { "%d. Infanteriy-Divizyon 'Arishe Royber'" }
		9 = { "%d. Infanteriy-Divizyon 'Bolsheviken-Yeger'" }
		10 = { "%d. Infanteriy-Divizyon 'Ariyer-Syone'" }
		11 = { "%d. Infanteriy-Divizyon 'Ostlendishe Kriyger'" }
		12 = { "%d. Infanteriy-Divizyon 'Strame Burshen'" }
		13 = { "%d. Infanteriy-Divizyon 'Rorkrepiyrer'" }
		14 = { "%d. Infanteriy-Divizyon 'Pantser-Foyste'" }
		15 = { "%d. Infanteriy-Divizyon 'Ostlendishe Kriyger'" }
		16 = { "%d. Infanteriy-Divizyon 'Abver-Kanone'" }
		17 = { "%d. Infanteriy-Divizyon 'Shvere Gyustaf'" }
		18 = { "%d. Infanteriy-Divizyon 'Geyst den Ariyer'" }
		19 = { "%d. Infanteriy-Divizyon 'Permhaym-Gebryuder'" }
		20 = { "%d. Infanteriy-Divizyon 'Vaterland'" }
		21 = { "%d. Infanteriy-Divizyon 'Adler'" }
		22 = { "%d. Infanteriy-Divizyon 'Shtyurmer'" }
		23 = { "%d. Infanteriy-Divizyon 'Ariyer'" }
		24 = { "%d. Infanteriy-Divizyon 'Donerblits'" }
		25 = { "%d. Infanteriy-Divizyon 'Yefreyte'" }
		26 = { "%d. Infanteriy-Divizyon 'Hamershlag'" }
	}
}

PRM_Light_Infantry = {
	name = "Leikhte Gilvs-Yeger"

	for_countries = { PRM }

	can_use = { always = yes }

	division_types = { "light_infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Leikhte Gilvs-Yeger"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1. Gilvs-Yeger 'Fukhs'" }
		2 = { "2. Gilvs-Yeger 'Shpershpits'" }
		3 = { "3. Gilvs-Yeger 'Virbelvind'" }
		4 = { "4. Gilvs-Yeger 'Ostvind'" }
		5 = { "5. Gilvs-Yeger 'Royber'" }
		6 = { "6. Gilvs-Yeger 'Yeger'" }
		7 = { "7. Gilvs-Yeger 'Yakht-Fogel'" }
		8 = { "8. Gilvs-Yeger 'Gryune Rekruten'" }
		9 = { "9. Gilvs-Yeger 'Gever-Tentser'" }
		10 = { "10. Gilvs-Yeger 'Vilde Ruteniyer'" }
		11 = { "11. Gilvs-Yeger 'Yune Ariyer'" }
		12 = { "12. Gilvs-Yeger 'Vervolfe'" }
		13 = { "13. Gilvs-Yeger 'Valshe Militsen'" }
		14 = { "14. Gilvs-Yeger 'Shturm-Folk'" }
		15 = { "15. Gilvs-Yeger 'Lumpenpak'" }
		16 = { "16. Gilvs-Yeger 'Shvartse Bauvern'" }
		17 = { "17. Gilvs-Yeger 'Arishe Shyutsen'" }
		18 = { "18. Gilvs-Yeger 'Man an Gever'" }
		19 = { "19. Gilvs-Yeger 'Bastade'" }
		20 = { "20. Gilvs-Yeger 'Kanonen-Futer'" }
	}
}

PRM_Elite_Infantry = {
	name = "Shvere Shturm-Divizyon"

	for_countries = { PRM }

	can_use = { always = yes }

	division_types = { "elite_infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Shvere Shturm-Divizyon"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1. Shturm-Divizyon 'Laybshtandarte Gutrum Vagner'" }
		2 = { "2. Shturm-Divizyon 'Khail' Gitler'" }
		3 = { "3. Shturm-Divizyon 'Ariyer-Fakel'" }
		4 = { "4. Shturm-Divizyon 'Totenkof'" }
		5 = { "5. Shturm-Divizyon 'Hyperbaiyern'" }
		6 = { "6. Shturm-Divizyon 'Ragnarok'" }
		7 = { "7. Shturm-Divizyon 'Blyut und Boden'" }
		8 = { "8. Shturm-Divizyon 'Nord-Shtern'" }
		9 = { "9. Shturm-Divizyon 'Nidgogs-Diner'" }
		10 = { "10. Shturm-Divizyon 'Odinshamer'" }
		11 = { "11. Shturm-Divizyon 'Thorshlakh'" }
		12 = { "12. Shturm-Divizyon 'Shvarze Kroyts'" }
		13 = { "13. Shturm-Divizyon 'Harte Yuns'" }
		14 = { "14. Shturm-Divizyon 'Valkyuren'" }
		15 = { "15. Shturm-Divizyon 'Akse'" }
		16 = { "16. Shturm-Divizyon 'Aynfolk'" }
		17 = { "17. Shturm-Divizyon 'Aynreykh'" }
		18 = { "18. Shturm-Divizyon 'Aynfyurer'" }
		19 = { "19. Shturm-Divizyon 'Bryudershaft'" }
		20 = { "20. Shturm-Divizyon 'Erentod'" }
	}
}

PRM_Air_Assault = {
	name = "Lyufteynzatsgrupe"

	for_countries = { PRM }

	can_use = { always = yes }

	division_types = { "air_assault" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Lyufteynzatsgrupe"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1. Lyufteynzatsgrupe 'Tod von Oben'" }
		2 = { "2. Lyufteynzatsgrupe 'Blitseynshlag'" }
		3 = { "3. Lyufteynzatsgrupe 'Shvartse Geyer'" }
		4 = { "4. Lyufteynzatsgrupe 'Falken'" }
		5 = { "5. Lyufteynzatsgrupe 'Meteyor'" }
		6 = { "6. Lyufteynzatsgrupe 'Shteyn-Shlag'" }
		7 = { "7. Lyufteynzatsgrupe 'Stalfogel'" }
		8 = { "8. Lyufteynzatsgrupe 'Foyer-Shauver'" }
		9 = { "9. Lyufteynzatsgrupe 'Arishe Vile'" }
		10 = { "10. Lyufteynzatsgrupe 'Nakhtshaten'" }
		11 = { "11. Lyufteynzatsgrupe 'Fyurer-Heksen'" }
		12 = { "12. Lyufteynzatsgrupe 'Fligende Ariyer'" }
		13 = { "13. Lyufteynzatsgrupe 'Shturts-Bomber'" }
		14 = { "14. Lyufteynzatsgrupe 'Kyune Shprinyer'" }
		15 = { "15. Lyufteynzatsgrupe 'Greyf'" }
		16 = { "16. Lyufteynzatsgrupe 'Flyugelfolk'" }
		17 = { "17. Lyufteynzatsgrupe 'Ikarus'" }
		18 = { "18. Lyufteynzatsgrupe 'Vansin'" }
		19 = { "19. Lyufteynzatsgrupe 'Yeger der Lyuft'" }
		20 = { "20. Lyufteynzatsgrupe 'Shternshnupen'" }
	}
}

PRM_Mountaineers = {
	name = "Bergshyutsen"

	for_countries = { PRM }

	can_use = { always = yes }

	division_types = { "mountaineers" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Bergshyutsen"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1. Bergshyutsen 'Arisher Alpenyeger'" }
		2 = { "2. Bergshyutsen 'Uralkriyger'" }
		3 = { "3. Bergshyutsen 'Yedelweys'" }
		4 = { "4. Bergshyutsen 'Slukhtenvakhter'" }
		5 = { "5. Bergshyutsen 'Vald-Meyster'" }
		6 = { "6. Bergshyutsen 'Shpitsen-Reyter'" }
		7 = { "7. Bergshyutsen 'Berg-Tsviner'" }
		8 = { "8. Bergshyutsen 'Menshlikh Lavine'" }
		9 = { "9. Bergshyutsen 'Shteynhern'" }
		10 = { "10. Bergshyutsen 'Shnefloke'" }
	}
}

PRM_Marines = {
	name = "Ariyer tsur Zee"

	for_countries = { PRM }

	can_use = { always = yes }

	division_types = { "marine" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Ariyer tsur Zee"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1. Ariyer tsur Zee 'Mershpartaner'" }
		2 = { "2. Ariyer tsur Zee 'Kyusten-Shturm'" }
		3 = { "3. Ariyer tsur Zee 'Siyntflyut'" }
		4 = { "4. Ariyer tsur Zee 'Pozeydon'" }
		5 = { "5. Ariyer tsur Zee 'Yebryuder auf Mere'" }
		6 = { "6. Ariyer tsur Zee 'Akva-Arier'" }
		7 = { "7. Ariyer tsur Zee 'Mer-Unyehoyer'" }
		8 = { "8. Ariyer tsur Zee 'Velenreyter'" }
		9 = { "9. Ariyer tsur Zee 'Vaser-Geger'" }
		10 = { "10. Ariyer tsur Zee 'Otseyan'" }
	}
}

PRM_MBTs = {
	name = "Pantser-Divizyon"

	for_countries = { PRM }

	can_use = { always = yes }

	division_types = { "MBT" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Pantser-Divizyon"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1. Pantser-Divizyon 'Blyits-Kriyg'" }
		2 = { "2. Pantser-Divizyon 'Eysen-Bestiye'" }
		3 = { "3. Pantser-Divizyon 'Tysuklop'" }
		4 = { "4. Pantser-Divizyon 'Shtal-Gigant'" }
		5 = { "5. Pantser-Divizyon 'Gyoter-Favst'" }
		6 = { "6. Pantser-Divizyon 'Tod von Kyeten'" }
		7 = { "7. Pantser-Divizyon 'Angriyf-Shverpunt'" }
		8 = { "8. Pantser-Divizyon 'Arisher Morgenshtern'" }
		9 = { "9. Pantser-Divizyon 'Reykhs-Vafe'" }
		10 = { "10. Pantser-Divizyon 'Shus-Kasten'" }
		11 = { "11. Pantser-Divizyon 'Metal-Tiyger'" }
		12 = { "12. Pantser-Divizyon 'Shtalmener'" }
		13 = { "13. Pantser-Divizyon 'Feyndfeger'" }
		14 = { "14. Pantser-Divizyon 'Durkh-Brukh'" }
		15 = { "15. Pantser-Divizyon 'Romels-Reyter'" }
		16 = { "16. Pantser-Divizyon 'Shturmanfyurer'" }
		17 = { "17. Pantser-Divizyon 'Ruyr-Kemfer'" }
		18 = { "18. Pantser-Divizyon 'Metal-Prese'" }
		19 = { "19. Pantser-Divizyon 'Landshif'" }
		20 = { "20. Pantser-Divizyon 'Rolende Bunker'" }
	}
}

PRM_motorized = {
	name = "Motor-Infanteriy-Divizyon"

	for_countries = { PRM }

	can_use = { always = yes }

	division_types = { "motorized" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Motor-Infanteriy-Divizyon"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1. Motor-Infanteriy-Divizyon 'Manshteins Mener'" }
		2 = { "2. Motor-Infanteriy-Divizyon 'Shnele Ariyer'" }
		3 = { "3. Motor-Infanteriy-Divizyon 'Farende Zoldaten'" }
		4 = { "4. Motor-Infanteriy-Divizyon 'Kriyger auf Redern'" }
		5 = { "5. Motor-Infanteriy-Divizyon 'Pantser-Froynde'" }
		6 = { "6. Motor-Infanteriy-Divizyon 'Flinke Vizel'" }
		7 = { "7. Motor-Infanteriy-Divizyon 'Puma'" }
		8 = { "8. Motor-Infanteriy-Divizyon 'Gungnir'" }
		9 = { "9. Motor-Infanteriy-Divizyon 'Holmgards Vekhter'" }
		10 = { "10. Motor-Infanteriy-Divizyon 'Paulus'" }
	}
}


PRM_Mechanized = {
	name = "Mekhaniziyrte Infanteriy-Divizyon"

	for_countries = { PRM }

	can_use = { always = yes }

	division_types = { "MBT" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Mekhaniziyrte Infanteriy-Divizyon"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1. Mekhaniziyrte Infanteriy-Divizyon 'Forhut'" }
		2 = { "2. Mekhaniziyrte Infanteriy-Divizyon 'Shtostrup'" }
		3 = { "3. Mekhaniziyrte Infanteriy-Divizyon 'Shverpunt'" }
		4 = { "4. Mekhaniziyrte Infanteriy-Divizyon 'Angrif'" }
		5 = { "5. Mekhaniziyrte Infanteriy-Divizyon 'Durkhbrukh'" }
		6 = { "6. Mekhaniziyrte Infanteriy-Divizyon 'Sikhel-Shnit'" }
		7 = { "7. Mekhaniziyrte Infanteriy-Divizyon 'Laevateinn'" }
		8 = { "8. Mekhaniziyrte Infanteriy-Divizyon 'Metalfaust'" }
		9 = { "9. Mekhaniziyrte Infanteriy-Divizyon 'Braukhitsh'" }
		10 = { "10. Mekhaniziyrte Infanteriy-Divizyon 'Shtalbeyser'" }
	}
}