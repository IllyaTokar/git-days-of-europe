﻿# Division template historical names system. Is a new method of naming the divisions based on the names-group assigned to it's template.
# If we run out of all historical names, the names will be assigned in the old way.
#
# Each group has a made up tag. Use it if you want to append more, or replace existing names with the extra txt files (modding).
#
# for_countries - lists all countries that can use it. If empty, or entire tag is missing, all countries in the world can use it.
#
# can_use - is a trigger that locks/unlocks the group under specific circumstances. The trigger is in a country scope.
#
# division_types - is a list of tokens to corresponding unit types. A player can in fact use any group of names for a div.template
#                  however this tag is a helper for an automated choice (for AI, or if the group must switch on it's own, because
#                  for example the current one is no longer available due to the can_use trigger saying so).
#                  In automated choice, the division template must have at least 1 of the following types for it to be chosen.
#
# fallback_name - Is going to be used if we run out of the scripted historical names. If you want to use the old division naming
#                 mechanics to be used for fallbacks, then just skip this option.
#
# unordered - It's a list of historical division names that did not have a number. Regardless if such names happened in history or not
#             this option is available here mainly for a mods.
#
# ordered - Is a list of all historical names. 
#           Numbers must start from 1 and up. 
#           Numbers can't repeat in one scope.
#           If you script the name for this group, at the same number (for example in a mod in another file), the name will be override.
#           All arguments between the brackets must be separated by spaces. Each argument is wrapped in "quotas".
#           1st argument = The name. It must contain either: 
#                          %d (for decimal number placement)
#                          %s (for string number placement - ROMAN numbers like XIV).
#           2nd argument is optional = A localizable text describing this historical division. The text will be visible in the tooltip
#                                      where you choose the historical division name.
#           3rd argument is optional = An URL preferably pointing to the WIKI. It's a future feature that is not currently working in
#                                      the current game version.
ISH_Infantry = {
	name = "Strelkovaya Diviziya"

	for_countries = { ISH }

	can_use = { always = yes }

	division_types = { "infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%dya Strelkovaya Diviziya"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "%dya Strelkovaya Diviziya 'Tyumen'" }
		2 = { "%dya Strelkovaya Diviziya" }
		3 = { "%dya Strelkovaya Diviziya" }
		4 = { "%dya Strelkovaya Diviziya" }
		5 = { "%dya Strelkovaya Diviziya" }
		6 = { "%dya Strelkovaya Diviziya" }
		7 = { "%dya Strelkovaya Diviziya" }
		8 = { "%dya Strelkovaya Diviziya" }
		9 = { "%dya Strelkovaya Diviziya" }
		10 = { "%dya Strelkovaya Diviziya" }
		11 = { "%dya Strelkovaya Diviziya" }
		12 = { "%dya Strelkovaya Diviziya" }
		13 = { "%dya Strelkovaya Diviziya 'Krasnaya Volna'" }
		14 = { "%dya Strelkovaya Diviziya" }
		15 = { "%dya Strelkovaya Diviziya" }
		16 = { "%dya Strelkovaya Diviziya" }
		17 = { "%dya Strelkovaya Diviziya" }
		18 = { "%dya Strelkovaya Diviziya 'Velikoe Srazhenie'" }
		19 = { "%dya Strelkovaya Diviziya" }
		20 = { "%dya Strelkovaya Diviziya" }
		21 = { "%dya Strelkovaya Diviziya" }
		22 = { "%dya Strelkovaya Diviziya" }
		23 = { "%dya Strelkovaya Diviziya 'Fantom'" }
		24 = { "%dya Strelkovaya Diviziya" }
		25 = { "%dya Strelkovaya Diviziya" }
		26 = { "%dya Strelkovaya Diviziya" }
		27 = { "%dya Strelkovaya Diviziya" }
		28 = { "%dya Strelkovaya Diviziya" }
		29 = { "%dya Strelkovaya Diviziya" }
		30 = { "%dya Strelkovaya Diviziya" }
		31 = { "%dya Strelkovaya Diviziya" }
		32 = { "%dya Strelkovaya Diviziya 'Iosif Stalin'" }
		33 = { "%dya Strelkovaya Diviziya" }
		34 = { "%dya Strelkovaya Diviziya" }
		35 = { "%dya Strelkovaya Diviziya" }
		36 = { "%dya Strelkovaya Diviziya" }
		37 = { "%dya Strelkovaya Diviziya" }
		38 = { "%dya Strelkovaya Diviziya" }
		39 = { "%dya Strelkovaya Diviziya" }
		40 = { "%dya Strelkovaya Diviziya" }
	}
}

ISH_Conscripts = {
	name = "Krasnogvardeyskoye Opolcheniye"

	for_countries = { ISH }

	can_use = { always = yes }

	division_types = { "light_infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { AUS_INF_01 }

	fallback_name = "%d. Krasnogvardeyskoye Opolcheniye"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		2 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		3 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		4 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		5 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		6 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		7 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		8 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		9 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		10 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		11 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		12 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		13 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		14 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		15 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		16 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		17 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		18 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		19 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		20 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		21 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		22 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		23 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		24 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		25 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		26 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		27 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		28 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		29 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		30 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		31 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		32 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		33 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		34 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		35 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		36 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		37 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		38 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		39 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		40 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		41 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		42 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		43 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		44 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		45 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		46 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		47 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		48 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		49 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		50 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		51 = { "%d. Krasnogvardeyskoye Opolcheniye" }
		52 = { "%d. Krasnogvardeyskoye Opolcheniye" }
	}
}