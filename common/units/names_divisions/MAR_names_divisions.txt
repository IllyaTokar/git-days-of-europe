﻿MAR_Infantry = {
	name = "Tsarskiye Zashchitniki"

	for_countries = { MAR }

	can_use = { always = yes }

	division_types = { "infantry" }

	fallback_name = "%d. Tsarskiye Zashchitniki"

	ordered = {
		1 = { "%d. Tsarskiye Zashchitniki" }
		2 = { "%d. Tsarskiye Zashchitniki" }
		3 = { "%d. Tsarskiye Zashchitniki" }
		4 = { "%d. Tsarskiye Zashchitniki" }
		5 = { "%d. Tsarskiye Zashchitniki" }
		6 = { "%d. Tsarskiye Zashchitniki" }
		7 = { "%d. Tsarskiye Zashchitniki" }
		8 = { "%d. Tsarskiye Zashchitniki" }
		9 = { "%d. Tsarskiye Zashchitniki" }
		10 = { "%d. Tsarskiye Zashchitniki" }
		11 = { "%d. Tsarskiye Zashchitniki" }
		12 = { "%d. Tsarskiye Zashchitniki" }
		13 = { "%d. Tsarskiye Zashchitniki" }
		14 = { "%d. Tsarskiye Zashchitniki" }
		15 = { "%d. Tsarskiye Zashchitniki" }
		16 = { "%d. Tsarskiye Zashchitniki" }
		17 = { "%d. Tsarskiye Zashchitniki" }
		18 = { "%d. Tsarskiye Zashchitniki" }
		19 = { "%d. Tsarskiye Zashchitniki" }
		20 = { "%d. Tsarskiye Zashchitniki" }
		21 = { "%d. Tsarskiye Zashchitniki" }
		22 = { "%d. Tsarskiye Zashchitniki" }
		23 = { "%d. Tsarskiye Zashchitniki" }
		24 = { "%d. Tsarskiye Zashchitniki" }
		25 = { "%d. Tsarskiye Zashchitniki" }
		26 = { "%d. Tsarskiye Zashchitniki" }
		27 = { "%d. Tsarskiye Zashchitniki" }
		28 = { "%d. Tsarskiye Zashchitniki" }
		29 = { "%d. Tsarskiye Zashchitniki" }
		30 = { "%d. Tsarskiye Zashchitniki" }
	}
}

MAR_Conscripts = {
	name = "Narodnoye Opolcheniye"

	for_countries = { MAR }

	can_use = { always = yes }

	division_types = { "light_infantry" }

	fallback_name = "%d. Narodnoye Opolcheniye"

	ordered = {
		1 = { "%d. Narodnoye Opolcheniye" }
		2 = { "%d. Narodnoye Opolcheniye" }
		3 = { "%d. Narodnoye Opolcheniye" }
		4 = { "%d. Narodnoye Opolcheniye" }
		5 = { "%d. Narodnoye Opolcheniye" }
		6 = { "%d. Narodnoye Opolcheniye" }
		7 = { "%d. Narodnoye Opolcheniye" }
		8 = { "%d. Narodnoye Opolcheniye" }
		9 = { "%d. Narodnoye Opolcheniye" }
		10 = { "%d. Narodnoye Opolcheniye" }
		11 = { "%d. Narodnoye Opolcheniye" }
		12 = { "%d. Narodnoye Opolcheniye" }
		13 = { "%d. Narodnoye Opolcheniye" }
		14 = { "%d. Narodnoye Opolcheniye" }
		15 = { "%d. Narodnoye Opolcheniye" }
		16 = { "%d. Narodnoye Opolcheniye" }
		17 = { "%d. Narodnoye Opolcheniye" }
		18 = { "%d. Narodnoye Opolcheniye" }
		19 = { "%d. Narodnoye Opolcheniye" }
		20 = { "%d. Narodnoye Opolcheniye" }
		21 = { "%d. Narodnoye Opolcheniye" }
		22 = { "%d. Narodnoye Opolcheniye" }
		23 = { "%d. Narodnoye Opolcheniye" }
		24 = { "%d. Narodnoye Opolcheniye" }
		25 = { "%d. Narodnoye Opolcheniye" }
		26 = { "%d. Narodnoye Opolcheniye" }
		27 = { "%d. Narodnoye Opolcheniye" }
		28 = { "%d. Narodnoye Opolcheniye" }
		29 = { "%d. Narodnoye Opolcheniye" }
		30 = { "%d. Narodnoye Opolcheniye" }
	}
}

MAR_Motorized = {
	name = "Motorizovannaya Pekhota"

	for_countries = { MAR }

	can_use = { always = yes }

	division_types = { "motorized" }

	fallback_name = "%d. Motorizovannaya Pekhota"

	ordered = {
		1 = { "%d. Motorizovannaya Pekhota" }
		2 = { "%d. Motorizovannaya Pekhota" }
		3 = { "%d. Motorizovannaya Pekhota" }
		4 = { "%d. Motorizovannaya Pekhota" }
		5 = { "%d. Motorizovannaya Pekhota" }
		6 = { "%d. Motorizovannaya Pekhota" }
		7 = { "%d. Motorizovannaya Pekhota" }
		8 = { "%d. Motorizovannaya Pekhota" }
		9 = { "%d. Motorizovannaya Pekhota" }
		10 = { "%d. Motorizovannaya Pekhota" }
		11 = { "%d. Motorizovannaya Pekhota" }
		12 = { "%d. Motorizovannaya Pekhota" }
		13 = { "%d. Motorizovannaya Pekhota" }
		14 = { "%d. Motorizovannaya Pekhota" }
		15 = { "%d. Motorizovannaya Pekhota" }
		16 = { "%d. Motorizovannaya Pekhota" }
		17 = { "%d. Motorizovannaya Pekhota" }
		18 = { "%d. Motorizovannaya Pekhota" }
		19 = { "%d. Motorizovannaya Pekhota" }
		20 = { "%d. Motorizovannaya Pekhota" }
		21 = { "%d. Motorizovannaya Pekhota" }
		22 = { "%d. Motorizovannaya Pekhota" }
		23 = { "%d. Motorizovannaya Pekhota" }
		24 = { "%d. Motorizovannaya Pekhota" }
		25 = { "%d. Motorizovannaya Pekhota" }
		26 = { "%d. Motorizovannaya Pekhota" }
		27 = { "%d. Motorizovannaya Pekhota" }
		28 = { "%d. Motorizovannaya Pekhota" }
		29 = { "%d. Motorizovannaya Pekhota" }
		30 = { "%d. Motorizovannaya Pekhota" }
	}
}