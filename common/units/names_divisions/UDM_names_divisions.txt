﻿UDM_Guards = {
	name = "Gvardiya Strelkovaya Diviziya"

	for_countries = { UDM }

	can_use = { always = yes }

	division_types = { "elite_infantry" }

	fallback_name = "%dya Gvardiya Strelkovaya Diviziya"

	ordered = {
		1 = { "%dya Gvardiya Strelkovaya Diviziya" }
	}
}

UDM_Motorized = {
	name = "Gvardiya Motostrelkovaya Diviziya"

	for_countries = { UDM }

	can_use = { always = yes }

	division_types = { "motorized" }

	fallback_name = "%dya Gvardiya Motostrelkovaya Diviziya"

	ordered = {
		1 = { "%dya Gvardiya Motostrelkovaya Diviziya" }
	}
}

UDM_Infantry = {
	name = "Strelkovaya Diviziya"

	for_countries = { UDM }

	can_use = { always = yes }

	division_types = { "infantry" }

	fallback_name = "%dya Strelkovaya Diviziya"

	ordered = {
		1 = { "%dya Strelkovaya Diviziya" }
	}
}