sub_units = {

	APC = {
		sprite = mechanized
		map_icon_category = infantry

		priority = 610
		ai_priority = 200
		active = yes

		type = {
			mechanized
		}
		
		group = mobile
		
		categories = {
			category_front_line
			category_army
			category_APC_1950
		}

		combat_width = 2
		
		#Size Definitions
		max_strength = 30
		max_organisation = 60
		default_morale = 0.3
		manpower = 1200

		#Misc Abilities
		training_time = 120
		suppression = 5
		weight = 1
		
		supply_consumption = 0.18
		
		ap_attack = -0.725
		hard_attack = -0.55
		soft_attack = 0.3
		
		# needed since we give so much bonus to infantry even with no mech equipment
		essential = {
			infantry_equipment
			APC_equipment
		}

		# this is what moves us and sets speed
		transport = APC_equipment

		need = {
			APC_equipment = 50
			infantry_equipment = 100
			anti_tank_equipment = 20
		}

		forest = {
			attack = -0.2
			movement = -0.2
		}
		mountain = {
			attack = -0.05
			movement = -0.2
		}
		jungle = {
			attack = -0.3
			movement = -0.5
		}
		marsh = {
			attack = -0.1
			movement = -0.5
		}
		urban = {
			attack = -0.2
			defence = -0.05
			movement = 0.2
		}
		river = {
			attack = -0.2
			movement = -0.2
		}
		amphibious = {
			attack = -0.4
			movement = -0.3
		}
	}
}