GER_PROXY_STRAT = {
	enable = {
		original_tag = GER
		YEM = {	has_war_with = NYM }
	}

	ai_strategy = {
		type = send_volunteers_desire
		id = YEM
		value = 1000
	}
}

GER_PROXY_STRAT_YEM = {
	enable = {
		original_tag = GER
		YEM = {	has_war_with = NYM }
	}

	ai_strategy = {
		type = send_volunteers_desire
		id = NYM
		value = 1000
	}
}

GGR_SAW_STRAT_ANG = {
	enable = {
		original_tag = GER
		MZB = { has_war_with = SAF }
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = ANG
		value = 1000
	}
}

GGR_SAW_STRAT_COG = {
	enable = {
		original_tag = GER
		MZB = { has_war_with = SAF }
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = COG
		value = 1000
	}
}