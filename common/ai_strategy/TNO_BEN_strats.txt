BEN_strat_dont_go_to_fucking_mongolia = {
	enable = {
		tag = BEN
		OR = {
			RAJ = {	exists = yes }
			INC = {	exists = yes }
			INF = {	exists = yes }
		}
	}

	ai_strategy = {
		type = conquer
		id = RAJ
		value = 1000
	}
	ai_strategy = {
		type = conquer
		id = INC
		value = 1000
	}
	ai_strategy = {
		type = conquer
		id = INF
		value = 1000
	}
}
BEN_strat_dont_go_to_fucking_mongolia_2 = {
	enable = {
		tag = BEN
		MON = {	exists = yes }
	}

	ai_strategy = {
		type = conquer
		id = MON
		value = -1000
	}
}
BEN_strat_butterfly = {
	enable = {
		tag = BEN
		BEN = {	has_war_with = INC }
	}

	ai_strategy = {
		type = conquer
		id = INC  
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = INC  
		value = 1000
	}
}
BEN_strat_elephant = {
	enable = {
		tag = BEN
		BEN = {	has_war_with = RAJ }
	}

	ai_strategy = {
		type = conquer
		id = RAJ  
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = RAJ  
		value = 1000
	}
}
BEN_strat_reconduction = {
	enable = {
		tag = BEN
		BEN = {	has_war_with = INF }
	}

	ai_strategy = {
		type = conquer
		id = INF  
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = INF  
		value = 1000
	}
}
INF_fuck_raj = {
	enable = {
	tag = INF
		INF = {	has_war_with = RAJ }
	}

	ai_strategy = {
		type = conquer
		id = RAJ  
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = RAJ  
		value = 1000
	}
}
INF_fuck_inc = {
	enable = {
		tag = INF
		INF = {	has_war_with = INC }
	}

	ai_strategy = {
		type = conquer
		id = INC  
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = INC  
		value = 1000
	}
}
INC_fuck_raj = {
	enable = {
		tag = INC
		INC = {	has_war_with = RAJ }
	}

	ai_strategy = {
		type = conquer
		id = RAJ  
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = RAJ  
		value = 1000
	}
	}
INC_fuck_inf = {
	enable = {
		tag = INC
		INC = {	has_war_with = INF }
	}

	ai_strategy = {
		type = conquer
		id = INF
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = INF
		value = 1000
	}
}
RAJ_fuck_INC = {
	enable = {
		tag = RAJ
		RAJ = {	has_war_with = INC }
			}

	ai_strategy = {
		type = conquer
		id = INC  
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = INC  
		value = 1000
	}
}
RAJ_fuck_INF = {
	enable = {
		tag = RAJ
		RAJ = {	has_war_with = INF }
	}

	ai_strategy = {
		type = conquer
		id = INF 
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = INF
		value = 1000
	}
}