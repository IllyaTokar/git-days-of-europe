ORS_strat_great_raid = {
	enable = {
		tag = ORS
		ORS = {	has_war_with = URL }
	}

	ai_strategy = {
		type = conquer
		id = URL
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = URL
		value = 1000
	}
}

ORS_strat_great_raid_two = {
	enable = {
		tag = ORS
		ORS = {	has_war_with = ORE }
	}

	ai_strategy = {
		type = conquer
		id = ORE
		value = 1000
	}
	ai_strategy = {
		type = consider_weak
		id = ORE
		value = 1000
	}
}

URL_strat_anti_dirlewanger = {
	enable = {
		tag = URL
		URL = {	has_war_with = ORS }
	}

	ai_strategy = {
		type = protect
		id = ORE
		value = 1000
	}
	ai_strategy = {
		type = befriend
		id = ORE
		value = 1000
	}
	ai_strategy = {
		type = conquer
		id = ORS
		value = 50
	}
}

ORE_strat_anti_dirlewanger = {
	enable = {
		tag = ORE
		ORE = {	has_war_with = ORS }
	}
	ai_strategy = {
		type = conquer
		id = ORS
		value = 10
	}
}
