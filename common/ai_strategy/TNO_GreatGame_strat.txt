great_game_isolation_strat = {
	enable = {
		OR = {
			AND = {
				original_tag = BUL
				has_global_flag = GreatGame_game_1_tie
			}
			AND = {
				original_tag = HUN
				has_global_flag = GreatGame_game_2_tie
			}
			AND = {
				original_tag = ROM
				has_global_flag = GreatGame_game_3_tie
			}
			AND = {
				original_tag = FRA
				has_global_flag = GreatGame_game_4_tie
			}
			AND = {
				original_tag = SER
				has_global_flag = GreatGame_game_5_tie
			}
		}
	}
	abort = {
		NOT = { country_exists = GER }
		NOT = { country_exists = ITA }
	}

	ai_strategy = {
		type = alliance
		id = GER
		value = -200
	}
	ai_strategy = {
		type = alliance
		id = ITA
		value = -200
	}
}

great_game_german_aligned_strat = {
	enable = {
		OR = {
			AND = {
				original_tag = BUL
				has_global_flag = GreatGame_game_1_GER_victory
			}
			AND = {
				original_tag = HUN
				has_global_flag = GreatGame_game_2_GER_victory
			}
			AND = {
				original_tag = SER
				has_global_flag = GreatGame_game_5_GER_victory
			}
		}
	}
	abort = {
		NOT = { country_exists = GER }
	}

	ai_strategy = {
		type = alliance
		id = GER
		value = 200
	}
	ai_strategy = {
		type = alliance
		id = ITA
		value = -200
	}
}

great_game_italian_aligned_strat = {
	enable = {
		OR = {
			AND = {
				original_tag = BUL
				has_global_flag = GreatGame_game_1_ITA_victory
			}
			AND = {
				original_tag = HUN
				has_global_flag = GreatGame_game_2_ITA_victory
			}
			AND = {
				original_tag = FRA
				has_global_flag = GreatGame_game_4_ITA_victory
			}
			AND = {
				original_tag = SER
				has_global_flag = GreatGame_game_5_ITA_victory
			}
		}
	}
	abort = {
		NOT = { country_exists = ITA }
	}

	ai_strategy = {
		type = alliance
		id = GER
		value = -200
	}
	ai_strategy = {
		type = alliance
		id = ITA
		value = 200
	}
}
