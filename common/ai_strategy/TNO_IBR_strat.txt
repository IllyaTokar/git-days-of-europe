IBR_ALG_war_strat = {
	enable = {
		original_tag = IBR
		SLG = {
			has_war_with = ILG
		}
	}

	ai_strategy = {
		type = send_volunteers_desire
		id = SLG
		value = 1000
	}
}

IBR_SAW_strat = {
	enable = {
		original_tag = IBR
		SAF = {
			has_war_with = BOR
		}
		has_global_flag = south_african_war
		has_country_flag = IBR_Intervine_SAW
		NOT = {
			SLG = {
				has_war_with = ILG
			}
		}
	}

	ai_strategy = {
		type = send_volunteers_desire
		id = SAF
		value = 1000
	}
}

IBR_ICW_pre_strat = {
	enable = {
		original_tag = IBR
		has_global_flag = Iberian_Wars
		num_divisions < 40
	}

	abort = {
		OR = {
			has_global_flag = Iberian_Anarchy_flag
			has_country_flag = Iberian_Wars_Resigned
		}
	}

	ai_strategy = {
		type = put_unit_buffers

		ratio = 100

		states = {
			41
			1013
			999
			175
		}

		subtract_invasions_from_need = no
	}
}
