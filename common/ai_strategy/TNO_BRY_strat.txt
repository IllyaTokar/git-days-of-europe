BRY_mutiny_strat = {
	enable = {
		tag = BRY
		MEN = {
			has_war_with = MON
		}
	}

	abort = {
		always = no
	}

	abort_when_not_enabled = yes

	ai_strategy = {
		type = front_unit_request
		
		state = 815
		state = 330

		value = -100
	}
}
