US_PROXY_STRAT = {
	enable = {
		OR = {
			original_tag = USA
			AND = {
				OR = {
					original_tag = CAN
					original_tag = NZL
					original_tag = AST
					original_tag = GUY
					original_tag = WIN
				}
				has_country_flag = will_send_troops
			}
		}
		OR = {
			SAF = {	has_war_with = BOR }
			SAF = {	has_war_with = ANG }
			SAF = {	has_war_with = MZB }
			SAF = {	has_war_with = COG }
		}
	}

	abort = {
		OR = {
			has_global_flag = SAF_ceasefire
			NOT = { country_exists = SAF }
		}
	}

	ai_strategy = {
		type = send_volunteers_desire
		id = SAF
		value = 1000
	}

	ai_strategy = {
		type = conquer
		id = BOR
		value = 200
	}

	ai_strategy = {
		type = conquer
		id = COG
		value = 200
	}

	ai_strategy = {
		type = conquer
		id = ANG
		value = 200
	}

	ai_strategy = {
		type = conquer
		id = MZB
		value = 200
	}

	ai_strategy = {
		type = area_priority
		id = central_south_africa
		value = 300
	}
}

US_FRI_help = {
	enable = {
		original_tag = USA
		FRI = {	has_war_with = INS } #indonesian civil war
	}

	abort_when_not_enabled = yes

	ai_strategy = {
		type = send_volunteers_desire
		id = FRI #free indonesia
		value = 1000
	}

	ai_strategy = {
		type = conquer
		id = INS
		value = 200
	}
}

US_IRL_help = {
	enable = {
		original_tag = USA
		IRL = { has_war = yes } #iranian civil war
	}

	abort_when_not_enabled = yes

	ai_strategy = {
		type = send_volunteers_desire
		id = IRL #Iran libs
		value = 1000
	}
	
	ai_strategy = {
		type = conquer
		id = PER
		value = 200
	}

	ai_strategy = {
		type = conquer
		id = IRI #Islamic Republic of Iran
		value = 200
	}

	ai_strategy = {
		type = conquer
		id = IRC #Kingdom of Iran
		value = 200
	}

	ai_strategy = {
		type = conquer
		id = IRN #National State of Iran
		value = 200
	}

	ai_strategy = {
		type = conquer
		id = IRB #Iranian Baluchi
		value = 200
	}

	ai_strategy = {
		type = conquer
		id = IKR #Iranian Kurds
		value = 200
	}

	ai_strategy = {
		type = conquer
		id = IRA #Iranian Azeri
		value = 200
	}
}

US_conquer_GUY = {
	enable = {
		original_tag = USA
		has_war_with = GUY
	}

	ai_strategy = {
		type = conquer
		id = "GUY"
		value = 1000
	}

	ai_strategy = {
		type = invade
		id = "GUY"
		value = 1000
	}
	ai_strategy = {
		type = garrison
		value = -500
	}
}


US_PROXY_STRAT_MAD = {
	enable = {
		original_tag = USA
		OR = {
			MAD = {	has_war_with = MDG }
			MAD = {	has_war_with = MDM }
			MAD = {	has_war_with = MDJ }
		}
	}

	abort_when_not_enabled = yes

	ai_strategy = {
		type = send_volunteers_desire
		id = MAD
		value = 1000
	}

	#ai_strategy = {
		#	type = conquer
		#	id = MDG
		#	value = 200
		#}
	#
	#ai_strategy = {
		#	type = conquer
		#	id = MDM
		#	value = 200
		#}
	#
	#ai_strategy = {
		#	type = conquer
		#	id = MDJ
		#	value = 200
		#}
}
