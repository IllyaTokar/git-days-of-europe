TNO_improve_poverty_low = {
	custom_effect_tooltip = improve_poverty_small
	add_to_variable = { poverty_monthly_change = 1 }
}

TNO_improve_poverty_med = {
	custom_effect_tooltip = improve_poverty_medium
	add_to_variable = { poverty_monthly_change = 2 }
}

TNO_improve_poverty_high = {
	custom_effect_tooltip = improve_poverty_big
	add_to_variable = { poverty_monthly_change = 3 }
}

TNO_worsen_poverty_low = {
	custom_effect_tooltip = worsen_poverty_small
	add_to_variable = { poverty_monthly_change = -1 }
}

TNO_worsen_poverty_med = {
	custom_effect_tooltip = worsen_poverty_medium
	add_to_variable = { poverty_monthly_change = -2 }
}

TNO_worsen_poverty_high = {
	custom_effect_tooltip = worsen_poverty_big
	add_to_variable = { poverty_monthly_change = -3 }
}

TNO_improve_academic_base_low = {
	custom_effect_tooltip = improve_academic_base_small
	add_to_variable = { academic_base_monthly_change = 1 }
}

TNO_improve_academic_base_med = {
	custom_effect_tooltip = improve_academic_base_medium
	add_to_variable = { academic_base_monthly_change = 2 }
}

TNO_improve_academic_base_high = {
	custom_effect_tooltip = improve_academic_base_big
	add_to_variable = { academic_base_monthly_change = 3 }
}

TNO_worsen_academic_base_low = {
	custom_effect_tooltip = worsen_academic_base_small
	add_to_variable = { academic_base_monthly_change = -1 }
}

TNO_worsen_academic_base_med = {
	custom_effect_tooltip = worsen_academic_base_medium
	add_to_variable = { academic_base_monthly_change = -2 }
}

TNO_worsen_academic_base_high = {
	custom_effect_tooltip = worsen_academic_base_big
	add_to_variable = { academic_base_monthly_change = -3 }
}

TNO_improve_research_facilities_low = {
	custom_effect_tooltip = improve_research_facilities_small
	add_to_variable = { research_facilities_monthly_change = 1 }
}

TNO_improve_research_facilities_med = {
	custom_effect_tooltip = improve_research_facilities_medium
	add_to_variable = { research_facilities_monthly_change = 2 }
}

TNO_improve_research_facilities_high = {
	custom_effect_tooltip = improve_research_facilities_big
	add_to_variable = { research_facilities_monthly_change = 3 }
}

TNO_worsen_research_facilities_low = {
	custom_effect_tooltip = worsen_research_facilities_small
	add_to_variable = { research_facilities_monthly_change = -1 }
}

TNO_worsen_research_facilities_med = {
	custom_effect_tooltip = worsen_research_facilities_medium
	add_to_variable = { research_facilities_monthly_change = -2 }
}

TNO_worsen_research_facilities_high = {
	custom_effect_tooltip = worsen_research_facilities_big
	add_to_variable = { research_facilities_monthly_change = -3 }
}

TNO_improve_industrial_equipment_low = {
	custom_effect_tooltip = improve_industrial_equipment_small
	add_to_variable = { industrial_equipment_monthly_change = 1 }
}

TNO_improve_industrial_equipment_med = {
	custom_effect_tooltip = improve_industrial_equipment_medium
	add_to_variable = { industrial_equipment_monthly_change = 2 }
}

TNO_improve_industrial_equipment_high = {
	custom_effect_tooltip = improve_industrial_equipment_big
	add_to_variable = { industrial_equipment_monthly_change = 3 }
}

TNO_worsen_industrial_equipment_low = {
	custom_effect_tooltip = worsen_industrial_equipment_small
	add_to_variable = { industrial_equipment_monthly_change = -1 }
}

TNO_worsen_industrial_equipment_med = {
	custom_effect_tooltip = worsen_industrial_equipment_medium
	add_to_variable = { industrial_equipment_monthly_change = -2 }
}

TNO_worsen_industrial_equipment_high = {
	custom_effect_tooltip = worsen_industrial_equipment_big
	add_to_variable = { industrial_equipment_monthly_change = -3 }
}

TNO_improve_industrial_expertise_low = {
	custom_effect_tooltip = improve_industrial_expertise_small
	add_to_variable = { industrial_expertise_monthly_change = 1 }
}

TNO_improve_industrial_expertise_med = {
	custom_effect_tooltip = improve_industrial_expertise_medium
	add_to_variable = { industrial_expertise_monthly_change = 2 }
}

TNO_improve_industrial_expertise_high = {
	custom_effect_tooltip = improve_industrial_expertise_big
	add_to_variable = { industrial_expertise_monthly_change = 3 }
}

TNO_worsen_industrial_expertise_low = {
	custom_effect_tooltip = worsen_industrial_expertise_small
	add_to_variable = { industrial_expertise_monthly_change = -1 }
}

TNO_worsen_industrial_expertise_med = {
	custom_effect_tooltip = worsen_industrial_expertise_medium
	add_to_variable = { industrial_expertise_monthly_change = -2 }
}

TNO_worsen_industrial_expertise_high = {
	custom_effect_tooltip = worsen_industrial_expertise_big
	add_to_variable = { industrial_expertise_monthly_change = -3 }
}

TNO_improve_agriculture_low = {
	custom_effect_tooltip = improve_agriculture_small
	add_to_variable = { agriculture_monthly_change = 1 }
}

TNO_improve_agriculture_med = {
	custom_effect_tooltip = improve_agriculture_medium
	add_to_variable = { agriculture_monthly_change = 2 }
}

TNO_improve_agriculture_high = {
	custom_effect_tooltip = improve_agriculture_big
	add_to_variable = { agriculture_monthly_change = 3 }
}

TNO_worsen_agriculture_low = {
	custom_effect_tooltip = worsen_agriculture_small
	add_to_variable = { agriculture_monthly_change = -1 }
}

TNO_worsen_agriculture_med = {
	custom_effect_tooltip = worsen_agriculture_medium
	add_to_variable = { agriculture_monthly_change = -2 }
}

TNO_worsen_agriculture_high = {
	custom_effect_tooltip = worsen_agriculture_big
	add_to_variable = { agriculture_monthly_change = -3 }
}

TNO_improve_army_professionalism_low = {
	custom_effect_tooltip = improve_army_professionalism_small
	add_to_variable = { army_professionalism_monthly_change = 1 }
}

TNO_improve_army_professionalism_med = {
	custom_effect_tooltip = improve_army_professionalism_medium
	add_to_variable = { army_professionalism_monthly_change = 2 }
}

TNO_improve_army_professionalism_high = {
	custom_effect_tooltip = improve_army_professionalism_big
	add_to_variable = { army_professionalism_monthly_change = 3 }
}

TNO_worsen_army_professionalism_low = {
	custom_effect_tooltip = worsen_army_professionalism_small
	add_to_variable = { army_professionalism_monthly_change = -1 }
}

TNO_worsen_army_professionalism_med = {
	custom_effect_tooltip = worsen_army_professionalism_medium
	add_to_variable = { army_professionalism_monthly_change = -2 }
}

TNO_worsen_army_professionalism_high = {
	custom_effect_tooltip = worsen_army_professionalism_big
	add_to_variable = { army_professionalism_monthly_change = -3 }
}

TNO_improve_nuclear_stockpile_low = {
	custom_effect_tooltip = improve_nuclear_stockpile_small
	add_to_variable = { nuclear_stockpile_monthly_change = 1 }
}

TNO_improve_nuclear_stockpile_med = {
	custom_effect_tooltip = improve_nuclear_stockpile_medium
	add_to_variable = { nuclear_stockpile_monthly_change = 2 }
}

TNO_improve_nuclear_stockpile_high = {
	custom_effect_tooltip = improve_nuclear_stockpile_big
	add_to_variable = { nuclear_stockpile_monthly_change = 3 }
}

TNO_worsen_nuclear_stockpile_low = {
	custom_effect_tooltip = worsen_nuclear_stockpile_small
	add_to_variable = { nuclear_stockpile_monthly_change = -1 }
}

TNO_worsen_nuclear_stockpile_med = {
	custom_effect_tooltip = worsen_nuclear_stockpile_medium
	add_to_variable = { nuclear_stockpile_monthly_change = -2 }
}

TNO_worsen_nuclear_stockpile_high = {
	custom_effect_tooltip = worsen_nuclear_stockpile_big
	add_to_variable = { nuclear_stockpile_monthly_change = -3 }
}
