TNO_RAJ_upgrade_planning_minor = {
	add_to_variable = {
			var = RAJElectrificationPlanning
			value = 1
	}
}

TNO_RAJ_upgrade_planning = {
	add_to_variable = {
			var = RAJElectrificationPlanning
			value = 3
	}
}

TNO_RAJ_upgrade_planning_major = {
	add_to_variable = {
			var = RAJElectrificationPlanning
			value = 5
	}
}

TNO_RAJ_upgrade_planning_dynamic = {
	set_temp_variable = {
		var = planningtemp
		value = party_popularity@ruling_party
	}
	multiply_temp_variable = { planningtemp = 1 }
	add_to_variable = { RAJElectrificationPlanning = planningtemp }
}

TNO_RAJ_upgrade_output_minor = {
	set_temp_variable = {
		var = outputtemp
		value = 0.01
	}
	set_temp_variable = {
		var = multiplier1
		value = RAJElectrificationPlanning
	}
	multiply_temp_variable = { outputtemp = multiplier1 }
	set_temp_variable = {
		var = tempboost
		value = RAJBonusEfficiencyFactor
	}
	divide_temp_variable = { tempboost = 100 }
	add_to_temp_variable = { tempboost = 1 }
	multiply_temp_variable = { outputtemp = tempboost }
	divide_temp_variable = { outputtemp = 2 }
	add_to_variable = { RAJElectrificationOutput = outputtemp }
}

TNO_RAJ_upgrade_output = {
	set_temp_variable = {
		var = outputtemp
		value = 0.025
	}
	set_temp_variable = {
		var = multiplier1
		value = RAJElectrificationPlanning
	}
	multiply_temp_variable = { outputtemp = multiplier1 }
	set_temp_variable = {
		var = tempboost
		value = RAJBonusEfficiencyFactor
	}
	divide_temp_variable = { tempboost = 100 }
	add_to_temp_variable = { tempboost = 1 }
	multiply_temp_variable = { outputtemp = tempboost }
	divide_temp_variable = { outputtemp = 2 }
	add_to_variable = { RAJElectrificationOutput = outputtemp }
}

TNO_RAJ_upgrade_output_major = {
	set_temp_variable = {
		var = outputtemp
		value = 0.05
	}
	set_temp_variable = {
		var = multiplier1
		value = RAJElectrificationPlanning
	}
	multiply_temp_variable = { outputtemp = multiplier1 }
	set_temp_variable = {
		var = tempboost
		value = RAJBonusEfficiencyFactor
	}
	divide_temp_variable = { tempboost = 100 }
	add_to_temp_variable = { tempboost = 1 }
	multiply_temp_variable = { outputtemp = tempboost }
	divide_temp_variable = { outputtemp = 2 }
	add_to_variable = { RAJElectrificationOutput = outputtemp }
}

TNO_RAJ_upgrade_consumption_minor = {
	add_to_variable = {
			var = RAJElectrificationConsumption
			value = 0.75
	}
}

TNO_RAJ_upgrade_consumption = {
	add_to_variable = {
			var = RAJElectrificationConsumption
			value = 1.5
	}
}

TNO_RAJ_upgrade_consumption_major = {
	add_to_variable = {
			var = RAJElectrificationConsumption
			value = 2.25
	}
}

TNO_RAJ_upgrade_boost_major = {
	add_to_variable = {
			var = RAJBonusEfficiencyFactor
			value = 5
	}
}

TNO_RAJ_upgrade_boost = {
	add_to_variable = {
			var = RAJBonusEfficiencyFactor
			value = 3
	}
}

TNO_RAJ_upgrade_boost_minor = {
	add_to_variable = {
			var = RAJBonusEfficiencyFactor
			value = 1
	}
}

TNO_RAJ_increase_fossil_fuels_minor = {
	add_to_variable = {
			var = RAJPowerFossilFuels
			value = 0.1
	}
}

TNO_RAJ_increase_fossil_fuels = {
	add_to_variable = {
			var = RAJPowerFossilFuels
			value = 0.3
	}
}

TNO_RAJ_increase_fossil_fuels_major = {
	add_to_variable = {
			var = RAJPowerFossilFuels
			value = 0.5
	}
}

TNO_RAJ_increase_sustainable_minor = {
	add_to_variable = {
			var = RAJPowerFossilFuels
			value = -0.1
	}
}

TNO_RAJ_increase_sustainable = {
	add_to_variable = {
			var = RAJPowerFossilFuels
			value = -0.3
	}
}

TNO_RAJ_increase_sustainable_major = {
	add_to_variable = {
			var = RAJPowerFossilFuels
			value = -0.5
	}
}

TNO_RAJ_increase_urban_minor = {
	add_to_variable = {
			var = RAJRuralPowerUse
			value = -0.1
	}
}

TNO_RAJ_increase_urban = {
	add_to_variable = {
			var = RAJRuralPowerUse
			value = -0.3
	}
}

TNO_RAJ_increase_urban_major = {
	add_to_variable = {
			var = RAJRuralPowerUse
			value = -0.5
	}
}

TNO_RAJ_increase_rural_minor = {
	add_to_variable = {
			var = RAJRuralPowerUse
			value = 0.1
	}
}

TNO_RAJ_increase_rural = {
	add_to_variable = {
			var = RAJRuralPowerUse
			value = 0.3
	}
}

TNO_RAJ_increase_rural_major = {
	add_to_variable = {
			var = RAJRuralPowerUse
			value = 0.5
	}
}

TNO_RAJ_increase_households_minor = {
	set_temp_variable = {
		var = outputtemp
		value = 0.01
	}
	set_temp_variable = {
		var = multiplier1
		value = RAJElectrificationPlanning
	}
	multiply_temp_variable = { outputtemp = multiplier1 }
	add_to_variable = { RAJPowerHouseholdsCovered = outputtemp }
}

TNO_RAJ_increase_households = {
	set_temp_variable = {
		var = outputtemp
		value = 0.02
	}
	set_temp_variable = {
		var = multiplier1
		value = RAJElectrificationPlanning
	}
	multiply_temp_variable = { outputtemp = multiplier1 }
	add_to_variable = { RAJPowerHouseholdsCovered = outputtemp }
}

TNO_RAJ_increase_households_major = {
	set_temp_variable = {
		var = outputtemp
		value = 0.03
	}
	set_temp_variable = {
		var = multiplier1
		value = RAJElectrificationPlanning
	}
	multiply_temp_variable = { outputtemp = multiplier1 }
	add_to_variable = { RAJPowerHouseholdsCovered = outputtemp }
}

TNO_RAJ_electrification_monthly_check = {
	RAJ = {
	TNO_RAJ_calculate_agriculture_effect = yes
	TNO_RAJ_calculate_poverty_effect = yes
	TNO_RAJ_calculate_industrial_effect = yes
	}
}

TNO_RAJ_calculate_agriculture_effect = {
	set_temp_variable = {
		var = toagriculture
		value = RAJRuralPowerUse
	}
	set_temp_variable = {
		var = additiontorural
		value = RAJElectrificationOutput
	}
	divide_temp_variable = { additiontorural = 2 }
	add_to_temp_variable = { toagriculture = additiontorural }
	divide_temp_variable = { toagriculture = 10 }
	if = {
		limit = {
			check_variable = { RAJElectrificationConsumption > RAJElectrificationOutput }
		}
		divide_temp_variable = { toagriculture = 2 }
	}
	subtract_from_variable = { agriculture_monthly_change = toagriculturesave }
	add_to_variable = { agriculture_monthly_change = toagriculture }
	set_variable = {
		var = toagriculturesave
		value = toagriculture
	}
}

TNO_RAJ_calculate_poverty_effect = {
	set_temp_variable = {
		var = topoverty
		value = RAJPowerHouseholdsCovered
	}
	set_temp_variable = {
		var = additiontopoverty
		value = RAJElectrificationOutput
	}
	divide_temp_variable = { additiontopoverty = 2 }
	add_to_temp_variable = { topoverty = additiontopoverty }
	divide_temp_variable = { topoverty = 10 }
	if = {
		limit = {
			check_variable = { RAJElectrificationConsumption > RAJElectrificationOutput }
		}
		divide_temp_variable = { topoverty = 2 }
	}
	subtract_from_variable = { poverty_monthly_change = topovertysave }
	add_to_variable = { poverty_monthly_change = topoverty }
	set_variable = {
		var = topovertysave
		value = topoverty
	}
}

TNO_RAJ_calculate_industrial_effect = {
	set_temp_variable = {
		var = toindustry
		value = 1
	}
	subtract_from_variable = { toindustry =	RAJRuralPowerUse }
	set_temp_variable = {
		var = additiontoindustry
		value = RAJElectrificationOutput
	}
	divide_temp_variable = { additiontoindustry = 2 }
	add_to_temp_variable = { toindustry = additiontoindustry }
	divide_temp_variable = { toindustry = 10 }
	if = {
		limit = {
			check_variable = { RAJElectrificationConsumption > RAJElectrificationOutput }
		}
		divide_temp_variable = { toindustry = 2 }
	}
	subtract_from_variable = { industrial_equipment_monthly_change = toindustrysave }
	add_to_variable = { industrial_equipment_monthly_change = toindustry }
	set_variable = {
		var = toindustrysave
		value = toindustry
	}
}


