ORE_last_issue_check = {
	hidden_effect = {
		set_temp_variable = { burba_popularity = party_popularity@authoritarian_democrat } 
		subtract_from_temp_variable = { burba_popularity = party_popularity@communist }
		if = {
			limit = {
				check_variable = {
					burba_popularity > 0
				}
			}
			country_event = { id = ORE.124 days = 7 }
		}
		else_if = {
			limit = {
				check_variable = {
					burba_popularity < 0
				}
				check_variable = {
					burba_popularity > -6
				}
			}
			country_event = { id = ORE.126 days = 7 }
		}
		else = {
			country_event = { id = ORE.131 days = 7 }
		}
	}
}