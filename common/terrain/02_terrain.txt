##########################
### Terrain Categories ###
##########################

categories =  {

	###CITIES

	berlin = {
		color = { 155 0 255 }
		movement_cost = 1.4
		ai_terrain_importance_factor = 6.0
		sound_type = plains

		units = {
			attack = -0.5
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	la = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	laport = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains
	}
	
	sanfran = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	sanfranport = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains
	}
	
	madrid = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	nyc = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	rome = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	hamburg = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	frankfurt = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	munchen = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	madrid = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	bilbao = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	barcelona = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	porto = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	lisbon = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	sevilla = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	gibraltar = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	brest = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	poitiers = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	bordeaux = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	clermont_ferrand = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	marseille = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	west_paris = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	bern = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	amsterdam = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	east_paris = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	dijon = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	lyon = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	brussel = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	dublin = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	belfast = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	truro = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	cardiff = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	edinburgh = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	glasgow = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	london = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	portsmouth = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	norwich = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	birmingham = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	liverpool = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	manchester = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	newcastle = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	copenhagen = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	oslo = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	stockholm = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	helsinki = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	viipuri = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	murmansk = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	kiev = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	sevastopol = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	minsk = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	riga = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	prague = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	krakow = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	bratislava = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	budapest = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	bucharest = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	chisinau = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	zagreb = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	sarajevo = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	belgrade = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	sofia = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	thessaloniki = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	athens = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	istanbul = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	napoli = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	palermo = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	tirana = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	milan = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
	
	torino = {
		color = { 155 0 255 }
		movement_cost = 1.2
		ai_terrain_importance_factor = 4.0
		sound_type = plains

		units = {
			attack = -0.3
			#movement = -0.2
		}

		enemy_army_bonus_air_superiority_factor = -0.50
	}
}