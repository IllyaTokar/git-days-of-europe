ideas = {
	country = {
		GBE_idea_ghosts_republic_dahomey = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea GBE_idea_ghosts_republic_dahomey"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1

			#picture = 

			modifier = {
			    
			}
		}

		GBE_idea_militia_based_army = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea GBE_idea_militia_based_army"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1

			#picture = 

			modifier = {
			    
			}
		}
	}
}