#################
### Siamese Ideas
#################

ideas = {
	###############
	# Country Ideas
	###############
	country = {

		SIA_japan_control_economy = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea SIA_japan_control_economy"}

			allowed = { always = no }
			allowed_civil_war = { always = yes }
			picture = JAP_Army_Indocrination
			removal_cost = -1

			modifier = {
				consumer_goods_factor = 0.05
				min_export = 0.10
			}
		}

		SIA_thai_for_thai = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea SIA_thai_for_thai"}

			allowed = { always = no }
			allowed_civil_war = { always = yes }
			picture = generic_idealistic_democracy
			removal_cost = -1

			modifier = {
				stability_factor = 0.10
				war_support_factor = 0.10
				conscription_factor = 0.05
				army_core_attack_factor = 0.05
				army_core_defence_factor = 0.1
			}
		}
		SIA_Burmese_Refugees = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea SIA_Burmese_Refugees"}

			allowed = { always = no }
			allowed_civil_war = { always = yes }
			picture = URL_refugee_crisis
			removal_cost = -1

			modifier = {
				stability_factor = -0.05
				MONTHLY_POPULATION = 0.01
			}
		}

		SIA_thai_for_thai_pridi = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea SIA_thai_for_thai_pridi"}

			allowed = { always = no }
			allowed_civil_war = { always = yes }
			picture = generic_idealistic_democracy
			removal_cost = -1

			modifier = {
				stability_factor = 0.05
				war_support_factor = 0.05
				conscription_factor = 0.02
				army_core_attack_factor = 0.05
				army_core_defence_factor = 0.03
			}
		}
	}
}
