ideas = {
	country = {
		KAY_idea_islamic_influences = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KAY_idea_islamic_influences"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1

			#picture = 

			modifier = {
			    
			}
		}

		KAY_idea_decentralized_high_command = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KAY_idea_decentralized_high_command"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1

			#picture = 

			modifier = {
			    
			}
		}
	}
}