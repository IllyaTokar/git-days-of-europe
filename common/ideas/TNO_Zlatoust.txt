ideas = {
	country = {
		UDM_golden_republic = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea UDM_golden_republic"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = UDM_golden_republic
			modifier = {
				industrial_capacity_factory = 0.3 
				global_building_slots_factor = 0.1
			}
		}
		UDM_the_two_consuls = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea UDM_the_two_consuls"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = UDM_the_two_consuls
			modifier = {
				stability_factor = 0.2 
				production_speed_arms_factory_factor = 0.2
			}
		}
		UDM_black_market_payments = { #warlord black market payment
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea UDM_black_market_payments"}
			
			picture = BRT_black_market_payments
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			allowed_civil_war = {
				always = yes
			}
			
			modifier = {
				consumer_goods_factor = 0.15
				stability_factor = -0.05
				political_power_factor = -0.15
			}
		}
		UDM_warlord_interventions = { #sending volunteers
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea UDM_warlord_interventions"}
			
			picture = BRT_black_market_payments
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			allowed_civil_war = {
				always = yes
			}
			
			modifier = {
				send_volunteer_size = 3
				send_volunteers_tension = -0.75
				send_volunteer_divisions_required = -1
			 }
		}
		UDM_stocking_up = { 
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea UDM_stocking_up"}
			
			picture = BRT_black_market_payments
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			allowed_civil_war = {
				always = yes
			}
			
			equipment_bonus = {
				infantry_equipment = {
					build_cost_ic = -0.15
				}
			}
		}
		UDM_zlatoust_security_forces = { 
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea UDM_zlatoust_security_forces"}
			
			picture = BRT_black_market_payments
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			allowed_civil_war = {
				always = yes
			}
			
			modifier = {
				army_org_factor = 0.15
				army_core_defence_factor = 0.25
			}
		}
		UDM_final_defenses = { 
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea UDM_final_defenses"}
			
			picture = BRT_black_market_payments
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			allowed_civil_war = {
				always = yes
			}
			
			modifier = {
				production_speed_bunker_factor = 0.3
				production_speed_anti_air_building_factor = 0.15
				max_planning = 0.2
				attrition = -0.1
			}
		}
		UDM_zlatoust_advisors = { 
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea UDM_zlatoust_advisors"}
			
			picture = BRT_black_market_payments
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			allowed_civil_war = {
				always = yes
			}
			
			modifier = {
				army_org_factor = 0.15
				experience_gain_army_factor = 0.10
			}
		}
	}
}