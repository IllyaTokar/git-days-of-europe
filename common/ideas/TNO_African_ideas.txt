ideas = {
	hidden_ideas = {
		AFRICA_SAW_defense_buff = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea AFICA_SAW_defense_buff"}

			picture = GER_unknown

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				ai_focus_defense_factor = 0.15
			}
		}
	}
	country = {
		WEST_AFRICA_idea_terror_bombing = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea WEST_AFRICA_idea_terror_bombing"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1

			picture = WOL_terror_bombing

			modifier = {
			    production_speed_buildings_factor = -0.60
				production_factory_max_efficiency_factor = -0.40
				production_factory_efficiency_gain_factor = -0.35
				consumer_goods_factor = 0.25
			}
		}

		WEST_AFRICA_idea_obliterated_infrastructures = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea WEST_AFRICA_idea_obliterated_infrastructures"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			
			picture = WOL_terror_bombing

			modifier = {
			    production_speed_buildings_factor = -0.60
				production_factory_max_efficiency_factor = -0.40
				production_factory_efficiency_gain_factor = -0.35
				consumer_goods_factor = 0.25
			}
		}
	}
}