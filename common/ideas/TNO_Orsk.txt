ideas = {
	country = {
		ORS_cornered_dog = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_cornered_dog"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_cornered_dog
			modifier = {
				Army_morale_factor = 0.5
				army_defence_factor = 0.2
			}
		}

		ORS_new_raids = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_new_raids"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_bandit_state
			modifier = {
				Army_attack_factor = 0.05
				Army_defence_factor = 0.05
			}
		}

		ORS_bandit_state_1 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_bandit_state_1"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_bandit_state
			modifier = {
				Army_org = 2.5
				Army_morale_factor = 0.1
				Army_attack_factor = 0.05
				war_support_factor = 0.35
				Production_factory_max_efficiency_factor = -0.35
				research_speed_factor = -0.15
				ai_focus_aggressive_factor = 0.25
				ai_badass_factor = 0.25
				political_power_factor = -0.35
			}
		}

		ORS_to_the_slaughter = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_to_the_slaughter"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_to_the_slaughter
			modifier = {
				army_morale_factor = 0.2
				army_attack_factor = 0.6
				army_defence_factor = -0.7
				out_of_supply_factor = -0.2
				ai_focus_aggressive_factor = 0.50
				planning_speed = 1
			}
		}
		ORS_band_exhausted = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_band_exhausted"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = BAS_Magnitogorsk_Collapse
			modifier = {
				army_morale_factor = -0.5
				army_attack_factor = -0.4
				army_defence_factor = -0.9
				attrition = 0.3

			}
		}
		ORS_think_tank_idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_think_tank_idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = BAS_mad_scientist
			modifier = {
				research_speed_factor = 0.06
			}
		}
		ORS_homemade_body_armor_idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_homemade_body_armor_idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = URL_volunteer_militia_programs
			modifier = {
				army_infantry_defence_factor = 0.05
			}
		}
		ORS_band_collapsed = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_band_collapsed"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_exhausted_band
			modifier = {
				army_morale_factor = -0.7
				army_attack_factor = -0.7
				army_defence_factor = -0.7
				attrition = 0.5
				conscription = -0.5
			}
		}
		ORS_recently_raided = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_recently_raided"}
			allowed = {
				always = no
			}
			removal_cost = -1
			picture = ORS_bandit_state
			modifier = {
				political_power_gain = -0.2
			}
		}

		ORS_Rage_Inducing_Hormones = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Rage_Inducing_Hormones"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Rage_Inducing_Hormones
			modifier = {
				army_morale_factor = -0.1
				army_attack_factor = 0.1
				army_defence_factor = -0.1
				out_of_supply_factor = -0.1
				ai_focus_aggressive_factor = 0.25
			}
		}
		ORS_Steroids = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Steroids"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Steroids
			modifier = {
				army_org_factor = -0.1
				army_attack_factor = 0.05
				army_defence_factor = -0.05
				out_of_supply_factor = -0.05
				ai_focus_aggressive_factor = 0.15
			}
		}
		ORS_Soldiers_Happy_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Soldiers_Happy_Idea"}
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Well_Organized
			modifier = {
				army_org_factor = 0.15
				army_attack_factor = 0.15
				army_defence_factor = 0.15
			}
		}
		ORS_Soldiers_Okay_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Soldiers_Okay_Idea"}
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Okay
			modifier = {
				army_org_factor = -0.05
				army_attack_factor = -0.05
				army_defence_factor = -0.05
			}
		}
		ORS_Soldiers_A_Bit_Happy_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Soldiers_A_Bit_Happy_Idea"}
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Organized
			modifier = {
				army_org_factor = 0.10
				army_attack_factor = 0.10
				army_defence_factor = 0.10
			}
		}
		ORS_Soldiers_Discontent_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Soldiers_Discontent_Idea"}
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Unorganized
			modifier = {
				army_org_factor = -0.10
				army_attack_factor = -0.10
				army_defence_factor = -0.10
			}
		}
		ORS_Soldiers_Extra_Discontent_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Soldiers_Extra_Discontent_Idea"}
			allowed = {
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Disorderly
			modifier = {
				army_org_factor = -0.2
				army_attack_factor = -0.2
				army_defence_factor = -0.2
			}
		}
		ORS_Officers_Unswayingly_Loyal_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Officers_Unswayingly_Loyal_Idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Officers_Happy
			modifier = {
				out_of_supply_factor = -0.25
				experience_gain_army_factor = 0.35
				dig_in_speed_factor = 0.15
			}
		}
		ORS_Officers_Respect_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Officers_Respect_Idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Officers_Kinda_Happy
			modifier = {
				out_of_supply_factor = -0.1
				experience_gain_army_factor = 0.15
				dig_in_speed_factor = 0.1
			}
		}
		ORS_Officers_Normal_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Officers_Normal_Idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Officers_Content
			modifier = {
				out_of_supply_factor = -0.05
				experience_gain_army_factor = 0.1
				dig_in_speed_factor = 0.05
			}
		}
		ORS_Officers_Disrespectful_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Officers_Disrespectful_Idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Officers_Unhappy
			modifier = {
				out_of_supply_factor = 0.1
			}
		}
		ORS_Officers_Disloyal_Idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Officers_Disloyal_Idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Officers_No
			modifier = {
				out_of_supply_factor = 0.2
				experience_gain_army_factor = -0.45
				dig_in_speed_factor = -0.35
			}
		}
		ORS_Officer_Purge = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_Officer_Purge" }
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_Officers_No
			modifier = {
				army_morale_factor = -0.15
				experience_gain_army_factor = -0.75
				army_speed_factor = -0.2
				army_org_factor = -0.10
			}
		}
		ORS_bandit_state_2 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_bandit_state_2"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_bandit_state
			modifier = {
				Army_org = 3
				Army_morale_factor = 0.15
				Army_attack_factor = 0.05
				war_support_factor = 0.35
				Production_factory_max_efficiency_factor = -0.35
				research_speed_factor = -0.15
				ai_focus_aggressive_factor = 0.25
				ai_badass_factor = 0.25
				political_power_factor = -0.35
			}
		}
		ORS_bandit_state_3 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_bandit_state_3"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_bandit_state
			modifier = {
				Army_org = 3
				Army_morale_factor = 0.15
				Army_attack_factor = 0.1
				war_support_factor = 0.35
				Production_factory_max_efficiency_factor = -0.35
				research_speed_factor = -0.15
				ai_focus_aggressive_factor = 0.25
				ai_badass_factor = 0.25
				political_power_factor = -0.35
			}
		}
		ORS_bandit_state_4 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_bandit_state_4"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_bandit_state
			modifier = {
				Army_org = 3
				Army_morale_factor = 0.15
				Army_attack_factor = 0.15
				war_support_factor = 0.4
				Production_factory_max_efficiency_factor = -0.35
				research_speed_factor = -0.15
				ai_focus_aggressive_factor = 0.25
				ai_badass_factor = 0.25
				political_power_factor = -0.35
			}
		}
		ORS_bandit_state_5 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_bandit_state_5"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_bandit_state
			modifier = {
				Army_org = 3
				Army_morale_factor = 0.15
				Army_attack_factor = 0.15
				war_support_factor = 0.4
				planning_speed = 0.02
				Production_factory_max_efficiency_factor = -0.35
				research_speed_factor = -0.15
				ai_focus_aggressive_factor = 0.25
				ai_badass_factor = 0.25
				political_power_factor = -0.35
			}
		}
		ORS_enemies_on_all_sides = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea ORS_enemies_on_all_sides"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = ORS_bandit_state
			modifier = {
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
			}
		}
	}
}