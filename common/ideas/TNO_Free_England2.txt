ideas = {
	country = {
		FEN_English_Civil_War = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_English_Civil_War" }
			allowed = { always = no }
			cancel = {
				NOT = {
					has_war_with = ENG
				}
			}
			removal_cost = -1
			picture = FEN_English_Civil_War
			modifier = {
				stability_weekly = -0.025
				war_support_weekly = -0.025
				surrender_limit = 0.8
			}
		}
		FEN_Reconstruction_Of_England = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EN_Reconstruction_Of_England"}
			allowed = { always = no }
			removal_cost = -1
			picture = postwar_consensus
			modifier = {
				gdp_growth_modifier = -0.03
				taxable_population_modifier = -0.03
				income_rate_factor = -0.03
				poverty_monthly_rate = -1
				stability_weekly = -0.005
				MONTHLY_POPULATION = -0.30
				stability_factor = -0.1
				production_speed_buildings_factor = -0.3
			}
		}

		FEN_Reconstruction_Of_England_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EN_Reconstruction_Of_England_2"}
			allowed = { always = no }
			removal_cost = -1
			picture = postwar_consensus
			modifier = {
				gdp_growth_modifier = -0.01
				taxable_population_modifier = -0.01
				income_rate_factor = -0.01
				poverty_monthly_rate = -0.5
				stability_weekly = -0.002
				MONTHLY_POPULATION = -0.15
				stability_factor = -0.05
				production_speed_buildings_factor = -0.3
			}
		}

		FEN_Food_Insecurity = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Food_Insecurity"}
			allowed = { always = no }
			removal_cost = -1
			picture = WRS_rations
			modifier = {
				gdp_growth_modifier = -0.03
				taxable_population_modifier = -0.03
				income_rate_factor = -0.03
				stability_weekly = -0.005
				MONTHLY_POPULATION = -0.30
				stability_factor = -0.1
				army_org = -2
				poverty_monthly_rate = -1
			}
		}
		
		FEN_Food_Insecurity_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Food_Insecurity_2"}
			allowed = { always = no }
			removal_cost = -1
			picture = WRS_rations
			modifier = {
				gdp_growth_modifier = -0.02
				taxable_population_modifier = -0.02
				income_rate_factor = -0.02
				stability_weekly = -0.002
				MONTHLY_POPULATION = -0.15
				stability_factor = -0.05
				army_org = -1
				poverty_monthly_rate = -0.75
			}
		}
		
		FEN_Food_Insecurity_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Food_Insecurity_3"}
			allowed = { always = no }
			removal_cost = -1
			picture = federal_food_banks
			modifier = {
				gdp_growth_modifier = -0.01
				poverty_monthly_rate = -0.5
				MONTHLY_POPULATION = -0.2
				production_speed_buildings_factor = -0.2
				
			}
		}
	
		FEN_Food_Insecurity_4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Food_Insecurity_4"}
			allowed = { always = no }
			removal_cost = -1
			picture = federal_food_banks
			modifier = {
				poverty_monthly_rate = -0.25
				MONTHLY_POPULATION = -0.1
				production_speed_buildings_factor = -0.15
				
			}
		}
	
	
		FEN_Food_Insecurity_5 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Food_Insecurity_5"}
			allowed = { always = no }
			removal_cost = -1
			picture = federal_food_banks
			modifier = {
				MONTHLY_POPULATION = -0.05
				production_speed_buildings_factor = -0.1
			}
		}
		
		FEN_Urban_Crime = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Urban_Crime"}
			allowed = { always = no }
			cancel = { has_war_with = FEN }
			removal_cost = -1
			picture = NZL_Urbanization_Crisis
			modifier = {
				gdp_growth_modifier = -0.05
				taxable_population_modifier = -0.05
				income_rate_factor = -0.05
				poverty_monthly_rate = -1
				stability_factor = -0.2
				production_factory_max_efficiency_factor = -0.25
				industrial_capacity_factory  = -0.25
				local_resources_factor = -0.15
				supply_consumption_factor = -0.05
				line_change_production_efficiency_factor = -0.2				
			}
		}
		
		FEN_Urban_Crime_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Urban_Crime_2"}
			allowed = { always = no }
			cancel = { has_war_with = FEN }
			removal_cost = -1
			picture = NZL_Urbanization_Crisis
			modifier = {
				gdp_growth_modifier = -0.04
				taxable_population_modifier = -0.04
				income_rate_factor = -0.04
				poverty_monthly_rate = -0.75
				stability_factor = -0.15
				production_factory_max_efficiency_factor = -0.20
				industrial_capacity_factory  = -0.20
				local_resources_factor = -0.20
				supply_consumption_factor = -0.15
				line_change_production_efficiency_factor = -0.15				
			}
		}
		
		FEN_Urban_Crime_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Urban_Crime_3"}
			allowed = { always = no }
			cancel = { has_war_with = FEN }
			removal_cost = -1
			picture = NZL_Urbanization_Crisis
			modifier = {
				gdp_growth_modifier = -0.03
				taxable_population_modifier = -0.03
				income_rate_factor = -0.03
				poverty_monthly_rate = -0.5
				stability_factor = -0.15
				production_factory_max_efficiency_factor = -0.1
				industrial_capacity_factory  = -0.1
				local_resources_factor = -0.1
				supply_consumption_factor = -0.02
				line_change_production_efficiency_factor = -0.1			
			}
		}
		
		FEN_Urban_Crime_4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Urban_Crime_4"}
			allowed = { always = no }
			cancel = { has_war_with = FEN }
			removal_cost = -1
			picture = NZL_Urbanization_Crisis
			modifier = {
				gdp_growth_modifier = -0.02
				taxable_population_modifier = -0.02
				income_rate_factor = -0.02
				poverty_monthly_rate = -0.25
				stability_factor = -0.1
				production_factory_max_efficiency_factor = -0.05
				industrial_capacity_factory  = -0.05
				local_resources_factor = -0.05
				supply_consumption_factor = -0.05
				line_change_production_efficiency_factor = -0.05		
			}
		}
		
		FEN_Urban_Crime_5 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Urban_Crime_5"}
			allowed = { always = no }
			cancel = { has_war_with = FEN }
			removal_cost = -1
			picture = NZL_Urbanization_Crisis
			modifier = {
				gdp_growth_modifier = -0.01
				taxable_population_modifier = -0.01
				income_rate_factor = -0.01
				stability_factor = -0.05
				production_factory_max_efficiency_factor = -0.02
				industrial_capacity_factory  = -0.02
				local_resources_factor = -0.02
				supply_consumption_factor = -0.02
				line_change_production_efficiency_factor = -0.02	
			}
		}
	}
	
####### Free England Ministers #######
	
	head_of_government = {
		FEN_Claude_Auchinleck_hog = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Claude_Auchinleck_hog" }
			allowed = { original_tag = FEN }
			picture = "Minister_Claude_Auchinleck"
			available = { always = no }
			traits = {
				head_of_government
				social_conservative
				hog_old_general
			}
			cancel_if_invalid = no
			ledger = civilian
		}
		FEN_david_eccles_hog = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea FEN_david_eccles_hog"}
			picture = "FEN_David_Eccles_hog"
			allowed = {
				original_tag = FEN
			}
			available = {

			}
			traits = {
				head_of_government
				social_conservative
			}
			cancel_if_invalid = yes
			ledger = civilian
		}
		FEN_philip_toynbee_hog = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea FEN_philip_toynbee_hog"}
			picture = "FEN_philip_toynbee_hog"
			allowed = {
				original_tag = FEN
			}
			available = {

			}
			traits = {
				head_of_government
				communist
			}
			cancel_if_invalid = yes
			ledger = civilian
		}
	}
	
	foreign_minister = {
		FEN_Bill_Alexander_for = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Bill_Alexander_for" }
			allowed = { original_tag = FEN }
			picture = "Minister_Bill_Alexander"
			available = { always = no }
			traits = {
				foreign_minister
				socialist
				for_the_cloak_n_dagger_schemer
			}
			cancel_if_invalid = no
			ledger = civilian
		}
		FEN_Norman_St_John_Stevas_for = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Norman_St_John_Stevas_for" }
			picture = "GFX_idea_FEN_minister_Norman_St_John_Stevas_for"
			allowed = {
				original_tag = FEN
			}
			traits = {
				foreign_minister
				social_liberal
				for_political_appointment
			}
			cancel_if_invalid = yes
			ledger = civilian
		}
		FEN_peter_shore_for = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea FEN_peter_shore_for"}
			#picture = "GFX_idea_FEN_minister_Norman_St_John_Stevas_for"
			allowed = {
				original_tag = FEN
			}

			traits = {
				foreign_minister
				socialist
			}
			cancel_if_invalid = yes
			ledger = civilian
		}
	}
	
	economy_minister = {
		FEN_Reg_Birch_eco = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_Reg_Birch_eco" }
			allowed = { original_tag = FEN }
			picture = "Minister_Reg_Birch"
			available = { always = no }
			traits = {
				economy_minister
				communist
				eco_union_buster
			}
			cancel_if_invalid = no
			ledger = civilian
		}
		FEN_bert_ramelson_eco = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_bert_ramelson_eco" }
			allowed = { original_tag = FEN }
			picture = "FEN_bert_ramelson_eco"
			available = { always = no }
			traits = {
				economy_minister
				communist
			}
			cancel_if_invalid = no
			ledger = civilian
		}
	}
	
	security_minister = {
		FEN_David_Stirling_sec = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_David_Stirling_sec" }
			allowed = { original_tag = FEN }
			picture = "Minister_David_Stirling"
			available = { always = no }
			traits = {
				security_minister
				authoritarian_democrat
				sec_prince_of_terror
			}
			cancel_if_invalid = no
			ledger = civilian
		}
		FEN_kim_philby_sec = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FEN_kim_philby_sec" }
			allowed = { original_tag = FEN }
			picture = "FEN_kim_philby_sec"
			available = { always = no }
			traits = {
				security_minister
				communist
			}
			cancel_if_invalid = no
			ledger = civilian
		}
	}
}