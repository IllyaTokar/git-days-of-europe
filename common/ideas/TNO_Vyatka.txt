ideas = {
	country = {
		KIR_unrepentant_reaction = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_unrepentant_reaction"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_Unrepentant_Reaction
			
			modifier = {
				industrial_capacity_factory = -0.05 
				stability_factor = 0.1
				army_morale_factor = 0.075
			}
		}
		KIR_the_tsar_and_himself = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_the_tsar_and_himself"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_The_Tsar_and_Himself
			
			modifier = {
				war_support_factor = 0.1 
				political_power_gain = 0.2
			}
		}
		KIR_officer_infighting = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_officer_infighting"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_Officer_Infighting
			
			modifier = {
				political_power_gain = -0.3
				army_org_factor = -0.1
				army_core_defence_factor = -0.1
				planning_speed = -0.1
				conscription_factor = -0.05
				training_time_army_factor = 0.025
			}
		}
		KIR_officers_reigned_in = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_officers_reigned_in"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = ZAR_idea_warlord_state
			
			modifier = {
				political_power_gain = -0.05
				conscription_factor = -0.025
				army_core_defence_factor = 0.05
				planning_speed = 0.05
				army_morale_factor = 0.05
			}
		}
		KIR_imperial_detachment_idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_imperial_detachment_idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = KIR_Imperial_Detachment
			
			modifier = {
				political_power_gain = -0.15
				stability_factor = 0.1
				production_factory_max_efficiency_factor = 0.1
			}
		}
		KIR_the_peoples_tsar_idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_the_peoples_tsar_idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = KIR_The_Peoples_Tsar
			
			modifier = {
				political_power_gain = 0.1
				industrial_capacity_factory = 0.1
				production_factory_max_efficiency_factor = -0.1
			}
		}
		KIR_kirov_vodka = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_kirov_vodka"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = KIR_Vyatka_Vodka
			
			modifier = {
				industrial_capacity_factory = 0.05
				consumer_goods_factor = -0.05
			}
		}
		KIR_modern_age_okhrana = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_modern_age_okhrana"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_A_Modern_Age_Okhrana
			
			modifier = {
				encryption_factor = 0.075
				decryption_factor = 0.075
				recon_factor = 0.05
				resistance_damage_to_garrison = -0.15
			}
		}
		KIR_modern_age_okhrana_2 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_modern_age_okhrana_2"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_A_Modern_Age_Okhrana
			
			modifier = {
				encryption_factor = 0.05
				decryption_factor = 0.05
				recon_factor = 0.05
				#enemy_#partisan_effect = -0.10
				stability_factor = 0.1
			}
		}
		KIR_modern_age_okhrana_3 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_modern_age_okhrana_3"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_A_Modern_Age_Okhrana
			
			modifier = {
				encryption_factor = 0.1
				decryption_factor = 0.1
				recon_factor = 0.05
				resistance_damage_to_garrison = -0.25
			}
		}
		KIR_modern_age_okhrana_kadet = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_the_sovereignty"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_A_Modern_Age_Okhrana
			
			modifier = {
				encryption_factor = 0.025
				decryption_factor = 0.025
				recon_factor = 0.05
				stability_factor = 0.1
			}
		}
		KIR_white_army_traditions_idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_white_army_traditions_idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_White_Army_Tactics
			
			modifier = {
				max_planning = 0.05
				army_attack_factor = 0.1
				army_org_factor = 0.075
				land_reinforce_rate = 0.1
			}
		}
		KIR_white_army_traditions_idea2 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_white_army_traditions_idea2"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_White_Army_Tactics
			
			modifier = {
				max_planning = 0.1
				planning_speed = 0.1
				army_attack_factor = 0.1
				army_org_factor = 0.1
				land_reinforce_rate = 0.1
			}
		}
		KIR_white_army_traditions_idea_kadet = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_white_army_traditions_idea2"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_White_Army_Tactics
			
			modifier = {
				max_planning = 0.1
				planning_speed = 0.1
				army_attack_factor = 0.15
				army_org_factor = 0.15
				land_reinforce_rate = 0.15
			}
		}
		KIR_imperial_special_forces = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_imperial_special_forces"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = KIR_imperial_special_forces
			
			modifier = {
				special_forces_attack_factor = 0.1
				special_forces_defence_factor = 0.1
				conscription_factor = -0.05
			}
		}
		KIR_strength_in_numbers_idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_strength_in_numbers_idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = KIR_Strength_in_Numbers
			
			modifier = {
				conscription_factor = 0.075
				army_org_factor = -0.025
			}
		}
		KIR_general_staff_academy = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_general_staff_academy"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = KIR_Vyatka_General_Staff_Academy
			
			modifier = {
				experience_gain_army_factor = 0.05
				army_org_factor = 0.1
				army_morale_factor = 0.05
				dig_in_speed_factor = 0.1
				supply_consumption_factor = -0.2
			}
		}
		KIR_general_staff_academy2 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_general_staff_academy2"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = KIR_Vyatka_General_Staff_Academy
			
			modifier = {
				experience_gain_army_factor = 0.05
				army_org_factor = 0.1
				army_morale_factor = 0.05
				dig_in_speed_factor = 0.1
				supply_consumption_factor = -0.2
				army_leader_start_level = 1
			}
		}
		KIR_fortified_position = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_fortified_position"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = BAS_fort
			
			modifier = {
				army_defence_factor = 0.15
				attrition = -0.1
				out_of_supply_factor = -0.2
				no_supply_grace = 48
			}
		}
		KIR_archives_of_samara = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_archives_of_samara"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = NIK_plan_red
			
			targeted_modifier = {
				tag = WRS
				defense_bonus_against = 0.1
			}
		}
		KIR_the_bridgeburners = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_the_bridgeburners"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = KIR_the_bridgeburners
			
			targeted_modifier = {
				tag = WRS
				attack_bonus_against = 0.05
				stability_factor = -0.1
			}
		}
		KIR_solidarist_constitution = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_solidarist_constitution"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			#picture = 
			
			modifier = {
				war_support_factor = 0.1
				stability_factor = 0.05
			}
		}
		KIR_unionist_constitution = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_unionist_constitution"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			#picture = 
			
			modifier = {
				stability_factor = 0.05
				war_support_factor = 0.05
				political_power_gain = 0.1
			}
		}
		KIR_kadet_constitution = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_kadet_constitution"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			#picture = 
			
			modifier = {
				political_power_gain = 0.25
				stability_factor = 0.05
			}
		}
		KIR_solidarist_economy = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_solidarist_economy"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = generic_planned_economy
			
			modifier = {
				line_change_production_efficiency_factor = 0.1
				production_factory_max_efficiency_factor = 0.1
			}
		}
		KIR_unionist_economy = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_unionist_economy"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = generic_coins
			
			modifier = {
				consumer_goods_factor = -0.1
				conversion_cost_civ_to_mil_factor = -0.2
			}
		}
		KIR_kadet_economy = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_kadet_economy"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = ffr_precious_metals
			
			modifier = {
				industrial_capacity_factory = 0.1
				industrial_capacity_dockyard = 0.1
			}
		}
		KIR_kadet_economy_2 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_kadet_economy_2"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = ffr_precious_metals
			
			modifier = {
				industrial_capacity_factory = 0.1
				industrial_capacity_dockyard = 0.1
				MONTHLY_POPULATION = 0.1
			}
		}
		KIR_kadet_economy_3 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_kadet_economy_2"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			available = {
				has_country_leader = {
					name = "Vladimir III"
					ruling_only = yes
				}
			}
			removal_cost = -1
			picture = ffr_precious_metals
			
			modifier = {
				industrial_capacity_factory = 0.1
				industrial_capacity_dockyard = 0.1
				MONTHLY_POPULATION = 0.1
				production_speed_buildings_factor = 0.15
			}
		}
		KIR_reopened_universities = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_reopened_universities"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = BAS_Constructive_Science
			
			modifier = {
				research_speed_factor = 0.05
				political_power_factor = 0.025
			}
		}
		KIR_decommunization = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_decommunization"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = IRK_secure_union
			
			modifier = {
				stability_factor = -0.15
				drift_defence_factor = 0.15
				political_power_factor = 0.075
			}
		}
		KIR_ural_reconstruction_program = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_ural_reconstruction_program"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			#picture = 
			
			modifier = {
				resistance_damage_to_garrison = -0.25
				consumer_goods_factor = 0.025
				production_factory_max_efficiency_factor = 0.1
			}
		}
		KIR_revitalizing_the_russian_spirit = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_revitalizing_the_russian_spirit"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			#picture = 
			
			modifier = {
				political_power_gain = 0.035
				army_morale_factor = 0.05
				consumer_goods_factor = -0.05
				production_factory_max_efficiency_factor = 0.1
			}
		}
		KIR_tsars_golden_purse_idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_tsars_golden_purse_idea"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			#picture = 
			
			modifier = {
				consumer_goods_factor = -0.05
				local_resources_factor = 0.1
				industrial_capacity_factory = 0.1
				production_factory_max_efficiency_factor = 0.05
			}
		}
		KIR_ural_drive_program = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_ural_drive_program"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = MZB_USA_Lackluster_Infrastructures
			
			modifier = {
				production_speed_infrastructure_factor = 0.1
			}
		}
		KIR_ural_drive_program_2 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea KIR_ural_drive_program"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = MZB_USA_Lackluster_Infrastructures
			
			modifier = {
				production_speed_infrastructure_factor = 0.2
			}
		}
	}
}