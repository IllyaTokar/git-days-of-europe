ideas = {
	country = {
		VLG_volga_settler_conflict = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VLG_wolga_german_settler_conflict" }
			removal_cost = -1
			picture = GER_wary_wehrmacht
			
			allowed = {
				always = no
			}
			modifier = {
				stability_factor = -0.05
				war_support_factor = -0.05
			}
		}

		VLG_local_russian_resistance = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VLG_wolga_german_settler_conflict" }
			removal_cost = -1
			picture = BRY_Idealistic_Revolutionaries
			
			allowed = {
				always = no
			}
			modifier = {
				conscription_factor = -0.25
			}
		}

		VLG_NSDAP_meddling = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VLG_wolga_german_settler_conflict" }
			removal_cost = -1
			picture = LIB_idea_gridlocked_politics
			
			allowed = {
				always = no
			}
			modifier = {
				political_power_gain = -0.2
				custom_modifier_tooltip = VLG_mandatory_NSDAP_seats_tooltip
			}
		}
		VLG_volga_autonomous_region = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VLG_volga_autonomous_region" }
			removal_cost = -1
			picture = MST_Social_Rights_2
			
			allowed = {
				always = no
			}
			modifier = {
				political_power_gain = -0.2
				conscription_factor = -0.1
			}
		}
	}
}