ideas = {
	country = {
		advisor_level_1 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea advisor_level_1"}

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			picture = GFX_idea_advisor_level_1
			modifier = {
				send_volunteer_size = 1
			}

		}

		advisor_level_2 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea advisor_level_2"}

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			picture = GFX_idea_advisor_level_2
			removal_cost = -1

			modifier = {
				send_volunteer_size = 2
			}

		}

		advisor_level_3 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea advisor_level_3"}

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			picture = GFX_idea_advisor_level_3
			modifier = {
				send_volunteer_size = 3
			}

		}

		advisor_level_4 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea advisor_level_4"}

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			picture = GFX_idea_advisor_level_4
			modifier = {
				send_volunteer_size = 4
			}

		}

		advisor_level_5 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea advisor_level_5"}

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			picture = GFX_idea_advisor_level_5
			removal_cost = -1

			modifier = {
				send_volunteer_size = 5
			}

		}

		policing_level_1 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea policing_level_1"}
			picture = GFX_idea_generic_army_2

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				send_volunteer_size = 1
			}

		}

		policing_level_2 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea policing_level_2"}
			picture = GFX_idea_generic_army_2

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				send_volunteer_size = 3
			}

		}

		policing_level_3 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea policing_level_3"}
			picture = GFX_idea_generic_army_2

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				send_volunteer_size = 7
			}

		}

		policing_level_4 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea policing_level_4"}
			picture = GFX_idea_generic_army_2

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				send_volunteer_size = 12
			}

		}

		policing_level_5 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea policing_level_5"}
			picture = GFX_idea_generic_army_2

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				send_volunteer_size = 16
			}

		}

		policing_level_6 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea policing_level_6"}
			picture = GFX_idea_generic_army_2

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				send_volunteer_size = 20
			}

		}

		policing_level_7 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea policing_level_7"}
			picture = GFX_idea_generic_army_2

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				send_volunteer_size = 25
			}

		}

		policing_level_8 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea policing_level_8"}
			picture = GFX_idea_generic_army_2

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				send_volunteer_size = 33
			}

		}

		Start_withdrawal = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea Start_withdrawal"}

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			modifier = {
				send_volunteer_size = -38

			}

		}
	}
	hidden_ideas = {
		TNO_division_limit_exemption = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea TNO_division_limit_exemption"}
			#picture =
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				send_volunteer_divisions_required = -1
			}
		}
	}
}