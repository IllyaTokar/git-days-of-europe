ideas = {
	country = {
		MIR_tukhachevskys_fiefdom = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea MIR_tukhachevskys_fiefdom"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = MIR_tukhachevskys_fiefdom
			modifier = {
				army_org_Factor = 0.1 
				army_defence_factor = 0.1
			}
		}
		MIR_fortification_projects = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea MIR_tukhchavsky's_fiefdom"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = BAS_fort
			modifier = {
			   production_speed_bunker_factor = 0.3
			}
		}
	}
}