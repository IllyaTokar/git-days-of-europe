ideas = {
	country = {
		FFR_military_administration = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea FFR_military_administration"}
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = 0.15
				conscription_factor = 0.1
				political_power_gain = -0.2
				planning_speed = -0.6
				army_org_factor = -0.4
			}
		}
		
		FFR_precious_metal = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea FFR_precious_metal"}
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.1
				min_export = 0.2
			}
		}
		
		FFR_spirit_french = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea FFR_spirit_french"}
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1
			
			modifier = {
				stability_factor = 0.15
				political_power_gain = 0.25
			}
		}

		FFR_case_belgian_citizenship = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea FFR_case_belgian_citizenship"}
			
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1
			
			modifier = {
				stability_factor = -0.05
				political_power_gain = -0.1
			}
		}
	}
}