ideas = {
	country = {
		VOL_neutral_zone = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VOL_neutral_zone"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = VOL_Neutral_Zone
			modifier = {
				defence = 0.15
				army_core_attack_factor = 0.10
				war_support_factor = -0.15
			}
		}
		VOL_Leadership_Killed = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VOL_Leadership_Killed"}
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = BAS_army_bad
			modifier = {
				army_defence_factor = -0.2
				army_attack_factor = -0.2
			}
		}
	}
} 