shared_focus = {
	id = GGN_blankfocus
	icon = GFX_cau_occultic_adventures
	bypass = {
	}
	offset = {
		x = 1
		y = 1
		trigger = {
			tag = GGN
		}
	}
	allow_branch = {
		has_country_flag = neverflag
	}
	cost = 2
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus GGN_blankfocus"
	}
}



