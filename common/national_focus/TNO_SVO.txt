#Organ Tree by Wendell08

focus_tree = {
	id = SVO_starting_tree
	country = {
		factor = 0
		modifier = {
			add = 20
			tag = SVO
		}
	}
	default = no
	shared_focus = SVO_The_Eastern_Conflict
}

focus_tree = {
	id = SVO_war_tree
	country = {
		factor = 0
		modifier = {
			add = 20
			tag = SVO
		}
	}
	default = no
	shared_focus = SVO_The_Eastern_Conflict
}

focus_tree = {
	id = SVO_post_war_tree
	country = {
		factor = 0
		modifier = {
			add = 20
			tag = SVO
		}
	}
	default = no
	shared_focus = SVO_Aftermath_of_the_War
	shared_focus = SVO_Red_Spring
}

focus_tree = {
	id = SVO_blank_tree
	country = {
		factor = 0
		modifier = {
			add = 20
			tag = SVO
		}
	}
	default = no
}

focus_tree = {
	id = SVO_regional_tree
	country = {
		factor = 0
		modifier = {
			add = 20
			tag = SVO
		}
	}
	default = no
	shared_focus = SVO_Secure_our_Position
	shared_focus = SVO_Into_the_Military_Front
	shared_focus = SVO_Restoring_our_Riches
	shared_focus = SVO_The_International_Field
}


focus_tree = {
	id = SVO_superregional_tree
	country = {
		factor = 0
		modifier = {
			add = 20
			tag = SVO
		}
	}
	default = no
	shared_focus = SVO_MIL_A_United_Siberia
	shared_focus = RUS_SIB_nukes_into_the_atomic_age
}