focus_tree = {
	# seyss-inquart part one
	id = netherlands_si_one
	country = {
		factor = 0
	}
	default = no
}
focus_tree = {
	# Interim Government
	id = netherlands_interim_government
	country = {
		factor = 0
	}
	default = no
}
focus_tree = {
	# Biesheuvel Dictatorship I
	id = netherlands_biesheuvel_dictatorship_I
	country = {
		factor = 0
	}
	default = no
}

focus_tree = {
	# Feldmeijer Purges
	id = netherlands_feldmeijer
	country = {
		factor = 0
	}
	default = no
}

focus_tree = {
	# Van Geelkerken/Rochemont RK I 
	id = netherlands_RK_I
	country = {
		factor = 0
	}
	default = no	
}