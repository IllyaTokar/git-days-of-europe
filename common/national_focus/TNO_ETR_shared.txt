shared_focus = {
	id = ETR_The_Black_Wolf
	icon = GFX_goal_unknown
	cost = 5.00
	allow_branch = {
		has_country_flag = ETR_blackwolf
	}
	x = 7
	y = 0
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Black_Wolf"
	}
}

shared_focus = {
	id = ETR_The_Flames_of_War
	icon = GFX_goal_unknown
	cost = 5.00
	allow_branch = {
		has_country_flag = ETR_blackwolf
	}
	x = 15
	y = 0
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Flames_of_War"
	}
}

shared_focus = {
	id = ETR_Strike_the_East
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Black_Wolf
	}
	x = 7
	y = 1
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Strike_the_East"
	}
}

shared_focus = {
	id = ETR_Splitting_Skulls
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Strike_the_East
	}
	x = 5
	y = 2
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Splitting_Skulls"
	}
}

shared_focus = {
	id = ETR_Horse_Warriors
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Splitting_Skulls
	}
	x = 4
	y = 3
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Horse_Warriors"
	}
}

shared_focus = {
	id = ETR_Pillage_and_Raze
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Strike_the_East
	}
	x = 9
	y = 2
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Pillage_and_Raze"
	}
}

shared_focus = {
	id = ETR_The_Pan_Turkist_Agenda
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Splitting_Skulls
	}
	prerequisite = {
		focus = ETR_Pillage_and_Raze
	}
	x = 7
	y = 3
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Pan_Turkist_Agenda"
	}
}

shared_focus = {
	id = ETR_The_Yellow_Banner
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Pan_Turkist_Agenda
	}
	x = 6
	y = 4
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Yellow_Banner"
	}
}

shared_focus = {
	id = ETR_A_New_Education_System
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Yellow_Banner
	}
	x = 6
	y = 5
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_A_New_Education_System"
	}
}

shared_focus = {
	id = ETR_The_Reforms
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Pan_Turkist_Agenda
	}
	x = 8
	y = 4
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Reforms"
	}
}

shared_focus = {
	id = ETR_The_Purge
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_A_New_Education_System
	}
	prerequisite = {
		focus = ETR_The_Reforms
	}
	x = 7
	y = 6
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Purge"
	}
}

shared_focus = {
	id = ETR_Chained_and_Shackled
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Pillage_and_Raze
	}
	x = 11
	y = 3
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Chained_and_Shackled"
	}
}

shared_focus = {
	id = ETR_Engines_of_War
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Chained_and_Shackled
	}
	x = 10
	y = 4
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Engines_of_War"
	}
}

shared_focus = {
	id = ETR_The_Binds_of_the_Factory
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Chained_and_Shackled
	}
	x = 11
	y = 5
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Binds_of_the_Factory"
	}
}

shared_focus = {
	id = ETR_The_Riders
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Flames_of_War
	}
	x = 12
	y = 1
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Riders"
	}
}

shared_focus = {
	id = ETR_Sustenance
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Chained_and_Shackled
	}
	x = 12
	y = 4
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Sustenance"
	}
}

shared_focus = {
	id = ETR_No_Space_for_Deserters
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Flames_of_War
	}
	x = 14
	y = 1
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_No_Space_for_Deserters"
	}
}

shared_focus = {
	id = ETR_The_Wolf_Strikes
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Pillage_and_Raze
	}
	prerequisite = {
		focus = ETR_Splitting_Skulls
	}
	x = 16
	y = 3
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Wolf_Strikes"
	}
}

shared_focus = {
	id = ETR_Burning_Crimson
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Wolf_Strikes
	}
	x = 14
	y = 4
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Burning_Crimson"
	}
}

shared_focus = {
	id = ETR_No_Space_for_the_Unfit
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Flames_of_War
	}
	x = 16
	y = 1
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_No_Space_for_the_Unfit"
	}
}

shared_focus = {
	id = ETR_Gather_Men
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_No_Space_for_Deserters
	}
	prerequisite = {
		focus = ETR_No_Space_for_the_Unfit
	}
	x = 15
	y = 2
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Gather_Men"
	}
}

shared_focus = {
	id = ETR_The_Bond_of_Blood
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Wolf_Strikes
	}
	x = 16
	y = 4
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Bond_of_Blood"
	}
}

shared_focus = {
	id = ETR_The_Steppes_Call
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Burning_Crimson
	}
	prerequisite = {
		focus = ETR_The_Bond_of_Blood
	}
	x = 15
	y = 5
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Steppes_Call"
	}
}

shared_focus = {
	id = ETR_Into_the_Desert
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Steppes_Call
	}
	x = 14
	y = 6
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Into_the_Desert"
	}
}

shared_focus = {
	id = ETR_Past_the_Mountains
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Wolf_Strikes
	}
	x = 18
	y = 4
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Past_the_Mountains"
	}
}

shared_focus = {
	id = ETR_The_Northern_Wastes
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Bond_of_Blood
	}
	prerequisite = {
		focus = ETR_Past_the_Mountains
	}
	x = 17
	y = 5
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_The_Northern_Wastes"
	}
}

shared_focus = {
	id = ETR_Destroy_the_Pretenders
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Steppes_Call
	}
	prerequisite = {
		focus = ETR_The_Northern_Wastes
	}
	x = 16
	y = 6
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Destroy_the_Pretenders"
	}
}

shared_focus = {
	id = ETR_Blood_and_Glory
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_Horse_Warriors
	}
	x = 4
	y = 4
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Blood_and_Glory"
	}
}

shared_focus = {
	id = ETR_Mashallah
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Purge
	}
	x = 7
	y = 7
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_Mashallah"
	}
}

shared_focus = {
	id = ETR_To_the_Sea
	icon = GFX_goal_unknown
	cost = 5.00
	prerequisite = {
		focus = ETR_The_Northern_Wastes
	}
	x = 18
	y = 6
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus ETR_To_the_Sea"
	}
}

