#####################
##Syonan Focus Tree##
#####By Roniius######
#####################
focus_tree = {
	id = syonan_debates
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SYO
		}
	}
	default = no
	shared_focus = SYO_The_Syonan_Diet
	shared_focus = SYO_The_Kongres_Kebangsaan_Melayu
}

focus_tree = {
	id = syonan_yamashita
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SYO
		}
	}
	default = no
	shared_focus = SYO_The_Tiger_of_Malaya
}

focus_tree = {
	id = syonan_lee
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SYO
		}
	}
	default = no
	shared_focus = SYO_LKY
}

focus_tree = {
	id = syonan_tunku
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SYO
		}
	}
	default = no
	shared_focus = SYO_Tunku
}

