###Blank Focus Tree###
focus_tree = {
	id = TUR_blank
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = TUR
		}
	}
	default = no
	shared_focus = TUR_blankfocus
}

###Turkey Intro Tree###
focus_tree = {
	id = TUR_intro
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = TUR
		}
	}
	default = no
	shared_focus = TUR_twenty_years
}

