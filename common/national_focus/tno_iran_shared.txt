shared_focus = {
	id = PER_blankfocus
	icon = GFX_generic_little_entente
	x = 0
	y = 2
	cost = 5
	allow_branch = {
		has_country_flag = neversetthisflag
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_blankfocus"
	}
}

### Civil War Tree ###

shared_focus = {
	id = PER_the_iranian_civil_war
	icon = GFX_unknown
	cost = 5
	allow_branch = {
	    has_country_leader = {
               name = "Farah Pahlavi"
               ruling_only = yes
        }
    }
	x = 6
	y = 0
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_the_iranian_civil_war"
		add_war_support = 0.2
	}
}

shared_focus = {
	id = PER_arm_the_people
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_the_iranian_civil_war
	}
	x = 3
	y = 1
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_arm_the_people"
		add_equipment_to_stockpile = {
			type = infantry_equipment
			amount = 2500
			producer = PER
		}
	}
}

shared_focus = {
	id = PER_filling_the_line
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_arm_the_people
	}
	x = 2
	y = 2
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_filling_the_line"
		custom_effect_tooltip = PER_filling_the_line_tooltip
		hidden_effect = {
			266 = {
				create_unit = {
					division = "name = \"Loyalist Militia\" division_template = \"Militia Division\" start_experience_factor = 0.1 start_equipment_factor = 0.8"
					owner = PER
					prioritize_location = 10837
				}
				create_unit = {
					division = "name = \"Loyalist Militia\" division_template = \"Militia Division\" start_experience_factor = 0.1 start_equipment_factor = 0.8"
					owner = PER
					prioritize_location = 10837
				}
			}
		}
	}
}

shared_focus = {
	id = PER_instigate_defections
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_filling_the_line
	}
	x = 2
	y = 3
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_instigate_defections"
		custom_effect_tooltip = PER_instigate_defections_tooltip
		hidden_effect = {
			266 = {
				create_unit = {
					division = "name = \"Loyalist Defectors\" division_template = \"Infantry Division\" start_experience_factor = 0.4 start_equipment_factor = 1.0"
					owner = PER
					prioritize_location = 10837
				}
			}
		}
	}
}

shared_focus = {
	id = PER_learn_on_the_job
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_arm_the_people
	}
	x = 4
	y = 2
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_learn_on_the_job"
		add_tech_bonus = {
			bonus = 0.75
			uses = 1
			category = land_doctrine
		}
	}
}

shared_focus = {
	id = PER_raid_the_airfields
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_learn_on_the_job
	}
	x = 4
	y = 3
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_raid_the_airfields"
		add_equipment_to_stockpile = {
			type = fighter_equipment
			amount = 150
			producer = PER
		}
	}
}

shared_focus = {
	id = PER_defending_tehran
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_the_iranian_civil_war
	}
	available = {
		controls_state = 266
	}
	x = 6
	y = 1
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_defending_tehran"
		266 = {
			add_building_construction = {
				type = bunker
				level = 2
				instant_build = yes
				province = 10837
			}
		}
	}
}

shared_focus = {
	id = PER_defending_the_skies
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_defending_tehran
	}
	available = {
		controls_state = 266
	}
	x = 6
	y = 2
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_defending_the_skies"
		266 = {
			add_building_construction = {
				type = anti_air_building
				level = 3
				instant_build = yes
			}
		}
	}
}

shared_focus = {
	id = PER_defending_our_secrets
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_defending_the_skies
	}
	x = 6
	y = 3
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_defending_our_secrets"
		add_tech_bonus = {
			bonus = 0.5
			uses = 1
			category = encryption_tech
		}
	}
}

shared_focus = {
	id = PER_a_war_industry
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_the_iranian_civil_war
	}
	x = 9
	y = 1
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_a_war_industry"
		add_ideas = tno_economic_focus_war_economy
	}
}

shared_focus = {
	id = PER_improvised_factories
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_a_war_industry
	}
	x = 8
	y = 2
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_improvised_factories"
		random_owned_controlled_state  = {
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = industrial_complex
				level = 2
				instant_build = yes
			}
		}
	}
}

shared_focus = {
	id = PER_bargain_with_investors
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_improvised_factories
	}
	x = 8
	y = 3
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_bargain_with_investors"
		add_offsite_building = { type = industrial_complex level = 2 }
	}
}

shared_focus = {
	id = PER_an_iranian_bullet
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_a_war_industry
	}
	x = 10
	y = 2
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_an_iranian_bullet"
		random_owned_controlled_state  = {
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = arms_factory
				level = 2
				instant_build = yes
			}
		}
	}
}

shared_focus = {
	id = PER_in_a_german_gun
	icon = GFX_unknown
	cost = 5
	prerequisite = {
		focus = PER_an_iranian_bullet
	}
	x = 10
	y = 3
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus PER_in_a_german_gun"
		add_offsite_building = { type = arms_factory level = 2 }
	}
}