focus_tree = {
	id = IRI_blankfocus
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = IRI
		}
	}
	default = no
	shared_focus = IRI_blankfocus
}

focus_tree = {
	id = IRI_civil_war
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = IRI
		}
	}
	default = no
	shared_focus = IRI_the_iranian_civil_war
}
