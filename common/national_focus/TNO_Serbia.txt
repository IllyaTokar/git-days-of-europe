focus_tree = {
	id = SER_Start
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SER
		}
	}
	default = no
	#shared_focus = SER_Serbia_start
}
focus_tree = {
	id = SER_Freedom
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = SER
		}
	}
	default = no
	#shared_focus = SER_Serbia_freedom
}