#BEST'S TREE - PLEASE BE QUIET
shared_focus = {
	id = DEN_b_man_in_charge
	icon = GFX_Generic_Army_Officers
	cost = 5
	x = 11
	y = 1
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_man_in_charge"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_border_controls
	icon = GFX_USA_Immigration_and_Nationality_Act
	cost = 5
	x = 13
	y = 2
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_man_in_charge
	}
	mutually_exclusive = {
		focus = DEN_b_let_them_in
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_border_controls"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_ask_hanneken
	icon = GFX_SGR_Military_Redevelopment
	cost = 5
	x = 13
	y = 3
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_border_controls
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_ask_hanneken"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_increase_mandate
	icon = GFX_MAD_o_deutschland_we_yearn_for_thee
	cost = 5
	x = 13
	y = 4
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_ask_hanneken
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_increase_mandate"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_give_in
	icon = GFX_new_model_army
	cost = 5
	x = 12
	y = 5
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_increase_mandate
	}
	mutually_exclusive = {
		focus = DEN_b_under_control
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_give_in"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_under_control
	icon = GFX_BRT_The_Breton_Standard
	cost = 5
	x = 14
	y = 5
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_increase_mandate
	}
	mutually_exclusive = {
		focus = DEN_b_give_in
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_under_control"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_open_zealand
	icon = GFX_Generic_Refugees
	cost = 5
	x = 12
	y = 6
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_give_in
		focus = DEN_b_increase_mandate
	}
	mutually_exclusive = {
		focus = DEN_b_too_fragile
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_open_zealand"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_too_fragile
	icon = RAJ_India_Diplo_Japan_Border
	cost = 5
	x = 14
	y = 6
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_give_in
		focus = DEN_b_increase_mandate
	}
	mutually_exclusive = {
		focus = DEN_b_open_zealand
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_too_fragile"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_temporary_measure
	icon = GFX_OST_The_future_frozen
	cost = 5
	x = 12
	y = 7
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_open_zealand
		focus = DEN_b_too_fragile
	}
	mutually_exclusive = {
		focus = DEN_b_crisis_over
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_temporary_measure"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_crisis_over
	icon = GFX_RAJ_India_Minority_Protections
	cost = 5
	x = 14
	y = 7
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_open_zealand
		focus = DEN_b_too_fragile
	}
	mutually_exclusive = {
		focus = DEN_b_temporary_measure
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_crisis_over"
		add_political_power = 50
	}
}
#LET THEM IN PATH #########################
shared_focus = {
	id = DEN_b_let_them_in
	icon = GFX_IBR_open_road
	cost = 5
	x = 9
	y = 2
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_man_in_charge
	}
	mutually_exclusive = {
		focus = DEN_b_border_controls
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_let_them_in"
		add_political_power = 50
	}
}
shared_focus = {
	id = DEN_b_jutland_cannot_all
	icon = GFX_OST_Fight_for_Freedom
	cost = 5
	x = 4
	y = 3
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_let_them_in
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_jutland_cannot_all"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_contain_protests
	icon = GFX_CAU_stomp_on_partisans
	cost = 5
	x = 4
	y = 4
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_jutland_cannot_all
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_contain_protests"
		add_political_power = 50
	}
}
shared_focus = {
	id = DEN_b_root_paramilitaries
	icon = GFX_ENG_stop_the_terrorists
	cost = 5
	x = 3
	y = 5
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_contain_protests
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_root_paramilitaries"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_detain_schalburgcorps
	icon = GFX_USA_detain_leaders
	cost = 5
	x = 3
	y = 6
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_contain_protests
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_root_paramilitaries"
		hidden_effect = {
			random_list = {
				50 = {
					add_popularity = {
						ideology = national_socialism
    					popularity = -0.10
					}
				}
				50 = {
					kill_country_leader = yes
					set_politics = { ruling_party = national_socialism }
					add_popularity = {
						ideology = national_socialism
    					popularity = 0.10
					}
					add_popularity = {
						ideology = authoritarian_democrat
    					popularity = -0.15
					}
					set_country_flag = DEN_schalburg_rule
					set_country_flag = DEN_schalburg_coup_success
					remove_ideas = DEN_permanent_secretaries
					remove_ideas = DEN_threat_from_silkeborg
				}
			}
		}
	}
}

shared_focus = {
	id = DEN_b_crush_resistance
	icon = GFX_VIN_Full_Legal_Protections
	cost = 5
	x = 5
	y = 5
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_contain_protests
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_crush_resistance"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_embolden_hipo
	icon = GFX_SGR_Military_Redevelopment
	cost = 5
	x = 5
	y = 6
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_crush_resistance
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_embolden_hipo"
		add_political_power = 50
	}
}
shared_focus = {
	id = DEN_b_stronger_than_ever
	icon = GFX_USA_duty_to_country
	cost = 5
	x = 4
	y = 7
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_detain_schalburgcorps
	}
	prerequisite = {
		focus = DEN_b_embolden_hipo
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_stronger_than_ever"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_refugee_camps
	icon = GFX_RAJ_India_Diplo_Japan_Border
	cost = 5
	x = 8
	y = 3
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_let_them_in
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_refugee_camps"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_eye_on_nationalists
	icon = GFX_CNT_take_it_as_it_comes
	cost = 5
	x = 7
	y = 4
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_refugee_camps
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_eye_on_nationalists"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_disband_hipo
	icon = GFX_goal_policecar
	cost = 5
	x = 7
	y = 5
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_eye_on_nationalists
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_disband_hipo"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_contain_SS
	icon = GFX_RAJ_Deal_With_Fascist_Militia
	cost = 5
	x = 9
	y = 4
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_refugee_camps
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_contain_SS"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_calm_wehrmacht
	icon = GFX_RAJ_Cut_Military_Spending
	cost = 5
	x = 9
	y = 5
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_contain_SS
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_calm_wehrmacht"
		add_political_power = 50
	}
}

shared_focus = {
	id = DEN_b_status_quo
	icon = GFX_GER_CN_invite_secondary_powers
	cost = 5
	x = 7
	y = 6
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_disband_hipo
	}
	prerequisite = {
		focus = DEN_b_calm_wehrmacht
	}
	mutually_exclusive = {
		focus = DEN_b_audience_with_king
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_calm_wehrmacht"
		add_political_power = 50
		add_popularity = {
			ideology = authoritarian_democrat
    		popularity = 0.05
		}
	}
}

shared_focus = {
	id = DEN_b_audience_with_king
	icon = GFX_Generic_Shady_Deal
	cost = 5
	x = 9
	y = 6
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	prerequisite = {
		focus = DEN_b_disband_hipo
	}
	prerequisite = {
		focus = DEN_b_calm_wehrmacht
	}
	mutually_exclusive = {
		focus = DEN_b_status_quo
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_b_calm_wehrmacht"
		hidden_effect = {
			random_list = {
				0 = {
					
				}
				100 = {
					kill_country_leader = yes
					set_politics = { ruling_party = fascism }
					add_popularity = {
						ideology = fascism
    					popularity = 0.05
					}
					add_popularity = {
						ideology = national_socialism
    					popularity = -0.10
					}
					set_country_flag = DEN_madsen_rule
					set_country_flag = DEN_madsen_king
					remove_ideas = DEN_permanent_secretaries
					remove_ideas = DEN_threat_from_silkeborg
				}
			}
		}
	}
}


#MARTINSEN'S TREE - DENMARK FOREVER
shared_focus = {
	id = DEN_m_destroying_denmark
	icon = GFX_RAJ_fight_the_system
	cost = 5
	x = 7
	y = 0
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_destroying_denmark"
		add_political_power = 50
	}
}



#HANNEKEN'S TREE - NO CIVILIANS ALLOWED
shared_focus = {
	id = DEN_h_no_control
	icon = GFX_RAJ_fight_the_system
	cost = 5
	x = 7
	y = 0
	available_if_capitulated = no
	ai_will_do = {
		factor = 1
	}
	completion_reward = {
		log = "[GetDateText]: [Root.GetName]: Focus DEN_h_no_control"
		add_political_power = 50
	}
}