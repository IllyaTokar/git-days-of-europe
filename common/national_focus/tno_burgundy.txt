
####BEFORE YOU ASK WHY THERES 4 IDENTICAL TREES I MERGED THEM TO FIX ISSUES WITH TREES REAPPEARING A SHITTON AND MERGING THEM INTO
####2 TREES WOULDVE MADE ME HAVE TO CHANGE THE SCRIPTED EFFECTS FOR EVERYTHING

focus_tree = {
	id = tno_burgundy_main
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = BRG
		}
	}
	default = no
	shared_focus = BRG_unternehmen_ludwig
	shared_focus = BRG_unter_der_schwarzen_sonne
	#shared_focus = BRG_massenvernichtungswaffe
	shared_focus = BRG_fall_ragnachar
	shared_focus = BRG_globalplan_kondor
	shared_focus = BRG_globalplan_wolf
	shared_focus = BRG_globalplan_bar
	shared_focus = BRG_globalplan_puma
	shared_focus = BRG_globalplan_adler
	shared_focus = BRG_wolf_globalplan_wolf

	shared_focus = BRG_lebenskultur
	shared_focus = BRG_arbeiterkultur
	shared_focus = BRG_our_true_goal
	shared_focus = BRG_das_landwirtschaftsamt
	shared_focus  = BRG_education_waiting_for_text
	shared_focus = BRG_unternehmen_ludwig
	shared_focus = BRG_die_letzle_schlacht
}

focus_tree = {
	id = tno_burgundy_blankfocus
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = BRG
		}
	}
	default = no
	shared_focus = BRG_blankfocus
}

focus_tree = {
	id = tno_burgundy_domestic_trees
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = BRG
		}
	}
	default = no
	shared_focus = BRG_lebenskultur
	shared_focus = BRG_arbeiterkultur
	shared_focus = BRG_our_true_goal
	shared_focus = BRG_das_landwirtschaftsamt
	shared_focus  = BRG_education_waiting_for_text
	shared_focus = BRG_unternehmen_ludwig
	shared_focus = BRG_die_letzle_schlacht
	shared_focus = BRG_unternehmen_ludwig
	shared_focus = BRG_unter_der_schwarzen_sonne
	#shared_focus = BRG_massenvernichtungswaffe
	shared_focus = BRG_fall_ragnachar
	shared_focus = BRG_globalplan_kondor
	shared_focus = BRG_globalplan_wolf
	shared_focus = BRG_globalplan_bar
	shared_focus = BRG_globalplan_puma
	shared_focus = BRG_globalplan_adler
	shared_focus = BRG_wolf_globalplan_wolf
}