focus_tree = {
	id = PAV_novosibirsk_start
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = PAV
		}
	}
	default = no
	shared_focus = PAV_land_of_the_strong
}

focus_tree = {
	id = PAV_novosibirsk_expansion
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = PAV
		}
	}
	default = no
	shared_focus = PAV_spring_rasputitsa
}

focus_tree = {
	id = PAV_novosibirsk_regional
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = PAV
		}
	}
	default = no
	shared_focus = PAV_the_victorious_federation
	shared_focus = PAV_the_siberian_economy
	shared_focus = PAV_expand_the_all_siberian_army
	shared_focus = PAV_our_geopolitical_future
}

focus_tree = {
	id = PAV_novosibirsk_superregional
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = PAV
		}
	}
	default = no
	shared_focus = PAV_the_siberian_mandate
	shared_focus = RUS_SIB_nukes_into_the_atomic_age
}

focus_tree = {
	id = PAV_novosibirsk_superregional_shukshin
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = PAV
		}
	}
	default = no
	shared_focus = PAV_the_siberian_mandate
	shared_focus = RUS_SIB_nukes_into_the_atomic_age
}

focus_tree = {
	id = PAV_novosibirsk_superregional_pokryshkin
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = PAV
		}
	}
	default = no
	shared_focus = PAV_the_siberian_mandate
	shared_focus = RUS_SIB_nukes_into_the_atomic_age
}