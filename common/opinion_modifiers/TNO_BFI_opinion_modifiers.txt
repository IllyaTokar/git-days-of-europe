#########################################################################
# OPINION MODIFIERS
##########################################################################
# value
# min_trust
# max_trust
# decay
# months/years/days = timer
# trade = yes/no

opinion_modifiers = {
	BFI_tourism_relations_2 = {
		value = 3
	}
	BFI_tourism_relations_5 = {
		value = 5
	}
	BFI_tourism_relations_10 = {
		value = 10
	}
	BFI_tourism_relations_20 = {
		value = 20
	}
	BFI_tourism_relations_30 = {
		value = 30
	}
	
	BFI_diplo_accords_15 = {
		value = 15
	}
	BFI_diplo_accords_20 = {
		value = 20
	}
	BFI_diplo_accords_30 = {
		value = 30
	}
	
	BFI_enemy_of_enemy_10 = {
		value = 10
	}
	BFI_enemy_of_enemy_20 = {
		value = 20
	}
	BFI_enemy_of_enemy_30 = {
		value = 30
	}
	BFI_enemy_of_enemy_40 = {
		value = 40
	}
	BFI_enemy_of_enemy_50 = {
		value = 50
	}
	
	
	BFI_trade_10 = {
		trade = yes
		value = 10
	}
	BFI_trade_15 = {
		trade = yes
		value = 15
	}
	BFI_trade_20 = {
		trade = yes
		value = 20
	}
	BFI_trade_25 = {
		trade = yes
		value = 25
	}
	BFI_trade_30 = {
		trade = yes
		value = 30
	}
	BFI_trade_40 = {
		trade = yes
		value = 40
	}
	BFI_trade_45 = {
		trade = yes
		value = 45
	}
	BFI_trade_50 = {
		trade = yes
		value = 50
	}
	BFI_trade_60 = {
		trade = yes
		value = 60
	}
}
