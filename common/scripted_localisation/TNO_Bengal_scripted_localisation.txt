defined_text = {
	name = BEN_Bengali_Ethnic_Tensions_Loc
	text = {
		trigger = {
			check_variable = { BEN_Bengali_Ethnic_Tensions > 99 }
		}
		localization_key = BEN_Bengali_Ethnic_Tensions_Boiling_Point
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Bengali_Ethnic_Tensions > 79 }
				check_variable = { BEN_Bengali_Ethnic_Tensions < 100 }
			}
		}
		localization_key = BEN_Scripted_Very_High
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Bengali_Ethnic_Tensions > 59 }
				check_variable = { BEN_Bengali_Ethnic_Tensions < 80 }
			}
		}
		localization_key = BEN_Scripted_High
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Bengali_Ethnic_Tensions > 39 }
				check_variable = { BEN_Bengali_Ethnic_Tensions < 60 }
			}
		}
		localization_key = BEN_Bengali_Ethnic_Tensions_Noticiable
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Bengali_Ethnic_Tensions > 19 }
				check_variable = { BEN_Bengali_Ethnic_Tensions < 40 }
			}
		}
		localization_key = BEN_Scripted_Low
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Bengali_Ethnic_Tensions > 0 }
				check_variable = { BEN_Bengali_Ethnic_Tensions < 20 }
			}
		}
		localization_key = BEN_Scripted_Very_Low
	}
	text = {
		trigger = {
			check_variable = { BEN_Bengali_Ethnic_Tensions < 1 }
		}
		localization_key = BEN_Bengali_Ethnic_Tensions_Non_Existant
	}
}

defined_text = {
	name = BEN_Army_Loyalty_Loc
	text = {
		trigger = {
			check_variable = { BEN_Army_Loyalty > 80 }
		}
		localization_key = BEN_Scripted_Very_High
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Army_Loyalty > 59 }
				check_variable = { BEN_Army_Loyalty < 80 }
			}
		}
		localization_key = BEN_Scripted_High
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Army_Loyalty > 39 }
				check_variable = { BEN_Army_Loyalty < 60 }
			}
		}
		localization_key = BEN_Scripted_Average
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Army_Loyalty > 19 }
				check_variable = { BEN_Army_Loyalty < 40 }
			}
		}
		localization_key = BEN_Scripted_Low
	}
	text = {
		trigger = {
			check_variable = { BEN_Army_Loyalty < 20 }
		}
		localization_key = BEN_Scripted_Very_Low
	}
}

defined_text = {
	name = BEN_Army_Independence_Loc
	text = {
		trigger = {
			check_variable = { BEN_Army_Independence > 80 }
		}
		localization_key = BEN_Scripted_Very_High
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Bengali_Ethnic_Tensions > 59 }
				check_variable = { BEN_Bengali_Ethnic_Tensions < 80 }
			}
		}
		localization_key = BEN_Scripted_High
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Army_Independence > 39 }
				check_variable = { BEN_Army_Independence < 60 }
			}
		}
		localization_key = BEN_Scripted_Average
	}
	text = {
		trigger = {
			AND = {
				check_variable = { BEN_Army_Independence > 19 }
				check_variable = { BEN_Army_Independence < 40 }
			}
		}
		localization_key = BEN_Scripted_Low
	}
	text = {
		trigger = {
			check_variable = { BEN_Army_Independence < 20 }
		}
		localization_key = BEN_Scripted_Very_Low
	}
}
