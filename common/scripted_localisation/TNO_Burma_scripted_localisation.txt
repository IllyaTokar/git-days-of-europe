defined_text = {
	name = GetBUR_SHA_unrest
	text = {
		trigger = {
			check_variable = { BUR_SHA_unrest > 80 }
		}
		localization_key = BUR_SHA_unrest_very_high
	}
	text = {
		trigger = {
			check_variable = { BUR_SHA_unrest > 60 }
		}
		localization_key = BUR_SHA_unrest_high
	}
	text = {
		trigger = {
			check_variable = { BUR_SHA_unrest > 40 }
		}
		localization_key = BUR_SHA_unrest_medium
	}
	text = {
		trigger = {
			check_variable = { BUR_SHA_unrest > 20 }
		}
		localization_key = BUR_SHA_unrest_low
	}
	text = {
		localization_key = BUR_SHA_unrest_very_low
	}
}

defined_text = {
	name = GetBUR_ARA_unrest
	text = {
		trigger = {
			check_variable = { BUR_ARA_unrest > 80 }
		}
		localization_key = BUR_ARA_unrest_very_high
	}
	text = {
		trigger = {
			check_variable = { BUR_ARA_unrest > 60 }
		}
		localization_key = BUR_ARA_unrest_high
	}
	text = {
		trigger = {
			check_variable = { BUR_ARA_unrest > 40 }
		}
		localization_key = BUR_ARA_unrest_medium
	}
	text = {
		trigger = {
			check_variable = { BUR_ARA_unrest > 20 }
		}
		localization_key = BUR_ARA_unrest_low
	}
	text = {
		localization_key = BUR_ARA_unrest_very_low
	}
}

defined_text = {
	name = GetBUR_MON_unrest
	text = {
		trigger = {
			check_variable = { BUR_MON_unrest > 80 }
		}
		localization_key = BUR_MON_unrest_very_high
	}
	text = {
		trigger = {
			check_variable = { BUR_MON_unrest > 60 }
		}
		localization_key = BUR_MON_unrest_high
	}
	text = {
		trigger = {
			check_variable = { BUR_MON_unrest > 40 }
		}
		localization_key = BUR_MON_unrest_medium
	}
	text = {
		trigger = {
			check_variable = { BUR_MON_unrest > 20 }
		}
		localization_key = BUR_MON_unrest_low
	}
	text = {
		localization_key = BUR_MON_unrest_very_low
	}
}

defined_text = {
	name = GetBUR_KAC_unrest
	text = {
		trigger = {
			check_variable = { BUR_KAC_unrest > 80 }
		}
		localization_key = BUR_KAC_unrest_very_high
	}
	text = {
		trigger = {
			check_variable = { BUR_KAC_unrest > 60 }
		}
		localization_key = BUR_KAC_unrest_high
	}
	text = {
		trigger = {
			check_variable = { BUR_KAC_unrest > 40 }
		}
		localization_key = BUR_KAC_unrest_medium
	}
	text = {
		trigger = {
			check_variable = { BUR_KAC_unrest > 20 }
		}
		localization_key = BUR_KAC_unrest_low
	}
	text = {
		localization_key = BUR_KAC_unrest_very_low
	}
}

defined_text = {
	name = GetBUR_KAR_unrest
	text = {
		trigger = {
			check_variable = { BUR_KAR_unrest > 80 }
		}
		localization_key = BUR_KAR_unrest_very_high
	}
	text = {
		trigger = {
			check_variable = { BUR_KAR_unrest > 60 }
		}
		localization_key = BUR_KAR_unrest_high
	}
	text = {
		trigger = {
			check_variable = { BUR_KAR_unrest > 40 }
		}
		localization_key = BUR_KAR_unrest_medium
	}
	text = {
		trigger = {
			check_variable = { BUR_KAR_unrest > 20 }
		}
		localization_key = BUR_KAR_unrest_low
	}
	text = {
		localization_key = BUR_KAR_unrest_very_low
	}
}

defined_text = {
	name = GetBUR_CHN_unrest
	text = {
		trigger = {
			check_variable = { BUR_CHN_unrest > 80 }
		}
		localization_key = BUR_CHN_unrest_very_high
	}
	text = {
		trigger = {
			check_variable = { BUR_CHN_unrest > 60 }
		}
		localization_key = BUR_CHN_unrest_high
	}
	text = {
		trigger = {
			check_variable = { BUR_CHN_unrest > 40 }
		}
		localization_key = BUR_CHN_unrest_medium
	}
	text = {
		trigger = {
			check_variable = { BUR_CHN_unrest > 20 }
		}
		localization_key = BUR_CHN_unrest_low
	}
	text = {
		localization_key = BUR_CHN_unrest_very_low
	}
}

defined_text = {
	name = GetBUR_CBR_unrest
	text = {
		trigger = {
			check_variable = { BUR_CBR_unrest > 80 }
		}
		localization_key = BUR_CBR_unrest_very_high
	}
	text = {
		trigger = {
			check_variable = { BUR_CBR_unrest > 60 }
		}
		localization_key = BUR_CBR_unrest_high
	}
	text = {
		trigger = {
			check_variable = { BUR_CBR_unrest > 40 }
		}
		localization_key = BUR_CBR_unrest_medium
	}
	text = {
		trigger = {
			check_variable = { BUR_CBR_unrest > 20 }
		}
		localization_key = BUR_CBR_unrest_low
	}
	text = {
		localization_key = BUR_CBR_unrest_very_low
	}
}
