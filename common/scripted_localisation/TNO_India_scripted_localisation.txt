#defined_text = { 
#	name = RAJ_Coal_in_the_Furnace_Change
#	text = {
#		trigger = {
#			RAJ = {
#				NOT = {
#					has_country_flag = RAJ_Devi_Help_Purges
#				}
#			}
#		}
#		localization_key = RAJ_Coal_in_the_Furnace_Change_NoDevi_loc
#	}
#	text = {
#		trigger = {
#			RAJ = {
#				has_country_flag = RAJ_Devi_Help_Purges
#			}
#		}
#		localization_key = RAJ_Coal_in_the_Furnace_Change_Devi_loc
#	}
#}
#
#defined_text = { 
#	name = PAK_PakistanInsurgency
#	text = {
#		trigger = {
#			tag = PAK
#			has_government = communist
#		}
#		localization_key = PAK_PakistanInsurgency_MKF_loc
#	}
#	text = {
#		trigger = {
#			tag = PAK
#			has_government = ultranational_socialism
#		}
#		localization_key = PAK_PakistanInsurgency_Mujahideen_loc
#	}
#	text = {
#		trigger = {
#			tag = RAJ
#		}
#		localization_key = PAK_PakistanInsurgency_India_loc
#	}
#}
#
#defined_text = { 
#	name = RAJ_GetPresident
#	text = {
#		trigger = {
#			RAJ = { has_country_flag = RAJ_President_Rajendra_Prasad }
#		}
#		localization_key = RAJ_GetPresident_President_Rajendra_Prasad_loc
#	}
#	text = {
#		trigger = {
#			RAJ = { has_country_flag = RAJ_President_Neelam_Sanjiva_Reddy }
#		}
#		localization_key = PAK_GetPresident_President_Neelam_Sanjiva_Reddy_loc
#	}
#	text = {
#		trigger = {
#			RAJ = { has_country_flag = RAJ_President_Basappa_Danappa_Jatti }
#		}
#		localization_key = PAK_GetPresident_President_Basappa_Danappa_Jatti_loc
#	}
#	text = {
#		trigger = {
#			RAJ = { has_country_flag = RAJ_President_Sarvepalli_Radhakrishnan }
#		}
#		localization_key = PAK_GetPresident_President_Sarvepalli_Radhakrishnan_loc
#	}
#	text = {
#		trigger = {
#			RAJ = { has_country_flag = RAJ_President_Chandra_Rajeswara_Rao }
#		}
#		localization_key = PAK_GetPresident_President_Chandra_Rajeswara_Rao_loc
#	}
#}