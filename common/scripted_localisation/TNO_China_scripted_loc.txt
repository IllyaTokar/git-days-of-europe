# Allows you to create your own dynamic keys
# to be called in localization.
# defined_text -> this is it, we're defining the text
# text -> a discrete entry that can be picked to display in loc.
# trigger -> determines if a text entry will be picked or not.
# (The triggers need to be valid for the scope the key is called in
# (eg Root or From.From).)
# localization_key -> points to the localization key
# that'll be used if trigger passes

#Variable Colours
defined_text = {
	name = GetJapanophileFactionInfluence
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_influence value = 0.85 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_inf_red
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_influence value = 0.75 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_inf_orange
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_influence value = 0.65 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_inf_yellow
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_influence value = 0.55 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_inf_lime
	}
	text = {
		localization_key = CHI_fac_japanophile_inf_green
	}
}

defined_text = { #Old Guard
	name = GetOldGuardFactionInfluence
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_influence value = 0.85 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_inf_red
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_influence value = 0.75 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_inf_orange
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_influence value = 0.65 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_inf_yellow
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_influence value = 0.55 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_inf_lime
	}
	text = {
		localization_key = CHI_fac_old_guard_inf_green
	}
}

defined_text = { #Reformist
	name = GetReformistFactionInfluence
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_influence value = 0.85 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_inf_red
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_influence value = 0.75 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_inf_orange
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_influence value = 0.65 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_inf_yellow
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_influence value = 0.55 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_inf_lime
	}
	text = {
		localization_key = CHI_fac_reformist_inf_green
	}
}

defined_text = {
	name = GetJapanophileFactionOpinion
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 15 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_opn_red
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 25 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_opn_orange
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 35 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_opn_yellow
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 45 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_opn_lime
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 85 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_opn_lavender
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 75 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_opn_lightblue
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 65 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_opn_lightbluegreen
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 55 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_japanophile_opn_bluegreen
	}
	text = {
		localization_key = CHI_fac_japanophile_opn_green
	}
}

defined_text = { #Old Guard
	name = GetOldGuardFactionOpinion
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 15 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_opn_red
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 25 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_opn_orange
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 35 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_opn_yellow
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 45 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_opn_lime
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 85 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_opn_lavender
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 75 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_opn_lightblue
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 65 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_opn_lightbluegreen
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 55 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_opn_bluegreen
	}
	text = {
		localization_key = CHI_fac_old_guard_opn_green
	}
}

defined_text = { #Reformist
	name = GetReformistFactionOpinion
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 15 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_reformist_opn_red
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 25 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_reformist_opn_orange
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 35 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_reformist_opn_yellow
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 45 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_reformist_opn_lime
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 85 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_opn_lavender
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 75 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_opn_lightblue
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 65 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_opn_lightbluegreen
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 55 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_opn_bluegreen
	}
	text = {
		localization_key = CHI_fac_reformist_opn_green
	}
}

defined_text = {
	name = GetProJapanOpinionChange
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 90 compare = greater_than}
		}
		localization_key = CHI_fac_pro_japan_high_decrease
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 75 compare = greater_than}
		}
		localization_key = CHI_fac_pro_japan_mid_decrease
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 50 compare = greater_than}
		}
		localization_key = CHI_fac_pro_japan_low_decrease
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 10 compare = less_than}
		}
		localization_key = CHI_fac_pro_japan_high_increase
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 25 compare = less_than}
		}
		localization_key = CHI_fac_pro_japan_mid_increase
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 50 compare = less_than}
		}
		localization_key = CHI_fac_pro_japan_low_increase
	}
	text = {
		localization_key = CHI_fac_pro_japan_no_change
	}
}

defined_text = {
	name = GetOldGuardOpinionChange
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 90 compare = greater_than}
		}
		localization_key = CHI_fac_old_guard_high_decrease
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 75 compare = greater_than}
		}
		localization_key = CHI_fac_old_guard_mid_decrease
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 50 compare = greater_than}
		}
		localization_key = CHI_fac_old_guard_low_decrease
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 10 compare = less_than}
		}
		localization_key = CHI_fac_old_guard_high_increase
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 25 compare = less_than}
		}
		localization_key = CHI_fac_old_guard_mid_increase
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 50 compare = less_than}
		}
		localization_key = CHI_fac_old_guard_low_increase
	}
	text = {
		localization_key = CHI_fac_old_guard_no_change
	}
}

defined_text = {
	name = GetReformistOpinionChange
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 90 compare = greater_than}
		}
		localization_key = CHI_fac_reformist_high_decrease
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 75 compare = greater_than}
		}
		localization_key = CHI_fac_reformist_mid_decrease
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 50 compare = greater_than}
		}
		localization_key = CHI_fac_reformist_low_decrease
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 10 compare = less_than}
		}
		localization_key = CHI_fac_reformist_high_increase
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 25 compare = less_than}
		}
		localization_key = CHI_fac_reformist_mid_increase
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 50 compare = less_than}
		}
		localization_key = CHI_fac_reformist_low_increase
	}
	text = {
		localization_key = CHI_fac_reformist_no_change
	}
}

defined_text = {
	name = GetJapanophileFactionModifierStatus
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 60 compare = greater_than_or_equals}
			check_variable = {var = CHI.faction_pro_japan_influence value = 0.70 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_pro_japan_modifier_good_high
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 60 compare = greater_than_or_equals}
			check_variable = {var = CHI.faction_pro_japan_influence value = 0.50 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_pro_japan_modifier_good_mid
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 60 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_pro_japan_modifier_good_low
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 40 compare = less_than_or_equals}
			check_variable = {var = CHI.faction_pro_japan_influence value = 0.70 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_pro_japan_modifier_bad_high
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 40 compare = less_than_or_equals}
			check_variable = {var = CHI.faction_pro_japan_influence value = 0.50 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_pro_japan_modifier_bad_mid
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_pro_japan_opinion value = 40 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_pro_japan_modifier_bad_low
	}
}



defined_text = {
	name = GetOldGuardFactionModifierStatus
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 60 compare = greater_than_or_equals}
			check_variable = {var = CHI.faction_old_guard_influence value = 0.70 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_modifier_good_high
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 60 compare = greater_than_or_equals}
			check_variable = {var = CHI.faction_old_guard_influence value = 0.50 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_modifier_good_mid
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 60 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_modifier_good_low
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 40 compare = less_than_or_equals}
			check_variable = {var = CHI.faction_old_guard_influence value = 0.70 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_modifier_bad_high
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 40 compare = less_than_or_equals}
			check_variable = {var = CHI.faction_old_guard_influence value = 0.50 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_modifier_bad_mid
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_old_guard_opinion value = 40 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_old_guard_modifier_bad_low
	}
}

defined_text = {
	name = GetReformistFactionModifierStatus
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 60 compare = greater_than_or_equals}
			check_variable = {var = CHI.faction_reformist_influence value = 0.70 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_modifier_good_high
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 60 compare = greater_than_or_equals}
			check_variable = {var = CHI.faction_reformist_influence value = 0.50 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_modifier_good_mid
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 60 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_modifier_good_low
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 40 compare = less_than_or_equals}
			check_variable = {var = CHI.faction_reformist_influence value = 0.70 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_modifier_bad_high
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 40 compare = less_than_or_equals}
			check_variable = {var = CHI.faction_reformist_influence value = 0.50 compare = greater_than_or_equals}
		}
		localization_key = CHI_fac_reformist_modifier_bad_mid
	}
	text = {
		trigger = {
			check_variable = {var = CHI.faction_reformist_opinion value = 40 compare = less_than_or_equals}
		}
		localization_key = CHI_fac_reformist_modifier_bad_low
	}
}

defined_text = {
	name = GetEduStatusTitle
	text = {
		trigger = {
			check_variable = {edu_tracker > 27}
		}
		localization_key = CHI_edu_very_good_title
	}
	text = {
		trigger = {
			check_variable = {edu_tracker > 20}
		}
		localization_key = CHI_edu_good_title
	}
	text = {
		trigger = {
			check_variable = {edu_tracker > 13}
		}
		localization_key = CHI_edu_ok_title
	}
	text = {
		trigger = {
			check_variable = {edu_tracker > 6}
		}
		localization_key = CHI_edu_bad_title
	}
	text = {
		localization_key = CHI_edu_very_bad_title
	}
}

defined_text = {
	name = GetEduStatusDesc
	text = {
		trigger = {
			check_variable = {edu_tracker > 27}
		}
		localization_key = CHI_edu_very_good_desc
	}
	text = {
		trigger = {
			check_variable = {edu_tracker > 20}
		}
		localization_key = CHI_edu_good_desc
	}
	text = {
		trigger = {
			check_variable = {edu_tracker > 13}
		}
		localization_key = CHI_edu_ok_desc
	}
	text = {
		trigger = {
			check_variable = {edu_tracker > 6}
		}
		localization_key = CHI_edu_bad_desc
	}
	text = {
		localization_key = CHI_edu_very_bad_desc
	}
}

defined_text = {
	name = GetNanjingGoods
	text = {
		trigger = {
			check_variable = { CHI_nanjing_fallout_consumer_goods < 0 }
		}
		localization_key = ChiNanjingGoodsGood
	}
	text = {
		trigger = {
			check_variable = { CHI_nanjing_fallout_consumer_goods > 0 }
		}
		localization_key = ChiNanjingGoodsBad
	}
	text = {
		localization_key = ChiNanjingGoodsZero
	}
}

defined_text = {
	name = GetNanjingPp
	text = {
		trigger = {
			check_variable = { CHI_nanjing_shockwave_pol > 0 }
		}
		localization_key = ChiNanjingPpGood
	}
	text = {
		trigger = {
			check_variable = { CHI_nanjing_shockwave_pol < 0 }
		}
		localization_key = ChiNanjingPpBad
	}
	text = {
		localization_key = ChiNanjingPpZero
	}
}

defined_text = {
	name = GetNanjingShockwaveStatusTitle
	text = {
		trigger = {
			check_variable = {nanjing_shockwave > 27}
		}
		localization_key = CHI_nanjing_shockwave_very_good_title
	}
	text = {
		trigger = {
			check_variable = {nanjing_shockwave > 20}
		}
		localization_key = CHI_nanjing_shockwave_good_title
	}
	text = {
		trigger = {
			check_variable = {nanjing_shockwave > 13}
		}
		localization_key = CHI_nanjing_shockwave_ok_title
	}
	text = {
		trigger = {
			check_variable = {nanjing_shockwave > 6}
		}
		localization_key = CHI_nanjing_shockwave_bad_title
	}
	text = {
		localization_key = CHI_nanjing_shockwave_very_bad_title
	}
}

defined_text = {
	name = GetNanjingFalloutStatusTitle
	text = {
		trigger = {
			check_variable = {nanjing_fallout > 27}
		}
		localization_key = CHI_nanjing_fallout_very_good_title
	}
	text = {
		trigger = {
			check_variable = {nanjing_fallout > 20}
		}
		localization_key = CHI_nanjing_fallout_good_title
	}
	text = {
		trigger = {
			check_variable = {nanjing_fallout > 13}
		}
		localization_key = CHI_nanjing_fallout_ok_title
	}
	text = {
		trigger = {
			check_variable = {nanjing_fallout > 6}
		}
		localization_key = CHI_nanjing_fallout_bad_title
	}
	text = {
		localization_key = CHI_nanjing_fallout_very_bad_title
	}
}

defined_text = {
	name = GetIJAalert
	text = {
		trigger = {
			check_variable = {ija_alert < 0.75}
		}
		localization_key = CHI_ija_alert_acceptable
	}
	text = {
		trigger = {
			check_variable = {ija_alert < 0.9}
		}
		localization_key = CHI_ija_alert_dangerous
	}
	text = {
		localization_key = CHI_ija_alert_critical
	}
}

defined_text = {
	name = GetWarlordCategory
	text = {
		trigger = {
			original_tag = CHI
		}
		localization_key = CHI_warlord_dynamic_description
	}
	text = {
		trigger = {
			original_tag = JAP
		}
		localization_key = JAP_warlord_dynamic_description
	}
}

defined_text = {
	name = GetInfluenceBreakdown
	text = {
		trigger = {
			THIS = {
				original_tag = SHX
				exists = yes
			}
		}
		localization_key = shx_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = HUI
				exists = yes
			}
		}
		localization_key = hui_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = SIK
				exists = yes
			}
		}
		localization_key = sik_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = ETR
				exists = yes
			}
		}
		localization_key = etr_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = XSM
				exists = yes
			}
		}
		localization_key = xsm_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = SIC
				exists = yes
			}
		}
		localization_key = sic_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = YAA
				exists = yes
			}
		}
		localization_key = yaa_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = YUN
				exists = yes
				NOT = { has_country_flag = YUN_long_yun_crusade }
			}
		}
		localization_key = yun_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = GUZ
				exists = yes
			}
		}
		localization_key = guz_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = GXC
				exists = yes
			}
		}
		localization_key = gxc_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = GNG
				exists = yes
			}
		}
		localization_key = gng_control_warlord_icon_values
	}
	text = {
		trigger = {
			THIS = {
				original_tag = MEN
				exists = yes
			}
		}
		localization_key = men_control_warlord_icon_values
	}
}

defined_text = {
	name = GetInfluenceLevel
	text = {
		trigger = {
			THIS = {
				check_variable = {
					var = chi_influence
					value = 5
					compare = less_than_or_equals
				}
			}
		}
		localization_key = warlord_influence_0
	}
	text = {
		trigger = {
			THIS = {
				check_variable = {
					chi_influence > 5
				}
				check_variable = {
					var = chi_influence
					value = 25
					compare = less_than_or_equals
				}
			}
		}
		localization_key = warlord_influence_1
	}
	text = {
		trigger = {
			THIS = {
				check_variable = {
					chi_influence > 25
				}
				check_variable = {
					var = chi_influence
					value = 40
					compare = less_than_or_equals
				}
			}
		}
		localization_key = warlord_influence_2
	}
	text = {
		trigger = {
			THIS = {
				check_variable = {
					chi_influence > 40
				}
				check_variable = {
					var = chi_influence
					value = 60
					compare = less_than_or_equals
				}
			}
		}
		localization_key = warlord_influence_25
	}
	text = {
		trigger = {
			THIS = {
				check_variable = {
					chi_influence > 60
				}
				check_variable = {
					var = chi_influence
					value = 75
					compare = less_than_or_equals
				}
			}
		}
		localization_key = warlord_influence_3
	}
	text = {
		trigger = {
			THIS = {
				check_variable = {
					chi_influence > 75
				}
				check_variable = {
					var = chi_influence
					value = 95
					compare = less_than_or_equals
				}
			}
		}
		localization_key = warlord_influence_4
	}
	text = {
		trigger = {
			THIS = {
				check_variable = {
					chi_influence > 95
				}
			}
		}
		localization_key = warlord_influence_5
	}
	text = {
		localization_key = ERROR_REPORT_THIS_AS_A_BUG
	}
}

defined_text = {
	name = GetYunNewFacilitiesText
	text = {
		trigger = {
			has_completed_focus = YUN_survey_the_land
			has_completed_focus = YUN_taming_the_land
		}
		localization_key = YUN_new_facilities_mining_construction
	}
	text = {
		trigger = {
			has_completed_focus = YUN_survey_the_land
		}
		localization_key = YUN_new_facilities_mining
	}
	text = {
		trigger = {
			has_completed_focus = YUN_taming_the_land
		}
		localization_key = YUN_new_facilities_construction
	}
}

defined_text = {
	name = GetYunDevastation

	text = {
		trigger = {
			check_variable = {
				YUN_devastation < 50
			}
		}
		localization_key = YUN_Devastation_Low
	}
	text = {
		trigger = {
			check_variable = {
				YUN_devastation < 80
			}
		}
		localization_key = YUN_Devastation_Medium
	}
	text = {
		localization_key = YUN_Devastation_High
	}
}

defined_text = {
	name = GetYunDevastationGain

	text = {
		trigger = {
			check_variable = {
				YUN_devastation_gain < 2
			}
		}
		localization_key = YUN_Devastation_Gain_Low
	}
	text = {
		trigger = {
			check_variable = {
				YUN_devastation_gain < 3
			}
		}
		localization_key = YUN_Devastation_Gain_Medium
	}
	text = {
		localization_key = YUN_Devastation_Gain_High
	}
}
