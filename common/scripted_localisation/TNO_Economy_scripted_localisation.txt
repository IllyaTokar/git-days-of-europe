defined_text = {
	name = get_topbar_economy_button_tt
	text = {
		trigger = {
			is_warlord = yes
		}
		localization_key = topbar_economy_button_tt_warlord
	}
	text = {
		trigger = {

		}
		localization_key = topbar_economy_button_tt_normal
	}
}

defined_text = {
	name = Get_nuclear_weapons_cost
	text = {
		trigger = {
			has_idea = tno_nuclear_stockpile_none
		}
		localization_key = tno_nuclear_stockpile_none_econ_tt
	}
	text = {
		trigger = {
			has_idea = tno_nuclear_stockpile_one
		}
		localization_key = tno_nuclear_stockpile_one_econ_tt
	}
	text = {
		trigger = {
			has_idea = tno_nuclear_stockpile_few
		}
		localization_key = tno_nuclear_stockpile_few_econ_tt
	}
	text = {
		trigger = {
			has_idea = tno_nuclear_stockpile_many
		}
		localization_key = tno_nuclear_stockpile_many_econ_tt
	}
	text = {
		trigger = {
			has_idea = tno_nuclear_stockpile_massive
		}
		localization_key = tno_nuclear_stockpile_massive_econ_tt
	}
	text = {
		trigger = {
			has_idea = tno_nuclear_stockpile_apocalyptic
		}
		localization_key = tno_nuclear_stockpile_apocalyptic_econ_tt
	}
}


defined_text = {
	name = Get_deficit_ratio
	text = {
		trigger = {
			check_variable = { deficit_ratio < 0.4 }
		}
		localization_key = deficit_ratio_green
	}
	text = {
		trigger = {
			check_variable = { deficit_ratio < 0.7 }
		}
		localization_key = deficit_ratio_yellow
	}
	text = {
		localization_key = deficit_ratio_red
	}
}

defined_text = {
	name = Get_GDP
	text = {
		trigger = {
			check_variable = { GDP < 1 }
		}
		localization_key = GDP_thousand
	}
	text = {
		trigger = {
			check_variable = { GDP > 1000 }
		}
		localization_key = GDP_billion
	}
	text = {
		localization_key = GDP_million
	}
}

defined_text = {
	name = Get_GDP_topbar
	text = {
		trigger = {
			check_variable = { GDP < 1 }
		}
		localization_key = GDP_thousand_topbar
	}
	text = {
		trigger = {
			check_variable = { GDP > 1000 }
		}
		localization_key = GDP_billion_topbar
	}
	text = {
		localization_key = GDP_million_topbar
	}
}

defined_text = {
	name = Get_annual_debt_payments
	text = {
		trigger = {
			check_variable = { GDP < 1 }
		}
		localization_key = annual_debt_payments_thousand
	}
	text = {
		trigger = {
			check_variable = { GDP > 1000 }
		}
		localization_key = annual_debt_payments_billion
	}
	text = {
		localization_key = annual_debt_payments_million
	}
}


defined_text = {
	name = Get_total_budget
	text = {
		trigger = {
			AND = {
				check_variable = { total_budget < 1 }
				check_variable = { total_budget > -1 }
			}
		}
		localization_key = total_budget_thousand
	}
	text = {
		trigger = {
			OR = {
				check_variable = { total_budget > 1000 }
				check_variable = { total_budget < -1000 }
			}
		}
		localization_key = total_budget_billion
	}
	text = {
		localization_key = total_budget_million
	}
}

defined_text = {
	name = Get_total_expenditures
	text = {
		trigger = {
			check_variable = { total_expenditures < 1 }
		}
		localization_key = total_expenditures_thousand
	}
	text = {
		trigger = {
			check_variable = { total_expenditures > 1000 }
		}
		localization_key = total_expenditures_billion
	}
	text = {
		localization_key = total_expenditures_million
	}
}

defined_text = {
	name = Get_civilian_expenditures
	text = {
		trigger = {
			check_variable = { civilian_expenditures < 1 }
		}
		localization_key = civilian_expenditures_thousand
	}
	text = {
		trigger = {
			check_variable = { civilian_expenditures > 1000 }
		}
		localization_key = civilian_expenditures_billion
	}
	text = {
		localization_key = civilian_expenditures_million
	}
}

defined_text = {
	name = Get_civilian_expenditures_topbar
	text = {
		trigger = {
			check_variable = { civilian_expenditures < 1 }
		}
		localization_key = civilian_expenditures_thousand_topbar
	}
	text = {
		trigger = {
			check_variable = { civilian_expenditures > 1000 }
		}
		localization_key = civilian_expenditures_billion_topbar
	}
	text = {
		localization_key = civilian_expenditures_million_topbar
	}
}

defined_text = {
	name = Get_military_expenditures
	text = {
		trigger = {
			check_variable = { military_expenditures < 1 }
		}
		localization_key = military_expenditures_thousand
	}
	text = {
		trigger = {
			check_variable = { military_expenditures > 1000 }
		}
		localization_key = military_expenditures_billion
	}
	text = {
		localization_key = military_expenditures_million
	}
}

defined_text = {
	name = Get_military_expenditures_topbar
	text = {
		trigger = {
			check_variable = { military_expenditures < 1 }
		}
		localization_key = military_expenditures_thousand_topbar
	}
	text = {
		trigger = {
			check_variable = { military_expenditures > 1000 }
		}
		localization_key = military_expenditures_billion_topbar
	}
	text = {
		localization_key = military_expenditures_million_topbar
	}
}


defined_text = {
	name = Get_construction_expenditures
	text = {
		trigger = {
			check_variable = { construction_expenditures < 1 }
		}
		localization_key = construction_expenditures_thousand
	}
	text = {
		trigger = {
			check_variable = { construction_expenditures > 1000 }
		}
		localization_key = construction_expenditures_billion
	}
	text = {
		localization_key = construction_expenditures_million
	}
}

defined_text = {
	name = Get_money_reserves
	text = {
		trigger = {
			check_variable = { money_reserves < 1 }
		}
		localization_key = money_reserves_thousand
	}
	text = {
		trigger = {
			check_variable = { money_reserves > 1000 }
		}
		localization_key = money_reserves_billion
	}
	text = {
		localization_key = money_reserves_million
	}
}

defined_text = {
	name = Get_national_debt
	text = {
		trigger = {
			check_variable = { national_debt < 1 }
		}
		localization_key = national_debt_thousand
	}
	text = {
		trigger = {
			check_variable = { national_debt > 1000 }
		}
		localization_key = national_debt_billion
	}
	text = {
		localization_key = national_debt_million
	}
}

defined_text = {
	name = Get_national_debt_topbar
	text = {
		trigger = {
			check_variable = { national_debt < 1 }
		}
		localization_key = national_debt_thousand_topbar
	}
	text = {
		trigger = {
			check_variable = { national_debt > 1000 }
		}
		localization_key = national_debt_billion_topbar
	}
	text = {
		localization_key = national_debt_million_topbar
	}
}

defined_text = {
	name = Get_deficit
	text = {
		trigger = {
			AND = {
				check_variable = { deficit < 1 }
				check_variable = { deficit > 0 }
			}
		}
		localization_key = deficit_thousand
	}
	text = {
		trigger = {
			OR = {
				check_variable = { deficit > 1000 }
				check_variable = { deficit < -1000 }
			}
		}
		localization_key = deficit_billion
	}
	text = {
		localization_key = deficit_million
	}
}

defined_text = {
	name = Get_manpower_costs
	text = {
		trigger = {
			check_variable = { manpower_costs < 1 }
		}
		localization_key = manpower_costs_thousand
	}
	text = {
		trigger = {
			check_variable = { manpower_costs > 1000 }
		}
		localization_key = manpower_costs_billion
	}
	text = {
		localization_key = manpower_costs_million
	}
}

defined_text = {
	name = Get_military_factory_costs
	text = {
		trigger = {
			check_variable = { military_factory_costs < 1 }
		}
		localization_key = military_factory_costs_thousand
	}
	text = {
		trigger = {
			check_variable = { military_factory_costs > 1000 }
		}
		localization_key = military_factory_costs_billion
	}
	text = {
		localization_key = military_factory_costs_million
	}
}

defined_text = {
	name = Get_misc_costs
	text = {
		trigger = {
			check_variable = { misc_costs_display < 1 }
		}
		localization_key = misc_costs_thousand
	}
	text = {
		trigger = {
			check_variable = { misc_costs_display > 1000 }
		}
		localization_key = misc_costs_billion
	}
	text = {
		localization_key = misc_costs_million
	}
}

defined_text = {
	name = Get_misc_costs_topbar
	text = {
		trigger = {
			check_variable = { misc_costs_display < 1 }
		}
		localization_key = misc_costs_thousand_topbar
	}
	text = {
		trigger = {
			check_variable = { misc_costs_display > 1000 }
		}
		localization_key = misc_costs_billion_topbar
	}
	text = {
		localization_key = misc_costs_million_topbar
	}
}


defined_text = {
	name = Get_misc_income
	text = {
		trigger = {
			check_variable = { misc_income_display < 1 }
		}
		localization_key = misc_income_thousand
	}
	text = {
		trigger = {
			check_variable = { misc_income_display > 1000 }
		}
		localization_key = misc_income_billion
	}
	text = {
		localization_key = misc_income_million
	}
}

defined_text = {
	name = Get_misc_income_topbar
	text = {
		trigger = {
			check_variable = { misc_income_display < 1 }
		}
		localization_key = misc_income_thousand_topbar
	}
	text = {
		trigger = {
			check_variable = { misc_income_display > 1000 }
		}
		localization_key = misc_income_billion_topbar
	}
	text = {
		localization_key = misc_income_million_topbar
	}
}


#Tooltip info

defined_text = {
	name = Get_tax_rate
	text = {
		trigger = {
			has_idea = tno_tax_rate_no_taxation
		}
		localization_key = tno_tax_rate_no_taxation_gui
	}
	text = {
		trigger = {
			has_idea = tno_tax_rate_low_taxation
		}
		localization_key = tno_tax_rate_low_taxation_gui
	}
	text = {
		trigger = {
			has_idea = tno_tax_rate_medium_taxation
		}
		localization_key = tno_tax_rate_medium_taxation_gui
	}
	text = {
		trigger = {
			has_idea = tno_tax_rate_high_taxes
		}
		localization_key = tno_tax_rate_high_taxes_gui
	}
	text = {
		trigger = {
			has_idea = tno_tax_rate_exploitative_taxation
		}
		localization_key = tno_tax_rate_exploitative_taxation_gui
	}
}

defined_text = {
	name = Get_income_tax
	text = {
		trigger = {
			has_idea = tno_income_taxation_elite_tax_exemptions
		}
		localization_key = tno_income_taxation_elite_tax_exemptions_gui
	}
	text = {
		trigger = {
			has_idea = tno_income_taxation_tax_havens
		}
		localization_key = tno_income_taxation_tax_havens_gui
	}
	text = {
		trigger = {
			has_idea = tno_income_taxation_low_income_weighted
		}
		localization_key = tno_income_taxation_low_income_weighted_gui
	}
	text = {
		trigger = {
			has_idea = tno_income_taxation_flat_taxes
		}
		localization_key = tno_income_taxation_flat_taxes_gui
	}
	text = {
		trigger = {
			has_idea = tno_income_taxation_high_income_weighted
		}
		localization_key = tno_income_taxation_high_income_weighted_gui
	}
}

defined_text = {
	name = Get_income_rate_factor
	text = {
		trigger = { has_idea = tno_poverty_rate_spartan_ideal }
		localization_key = tno_poverty_rate_spartan_ideal_gui
	}
	text = {
		trigger = { has_idea = tno_poverty_rate_80 }
		localization_key = tno_poverty_rate_80_gui
	}
	text = {
		trigger = { has_idea = tno_poverty_rate_50 }
		localization_key = tno_poverty_rate_50_gui
	}
	text = {
		trigger = { has_idea = tno_poverty_rate_25 }
		localization_key = tno_poverty_rate_25_gui
	}
	text = {
		trigger = { has_idea = tno_poverty_rate_15 }
		localization_key = tno_poverty_rate_15_gui
	}
	text = {
		trigger = { has_idea = tno_poverty_rate_10 }
		localization_key = tno_poverty_rate_10_gui
	}
	text = {
		trigger = { has_idea = tno_poverty_rate_10 }
		localization_key = tno_poverty_rate_10_gui
	}
	text = {
		trigger = { has_idea = tno_poverty_rate_5 }
		localization_key = tno_poverty_rate_5_gui
	}
	text = {
		trigger = { has_idea = tno_poverty_rate_0 }
		localization_key = tno_poverty_rate_0_gui
	}
}

## Civilian

defined_text = {
	name = Get_health_care
	text = {
		trigger = {
			has_idea = tno_health_care_no_health_care
		}
		localization_key = tno_health_care_no_health_care_gui
	}
	text = {
		trigger = {
			has_idea = tno_health_care_private_health_care
		}
		localization_key = tno_health_care_private_health_care_gui
	}
	text = {
		trigger = {
			has_idea = tno_health_care_public_health_care
		}
		localization_key = tno_health_care_public_health_care_gui
	}
	text = {
		trigger = {
			has_idea = tno_health_care_universal_health_care
		}
		localization_key = tno_health_care_universal_health_care_gui
	}
	text = {
		trigger = {
			has_idea = tno_health_care_free_universal_care
		}
		localization_key = tno_health_care_free_universal_care_gui
	}
}

defined_text = {
	name = Get_education
	text = {
		trigger = {
			has_idea = tno_education_elite_only
		}
		localization_key = tno_education_elite_only_gui
	}
	text = {
		trigger = {
			has_idea = tno_education_public_education
		}
		localization_key = tno_education_public_education_gui
	}
	text = {
		trigger = {
			has_idea = tno_education_subsidised_higher_education
		}
		localization_key = tno_education_subsidised_higher_education_gui
	}
	text = {
		trigger = {
			has_idea = tno_education_public_higher_education
		}
		localization_key = tno_education_public_higher_education_gui
	}
	text = {
		trigger = {
			has_idea = tno_education_free_education
		}
		localization_key = tno_education_free_education_gui
	}
}

defined_text = {
	name = Get_security_service
	text = {
		trigger = {
			has_idea = tno_security_anarchic
		}
		localization_key = tno_security_anarchic_gui
	}
	text = {
		trigger = {
			has_idea = tno_security_police
		}
		localization_key = tno_security_police_gui
	}
	text = {
		trigger = {
			has_idea = tno_security_security_service
		}
		localization_key = tno_security_security_service_gui
	}
	text = {
		trigger = {
			has_idea = tno_security_data_cohesion
		}
		localization_key = tno_security_data_cohesion_gui
	}
	text = {
		trigger = {
			has_idea = tno_security_wire_tapping
		}
		localization_key = tno_security_wire_tapping_gui
	}
	text = {
		trigger = {
			has_idea = tno_security_preemptive_security
		}
		localization_key = tno_security_preemptive_security_gui
	}
}

defined_text = {
	name = Get_pensions
	text = {
		trigger = {
			has_idea = tno_pensions_no_pensions
		}
		localization_key = tno_pensions_no_pensions_gui
	}
	text = {
		trigger = {
			has_idea = tno_pensions_trinket_pensions
		}
		localization_key = tno_pensions_trinket_pensions_gui
	}
	text = {
		trigger = {
			has_idea = tno_pensions_low_pensions
		}
		localization_key = tno_pensions_low_pensions_gui
	}
	text = {
		trigger = {
			has_idea = tno_pensions_acceptable_pensions
		}
		localization_key = tno_pensions_acceptable_pensions_gui
	}
	text = {
		trigger = {
			has_idea = tno_pensions_good_pensions
		}
		localization_key = tno_pensions_good_pensions_gui
	}
}

defined_text = {
	name = Get_unemployment
	text = {
		trigger = {
			has_idea = tno_unemployment_no_subsidies
		}
		localization_key = tno_unemployment_no_subsidies_gui
	}
	text = {
		trigger = {
			has_idea = tno_unemployment_trinket_subsidies
		}
		localization_key = tno_unemployment_trinket_subsidies_gui
	}
	text = {
		trigger = {
			has_idea = tno_unemployment_low_subsidies
		}
		localization_key = tno_unemployment_low_subsidies_gui
	}
	text = {
		trigger = {
			has_idea = tno_unemployment_generous_subsidies
		}
		localization_key = tno_unemployment_generous_subsidies_gui
	}
	text = {
		trigger = {
			has_idea = tno_unemployment_basic_income
		}
		localization_key = tno_unemployment_basic_income_gui
	}
}

#Oil Crisis
defined_text = {
	name = Get_oilcrisis_civilian
	text = {
		trigger = { has_idea = TNO_Oil_Crisis }
		localization_key = tno_oil_crisis_civilian_expenditure
	}
}

#Ausgrenzung
defined_text = {
	name = Get_ausgrenzung_civilian
	text = {
		trigger = { has_country_flag = BOR_EconomyMechanic_Active }
		localization_key = tno_ausgrenzung_civilian_expenditure
	}
}

## Military

defined_text = {
	name = Get_training
	text = {
		trigger = {
			has_idea = tno_training_none
		}
		localization_key = tno_training_none_gui
	}
	text = {
		trigger = {
			has_idea = tno_training_minimal_training
		}
		localization_key = tno_training_minimal_training_gui
	}
	text = {
		trigger = {
			has_idea = tno_training_basic_training
		}
		localization_key = tno_training_basic_training_gui
	}
	text = {
		trigger = {
			has_idea = tno_training_combat_schooling
		}
		localization_key = tno_training_combat_schooling_gui
	}
	text = {
		trigger = {
			has_idea = tno_training_advanced_training_methods
		}
		localization_key = tno_training_advanced_training_methods_gui
	}
}

defined_text = {
	name = Get_military_supervision
	text = {
		trigger = {
			has_idea = tno_military_supervision_kill_em_all
		}
		localization_key = tno_military_supervision_kill_em_all_gui
	}
	text = {
		trigger = {
			has_idea = tno_military_supervision_no_supervision
		}
		localization_key = tno_military_supervision_no_supervision_gui
	}
	text = {
		trigger = {
			has_idea = tno_military_supervision_military_policing
		}
		localization_key = tno_military_supervision_military_policing_gui
	}
	text = {
		trigger = {
			has_idea = tno_military_supervision_rules_of_engagement
		}
		localization_key = tno_military_supervision_rules_of_engagement_gui
	}
	text = {
		trigger = {
			has_idea = tno_military_supervision_watchdog_groups
		}
		localization_key = tno_military_supervision_watchdog_groups_gui
	}
	text = {
		trigger = {
			has_idea = tno_military_supervision_total_supervision
		}
		localization_key = tno_military_supervision_total_supervision_gui
	}
}

#Oil Crisis
defined_text = {
	name = Get_oilcrisis_military
	text = {
		trigger = { has_idea = TNO_Oil_Crisis }
		localization_key = tno_oil_crisis_military_expenditure
	}
}

defined_text = {
	name = Get_industrial_expertise
	text = {
		trigger = {
			has_idea = tno_industrial_expertise_pre_industrial
		}
		localization_key = tno_industrial_expertise_pre_industrial_gui
	}
	text = {
		trigger = {
			has_idea = tno_industrial_expertise_incompetent
		}
		localization_key = tno_industrial_expertise_incompetent_gui
	}
	text = {
		trigger = {
			has_idea = tno_industrial_expertise_nascent
		}
		localization_key = tno_industrial_expertise_nascent_gui
	}
	text = {
		trigger = {
			has_idea = tno_industrial_expertise_experienced
		}
		localization_key = tno_industrial_expertise_experienced_gui
	}
	text = {
		trigger = {
			has_idea = tno_industrial_expertise_innovative
		}
		localization_key = tno_industrial_expertise_innovative_gui
	}
}

defined_text = {
	name = Get_industrial_equipment
	text = {
		trigger = {
			has_idea = tno_industrial_equipment_hand_tools
		}
		localization_key = tno_industrial_equipment_hand_tools_gui
	}
	text = {
		trigger = {
			has_idea = tno_industrial_equipment_power_tools
		}
		localization_key = tno_industrial_equipment_power_tools_gui
	}
	text = {
		trigger = {
			has_idea = tno_industrial_equipment_manufacturing_lines
		}
		localization_key = tno_industrial_equipment_manufacturing_lines_gui
	}
	text = {
		trigger = {
			has_idea = tno_industrial_equipment_factory_complexes
		}
		localization_key = tno_industrial_equipment_factory_complexes_gui
	}
	text = {
		trigger = {
			has_idea = tno_industrial_equipment_modern
		}
		localization_key = tno_industrial_equipment_modern_gui
	}
	text = {
		trigger = {
			has_idea = tno_industrial_equipment_cutting_edge
		}
		localization_key = tno_industrial_equipment_cutting_edge_gui
	}
}

defined_text = {
	name = Get_construction_level
	text = {
		trigger = {
			has_idea = construction_level_0
		}
		localization_key = construction_level_0_gui
	}
	text = {
		trigger = {
			has_idea = construction_level_1
		}
		localization_key = construction_level_1_gui
	}
	text = {
		trigger = {
			has_idea = construction_level_2
		}
		localization_key = construction_level_2_gui
	}
	text = {
		trigger = {
			has_idea = construction_level_3
		}
		localization_key = construction_level_3_gui
	}
	text = {
		trigger = {
			has_idea = construction_level_4
		}
		localization_key = construction_level_4_gui
	}
	text = {
		trigger = {
			has_idea = construction_level_5
		}
		localization_key = construction_level_5_gui
	}
	text = {
		trigger = {
			has_idea = construction_level_6
		}
		localization_key = construction_level_6_gui
	}
	text = {
		trigger = {
			has_idea = construction_level_7
		}
		localization_key = construction_level_7_gui
	}
	text = {
		trigger = {
			has_idea = construction_level_8
		}
		localization_key = construction_level_8_gui
	}
	text = {
		trigger = {
			has_idea = construction_level_9
		}
		localization_key = construction_level_9_gui
	}
}

defined_text = {
	name = Get_safety
	text = {
		trigger = {
			has_idea = tno_safety_no_regulations
		}
		localization_key = tno_safety_no_regulations_gui
	}
	text = {
		trigger = {
			has_idea = tno_safety_minimal_regulations
		}
		localization_key = tno_safety_minimal_regulations_gui
	}
	text = {
		trigger = {
			has_idea = tno_safety_limited_regulations
		}
		localization_key = tno_safety_limited_regulations_gui
	}
	text = {
		trigger = {
			has_idea = tno_safety_acceptable_regulations
		}
		localization_key = tno_safety_acceptable_regulations_gui
	}
	text = {
		trigger = {
			has_idea = tno_safety_excellent_regulations
		}
		localization_key = tno_safety_excellent_regulations_gui
	}
}

defined_text = {
	name = Get_pollution
	text = {
		trigger = {
			has_idea = tno_pollution_no_controls
		}
		localization_key = tno_pollution_no_controls_gui
	}
	text = {
		trigger = {
			has_idea = tno_pollution_few_regulations
		}
		localization_key = tno_pollution_few_regulations_gui
	}
	text = {
		trigger = {
			has_idea = tno_pollution_some_regulations
		}
		localization_key = tno_pollution_some_regulations_gui
	}
	text = {
		trigger = {
			has_idea = tno_pollution_good_regulations
		}
		localization_key = tno_pollution_good_regulations_gui
	}
	text = {
		trigger = {
			has_idea = tno_pollution_strict_regulations
		}
		localization_key = tno_pollution_strict_regulations_gui
	}
}

defined_text = {
	name = Get_penal_system
	text = {
		trigger = {
			has_idea = tno_penal_system_penal_slavery
		}
		localization_key = tno_penal_system_penal_slavery_gui
	}
	text = {
		trigger = {
			has_idea = tno_penal_system_capital_punishment
		}
		localization_key = tno_penal_system_capital_punishment_gui
	}
	text = {
		trigger = {
			has_idea = tno_penal_system_penal_labor
		}
		localization_key = tno_penal_system_penal_labor_gui
	}
	text = {
		trigger = {
			has_idea = tno_penal_system_incarceration
		}
		localization_key = tno_penal_system_incarceration_gui
	}
	text = {
		trigger = {
			has_idea = tno_penal_system_rehabilitation
		}
		localization_key = tno_penal_system_rehabilitation_gui
	}
}

defined_text = {
	name = Get_minimum_wage
	text = {
		trigger = {
			has_idea = tno_minimum_wage_no_minimum_wage
		}
		localization_key = tno_minimum_wage_no_minimum_wage_gui
	}
	text = {
		trigger = {
			has_idea = tno_minimum_wage_trinket_minimum_wage
		}
		localization_key = tno_minimum_wage_trinket_minimum_wage_gui
	}
	text = {
		trigger = {
			has_idea = tno_minimum_wage_low_minimum_wage
		}
		localization_key = tno_minimum_wage_low_minimum_wage_gui
	}
	text = {
		trigger = {
			has_idea = tno_minimum_wage_acceptable_minimum
		}
		localization_key = tno_minimum_wage_acceptable_minimum_gui
	}
	text = {
		trigger = {
			has_idea = tno_minimum_wage_good_minimum_wage
		}
		localization_key = tno_minimum_wage_good_minimum_wage_gui
	}
}

defined_text = {
	name = Get_max_workhours
	text = {
		trigger = {
			has_idea = tno_max_workhours_unlimited_work_day
		}
		localization_key = tno_max_workhours_unlimited_work_day_gui
	}
	text = {
		trigger = {
			has_idea = tno_max_workhours_14_hour_work_day
		}
		localization_key = tno_max_workhours_14_hour_work_day_gui
	}
	text = {
		trigger = {
			has_idea = tno_max_workhours_12_hour_work_day
		}
		localization_key = tno_max_workhours_12_hour_work_day_gui
	}
	text = {
		trigger = {
			has_idea = tno_max_workhours_8_hour_work_day
		}
		localization_key = tno_max_workhours_8_hour_work_day_gui
	}
	text = {
		trigger = {
			has_idea = tno_max_workhours_6_hour_work_day
		}
		localization_key = tno_max_workhours_6_hour_work_day_gui
	}
}

defined_text = {
	name = Get_child_labor
	text = {
		trigger = {
			has_idea = tno_child_labor_legal
		}
		localization_key = tno_child_labor_legal_gui
	}
	text = {
		trigger = {
			has_idea = tno_child_labor_restricted
		}
		localization_key = tno_child_labor_restricted_gui
	}
	text = {
		trigger = {
			has_idea = tno_child_labor_illegal
		}
		localization_key = tno_child_labor_illegal_gui
	}
}


#tempoarary modifiers#
defined_text = {
	name = Get_military_budget_modifier
	text = {
		trigger = {
			has_idea = military_budget_boost
		}
		localization_key = military_budget_boost_gui
	}
	text = {
		trigger = {
			has_idea = military_budget_cuts
		}
		localization_key = military_budget_cuts_gui
	}
	text = {
		trigger = {
			has_idea = military_budget_cuts_heavy
		}
		localization_key = military_budget_cuts_heavy_gui
	}
	text = {
		trigger = {
			has_idea = bankrupt_military
		}
		localization_key = bankrupt_military_gui
	}
}

defined_text = {
	name = Get_civilian_budget_modifier
	text = {
		trigger = {
			has_idea = civilian_budget_boost
		}
		localization_key = civilian_budget_boost_gui
	}
	text = {
		trigger = {
			has_idea = civilian_budget_cuts
		}
		localization_key = civilian_budget_cuts_gui
	}
	text = {
		trigger = {
			has_idea = civilian_budget_cuts_heavy
		}
		localization_key = civilian_budget_cuts_heavy_gui
	}
	text = {
		trigger = {
			has_idea = bankrupt_civilian
		}
		localization_key = bankrupt_civilian_gui
	}
}

defined_text = {
	name = Get_1_Loot_cost_tooltip
	text = {
		trigger = {
			check_variable = { loot_amount > 1 }
		}
		localization_key = decision_cost_loot_1
	}
	text = {
		localization_key = decision_cost_loot_1_blocked
	}
}

defined_text = {
	name = Get_2_Loot_cost_tooltip
	text = {
		trigger = {
			check_variable = { loot_amount > 2 }
		}
		localization_key = decision_cost_loot_2
	}
	text = {
		localization_key = decision_cost_loot_2_blocked
	}
}

defined_text = {
	name = Get_3_Loot_cost_tooltip
	text = {
		trigger = {
			check_variable = { loot_amount > 3 }
		}
		localization_key = decision_cost_loot_3
	}
	text = {
		localization_key = decision_cost_loot_3_blocked
	}
}

defined_text = {
	name = Get_4_Loot_cost_tooltip
	text = {
		trigger = {
			check_variable = { loot_amount > 4 }
		}
		localization_key = decision_cost_loot_4
	}
	text = {
		localization_key = decision_cost_loot_4_blocked
	}
}

defined_text = {
	name = Get_5_Loot_cost_tooltip
	text = {
		trigger = {
			check_variable = { loot_amount > 5 }
		}
		localization_key = decision_cost_loot_5
	}
	text = {
		localization_key = decision_cost_loot_5_blocked
	}
}

defined_text = {
	name = GetCominformMonetaryValue
	text = {
		trigger = {
			check_variable = { global.COMECON_monetary_fund_value < 1 }
		}
		localization_key = COMECON_monetary_fund_thousand
	}
	text = {
		trigger = {
			check_variable = { global.COMECON_monetary_fund_value > 1000 }
		}
		localization_key = COMECON_monetary_fund_billion
	}
	text = {
		localization_key = COMECON_monetary_fund_million
	}
}

defined_text = {
	name = GetCominformDefenseValue
	text = {
		trigger = {
			check_variable = { global.COMECON_collective_defense_value < 1 }
		}
		localization_key = COMECON_defense_fund_thousand
	}
	text = {
		trigger = {
			check_variable = { global.COMECON_collective_defense_value > 1000 }
		}
		localization_key = COMECON_defense_fund_billion
	}
	text = {
		localization_key = COMECON_defense_fund_million
	}
}
