defined_text = {
	name = get_world_tension_defcon
	text = {
		trigger = {
			check_variable = { global.TNO_World_Tension > 80 }
		}
		localization_key = "GFX_world_tension_defcon_1"
	}
	text = {
		trigger = {
			check_variable = { global.TNO_World_Tension > 60 }
		}
		localization_key = "GFX_world_tension_defcon_2"
	}
	text = {
		trigger = {
			check_variable = { global.TNO_World_Tension > 40 }
		}
		localization_key = "GFX_world_tension_defcon_3"
	}
	text = {
		trigger = {
			check_variable = { global.TNO_World_Tension > 20 }
		}
		localization_key = "GFX_world_tension_defcon_4"
	}
	text = {
		trigger = {
			OR = {
				check_variable = { global.TNO_World_Tension > 0 }
				check_variable = { global.TNO_World_Tension = 0 }
			}
		}
		localization_key = "GFX_world_tension_defcon_5"
	}
}

defined_text = {
	name = get_world_tension_topbar_defcon
	text = {
		trigger = {
			check_variable = { global.TNO_World_Tension > 80 }
		}
		localization_key = "GFX_world_tension_topbar_defcon_1"
	}
	text = {
		trigger = {
			check_variable = { global.TNO_World_Tension > 60 }
		}
		localization_key = "GFX_world_tension_topbar_defcon_2"
	}
	text = {
		trigger = {
			check_variable = { global.TNO_World_Tension > 40 }
		}
		localization_key = "GFX_world_tension_topbar_defcon_3"
	}
	text = {
		trigger = {
			check_variable = { global.TNO_World_Tension > 20 }
		}
		localization_key = "GFX_world_tension_topbar_defcon_4"
	}
	text = {
		trigger = {
			OR = {
				check_variable = { global.TNO_World_Tension > 0 }
				check_variable = { global.TNO_World_Tension = 0 }
			}
		}
		localization_key = "GFX_world_tension_topbar_defcon_5"
	}
}