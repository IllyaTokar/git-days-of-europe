music_station = "axis"

music = {
	song = "axis_enemyshores"

}

music = {
	song = "axis_forthereich"
	
}

music = {
	song = "axis_meetthegeneral"
	
}

music = {
	song = "axis_meninline"
	
}

music = {
	song = "axis_preparethesubs"
	
}

music = {
	song = "axis_raisetheflag"
	
}

music = {
	song = "axis_returninghome"
	
}

music = {
	song = "axis_shipsatsea"
	
}

music = {
	song = "axis_sightoftheenemy"
	
}

music = {
	song = "axis_whenwecomehomeagain"
}
music = {
	song = "der_machtigste_konig_im_luftrevier"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = GER
					tag = AUS
				}
			}
		}
		modifier = {
			factor = 2
			has_war = no
		}
		modifier = {
			factor = 1.5
			any_country = {
				has_war_with = ROOT
				surrender_progress > 0.20
			}
		}
		modifier = {
			factor = 2
			any_country = {
				has_war_with = ROOT
				surrender_progress > 0.50
			}
		}
	}
}

music = {
	song = "panzerlied"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = GER
					tag = AUS
				}
			}
		}
		modifier = {
			factor = 2
			has_war = no
		}
		modifier = {
			factor = 1.5
			any_country = {
				has_war_with = ROOT
				surrender_progress > 0.10
			}
		}
		modifier = {
			factor = 2
			any_country = {
				has_war_with = ROOT
				surrender_progress > 0.50
			}
		}
	}
}

music = {
	song = "schwarzbraun_ist_die_haselnuss"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = {
				OR = {
					tag = GER
					tag = AUS
				}
			}
		}
		modifier = {
			factor = 2
			has_war = no
		}
		modifier = {
			factor = 1.5
			any_country = {
				has_war_with = ROOT
				surrender_progress > 0.10
			}
		}
		modifier = {
			factor = 2
			any_country = {
				has_war_with = ROOT
				surrender_progress > 0.50
			}
		}
	}
}